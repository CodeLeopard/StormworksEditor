# Run the restore operation in a headless/commandline-only Unity.


$ErrorActionPreference = "Stop"

# This needs to be a git ignored file with 'return "Path/To/Unity.exe"'
$Unity = & "./UnityInstallPath.ps1"

$unityLogFile = "$PSScriptRoot/NugetRestore.log"
$projectPath = "$PSScriptRoot/Editor"
$unityArguments = "-BatchMode", "-Quit", "-NoGraphics", "-LogFile $unityLogFile", "-IgnoreCompilerErrors", "-Projectpath $projectPath", "-ExecuteMethod NugetForUnity.NugetHelper.Restore"

Write-Host -ForegroundColor DarkGray "Starting Unity at '$Unity'"
Write-Host -ForegroundColor DarkGray "With ArgumentList '$unityArguments'"
Start-Process -Wait -FilePath $Unity -ArgumentList $unityArguments
