# Copyright 2022 CodeLeopard
# License: LGPL-3.0-or-later

<# This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
#>





param(
	[Switch]$Force,   # Ignore pending git changes
	[Switch]$NoBuild, # Skip build: fetch whatever is currently there.
	[Switch]$Deploy   # Use Deploy configuration instead of Debug
)

$ErrorActionPreference = "Stop" # We have to tell powershell to behave normally.

# Default configuration
$configuration = "Debug"
if ($Deploy)
{
	$configuration = "Deploy"
}

# Root for DataModel project.
$root = "$PSScriptRoot/Data"
# Path relative to project folder where binaries will be.
$bin_folder = "bin/$configuration/Netstandard2.1"
# Folder where binaries will be copied to.
$destination = "$PSScriptRoot/Editor/Assets/Libs/StormworksData/"

# Names of the projects to copy binaries for.
$names = "BinaryDataModel", "CurveGraph", "DataModel", "Extrusion", "Shared", "CustomSerialization", "LuaIntegration"



Write-Host "Configuration: $configuration"
Set-Location $root

# Try-Finally will prevent leaving the command prompt with unexpected working directory.
try
{
# Get git status in machine readable format.
$gitOut = & git status --porcelain

# If there is any output there are changes.
$gitDirty = -not [string]::IsNullOrWhiteSpace($gitOut)
$gitMessage = "Git reports changes for StormworksData repo!`nWill not fetch .dll's while there are unstaged changes.`n$gitOut"
$gitMessageForced = "Git reports changes for StormworksData repo!`n$gitOut"

if($gitDirty)
{
	if(-not $Force)
	{
		Write-Host -ForegroundColor Red $gitMessage
		exit 1
	}
	Write-Host -ForegroundColor Red $gitMessageForced = "Git reports changes for StormworksData repo!`n$gitOut"
}


if( -not $NoBuild)
{
	# Build the DataModel solution.
	& "$root/CI/Build.ps1" $configuration -Unity_Relevant_Subset
}


# Copy the files
foreach ($name in $names)
{
	$source = "$root/$name/$bin_folder"

	# Put the command for verification of parameters.
	Write-Host -ForegroundColor DarkGray `
	"& Robocopy.exe /S /J /PURGE /R:10 /TBD `"$source/`" `"$destination/$name/`" `"$name.dll`" `"$name.pdb`" `"$name.deps.json`""
	# /S copy subdirectories, except empty ones.
	# /J Copy using unbuffered IO (faster at scale).
	# /PURGE Remove files at destination that don't exist in source.
	# /R:10 retry 10 times.
	# /TBD Wait for network shares etc. to come online.
	# "..." Source folder.
	# "..." Destination folder.
	# <remainder> fileNames to copy.
	& Robocopy.exe /S /J /PURGE /R:10 /TBD "$source/" "$destination/$name/" "$name.dll" "$name.pdb" "$name.deps.json"

	# Empty line between RoboCopy invocations.
	Write-Host
}




if($gitDirty)
{
	# Write the message again at the end because there is a lot of output in between.
	Write-Host -ForegroundColor Red $gitMessageForced
}
}
finally
{
	# Prevent leaving the commandline in a unexpected location.
	Set-Location $PSScriptRoot
}

Write-Host "Configuration: $configuration"