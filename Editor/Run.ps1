# Run already built Player.


$latest = Resolve-Path "$PSScriptRoot/Build/*/StormworksEditor/StormworksEditor.exe" | `
Sort-Object LastWriteTime -Descending | Select-Object -First 1

Write-Host -ForegroundColor Gray $latest
& $latest