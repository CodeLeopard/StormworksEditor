See also controls.txt for information.




License:
	See the file LICENSE


	The Editor is built upon the Unity game engine, see it's terms of service here: https://unity3d.com/legal/terms-of-service

Privacy:
	The Editor does not send any information to me (CodeLeopard) automatically, 
	However if you send me things such as log files, or related xml files,
	 those may contain sensitive information including but not limited to:
		- Your user name, according to the Operating System
		- Your Steam identity
		- The location of the Editor on your computer
		- The location of Stormworks on your computer
	In the act of sending that information you may also provide your email address, or other messaging system identity.

	I have neither the expertise, time or money to implement proper data protection practices 
	so it's your responsibility to ensure that you don't send me confidential information.

	Keeping in mind the above, I will not share personally identifying information with anyone, except where required by Law.


	The Editor is built upon the Unity game engine, see it's privacy policy here: https://unity3d.com/legal/privacy-policy


