# Changelog

## [0.22.0] - Upcoming

### Added
- Add TrackSpeed, SuperElevation, TrackName and CatenaryGroup to curves. These are arbitrary properties that will be attached to the exported track xml. They has no effect in the game but can be used with other tools to extract the information again for various purposes.

### Reworked
- G menu Mesh&Phys creation: Added file picker dialog.

### Fixed
- Incorrect keybind info in controls.txt

### Known issues
- Split node keybind does not work.

## [0.21.11] - 2024-08-22

### Added
- Setting to toggle between placing new objects at the cursor (default) or at the orbit camera focus marker. #96
- Additive loading for staticObjects and Curves. This can be used to handle merge conflicts in git (manual file interaction needed though), note: it won't help in all cases.

### Changed
- Update dependency DataModel
- Improve handling of cancellation during tile loading (not all cases handled yet).

### Fixed
- Inconsistent zoom limit on ortho camera zoom between numpad and scroll controls.
- Playlist could end up generating components with id 0. #92
- Disable error about ScriptableObjectDB existing already because it is harmless.
- C keybind was not mentioned in all the relevant places in controls.txt
- Tile saving could fail if xl track lines ware not loaded due to NullReference.

## [0.21.10] - 2024-07-29

### Added
- Keybind to lock orbit camera rotation [shift + f]
- Mousewheel binding to ortho camera size [Shift + MouseWheel]

### Fixed
- Camera light would toggle when typing L into text field.

## [0.21.9] - 2024-05-31

### Improved
- Better handling of errors during loading of StaticObjects. An error now does not abort loading but tries to load all valid objects. Note though that auto-saving will be DISABLED if this happens.
- Big text box for Playlist Component Tags

### Fixed
- Missing tags on some Playlist Components
- Unable to add vehicles to playlist due to NullReferenceException.

## [0.21.8] - 2024-03-30

### Fixed
- Focussing a text input field would cause the camera to be disabled, which in turn would cause many things to be wonky, some are noted below but there is likely more.
- File picker would forget the last used folder.
- Gizmo arrows could disappear.

## [0.21.7] - 2024-03-25

### Added
- Editpanel support for UInt32

### Fixed
- EditPanel for Curve/Connection had error causing input field for curveSpec to not appear.

## [0.21.6] - 2023-12-16

### Added
- Playlists with zones where vehicles will be spanwed can refer to other playlists to fetch those vehicles for the vehicle-in-zone preview.

### Changed
- Reworked the code behind the playlist's vehicle-in-zone preview.

### Fixed
- Trying to remove an installation from the menu did not actually do anything.
- failure to initialize main menu when Steam is not installed or not found.
- some (not all) harmless startup errors.

## [0.21.5] - 2023-12-08

### Added
- Spotlight now has fov in both radians and degrees for convenience.
- MacOS build

### Fixed
- View manager options EditArea and Interactable did not hide the objects after the tile LoD level changed.

## [0.21.4] - 2023-11-19

### Added
- Per-Instance settings file. Currently only controls what tile groups (islands) to load.

### Fixed
- TrainTrack S bends being extruded as straight segment, see DataModel changelog for details.

## [0.21.3] - 2023-11-18

### Fixed
- Displayed size of tube light was small by factor of 2 in Editor.
- Fixed export failed when moon present due to incorrect rounding.

## [0.21.2] - 2023-11-08

### Added
- Support for moon tiles.
- Give the camera a light so it can see in caverns. Press L to toggle (off by default)

### Changed
- Improved TrainTrack export. Generated meshes should now not get phantom changes anymore.
- Increased shadow distance from 500m to 1000m

### Fixed
- Exporter status text numbers would go over maximum.
- Visibility manager counts items processed per tick correctly now, as a result it is way faster.

## [0.21.1] - 2023-11-05

### Added
- Menu screen with selector for Stormworks installation.
- Button to export all curves as a single .ply mesh for special use-cases. Reducing the level of detail of the CurveSpec is recommended.
- Logic to split a curve by inserting a new node somewhere along it, preserving its shape.
- Temporary (wonky) UI to split a curve. Press H with a curve selected to start the flow.
- Partial support for game version 1.6.2 (Industrial frontier).
	- Arid island is loaded as an island instead of individual tiles.
	- Handling for now-optional property 'category' in Definitions.
- LueCurveSpec MeshInfo now has a full Transform.
- Add support for DataModel `964ba3347f4c06b1dbaf52a9eda160749034952b`: CurveSpec / Extrusion added ability to split the curve into multiple segments each generating mesh and phys for the result. This will prevent a single mesh from being too big on very large or detailed curves.
- IsTerrain is now visible to users and saved to XML.
- ParentTileCorrection is now saved to XML.
- Exposed more settings for TileExporter:
  - ConnectedNodeDetectionRadius: Radius to search for nodes that should be connected (between the ends of curves).
  - EnableSmartjunctionhandling
  - SmartJunctionhandlingRadius: Smartly handles the two diverging tracks of a junction so that the game does not mess it up (create junction in unwanted spot) while preserving the smoothness of the curve.
  - EnableUnintendedJunctionPrevention
  - UnintendedJunctionPreventionRadius: Removes nodes that are too close together and could make an unexpected junction in-game
  - EnableTrackTileBorderMitigation: Mitigate problems for train track nodes that cross tile borders (Note: there are still a few edge cases).
- setting to load Rotation variants of tiles.
- option in View menu to toggle sea.
- keybind `h` to add playlist objects.
- setting for sea floor height
- setting for sea raycast, when enabled the sea will block raycast, so the camera focus will sit on the sea rather than the floor below, and raycast drag gizmo will also hit the sea.
- Menu bar -> World -> Flatten Terrain: This flattens all mesh and phys objects marked as Terrain, allowing you to work in 2D-ish. You probably want to hide trees when you enable this. It will automatically be disabled when saving/exporting.
- Updated DataModel adding xml compression for tiles *when saving* (it was already supported when loading).
- Added Exit button in menu (yes, that was missing for months).
- Added loading vehicles into mission zones based on a heuristic that looks at the tags. Can be disabled in the settings.
	Looks for a vehicle component with `spawn_category=variant` and zones can then load that vehicle using `category=variant` the first match is used. `category` and `variant` are variables.
- Playlist components can be toggled between environment and non-environment (mission) location with the checkbox (it is no longer read-only).
- Vehicles can be added to a playlist, from either debris folder or user's saved vehicles. See known issues.
- Support for lights from Stormworks: OmniLights, SpotLights and TubeLights. Note: Unity does not support TubeLights, so the light emitted from the TubeLight is just an OmniLight.
- Setting to control how many lights are rendered, since they can affect performance significantly.
- CurveSpecs update live using a FileWatcher. Note that any change will reload all of them, because untangling the dependency situation is non-trivial. I'll look at improving it *if* it becomes a problem.

### Changed
- Removed obsolete id based ignore matching of TileItems.
- Replace Text with TextMeshPro
- Moved Donkk and Volcanic tile groups around so there is more separation between groups.
- Updated to Unity 2022.3.10f1
- Curves and Objects will now be placed on the nearest tile (up to 1.5km from the center) instead of harshly cutting off at the boundary.
- General improvements to TileExporter maintainability by splitting out train track related logic to separate class that is in theory also testable.
- Changed position of tile groups to accommodate the additional tiles that would appear when the 'Load rotation variants' setting is enabled.
- Size of new playlist zones is now radius 1.
- Improved lighting so shadows are not pure black: you can now see detail on the shadow side.

### Fixed
- Playlists getting messed up when saved.
- Playlist vehicles LoD broken after moving to another tile.
- Orbit camera zoomed while mouse scrolled UI element.
- CurveSpecs with no Physics at all would cause Exceptions.
- About/VersionInfo BuildDate was shown in UTC instead of local.
- EditArea could appear with scale in-game.
- Camera focus marker text wouldn't be accurate sometimes.
- Attempting to close the program while tile loading was still running could take a long time to actually close or freeze completely.
- Camera would not respond to commands until the window was unfocussed and refocussed.
- TileExporter's Prevent unintended junction step was slow, and would run multiple times.
- - TileExporter's Prevent unintended junction step missing end in progress indication.
- Playlist zone could not be scaled.
- Playlist delete did remove the object from editor, but not from the playlist data, so would not really be removed.
- VehicleLoader Component did not unload vehicle when path was set to null.
- VehicleLoader Component did not load a vehicle when its path was set to null and then changed again to a vehicle that had been loaded earlier, because it thought there ware no changes.
- Playlist zone spawn position was floating above terrain (because size changed) now automatically determined based on zone size.
- Emissive shader didn't work properly (no color).
- Playlist vehicle missing tags input field.

### Known Issues
- Mission may fail to load due to changes to XML compression.
- Vehicles added to a playlist have their `vehicle_123.xml` file created immediately and that file will stick around even if the playlist is not saved.
    Note this does work properly the other way around: files for removed vehicles are only deleted when the playlist is actually saved. This is due to technical complexities and will be fixed 'somewhen in the future'™.
- Camera movement stutters sometimes.


## [0.19.10] - 2022-08-14

### Added
- Better Exception info when Export fails to save a `mesh` or `phys`.
- File paths are now enforced to use forward slash as directory separator when interacted with.

### Changed
- Trees can now be scaled beyond all reason.

### Fixed
- Reloading CurveSpecs wile files are missing would save them as null.
- Outline did not appear for subMeshes besides the first subMesh.
- Trees did not have correct scale in Editor.

### Known issues
- Slash direction correction causes janky interaction in inspector when triggered.

---
## [0.19.9] - 2022-08-11

### Added
- ConnectionBehaviour has Unique ID for logging and debugging purposes.
- More status updates from TileExporter.
- Saving Failed dialog also triggered by save (without exit) dialog.
- About/Version Info that shows Version, Git and Build information.
- Saving is now possible when some Tiles have failed to load. Use with caution though. #13

### Changed
- CurveGraph: Junctions now Absorb the held handle into the junction instead of the other way around.

### Fixed
- Typing numbers into Transform gizmo is Janky #21
- EditArea grid_size dropdown did not work. #22
- CurveNode inspector transform values didn't update.

---
## [0.19.8] - 2022-08-09

### Fixed
- TileExporter did not remove all `_CT_LoadIgnore` items.

---
## [0.19.7] - 2022-08-08

### Fixed
- Tile Xml Train Track Junctions missing nodes on the fork causing harsh physics jolts for trains.
- Tile Xml Train Track not connected over tile borders causing derailments.

---
## [0.19.6] - 2022-08-05
Updated submodule DataModel with some fixes.

---
## [0.19.5] - 2022-07-12

### Added
- GUI for Transform Gizmo Mode
- GUI for Transform Gizmo Space
- Errors in CurveSpec now appear in console.

### Fixed
- Transparency on Vehicles didn't work.
- Removed redundant button.
- CurveSpecLua did not work for non-default Stormworks Install (Selection in settings was not used)

---
## [0.19.4] - 2022-07-03
### Fixed
- Tile loading failure due to initialization order problem.

---
## [0.19.3] - 2022-07-03
### Added
- Visibility menu now affects Curves.
- Curves now generate physics for inspection in the Editor.
- Orbit Camera improved mouse interaction.
	Now moves the focus to exactly where you clicked and only moves it continuously after the mouse is *kept* depressed.

### Changed
- Change company name from DefaultCompany to CodeLeopard. This will change the location of EditorSettings and log files.
- Visibility was refactored out of TileLoader (Separation of concerns) into separate class ViewManager.

### Fixed
- CurveGraph was not saved correctly, causing failure to load the next time.
- Lighting was wrong on CI builds.

---
## [0.19.2] - 2022-06-15
### Added
- CurveSpecLua: Transform LocalQuaternion.
- CurveSpecLua: Reverse triangles option for LoopSet.
- CurveSpecLua: Color255 to lua curveSpec for integer color specification.
- Support for transparent meshes (note: visual appearance is not quite right yet).
- Support for lava flow shader (note: appears as gold for now).
- Context menu for adding TileItems, press G to activate.
- Separate visual indications for CurveSpec not found/invalid (rainbow) and error while extruding (red-yellow).

### Changed
- Transform LocalRotation is replaced by:
	- LocalRotationRadians
	- LocalRotationDegrees

### Fixed
- Broken curveSpec lua would not render anything.
- CurveSpec error could be misleading in some cases.
- RunOnMainThread edge case could swallow Exception.

---
## [0.19.1] - 2022-06-08
### Added
- Volcanic island.
- Fallback for CurveSpec: if the spec fails to load or fails during generation a fallback will be used so that it's at least still clickable.
- Limit to size/scale of EditArea and Interactable.
- About menu button with links to readme, changenotes, website, license, logFile.
- Confirmation dialog for 'Reload' and 'Exit without saving' to prevent accidents.

### Changed
- Simplified the buttons that relate to saving and exit etc.

### Fixed
- Save and Exit will prompt you what to do when saving failed instead of exiting anyway.

---
## [0.19.0] - 2022-05-29

### Changed
- Reimplement the specification of Curve Visual, Physics, etc.
	Now built using lua files placed in `rom/CreatorToolKitData/CurveSpec/*`
	Curves save file will not be compatible so a new file name was chosen: `Curves.v2.cs.xml`

	To convert the old format copy it to the new name and then do the following:
	- In the element:
			`<CustomData type="CurveGraph.ConnectionCustomData">`
	- All the content inside should be replaced by a single:
			`<CurveSpecFilePath value="" />`

	This will cause the editor to use a placeholder graphic.
	You can then select the placeholder to change the value to what it's supposed to be.

	Lua Specification details:
		See the example files that are provided separately, they are documented to explain the features used.
	To reload the file after modification on disk either
	- use the menu item File/reload crossSections.
	- completely clear the name field and re-enter it.

### Known issues
- CurveSpecLua: errors when loading only go to the log file.
- CurveSpec: Locked Transform orientations don't work properly.

---
## [0.18.20.1] - 2022-05-15

### Fixed
- Tile Exporter's train track could move junctions in some edge cases causing them to be extra nasty.

---
## [0.18.20] - 2022-05-07

# Added
- Time tracker that keeps a record of how long you have been using the Editor for.
	You can find the value in AppData/LocalLow/DefaultCompany/StormworksEditor/PlayTime.xml
	It is formatted as HH:MM:SS
- TrackLines only actually start loading when they are in view, because when there are lots of tracks it can take ages for all of them to complete.

### Changed
- Export is now smarter about how track nodes are removed from junctions.
	Note however that the problems with curves/junctions across tile borders are NOT fixed yet because that is hard.

### Fixed
- Unity broke OnPointerExit even further to the point where it's no impossible to use, even with a workaround.
	This means that UI elements that used to disappear when you moved the mouse outside now only disappear when you click a non-UI thing.
	Thanks Unity.

---
## [0.18.19] - 2022-04-17


# Added
- Keybind F to set the camera focus to the selected object.
	This keybind does not conflict with the keybind to create a new object because the now only triggers when there is nothing selected.
- Tile items added property 'AutoIDFormat'. When set the Editor will automatically set the id to that string appended with a unique number.
	The format is also saved in the Tile.xml, this should:tm: not disturb the game.
	When a hole in the numbering is created it will not re-number any existing users of the format automatically but new users of the format will fill gaps if possible.

### Fixed
- EditArea would error when selected under some circumstances.
- HashCode of CustomSerializer is not persistence capable and caused phantom changes in source control.
- Playlist some attributes ware not marked as optional.


---
## [0.18.18] - 2022-04-08

### Added
- Playlist Save-As will copy the opened playlist's not-editable contents (lua and vehicles) so the result is not a broken playlist.
- Playlist has per-tile LOD.
- Add better selection: selecting a child object will now select the parent.
- Add separate visibility settings for EditArea and EditArea holding a vehicle.
- By default EditAreas with a vehicle inside don't show their bounds, but there is a view option to force them to show.

### Fixed
- Compression missed body.unique_id was optional.
- Playlist component didn't save unless you pressed enter in the inputfield (not marked it as changed).
- Playlist locations_id_counter was wrong causing failure to load the location in Stormworks.
- Vehicle failed to load due to compression of bodies_id.

---
## [0.18.17] - 2022-04-03

### Added
- Added a new Serialization scheme for the curves that should be easier to work with in source control and manual xml editing.
- The new format will always be used to save, but the old serializers (DataContract and BinaryFormatter) will be used as fallback.
	Note that the DataContract format has severe issues and will be dropped at some point.
	The Binary format should™ remain usable for the forseeable future.
		There is a new option under CurveGraphSettings to enable using BinaryFormatter as a backup for the curves save file.
		When curves are saved they will save to both. (And if saving using the new serializer fails it will still try to save to binary)
		Note that you need to ENABLE this yourself.
- Added better protection against invalid values for CurveNodes.


### Changed
- Orbit camera moves WAY faster due to popular request.


### Fixed
- Toggle to edit 'terrain items' didn't work.
- Mesh & Phys initial visibility ignored, started always visible.
- Playlist objects selection hitbox was scaled incorrectly.
- Vehicle Colors broke due to Stormworks xml compression update.
- StaticObject (f menu) would fail to load collider for some meshes because Unity.
- Additional Stormworks Xml Compression compatibility.
- TileExporter fails if physics crossSection missing.
- CrossSectionSpecification could not be removed once set.
- Fix degenerate meshes for collider error again. Still unknown what the real cause is though.
- Playlist functionality completely broken. (See known issues though).
- Vehicle paint was applied to non-paintable zones.
- Vehicle paint was white on zones 2 and 3 when the xml did not define a color for them. (They should follow zone 1 in this case.)

### Known Issues
- Visually disabled items (using view menu) may re-appear when LOD level changes.

---
## [0.18.16] - 2022-03-09

### Added
- Setting for the extrusion that will appear on new track sections.
- Regex in settings (Accessible by xml only) for what object ids are considered part of the Terrain.
- Start and End Node Magnitude override in Connection.
- Highlighting on Start and End-Node of Connection.
- Add placing EditArea and InteractionArea.
- Implement support for vehicle xml compression (colors may still be wrong).
- Add logic to prevent Stormworks from making a junction in unintended places.
	Current logic is working but not smart. It is marked for improvement.
	There are configuration parameters in the settings for the adventurous.
- Added button to reload Vehicles whose files have updated (or force all).
- Added button to clear the mesh cache (for when you change the mesh used by a vehicle).

### Fixed
- Setting missing or broken CrossSection file on a connection would silently break.
	- Error will now appear in the log.
	- EditPanel will refuse the change.
- CurveNode's ForwardMagnitude was not initialized properly causing issues when it was messed with.
- Curve would not update it's extrusion while it was held by the TransformGizmo.
- Tile Items such as EditArea would break when position or rotation was edited by TransformEditPanel.
- If EditArea or InteractionZone has size such that it's invisible it will be reset.
- Fix TileItems would not actually be deleted.
- Error during vehicle loading would silently go to log and not appear to the user.

---
## [0.18.15] - 2022-02-11

### Important:
Due to (not yet released) changes in the Curve/Spline data a new save format is required.
This update adds a new xml based format which will survive the transition.
The binary format `Curves.sav` will not be compatible with the new version.
The Editor will automatically switch over to the xml format, but you do need to save it manually to apply the transition.
Once this is done it's recommended to delete `Stormworks/rom/CreatorToolKitData/Curves.sav` to prevent confusion later.
In a future update the mentioned changes will actually be applied and the xml format will still work.

### Added
- Start a more sensible versioning format. This is like [Semantic Versioning](https://semver.org/spec/v2.0.0.html) except that the Major version is always 0 because there is no stable API yet to break compatibility with.
- Playlist can load the game's built-in missions.

### Changed
- Added a check to prevent removal of selected object when any modifier key is pressed.
	This should prevent accidental out of inputField `[Ctrl] + [C]` from deleting the selection.
- Improve mesh loading (better threading) and thus improve loading times by 50%.
	Note that over various previous updates the loading time had increased by 50%
	so it should just be back to normal now.

### Fixed
- Publish archive was named `.zip` when it actually is a `.7z`
- If the folder 'Stormworks/rom/CreatorToolkitData' does not exists saving curves there would fail.
- Playlist Zones with zero scale could not be selected.
- TileExporter Junction weirdness mitigation could get stuck with large numbers of junctions.
- Program getting stuck when quitting while loading is still in progress.
	Quit is still not instant, but it should actually happen now.
- Race condition in Stormworks -> Unity mesh conversion that caused error in the log.

---
## [D18.14.2] - 2022-02-08

### Fixed
- When loading of Mesh or Phys is disabled the merged objects would not work properly.
	Now when either is disabled the data will still be loaded, but
	the objects themselves will not load their meshes to still comply with the setting.

- Automatic screenshot button would not set the ortho camera controller active.
	This allowed invalid rotations, amongst other problems.

---
## [D18.14.1] - 2022-02-05

### Added
- Toggle under Menu>World to control selecting 'Terrain' objects.

---
## [D18.14] - 2022-01-31

### Added
- Directly editing of Mesh and Phys elements from the Tile xml.
	- Implemented merging of Mesh and Phys elements that represent the same object.
		This ensure that when you move a Mesh the Phys is not left behind.
	- Implemented handling of such items in the UI.
	- Implemented Export for such items.
	- Note: the old way of placing objects is still supported, and will offer automatic migration when it becomes depreciated.

### Changed
- Reworked visibility implementation for Mesh and Phys because the existing one would not work there.

---
## [D18.13.8] - 2022-01-31

### Added
- Added limit to distance between connected curveNodes (set at 2500m).
	Any attempt to adjust the distance will be rejected.
	This is a safety feature, it prevents making rail sections that span multiple tiles (would break in-game)
	And should™ also help prevent accidentally creating huge track sections that are so large they eat all the RAM and crash the program.

### Changed
- ReOrganized some files.

### Fixed
- CurveNode Append connection would sometimes fail to copy connection properties.
- CurveNode Append connection did not copy AutoSmooth property.
- CurveNode could get NaN values from TransformEditPanel
	(note the underlying bug is not yet fixed, the node is just protected)
- TileItem stack overflow when setting parent (due to moving between tiles).


---
## [D18.13.7] - 2022-01-31

### Fixed
- View menu broken checkbox behaviour for Mesh and Phys.

---
## [D18.13.6] - 2022-01-31

### Changed
- CurveNode will not attempt to connect/make a junction when modified through TransformEditPanel
	because there is a bug that allows NaN/Infinity to slip through and that breaks things.
- Internal cleanup.

### Fixed
- Camera Focus Marker could throw errors when moved over a tile that was in the early stages of loading.

---
## [D18.13.5] - 2022-01-30

### Added
- Show Curve length in Connection Properties.
- CurveNodes are validated during loading any any invalid ones (Nan, Infinity, invalid 0)
	will not be spawned in. A message will appear in the log when this happens.

---
## [D18.13.4] - 2022-01-29

### Fixes
- CurveNode property 'forwardMagnitude' could be edited but the value would not be used.
	Note: it's value can only be wriWhenChanged:tten when AutoSmooth is OFF.
- TileBehaviour would unset it's "ChangedSinceLastSaved" too late in the loading process.
	After objects had already spawned, and could have been edited.
- TileItem would not set it's "ChangedSinceLastSaved" when the gizmo arrows ware used to change it.
- TileBehaviour would unset it's "ChangedSinceLastSaved" too late in the loading process.
	After objects had already spawned, and could have been edited.
- TileItem would not set it's "ChangedSinceLastSaved" when the gizmo arrows ware used to change it.

### Known Issues
- CurveNode property 'forwardMagnitude' is not marked readonly when AutoSmooth is on.
- CurveNode property 'curvature' has no effect when AutoSmooth is off (correct)
	but there is no visual indication of this (wrong).

---
## [D18.13.3] - 2022-01-28

### Added
- Text input fields now block keyboard shortcuts from triggering.

---
## [D18.13.2] - 2022-01-28

### Added
- When appending to an existing track section some of the properties are copied from it.

### Changed
- Decreased the junction creation distance so that laying double tracks does not unintentionally junction so often.
- Decreased lerp time for CurveNodeColor from 0.5s to 0.1s.
- CurveNodes can only make a single junction at a time but would highlight all potential nodes in range.
	Only the closest one* would actually be used.
	Now only the closest node is highlighted.
	* The junction maker will now not search beyond the closest node to find one where the junction is allowed.
	to prevent unexpected results.

### Fixed
- TileExporter would not mark a tile as 'unsaved changes' when a curveNode or StaticObject was removed from it, so when the *last* object was removed it would not export that change. This could cause Stormworks to crash because the meshes referenced by those tiles would be cleaned up.

---
## [D18.13.1] - 2022-01-26

### Added
- InputFields for numbers no longer overwrite the value while you are typing.
	- Fixes TransformEditPanel being awkward to type in.

---
## [D18.13] - 2022-01-26

### Added
- InspectorPanel now polls all the data fields to keep them up to date.
- InspectorPanel now sends updates when the user changes a data field so that
		those items can update their state properly.

### Fixed
- TileItem missing data in SidePanel.
- EditArea would set it's size to 0 when GridSize.Custom was entered.

---
## [D18.12] - 2022-01-26

### Added
- Implement Vector2 and Vector4 for the SidePanel.
	Currently CurveNode.Scale uses this feature.
- TransformEditPanel now sends events when values are changed so things update.
- TransformEditPanel now respects the AllowedAxis (all or nothing for now though).
- Added CurveNode.ForwardMagnitude.
	Behind the scenes a CurveNode has a Tangent Vector.
	This vector is split into a orientation (pitch yaw roll) and ForwardMagnitude for display purposes.
- TileItems (currently EditArea and Interactable) can now be moved between tiles: 
	the internal BookKeeping will automatically be updated.
	There is a toggle to disable this behaviour, though the state of the toggle is currently NOT saved to the xml.
	In case there is a id conflict due to the move, the to be moved item will receive a suffix.
	Resulting in an id like this: `[Original_ID]_Transferred_[Unique_Number]`
	Note that Trees, StaticObjects (meshes) and TrainTracks could already be moved between tiles.

### Changed
- Changed how CurveNodes send their information to the SidePanel for display.

### Fixed
- Orthographic camera AutoScreenshot did not ensure correct camera state when starting.
- When creating a new CurveNode the movement of the new node would be double the expected amount.
- When creating a new CurveNode the visually selected CurveNode would not always be taken into account
	causing a separate new spline to be created rather than attaching to the selected node.
	The bug only ocurred when this was done multiple times in a row.

### Known Issues
- The content of the SidePanel (except the TransformEditPanel) does not update until the object is re-selected.
- The title in the SidePanel for CurveNode and TileItem is `InspectorContentProvider`
	while it should be the type of the object.
	This is a known side-effect of the new features in these items and will be fixed at some point.

---
## [D18.11.4] - 2022-01-24

### Fixed
- The button `Reload Tiles` would invalidate the internal bookKeeping of the TileExporter
	causing export operations to fail in this situation:
	- Export
	- Reload Tiles
	- Export (breaks)

---

## [D18.11.3] - 2022-01-24

### Fixed
- File.OpenWrite does not truncate which could cause errors when loading such files later.
	This bug affected:
	- Objects.xml
	- PeriodicItem
	- TileExporter Meshes

---
## [D18.11.2] - 2022-01-23

### Changed
- Increased TransformEditPanel number of decimal places to 4 (from 2)

### Fixed
- Fix error in export.

### Known Issues
- Typing numbers into the TransformEditPanel is awkward.
	Easiest way is to select and overwrite one digit at a time.

---
## [D18.11.1] - 2022-01-23

### Changed
- Static Object Loader will fall back to binary format if xml format is not found.

---
## [D18.11] - 2022-01-23

### Added
- Implemented weight block diamond for mesh gen.
- Implemented optimization for enclosed pipe. (Base game uses a mesh, this is patched to a surface so it can be optimized)

### Changed
- Save files for objects and curves now live in rom/CreatorToolkitData so that they are unique to the install.

### Fixed
- TileExporter failing when there ware no train tracks to export. (Really for real this time)

---
## [D18.10] - 2022-01-21

### Added
- Add on/off LOD to Vehicles.
- Mesh Optimization for Vehicles: hidden faces are now removed (only the most common square faces are implemented right now)
	Should provide a big performance improvement, especially for huge vehicles.


### Changed
- Improved Build process w.r.t zip creation.
- Improve LOD related things in general.
- Camera near clip plane closer (objects don't disappear on closeup).

### Fixed
- Error in the log when selecting an EditArea or InteractionZone.
- Trees would re-generate their merged/LOD mesh every time a high-detail tree spawned in,
		causing the merged mesh to blink if the user zoomed out again after triggering high-detail loading.
- Playlist Vehicles would save scale into the transform causing breakage in Stormworks.
- Tiles would try to load mesh and phys regardless of settings causing errors.

---
## [D18.9] - 2022-01-09

### Added
- Add toggle and setting for sea floor visibility.

### Changed
- LoadingText now automatically resizes, upto a limit.
- When setting tree physics is off a warning will appear, because trees can't be selected in this state.

### Fixed
- Trees could not be selected.
- Trees would not detect they ware changed when a tree was added, causing them not to export.

---
## [D18.8] - 2022-01-08

### Added
- Vehicles in playlists now show up.
- Visualization of `.phys` files (not editable, disabled by default)

### Changed
- Improve View menu: items which are not loaded cannot be clicked on.

### Fixed
- Fixed some errors when messing with the visibility of unloaded things.
	.phys -> Unity conversion was broken.

---
## [D18.7] - 2022-01-08

### Added
- Reload tiles will now also reload vehicles in editAreas (even for unchanged tiles).

### Changed
- SettingsManager's log messages now explicitly mention they are from the SettingsManager.
- Improve loading information in the UI.

### Fixed
- Tile added new LOD handler on every reload.
- Trees got incorrect LOD state after tile reload.
- Reloading of all tiles at once slowing down and freezing.

---
## [D18.6.1] - 2022-01-06

### Changed
- Settings now live in their own special place rather than bolted onto some other component.
- Add more logging around Settings loading and saving.

### Fixed
- Increase scroll sensitivity for the SidePanel.

---
## [D18.6] - 2022-01-04

### Added
- Added scrollbar to Right SidePanel (only appears when needed).
- Added a bunch more settings.

- Train Junction Clearance Radius can now be configured.
	(Supposed to prevent bugging junctions, but optimal values not yet known)
	
- TrackLines can now be enabled using the settings (disabled by default).
	- TrackLines show the track xml nodes as blue orbs and lines.
	- TrackLines cannot be changed (yet).

### Changed
- Settings file changed from .json to .xml, defaults will be generated anew.
- Fiddled with the layout options of the various SidePanel components.

### Fixed
- SeaFloor had no collision for camera focus point.
- TrackLines would throw exceptions when destroyed due to dangling event.

### Known Issues
- Reloading tiles after changing the settings will reload all of them (intended)
	but this may bog down and eventually freeze.
	A timeout will occur after 1 minute, but tiles may have failed to load.
	Do not attempt to Export in this state.
	Saving tracks and objects should™ be safe though.

---
## [D18.5] - 2022-01-02

### Added
- Add button to reload the CrossSection Xml.

### Fixed
- Some layout related warnings.

---
## [D18.4] - 2022-01-02

### Added
- Add TileLoader Settings.

### Fixed
- Layout problems in the right SidePanel (wouldn't appear with the current usage)
	It can now correctly show multiple entries.
- CrossSection: AutoGenerate Normals didn't actually cause normals to be auto-generated.
- CrossSection: Generation of Normals would fail if it was somehow triggered anyway.
- Sea floor would clip the sea for a few moments during loading.
- Names of properties of a Curve Connection ware too long to display.

---
## [D18.3] - 2022-01-02

### Added
- Add !approximate! sea floor height visualization.


### Fixed
- Regex used to match ids of items to not be loaded did not matching against the start of the id.
- Other regex missing start ot end of string.

---
## [D18.2] - 2022-01-01

### Added
- Add settings file with path to stormworks install.
	It will appear at: `C:\Users\YOUR_USER_NAME\AppData\LocalLow\DefaultCompany\StormworksEditor\settings.json`

---
## [D18.1] - 2022-01-01

### Added
- Add Interactable.

- Add saving of EditArea and Interactable.
	Important: see known issues.

### Fixed
- EditAreas with "is_invisible_vehicle" true will no longer load the vehicle and show it anyway.
		(Note: this happened for the bed, but in that case the loader would fail due to the xml being weird)
- EditPanel - Enums now have proper width to display all names.
- TransformEditPanel throwing exceptions when writing values to the input fields.

### Known Issues
- Typing into TransformEditPanel fields is wonky. Best way is to select and overwrite one digit at a time.

- EditArea and Interactable saving will cause a (one-time) change to the Tile.xml due to serialization and floating point precision.
	It has been tested to not make any difference to the Game, but version control will be unhappy the first time.
	While it's theoretically possible to fix this it will be a huge amount of work so it won't happen any time soon or at all.

- Merely selecting an EditArea or Interactable will cause it to be marked as having unsaved changes.
	This can cause unexpected occurrences of the above issue.
	This is something that will probably be fixed (at least to some extent) in the future.

---
## [D18] - 2021-12-31

### Added
- Export settings:
	Added menu item "Settings", where settings for various components will be located.
	Currently contains setting for the export process: toggles for tracks, meshes, physics and trees.
- EditAreas:
	EditAreas will appear and, if configured to spawn a vehicle will show a best-effort representation of that vehicle.

Reworks and refactors:
- Rework the logic around selecting objects.
- The selection logic now lives in one place (for the most part).
- Spline editor is now no longer a heap of special cases but just uses the TransformGizmo.
	Note: the dragging of CurveNodes may feel slightly different to before.

- Inspector Panel (right side) is now easier to manage and deal with code wise,
	making future additions easier.

- Transform Edit panel only appears for objects where it makes sense.
- Transform Gizmo handles can be enabled with axis level granularity.

### Changed
- Update Unity to 2021.2.4f1

### Fixed
- Gizmo handles did not prevent selecting objects behind them.
- Inspector panel Transform values not working.
- Unity broke the top bar buttons in their update.
- When loading trees is disabled (not user accessible) it could cause Tile loading to report failure (while it didn't)

### Known Issues
- EditAreas can be edited, but the changes are NOT saved back to xml.

---
## [D17] - 2021-11-13

### Added
- Add Hot-Reloading of tiles (button).
- Add Edit Area visualization
	- Note: Does not support any interaction or modification yet.
	- Note: Button to toggle (under View) should not be used before loading is done.


### Changed
- Improve Tile Loading by shifting the responsibility to the individual tiles.
	Better handling of the threading involved also decreased loading times by about 30% (30s to 20s)


### Known Issues
- Transform edit panel does not work (does not populate with values, editing does nothing)

---
## [D16] - 2021-11-04

### Added
- Orthographic Camera
- Automatic tile screenshot: loops over all loaded tiles and takes a screenshot.

### Known Issues
- Export fails if there are no tracks to export.

---
## [D15.1] - 2021-10-22

### Fixed
- Rebuilt in Unity version 2021.1.26f1, 
	somehow that fixed a problem that appeared out of nowhere and broke ALL versions.

---

## [D15] - 2021-09-14

### Added
- Mesh transformations, to hopefully mitigate scale breaking across save-load transitions.

---
## [D14.1] - 2021-09-04


### Added
- TileExporter will report the tile that failed in the exception.

### Fixed
- TileExporter fail when there ware no tracks in a tile with other export work.


---
## [D14] - 2021-09-03

### Added
- Handling of track junctions in xml.
- TileExporter will now connect segments of track together in the xml tracks, and also make junctions.


### Fixed
- TileExporter would stop export process if there ware no tracks to export (fixed for real this time).
- Curve interpolation would yield duplicate end samples in some cases.
- CrossSection specification was still present in curves save file rendering the xml files pointless.


### Known issues
- Junctions may be a little wonky (sudden loss of detail in the track physics)
	This is caused by a mitigation needed because the game does weird things when nodes are too close to a junction.
	The implementation of this can probably be improved with some research, but for now it works.

---
## [D13] - 2021-08-24

### Added
-  Old generated .phys files will now be cleaned up during export. (but not those from previous naming scheme!)
- Camera Focus marker now shows tile name instead of only Sawyer coordinates.
- Objects now show the id that was assigned to them in the last Export.
	- Note: the value is not a guarantee about the future, merely info about the past.

### Changed
- The names given to generated .phys files now refer to the object they belong to.
	- Note this will be changed *again* in the future once the relevant systems are reworked to re-use identical files.

---
## [D12.1] - 2021-08-16

### Fixed
- Inspector hidden for track.
- Exporter failing to clean up old export data.

---
## [D12] - 2021-08-13

### Added
- Phys mesh generator for StaticObjects.
- Phys mesh generator, for when a staticObject has a .mesh but no .phys
	It is enabled by default.
- StaticObjects now show up in the Inspector panel.
- Implement Mesh Cache.

### Changed
- Edit panel Items start readonly and become writable only once their specification says so.

### Fixed
- TileLoader could crash due to threading.

---
## [D11.1] - 2021-08-12

### Fixed
- To be transformed `phys` meshes with folders in their name failed.

---
## [D11] - 2021-08-12

### Added
- Physics for scaled static objects
- Scaled static objects will now have physics as expected.
	It didn't work before because the game's physics engine does not allow scale.
	This is fixed by generating a scaled copy of the physics mesh and using that instead.
- Minor improvements to Vehicle loading/mesh generation.

### Fixed
- Exporter won't fail when there are only static objects to export. (For real this time)

---
## [D10] - 2021-08-04

Curve save file update.

### Added
- Under the hood changes to improve code quality.
- Additional Unit Tests.

### Changed
- Rework CrossSectionSpecification to be saved/loaded with files.
	- Files are stored in `~StormworksInstall~/rom/creatorToolkitData`
	- CSSpec for CrossSectionSpecifications
	- CSSpecSub for CrossSections
- There is a archive with examples included in this release, you must install it or tracks won't render.
- Curves Save File now stores the CrossSections of Connections as path to it's xml file.
	- Note this is not backwards compatible. Old saves may load but will misbehave due to an old bug.
- The Properties on Nodes now instantly updates the curve.
- The Properties on Curves now instantly updates the curve.
- Removed a bunch of dead code

### Fixed
- A Connection's CustomData (which stores the selected CrossSection) was a shared instance between all tracks causing any change to be applied to all tracks always.
- Inspector panel layouting.


### Known Issues
- Text input fields do not have exclusive keyboard control, so other actions will still be performed.
-- Workaround: use [Ctrl] + [V] to deposit text with side effects. (most notable is the letter c, which will destroy the currently selected object)
- Track extrusion does not have priority over Tile Loading, so while loading tracks won't update.

---
## [D9.1] - 2021-08-04

### Added
- Button to save Editor's state to it's own save file (without also quitting)
- Keybind [Ctrl] + [S] to save the Editor's state (not trees).

---
## [D9] - 2021-08-04

### Added
- Improved exception handling: 
	- All file interactions now mention the file with the problem.
	- Most Xml will also provide line numbers.
- Added CrossSections that match the vanilla look (The hideous Tram track).

### Changed
- Tweaked sea visual to be a little more in line with the Game.
- Raised the seabed to -50 meter (from -250)
- Reworked Xml handling internally to have fewer code paths (easier to maintain)

### Known Issues
- Using Shift to move train tracks vertically is broken.


---
## [D8] - 2021-07-31
This is a major update, it includes 379 + 271 = 650 commits!

### Added
- Reworked splines to support junctions properly.
- Exported train tracks now also have physics.
- Reworked tile loading to support changes to trees.
- Reworked trees to have LODs and not lagg.
- Trees load way faster.
- Individual trees load as-needed, so they should load only on the tile you look at and thus load faster.
- Tree gun to place trees.
- Gizmo interaction for trees.
- Exporting of trees to Stormworks.
- WIP support for editing playlists (aka missions or environment mods)
	- Only Environment zones are fully supported.
- The Editor now searches for Steam, and then asks Steam where Stormworks is located.
	- This should™ work (on Windows) in almost all cases.
- Add readme with barebones privacy and license info.

### Changed
- Rework Splines, Tree LOD, Tree Editing, Playlist editing
- Tree loading is now done at the tile level.

### Fixed
- Trees visibility toggle doesn't lagg.
- Trees can now be disabled.
- Junctions ware broken
- Train track physics.
- Tile loading will now fail on a tile by tile basis rather than stopping.
- Tree loading will now fail on a tile by tile basis rather than stopping.
- Exporter won't fail when there are only static objects to export (not tracks).
- Many other things I can't remember.

### Known issues
- Selected item panel sometimes doesn't go away when it should.
- Tracks don't save their selected extrusion properly, causing it to move to nearby tracks after save->load.
- - Note: you can;t see this happen unless you use the Unity Editor to change it, but still.
- Trees that ware selected become highlighted again after loading highest LOD.
- Don't resize the view, it causes errors to be spammed in the log.
- The file selection dialog causes warnings to be spammed in the log.
- Playlists will not load properly if the tiles they are on have not loaded yet.
- Save as only saves playlist.xml, it will not copy vehicles.


---
## [D7c] - 2020-12-5
BugFixes.

### Changed
- Improve error message if export fails.

### Fixed
- Error in Export (if phyMesh was null, would still attempt to compare physMeshPath)

---
## [D7b] - 2020-12-5

### Changed
- Space toggle keybind to T

### Fixed
- Newly created object had no collider, because Unity just does not make it unless I wiggle a magic wand in just the right way.
- Pivot point of transform gizmo was strange.
- Controls had wrong keybinds, r,t,y instead of actual w,e,r


---
## [D7a] - 2020-12-5


### Fixed
- Static object could fail to load.
- Player wouldn't load collision because Unity. (really no clue why it fails, or why the solution even works)

---
## [D7] - 2020-12-5

### Added
- Full support for static objects: Moving, rotating, even scaling (but please don't)
	- Also supports .phys files (will use them when provided).


### Changes:
- Camera prefab variant
- Moved Packages around

### Fixed
- Fly camera cursor will now disaster like it should.


---
## [D6] - 2020-12-2

### Added
- Support for normals to be defined with a crossSection.
- Static objects can be loaded from file, but that's it for now.
- If loading of save file fails, saving is disabled too, to prevent overwriting with nothing.
- Add outliner asset.

### Changed
- Update to Unity v2020.1.15f1


### Fixed
- Fixed normals that broke in Demo 5.
- Fixed load order of things caused problems
- Vehicle loader would break orientation, still not 100% though.


---
## [D5] - 2020-11-30

No major new features, mostly internal cleanup.

### Added
- No-Effort-November: Add sea and seaFloor.


### Changed
- Before Export splines are saved, in case Export dies. (this can happen on invalid spline)


### Fixed
- Track xml nodes would not be removed/replaced when exporting multiple times, causing  weirdness in-game and could even cause SW to fail to load (after many exports).
- Export could take an excessive amount of time. This is now fixed, and export is now multithreaded too (but that is a separate thing). Internally the code is a lot cleaner.


### Known Issues
- Broken: Colors on the extruded parts of the tracks are now black (in-game only). This is because the normals are gone (previously Unity generated them, but the internals have changed, and that can't be done anymore). 

---
## [D4] - 2020-11-13

BugFixes.
### Added
- Add position markers to Orbit Camera.
	- Attempt to improve Orbit camera movement close to target.
- Allow user to select crossSection.

---
## [D3] - 2020-11-6

### Added
- Exporting to Stormworks.
- Multiple crossSections support.
- Bridge crossSection.
- Focus marker for orbit camera.

---
## [D2] - 2020-11-2

Save Editor state & Internal improvements

### Added
- Editor state (splines) are automatically saved to a file.
- CurveGraph: Split node
- CurveGraph: Node Vertical movement


### Changed
- Move DataModel back into .dll


---
## [D1] - 2020-10-30

Initial demo release.

### Added 
- Orbit camera
- Fly camera
- Trees toggle
- Track tool
	- Add node
	- Move node
	- Delete node
