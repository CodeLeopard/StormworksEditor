# Create 8MB zip files to publish on Discord.


$solutionFiles = 
	"Controls.txt",
	"Readme.txt",
	"ChangeNotes.txt",
	"LICENSE"



$ProjectDir = "$PSScriptRoot"
$ProjectName = "StormworksEditor"
$TargetDir = "Build\Publish"

$zipFile = "$ProjectDir\Publish\$($ProjectName).zip"

$old_location = Get-Location
Set-Location $PSScriptRoot
try
{
	Remove-Item "$zipFile.*"

	Write-Host "Creating new Arcive..."


	foreach ($file in $solutionFiles)
	{
		Write-Host "Copy-Item `"$ProjectDir\$file`" `"$TargetDir\$file`""
		Copy-Item "$ProjectDir\$file" "$TargetDir\$file"
	}


	$folder = "$TargetDir\"

	& 7z a -t7z -mx -v8190k $zipFile $folder
	Write-Host "Archive creation Done."

}
finally
{
	Set-Location $old_location
}