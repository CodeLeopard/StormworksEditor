﻿using DataTypes.Attributes;

using UnityEditor;

using UnityEngine;

namespace PropertyDrawers
{
	[CustomPropertyDrawer(typeof(InspectorCustomNameAttribute))]
	public class InspectorCustomNameDrawer : PropertyDrawer
	{
		private GUIContent _myLabel;
		private GUIContent myLabel => _myLabel ?? (_myLabel = new GUIContent((attribute as InspectorCustomNameAttribute).Name));


		/// <inheritdoc />
		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return 0f;
		}


		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUILayout.PropertyField
				(
				 property
			   , myLabel
			   , includeChildren: true
				);
		}
	}
}
