﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using Shared;

using Tools;

using UnityEditor;

using UnityEngine;

using Vector3 = OpenTK.Mathematics.Vector3;

namespace CustomEditors
{
	[CustomEditor(typeof(VehicleLoader))]
	class VehicleLoaderInspector : UnityEditor.Editor
	{
		/// <inheritdoc />
		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();

			var instance = (VehicleLoader)target;
			var vehicle = instance.activeVehicle;
			if (null == vehicle)
			{
				GUILayout.Label($"No vehicle loaded (yet).");
				return;
			}

			GUILayout.Label($"Loaded vehicle stats:");
			GUILayout.Label($"Bodies: {vehicle.bodies.Count}");

			GUILayout.Label($"Voxel: Min:{vehicle.voxelBounds.Min}, Center:{vehicle.voxelBounds.Center}, Max:{vehicle.voxelBounds.Max}");
			GUILayout.Label($"VoxWorld: Min:{vehicle.voxelBoundsF.Min}, Center:{vehicle.voxelBoundsF.Center}, Max:{vehicle.voxelBoundsF.Max}");
			GUILayout.Label($"Mesh: Min:{vehicle.meshBounds.Min}, Center:{vehicle.meshBounds.Center}, Max:{vehicle.meshBounds.Max}");
		}
	}
}
