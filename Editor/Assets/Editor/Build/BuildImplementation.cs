﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using Shared;
using System;
using System.IO;
using System.Linq;
using Tools;
using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Build
{
	internal static class BuildImplementation
	{
		internal static BuildPlayerOptions DefaultOptions()
		{
			BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
			buildPlayerOptions.scenes = new[] { "Assets/Scenes/System/SharedSingletons.unity", "Assets/Scenes/System/Loading.unity", "Assets/Scenes/Menu.unity", "Assets/Scenes/World.unity", "Assets/Scenes/Vehicle.unity" };
			buildPlayerOptions.targetGroup = BuildTargetGroup.Standalone;
			buildPlayerOptions.target = BuildTarget.StandaloneWindows64;

			return buildPlayerOptions;
		}


		#region PreBuild

		private static void MyPreBuild(bool build_datamodel = true)
		{
			Console.WriteLine("[Build] Start.");

			if (build_datamodel)
			{
				FetchDataModel();
			}

			PreBuildActions.MakeBuildVersionInfo();
		}

		private static void FetchDataModel()
		{
			const int seconds_to_millis = 1000;
			const int timeout = 30 * seconds_to_millis;

			Console.WriteLine("[Build] Building dependency/submodule: DataModel. This may take a while...");

			string workingDir = Path.Combine(PathHelper.GetSolutionDirectoryOrCurrent().FullName, "..");
			string scriptFile = Path.Combine(workingDir, "FetchDataModel.ps1");

			var result = CommandLineProgramProcess.RunProgram("powershell", workingDir, $"-noLogo -NoProfile -ExecutionPolicy Bypass -File \"{scriptFile}\" -Force", timeout);
			result.ThrowIfFailed();
			Console.WriteLine(result.StandardOutput);
			Console.WriteLine("[Build] Building DataModel done.");
		}

		private static void MyPreBuildWithBuildSettings(BuildPlayerOptions options)
		{
			var path = Path.GetDirectoryName(options.locationPathName);
			Directory.CreateDirectory(path); // Unity is too stupid to do this for us :(

			// Try to fix lighting when the scene(s) to be built have not been opened.
			// See also: https://issuetracker.unity3d.com/issues/scene-is-brighter-in-standalone-player-if-it-was-open-in-the-editor-at-build-time
			// See also: https://issuetracker.unity3d.com/issues/player-lighting-not-generated-when-building-the-project-using-command-line
			foreach (string scene in options.scenes)
			{
				Console.WriteLine($"[Build] Opening '{scene}' to fix lighting issues...");
				EditorSceneManager.OpenScene(scene, OpenSceneMode.Single);

				// Perhaps this will make it work with multiple scenes?
				//Console.WriteLine($"[Build] baking '{scene}'...");
				//Lightmapping.Bake();
				//Console.WriteLine($"[Build] Baking '{scene}' done.");
			}
			Console.WriteLine($"[Build] Lighting fix completed.");
		}


		#endregion PreBuild


		#region Build


		internal static BuildReport CreateBuild(BuildPlayerOptions options, bool pdb_obfuscation, bool build_datamodel = true)
		{
			MyPreBuild(build_datamodel);
			MyPreBuildWithBuildSettings(options);

			BuildReport report;
			if(pdb_obfuscation)
			{
				report = Implementation_Build_Obfuscated(options);
			}
			else
			{
				report = Implementation_Build(options);
			}

			ReadBuildReport(report);
			MyPostBuild();

			return report;
		}



		private static BuildReport Implementation_Build(BuildPlayerOptions options)
		{
			try
			{
				var report = BuildPipeline.BuildPlayer(options);
				PostBuildActions.Always(options);
				return report;
			}
			catch (Exception e)
			{
				Console.WriteLine("[Build] Error");
				Debug.LogException(e);
				throw;
			}
		}

		/// <summary>
		/// Build with the .pdb changed so paths start at repo root.
		/// </summary>
		/// <param name="options"></param>
		/// <returns></returns>
		private static BuildReport Implementation_Build_Obfuscated(BuildPlayerOptions options)
		{
			string[] compilerArgs = PlayerSettings.GetAdditionalCompilerArgumentsForGroup(options.targetGroup);

			try
			{
				var myCompilerArgs = compilerArgs.ToArray();

				var assetsFolder = Application.dataPath;
				var projectFolder = assetsFolder.Substring
					(0, assetsFolder.LastIndexOfAny(new[] { '/', '\\' }))
					.Replace('/', '\\');

				string obfuscationArgument =
					$"/pathMap:{projectFolder}=StormworksEditor";
				if (!myCompilerArgs.Contains(obfuscationArgument))
				{
					myCompilerArgs = myCompilerArgs.Concat(new[] { obfuscationArgument }).ToArray();
					PlayerSettings.SetAdditionalCompilerArgumentsForGroup(BuildTargetGroup.Standalone, myCompilerArgs);
				}

				AssetDatabase.SaveAssets();

				BuildReport report = Implementation_Build(options);
				return report;
			}
			finally
			{
				PlayerSettings.SetAdditionalCompilerArgumentsForGroup(BuildTargetGroup.Standalone, compilerArgs);
				AssetDatabase.SaveAssets();
			}
		}

		#endregion Build

		#region PostBuild


		private static void ReadBuildReport(BuildReport report)
		{
			BuildSummary summary = report.summary;

			if (summary.result == BuildResult.Succeeded)
			{
				Debug.Log($"[Build] Build succeeded: '{summary.outputPath}', {summary.totalSize} bytes");
			}
			else
			{
				Debug.Log($"[Build] Build: {summary.result}");
			}
		}

		private static void MyPostBuild()
		{
			Console.WriteLine($"[Build] End {DateTime.Now}.");
		}
		#endregion PostBuild
	}
}
