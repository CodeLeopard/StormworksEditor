﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.IO;
using System.Linq;
using System.Text;

using Tools;

using UnityEditor;

using UnityEngine;

using Debug = UnityEngine.Debug;

namespace Build
{
	//https://docs.unity3d.com/ScriptReference/Callbacks.PostProcessBuildAttribute.html
	public static class PostBuildActions
	{
		private static readonly string[] solutionFiles =
			new string[] { "controls.txt", "Readme.txt", "ChangeLog.md", "LICENSE.txt", "../COPYING", "../COPYING.LESSER" };


		private static string projectFolder => GetProjectFolder();


		private static string GetProjectFolder()
		{
			var assetsFolder = Application.dataPath;
			var projectFolder = assetsFolder.Substring(0,
			                                           assetsFolder.LastIndexOfAny(new[] { '/', '\\' }));
			return projectFolder;
		}

		public static string GetBuildFolder(BuildPlayerOptions options)
		{
			string projectFolder = GetProjectFolder();

			string buildFolder = Path.Combine(projectFolder, options.locationPathName);
			// Unity is retarded and interprets the last part of the PATH as the name.
			buildFolder = buildFolder.Substring(0, buildFolder.LastIndexOfAny(new[] { '/', '\\' }));

			return buildFolder;
		}

		public static void Always(BuildPlayerOptions options)
		{
			PostBuildFileCopy(options);
		}


		public static void PostBuildFileCopy(BuildPlayerOptions options)
		{
			string buildFolder = GetBuildFolder(options);
			PostBuildFileCopy(buildFolder);
		}

		public static void PostBuildFileCopy(string buildFolder)
		{
			foreach (var solutionFile in solutionFiles)
			{
				var sourcePath = Path.Combine(projectFolder, solutionFile);
				var destPath = Path.Combine(buildFolder,     Path.GetFileName(solutionFile));

				File.Copy(sourcePath, destPath, true);
			}
		}
	}
}
