﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.IO;

using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;

using Shared;

using DataTypes;

namespace Build
{
	public class PreBuildActions : IPreprocessBuildWithReport
	{
		/// <inheritdoc />
		public int callbackOrder => 10;

		/// <inheritdoc />
		public void OnPreprocessBuild(BuildReport report)
		{
			// Ensure paths are updated in case they ware changed.
			ScriptableObjectDB.UpdatePaths();
		}

		/// <summary>
		/// Note: this should run BEFORE the build process starts, because our build pipeline causes changes in git.
		/// </summary>
		public static void MakeBuildVersionInfo()
		{
			var buildDate = DateTime.UtcNow;

			// https://stackoverflow.com/questions/2657935/checking-for-a-dirty-index-or-untracked-files-with-git
			// https://stackoverflow.com/questions/5432245/is-there-some-way-to-work-with-git-using-net-application

			string workingDir = Path.Combine(Application.dataPath, "..");

			var info = BuildAndVersionInfo.Create(workingDir, buildDate);

			string fullPath = Path.Combine(Application.streamingAssetsPath, BuildAndVersionInfo.resourcePath);
			string folderPath = Path.GetDirectoryName(fullPath);
			Directory.CreateDirectory(folderPath); // Create if not exist.

			info.Save(fullPath);

			if (info.GitWasDirty)
			{
				Debug.LogWarning("[Build] Git has unsaved changes!");
			}
		}

		[MenuItem("Build/Create VersionInfo file")]
		public static void Menu_MakeBuildVersionInfo()
		{
			MakeBuildVersionInfo();
			Debug.Log("BuildVersionInfo created.");
		}
	}
}
