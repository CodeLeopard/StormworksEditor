﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using UnityEditor;
using UnityEditor.Build.Reporting;

using static Build.BuildImplementation;

namespace Build
{
	// todo: Make the 'unsaved changes' popup not happen somehow.
	// todo: Build output is hidden due to Unity.
	// todo: somehow make [Ctrl] + [B] do the right thing.


	public static class CustomBuildButtons
	{
		private static void Guard()
		{
			if (EditorApplication.isPlaying)
			{
				throw new InvalidOperationException("Cannot be used during PlayMode");
			}
		}

		[MenuItem("Build/Debug", priority = 10)]
		public static void Build_Publish_Debug()
		{
			Guard();

			BuildPlayerOptions buildPlayerOptions = DefaultOptions();
			buildPlayerOptions.locationPathName = "Build/Publish/StormworksEditor/StormworksEditor.exe";
			buildPlayerOptions.options = BuildOptions.AllowDebugging
			                           | BuildOptions.Development
			                           | BuildOptions.DetailedBuildReport;

			CreateBuild(buildPlayerOptions, pdb_obfuscation: true);
		}

		[MenuItem("Build/Release", priority = 11)]
		public static void Build_Publish_Release()
		{
			Guard();

			BuildPlayerOptions buildPlayerOptions = DefaultOptions();
			buildPlayerOptions.locationPathName = "Build/Publish/StormworksEditor/StormworksEditor.exe";
			buildPlayerOptions.options = BuildOptions.AllowDebugging
			                           | BuildOptions.DetailedBuildReport;

			CreateBuild(buildPlayerOptions, pdb_obfuscation: true);
		}


		[MenuItem("Build/Internal/Debug", priority = 20)]
		public static void Build_Internal_Debug()
		{
			Guard();

			BuildPlayerOptions buildPlayerOptions = DefaultOptions();
			buildPlayerOptions.locationPathName = "Build/Internal/StormworksEditor/StormworksEditor.exe";
			buildPlayerOptions.options = BuildOptions.AllowDebugging
			                           | BuildOptions.Development
			                           | BuildOptions.DetailedBuildReport;

			CreateBuild(buildPlayerOptions, pdb_obfuscation: false);
		}

		[MenuItem("Build/Internal/Optimized", priority = 21)]
		public static void Build_Internal_Optimized()
		{
			Guard();

			BuildPlayerOptions buildPlayerOptions = DefaultOptions();
			buildPlayerOptions.locationPathName = "Build/Internal/StormworksEditor/StormworksEditor.exe";
			buildPlayerOptions.options = BuildOptions.AllowDebugging
			                           | BuildOptions.DetailedBuildReport;

			CreateBuild(buildPlayerOptions, pdb_obfuscation: false);
		}


		public static BuildReport Build_CI(BuildPlayerOptions options)
		{
			BuildReport report = CreateBuild(options, pdb_obfuscation: true, build_datamodel: false);
			return report;
		}
	}
}
