﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using BinaryDataModel.Converters;

using CurveGraph;

using UnityEditor;

using UnityEngine;

namespace Curves2
{
	[CustomEditor(typeof(CurveNodeBehaviour))]
	public class CurveNodeInspector : UnityEditor.Editor
	{

		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();


			var behaviour = target as CurveNodeBehaviour;
			var node = behaviour.node;

			if (null == node) return;

			var count = node.Connections.Count;

			EditorGUI.BeginDisabledGroup(true);
			EditorGUILayout.IntField("Connections", count);
			EditorGUI.EndDisabledGroup();

			node.AutoSmooth = EditorGUILayout.Toggle("AutoSmooth", node.AutoSmooth);
			node.Curvature  = EditorGUILayout.Slider("Curvature", node.Curvature, CurveNode.curvatureMinimum, CurveNode.curvatureMaximum);

			//node.RaiseNodeValuesChanged(true);
		}


		void DrawNodeDirection(CurveNode node)
		{
			Handles.color = Color.blue;
			Handles.DrawLine(node.Position.ToUnity(), (node.Position + node.Direction).ToUnity());
		}

		void DrawConnection(Connection connection)
		{
			var curve = connection.curve;

			var samples = curve.GetSamples();
			if (null == samples) return;
			var prev = samples[0].location.ToUnity();

			Handles.color = Color.green;
			for (int i = 1; i < samples.Length; i++)
			{
				var start = prev;
				var end = samples[i].location.ToUnity();

				prev = end;

				Handles.DrawLine(start, end);
			}
		}

		void DrawGraph(CurveNode start)
		{
			foreach (var connection in start.NetworkConnections())
			{
				DrawConnection(connection);
			}

			foreach (var node in start.NetworkNodes())
			{
				DrawNodeDirection(node);
			}
		}

		void OnSceneGUI()
		{
			var behaviour = target as CurveNodeBehaviour;
			var node = behaviour.node;


			if (null == node) return;

			DrawGraph(node);
		}
	}
}
