﻿using System.Linq;
using UnityEditor;
using UnityEditor.ShortcutManagement;
using UnityEngine;

namespace Editor.Tools
{
	[InitializeOnLoad]
	public static class DisableEditorShortcutsInPlayMode
	{
		private static string PlayModeProfileId = "PlayMode";
		private static string EditModeProfileId = null;
		static DisableEditorShortcutsInPlayMode()
		{
			EditorApplication.playModeStateChanged += ModeChanged;
			EditorApplication.quitting += Quitting;
		}

		static void ModeChanged(PlayModeStateChange playModeState)
		{
			if (playModeState == PlayModeStateChange.EnteredPlayMode)
			{
				EditModeProfileId = ShortcutManager.instance.activeProfileId;
				SetupPlaymodeProfile();
			}
			else if (playModeState == PlayModeStateChange.EnteredEditMode && EditModeProfileId != null)
			{
				ShortcutManager.instance.activeProfileId = EditModeProfileId;
				EditModeProfileId = null;
			}
		}

		private static string[] ShortcutsToClear = new string[]
		{
			"Main Menu/Edit/Undo",
			"Main Menu/Edit/Undo History",
			"Main Menu/File/Build And Run",
		};

		private static void SetupPlaymodeProfile()
		{
			var sm = ShortcutManager.instance;
			if (sm.GetAvailableProfileIds().Contains(PlayModeProfileId))
			{
				ShortcutManager.instance.activeProfileId = PlayModeProfileId;
				return;
			}

			sm.CreateProfile(PlayModeProfileId);
			sm.activeProfileId = PlayModeProfileId;

			foreach(var s in ShortcutsToClear)
			{
				sm.ClearShortcutOverride(s);
			}

			Debug.Log($"Created a new UnityEditor keyboard shortcuts profile '{PlayModeProfileId}' to use in Playmode.");
		}

		static void Quitting()
		{
			if (EditModeProfileId == null) return;
			ShortcutManager.instance.activeProfileId = EditModeProfileId;
		}
	}
}
