﻿// https://docs.unity3d.com/ScriptReference/PrefabUtility.html

using System.Collections.Generic;

using UnityEditor;

using UnityEngine;

namespace Tools {
	public class PrefabTools
	{
		private const string menuName = "Assets/Create/Prefab(s) from selected object(s)";


		// Creates a new menu item 'Examples > Create Prefab' in the main menu.
		[MenuItem(menuName)]
		static void CreatePrefab()
		{
			// Keep track of the currently selected GameObject(s)
			GameObject[] objectArray = Selection.gameObjects;

			CreatePrefab(objectArray);
		}


		static void CreatePrefab(ICollection<GameObject> objects)
		{
			// Loop through every GameObject in the array above
			foreach (GameObject gameObject in objects)
			{
				// Set the path as within the Assets folder,
				// and name it as the GameObject's name with the .Prefab format
				string localPath = "Assets/GeneratedPrefabs/" + gameObject.name;

				// Make sure the file name is unique, in case an existing Prefab has the same name.
				localPath = AssetDatabase.GenerateUniqueAssetPath(localPath + ".prefab");

				// Create the new Prefab.
				PrefabUtility.SaveAsPrefabAssetAndConnect(gameObject, localPath, InteractionMode.UserAction);
			}
		}


		// Disable the menu item if no selection is in place.
		[MenuItem(menuName, true)]
		static bool ValidateCreatePrefab()
		{
			return Selection.activeGameObject != null && !EditorUtility.IsPersistent(Selection.activeGameObject);
		}
	}
}
