﻿using UnityEditor;

namespace Tools
{

	/// <summary>
	/// This is needed because the unity setting "Recompile AFTER finished playing"
	/// actually means: "Recompile immediately and sometimes freeze".
	/// </summary>
	[InitializeOnLoad]
	public static class ReallyWaitWithCompileUntilFinishedPlaying
	{
		static ReallyWaitWithCompileUntilFinishedPlaying()
		{
			EditorApplication.playModeStateChanged += PlayModeStateChanged;
		}

		private static void PlayModeStateChanged(PlayModeStateChange obj)
		{
			switch (obj)
			{
				case PlayModeStateChange.ExitingEditMode:
				case PlayModeStateChange.EnteredPlayMode:
				{
					SetAutoRefresh(false);
					break;
				}
				case PlayModeStateChange.ExitingPlayMode:
				case PlayModeStateChange.EnteredEditMode:
				{
					SetAutoRefresh(true);
					break;
				}
			}
		}

		private static void SetAutoRefresh(bool value)
		{
			EditorPrefs.SetInt("kAutoRefresh", value ? 1 : 0);
			AssetDatabase.Refresh(); // Detect any changes that happened during autoRefresh being off.
		}
	}
}
