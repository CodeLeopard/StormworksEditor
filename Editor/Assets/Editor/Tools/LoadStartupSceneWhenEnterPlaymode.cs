﻿using UnityEditor;
using UnityEditor.SceneManagement;

using UnityEngine;

namespace Editor.Tools
{
	[InitializeOnLoad]
	public class EditorInit
	{
		static EditorInit()
		{
			var pathOfFirstScene = EditorBuildSettings.scenes[0].path;
			var sceneAsset = AssetDatabase.LoadAssetAtPath<SceneAsset>(pathOfFirstScene);

			if(EditorSceneManager.playModeStartScene != sceneAsset)
			{
				EditorSceneManager.playModeStartScene = sceneAsset;
				Debug.Log($"{pathOfFirstScene} was set as default play mode scene.");
			}
		}
	}
}
