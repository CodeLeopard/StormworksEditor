Shader "Custom/VertexColorEmissive" {
    Properties {
        _Emission ("Emmisive Color", Color) = (0,0,0,0)
        _Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }

    SubShader {
        ZWrite on

        Alphatest Greater[_Cutoff]
        Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
        Blend SrcAlpha
        OneMinusSrcAlpha
        Pass {
            Material {
                Emission [_Emission]
            }
            ColorMaterial Emission
        } 
    }
}