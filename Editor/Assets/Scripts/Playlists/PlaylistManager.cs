﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.IO;
using System.Runtime.Serialization;

using Behaviours;

using CustomSerialization;
using CustomSerialization.BuiltIn;
using CustomSerialization.Specification;

using DataModel.Missions;
using Shared.Serialization;

using Shared;

using SimpleFileBrowser;

using Tiles;

using UnityEngine;
using System.Threading.Tasks;
using GUI.ItemSelector;
using Control;
using DataTypes.Extensions;
using RuntimeGizmos;

using Component = DataModel.Missions.Component;
using System.Linq;
using GUI;
using Tools;

namespace Playlists
{
	[RequireComponent(typeof(TileLoader))]
	public class PlaylistManager : M0noBehaviour
	{
		[SerializeField]
		private new Camera camera;
		[SerializeField]
		private TransformGizmo transformGizmo;

		private LayerMask terrain;

		[Tooltip("The parent for Objects in a playlist. If null will use self.")]
		public GameObject PlaylistParent;

		[NonSerialized]
		public TileLoader tileLoader;

		public string activePlaylistPath;

		[NonSerialized]
		public GameObject activePlaylistGO;
		[NonSerialized]
		public Playlist activePlaylist;
		[NonSerialized]
		public PlaylistBehaviour activePlaylistBehaviour;

		public bool savingProblematic = false;

		private const string playlistFileName = "playlist.xml";

		// C:\Users\__USER__\AppData\Roaming\Stormworks\data\missions\__MISSIONFOLDER__\playlist.xml
		private readonly string userPlaylistsPath =
			Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)
			   , "Stormworks", "data", "missions");

		private static string builtinPlaylistsPath => StormworksPaths.Data.missions;

		//private static string workshopPlaylistsPath = 

		private static SpecGenerator.SpecGeneratorSettings settings;
		internal static GraphSerializationSpecification graphSpec;
		internal static StreamingContext context = new StreamingContext();


		private static readonly string ProblematicSavingMessage = "Playlist contains multiple locations on the same tile.\nThis is not supported and WILL break the playlist when saving.";


		static PlaylistManager()
		{
			settings = new SpecGenerator.SpecGeneratorSettings();
			settings.AllowUnguided = true;

			graphSpec = SpecGenerator.CreateGraphSerializationSpecification(typeof(Playlist), settings);
			graphSpec.NoHeader = true;

			OpenTKMathTypes.SetStormworksMathTypes(graphSpec);
		}


		protected override void OnAwake()
		{
			if(null == PlaylistParent) PlaylistParent = gameObject;

			tileLoader = this.GetRequiredComponent<TileLoader>();

			terrain = LayerMaskExt.NameToMask("Terrain");
		}

		private async void Update()
		{
			await keyboardInput();
		}

		private async Task keyboardInput()
		{
			if (activePlaylist == null) return;
			if (InputManagement.TextInputFocused) return;
			if (InputManagement.AnyModifierKeyPressed) return;

			if (Input.GetKeyUp(KeyCode.H))
			{
				await TriggerNewObjectmenu();
			}
		}


		#region ButtonActions

		public void B_OpenPlaylist()
		{
			OpenPlaylist(userPlaylistsPath);
		}

		public void B_OpenPlaylistBuiltin()
		{
			OpenPlaylist(builtinPlaylistsPath);
		}

		public void OpenPlaylist(string initialPath)
		{
			// Prevent multiple playlists open at the same time.
			B_ClosePlaylist();

			FileBrowser.SetFilters(false, ".xml");

			if (! FileBrowser.ShowLoadDialog
				(FileBrowser_OnSuccess
			   , FileBrowser_OnFileSelectionCanceled
			   , FileBrowser.PickMode.Folders
			   , false
			   , initialPath
			   , null
			   , "Select a Playlist folder to load."
				))
			{
				Debug.LogError($"Failed to open the SelectFileDialog.");
			}

			void FileBrowser_OnSuccess(string[] selection)
			{
				var playlistFilePath = Path.Combine(selection[0], playlistFileName);
				if (!File.Exists(playlistFilePath))
				{
					Debug.LogWarning($"Selected a folder without a {playlistFileName} file.");
					return;
				}

				LoadPlaylist(playlistFilePath);
			}
		}

		private void FileBrowser_OnFileSelectionCanceled()
		{
			Console.WriteLine($"User canceled file selection");
		}

		private void WarnuserAboutProblematicSaving()
		{
			var popup = ConfirmationPopupPanel.Create();
			popup.HeaderText = "Saving Playlist may loose data.";
			popup.PreferredWidth = 800;
			popup.BodyText = ProblematicSavingMessage + "\nYou will also be warned about this if you attempt to save.";

			popup.ConfirmText = "Close";
			popup.CancelText  = "Dismiss";
		}

		private async Task<bool> UserDoesNotWantToSaveProblematic()
		{
			var popup = ConfirmationPopupPanel.Create();
			popup.HeaderText = "Saving Playlist may loose data.";
			popup.PreferredWidth = 800;
			popup.BodyText = ProblematicSavingMessage;

			popup.ConfirmText = "Save anyway";
			popup.CancelText = "Cancel";
			bool? result = await popup.WaitForInteraction();

			bool DontSave = result == null || result.Value == false;
			return DontSave;
		}

		public void B_ReloadPlaylist()
		{
			if (null == activePlaylist) return;

			var playListPath = activePlaylist.meta_filePath;
			B_ClosePlaylist();
			LoadPlaylist(playListPath);
		}

		public async void B_SavePlaylist()
		{
			if (null == activePlaylist) return;

			if (savingProblematic && await UserDoesNotWantToSaveProblematic())
			{
				return;
			}

			Console.WriteLine($"Saving Open Playlist back to it's file '{activePlaylist.meta_filePath}'");
			SavePlaylist(activePlaylist.meta_filePath);
		}

		public async void B_SavePlaylistAs()
		{
			if (savingProblematic && await UserDoesNotWantToSaveProblematic())
			{
				return;
			}

			FileBrowser.SetFilters(false, ".xml");

			if (!FileBrowser.ShowLoadDialog
				(FileBrowser_OnSuccess
			   , FileBrowser_OnFileSelectionCanceled
			   , FileBrowser.PickMode.Folders
			   , false
			   , userPlaylistsPath
			   , null
			   , "Select a Playlist folder to save over."
				))
			{
				Debug.LogError($"Failed to open the SelectFileDialog.");
			}

			void FileBrowser_OnSuccess(string[] selection)
			{
				if (selection.Length > 1)
				{
					throw new ArgumentException("Expected exactly one folder to be selected.");
				}
				string folderPath = selection[0];
				var playlistFilePath = Path.Combine(folderPath, playlistFileName);

				Console.WriteLine($"Saving Open Playlist to a specified file '{playlistFilePath}'. Extra content will be copied.");

				var destDir = new DirectoryInfo(folderPath);
				if (destDir.Exists)
				{
					destDir.Delete(true);
					destDir.Create();
				}

				// Copy unEditable content such as vehicles and lua over.
				var sourceDir = new DirectoryInfo(Path.GetDirectoryName(activePlaylistPath));
				foreach (FileInfo file in sourceDir.EnumerateFiles())
				{
					file.CopyTo(Path.Combine(destDir.FullName, file.Name));
				}

				SavePlaylist(playlistFilePath);
			}
		}

		public void B_ClosePlaylist()
		{
			Destroy(activePlaylistGO);
			activePlaylist = null;
			activePlaylistGO = null;
			activePlaylistBehaviour = null;
		}

		#endregion ButtonActions

		private void LoadPlaylist(string path)
		{
			Console.WriteLine($"Loading Playlist {path}");
			savingProblematic = false;

			activePlaylistPath = path;

			var doc = XMLHelper.LoadFromFile(path);
			activePlaylist = Serializer.Deserialize<Playlist>(doc, graphSpec, context);

			(bool is_env, string tile) KeySelector(Location location)
			{
				// When tile name is null, the tile will not be found and thus not cause a problem when saving.
				// Assign it a GUID in the key to ensure it is unique.
				string tile_name = location.tile ?? Guid.NewGuid().ToString();

				return (location.is_env_mod, tile_name);
			}
			var locationGroupings = activePlaylist.Locations.ToLookup(KeySelector);
			bool GroupingCheck(IGrouping<(bool is_evn, string tile), Location> g)
			{
				return g.Count() > 1;
			}
			if(locationGroupings.Any(GroupingCheck))
			{
				savingProblematic = true;
				Debug.LogWarning(ProblematicSavingMessage);
				WarnuserAboutProblematicSaving();
			}

			((ISerializationEventReceiver)activePlaylist).AfterDeserialize(path);

			activePlaylistGO = new GameObject(activePlaylist.name);
			activePlaylistGO.transform.SetParent(PlaylistParent.transform, true);
			activePlaylistBehaviour = activePlaylistGO.AddComponent<PlaylistBehaviour>();
			activePlaylistBehaviour.Initialize(this, activePlaylist);
		}

		private void ReloadPlaylist()
		{
			var reloadFilePath = activePlaylistPath;

			B_ClosePlaylist();

			if (null != reloadFilePath)
			{
				LoadPlaylist(reloadFilePath);
			}
			else
			{
				B_OpenPlaylist();
			}
		}

		private void SavePlaylist(string path)
		{
			((ISerializationEventReceiver)activePlaylist).BeforeSerialize();
			var doc = Serializer.Serialize(activePlaylist, graphSpec, context);
			XMLHelper.SaveToFile(doc, path);

			activePlaylistBehaviour.ApplyPendingVehicleFilehandling();
		}

		#region Add Object

		public bool TerrainRayCast(out Ray ray, out RaycastHit hit)
		{
			return NewObjectPosition.RayCast(camera, terrain, out ray, out hit);
		}

		private ButtonGrid PlacementMenu;
		private async Task TriggerNewObjectmenu()
		{
			if (PlacementMenu != null) return;

			if (!NewObjectPosition.Position(camera, terrain, out Vector3 position))
			{
				Debug.LogWarning($"Could not spawn a TileObject because mouse is not over terrain, unable to find position to place the object", this);
				return;
			}

			var t = TileLoader.instance.GetNearestTile(position);

			if (null == t)
			{
				Debug.LogWarning($"Could not spawn a TileObject because there is no tile at the current mouse position.", this);
				return;
			}


			PlacementMenu = ButtonGrid.Create(null);
			PlacementMenu.gameObject.name = "PlaylistComponentSelector";
			PlacementMenu.Layout.cellSize = new Vector2(200, 50);
			PlacementMenu.AddCancelButton();



			GameObject go = null;

			PlaylistComponent Common(Component component, Vector3 global_position, bool is_env)
			{
				component.spawn_transform = OpenTK.Mathematics.Matrix4.Identity;
				var wrap = ComponentWrapper.Create(component);
				go = new GameObject(component.component_type.ToString());
				var b = go.AddComponent<PlaylistComponent>();
				b.Initialize(activePlaylistBehaviour, wrap);
				b.transform.position = global_position;
				
				// This will add the component to the correct location and to the active playlist.
				b.EnsureCorrectParentTile(force: true, location_type_is_environment: is_env);
				
				// The id is assigned after it's added to a location.
				component.cargo_zone_import_cargo_type = $"id_{component.id}";

				// Write initial data to component.
				b.OnUnityValuesChanged();

				b.Initialize2();

				return b;
			}

			PlacementMenu.AddButton("MissionZone", MissionZone);
			void MissionZone()
			{
				var p = position;
				
				var comp = new Component(ComponentType.MissionZone);
				comp.spawn_bounds = new Bounds3(-1, -1, -1, 1, 1, 1);
				p.y += comp.spawn_bounds.Size.Y / 2;
				Common(comp, p, is_env: false);
			}

			PlacementMenu.AddButton("EnvironmentZone", EnvZone);
			void EnvZone()
			{
				var p = position;

				var comp = new Component(ComponentType.EnvironmentZone);
				comp.spawn_bounds = new Bounds3(-1, -1, -1, 1, 1, 1);
				p.y += comp.spawn_bounds.Size.Y / 2;
				Common(comp, p, is_env: true);
			}
			
			PlacementMenu.AddButton("Vehicle - Debris", Vehicle_Debris);
			void Vehicle_Debris()
			{
				Vehicle_Common(StormworksPaths.Data.debris);
			}

			PlacementMenu.AddButton("Vehicle - User", Vehicle_User);
			void Vehicle_User()
			{
				Vehicle_Common(StormworksPaths.UserData.vehicles);
			}

			void Vehicle_Common(string base_path)
			{
				FileBrowser.SetFilters(false, ".xml");

				if (!FileBrowser.ShowLoadDialog
				(
					OnFilesSelected
					, OnFileSelectionCanceled
					, FileBrowser.PickMode.Files
					, false
					, base_path
					, null
					, "Select vehicle to load."
				))
				{
					Debug.LogError($"Failed to open the SelectFileDialog.");
				}

				void OnFilesSelected(string[] selection)
				{
					if (null == selection || selection.Length == 0)
					{
						Debug.Log("No file was selected.");
						return;
					}

					if (selection.Length > 1)
					{
						Debug.LogError("Multiple selection is not supported.");
						return;
					}

					var filePath = selection[0];

					var comp = new Component(ComponentType.Vehicle);

					var p = position;

					int id = activePlaylist.NextComponentID;
					comp.id = id;

					// arbitrary, we don't know the vehicle size.
					comp.spawn_bounds = new Bounds3(-0.5f, -0.5f, -0.5f, 0.5f, 0.5f, 0.5f);
					p.y += comp.spawn_bounds.Size.Y / 2;
					string vehicle_save_path = Path.Combine(Path.GetDirectoryName(activePlaylist.meta_filePath), $"vehicle_{id}.xml");
					string vehicle_xml_path = $"data/missions_working/vehicle_{id}.xml";

					File.Copy(filePath, vehicle_save_path, true);

					comp.vehicle_file_name = vehicle_xml_path;
					comp.vehicle_file_store = 4;

					Common(comp, p, is_env: false);

					// todo: set spawn position based on bounds somehow.
				}
			}


			await PlacementMenu.WaitForInteraction();

			if (null == go) return;

			SelectionManager.Instance.ClearAndAddTarget(go.transform);
			SelectionManager.DoNotSelectThisFrame();
		}

		private void OnFileSelectionCanceled()
		{
			Console.WriteLine($"User canceled file selection");
		}

		#endregion Add Object
	}
}
