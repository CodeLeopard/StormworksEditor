﻿// Copyright 2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using CustomSerialization;
using DataModel.Missions;
using Shared;
using Shared.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;
using Component = DataModel.Missions.Component;

namespace Playlists
{
	internal class SpawnableComponentsContext
	{
		public Dictionary<string, Component> spawnableVehicles = new Dictionary<string, Component>();


		private Queue<Playlist> playlistQueue = new Queue<Playlist>();
		private HashSet<string> enqueuedPlaylistSpecifiers = new HashSet<string>();

		internal void CheckForSpawnableComponents(Playlist playlist)
		{
			enqueuedPlaylistSpecifiers.Clear();

			playlistQueue.Enqueue(playlist);

			string specifier = GetPlaylistSpecifier(playlist);
			enqueuedPlaylistSpecifiers.Add(specifier);

			DoQueue();

			enqueuedPlaylistSpecifiers.Clear();
		}

		private void DoQueue()
		{
			while (playlistQueue.TryDequeue(out var playlist))
			{
				// Just to be safe.
				enqueuedPlaylistSpecifiers.Add(GetPlaylistSpecifier(playlist));

				Console.WriteLine($"Inspecting playlist '{playlist.name}' for spawnable vehicles.");
				foreach (var location in playlist.Locations)
				{
					ProcessLocation(location);
				}
			}
		}

		private void ProcessLocation(Location location)
		{
			foreach (var component in location.components)
			{
				CheckForSpanwableVehicleIncludes(component);

				CheckForSpawnableVehicle(component);
			}
		}

		private static readonly Regex spawnableVehicleRegex = new Regex("spawn_([^=]+)=([^,]+)");
		private void CheckForSpawnableVehicle(Component component)
		{
			if (component.is_environment) return;

			var tags = component.name;
			if (tags == null) return;

			var match = spawnableVehicleRegex.Match(tags);

			if (!match.Success) return;

			string category = match.Groups[1].Value;
			string variant = match.Groups[2].Value;

			string combined = $"{category}={variant}";

			spawnableVehicles[combined] = component;
			Console.WriteLine($"Adding Vehicle_in_zone candidate {combined}");
		}

		private static readonly Regex spawnableVehiclePlaylistReferenceRegex = new Regex("_CT_Load_spawnable_components_from_playlist=([^,]+)");
		private void CheckForSpanwableVehicleIncludes(Component component)
		{
			if (! component.is_environment) return;

			var tags = component.cargo_zone_import_cargo_type;
			if (tags == null) return;

			var matches = spawnableVehiclePlaylistReferenceRegex.Matches(tags);

			foreach (Match match in matches)
			{
				string specifier = match.Groups[1].Value;

				if(enqueuedPlaylistSpecifiers.Contains(specifier)) continue;

				var playlist = LoadPlaylistFromSpecifier(specifier);

				if (playlist != null)
				{
					playlistQueue.Enqueue(playlist);
					enqueuedPlaylistSpecifiers.Add(specifier);

					Console.WriteLine($"Enqueued '{specifier}' for scanning.");
				}
			}
		}

		private string GetPlaylistSpecifier(Playlist playlist)
		{
			string folder = Path.GetFileName(Path.GetDirectoryName(playlist.meta_filePath));
			if (playlist.path_id == null)
			{
				// User data
				return $"__user__/{folder}";
			}
			else
			{
				// Built in
				return $"__builtin__/{folder}";
			}
		}

		private static readonly Regex specifierRegex = new Regex("^(__user__|__builtin__)/(.*)$");
		private Playlist LoadPlaylistFromSpecifier(string specifier)
		{
			var match = specifierRegex.Match(specifier);
			if (!match.Success)
			{
				Debug.LogError($"Could not find specifier in '{specifier}'.");
				return null;
			}

			string whereKind = match.Groups[1].Value;
			string remainder = match.Groups[2].Value;

			string playlistFolderPath;
			if(whereKind == "__user__")
			{
				playlistFolderPath = Path.Combine(StormworksPaths.UserData.missions, remainder);
			}
			else if (whereKind == "__builtin__")
			{
				playlistFolderPath = Path.Combine(StormworksPaths.Data.missions, remainder);
			}
			else
			{
				throw new InvalidOperationException("Should never happen");
			}

			string fullPath = $"{playlistFolderPath}/playlist.xml";

			try
			{
				var doc = XMLHelper.LoadFromFile(fullPath);
				var playlist = Serializer.Deserialize<Playlist>(doc, PlaylistManager.graphSpec, PlaylistManager.context);


				((ISerializationEventReceiver)playlist).AfterDeserialize(fullPath);

				return playlist;
			}
			catch(Exception e)
			{
				Debug.LogException(e);
				return null;
			}
		}
	}
}
