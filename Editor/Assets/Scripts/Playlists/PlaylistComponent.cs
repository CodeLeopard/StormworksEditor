﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.IO;
using System.Text.RegularExpressions;
using Behaviours;

using BinaryDataModel.Converters;

using Control;

using DataModel.Missions;

using GUI;

using RuntimeGizmos;

using Tools;

using UnityEngine;
using PComponent = DataModel.Missions.Component;

namespace Playlists
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(MeshFilter))]
	[RequireComponent(typeof(MeshRenderer))]
	[RequireComponent(typeof(SelectionEventReceiver))]
	[RequireComponent(typeof(TransformGizmoEventReceiver))]
	public class PlaylistComponent : M0noBehaviour, IInspectorPanelContentProvider
	{
		private MeshFilter meshFilter;
		private MeshRenderer meshRenderer;
		private MeshCollider meshCollider;
		private BoxCollider boxCollider;

		/// <summary>
		/// Nullifies the effect of the scale from the main object on the vehicle.
		/// </summary>
		private GameObject vehicleScaleCompensator;
		/// <summary>
		/// Container for the vehicle so that a transformation can be applied cleanly.
		/// </summary>
		private GameObject vehicleContainer;
		/// <summary>
		/// The root object of the vehicle, it contains <see cref="vehicleLoader"/>.
		/// </summary>
		private GameObject vehicleLoaderGameObject;
		private VehicleLoader vehicleLoader;

		protected SelectionEventReceiver selectionEvents;
		protected TransformGizmoEventReceiver gizmoEvents;

		public PlaylistBehaviour PlaylistBehaviour;
		public Playlist MyPlaylist => PlaylistBehaviour.activePlaylist;
		public ComponentWrapper MyComponent;

		private string vehicleInZone_match;
		public Vehicle VehicleInZone_Component;

		/// <summary>
		/// Automatically try to put the component in the correct location.
		/// </summary>
		public bool ParentLocationCorrection = true;

		private bool IsRemoved = false;

		#region UnityMessages
		protected override void OnAwake()
		{
			meshFilter = GetRequiredComponent<MeshFilter>();
			meshRenderer = GetRequiredComponent<MeshRenderer>();

			selectionEvents = GetRequiredComponent<SelectionEventReceiver>();
			selectionEvents.AllowCopy = false;
			selectionEvents.AllowDelete = false;

			gizmoEvents = GetRequiredComponent<TransformGizmoEventReceiver>();
			//gizmoEvents.AllowedScaleAxis = Axis.None;
			gizmoEvents.GizmoEndInteract += GizmoEvents_GizmoEndInteract;

			gizmoEvents.GizmoDelete += GizmoEvents_GizmoDelete;
		}

		private void GizmoEvents_GizmoDelete(Transform obj)
		{
			// Only triggers for user initiated removal.
			// todo: if this misfires we could delete vehicle files as a step of saving the playlist.
			MyComponent.Remove();

			IsRemoved = true;

			if (MyComponent.ComponentType == ComponentType.Vehicle && MyComponent.WrappedComponent.vehicle_file_name != null)
			{
				// Cleanup vehicle file.
				string abs_path = MyPlaylist.GetVehiclePath(MyComponent.WrappedComponent.vehicle_file_name);
				if (File.Exists(abs_path))
				{
					PlaylistBehaviour.vehiclesFilesPendingDeletion.Add(abs_path);
				}
			}
		}

		private void OnDestroy()
		{
			if (IsRemoved) return;
			MyComponent.Remove();
		}


		#endregion


		#region Events and handlers


		/// <inheritdoc />
		object IInspectorPanelContentProvider.GetInspectorContent()
		{
			return MyComponent;
		}
		/// <inheritdoc />
		void IInspectorPanelContentProvider.OnInteract()
		{
			OnUnityValuesChanged();
		}



		private void GizmoEvents_GizmoEndInteract(Transform obj)
		{
			OnUnityValuesChanged();
		}

		public void OnUnityValuesChanged()
		{
			EnsureCorrectParentTile();

			// Save data back into Component.
			bool saveScale = true;
			if (MyComponent.componentType == ComponentType.EnvironmentZone
			 || MyComponent.componentType == ComponentType.MissionZone
			 || MyComponent.componentType == ComponentType.Vehicle
			)
			{
				saveScale = false;

				// todo: why is this commented out?
				//MyComponent.WrappedComponent.spawn_bounds.Center = OpenTK.Mathematics.Vector3.Zero;
				
				MyComponent.WrappedComponent.spawn_bounds.Size = transform.localScale.ToOpenTK();
			}

			MyComponent.Transform = transform.ToOpenTK(local: true, scale: saveScale);

			HandleVisual();
			FixVehicleScale();
		}

		#endregion Events and handlers

		public void Initialize(PlaylistBehaviour playlist, ComponentWrapper component)
		{
			PlaylistBehaviour = playlist ?? throw new ArgumentNullException(nameof(playlist));
			MyComponent       = component ?? throw new ArgumentNullException(nameof(component));

			MyComponent.Transform.SetDataOn(transform, true);
		}



		public void EnsureCorrectParentTile(bool force = false, bool? location_type_is_environment = null)
		{
			if(! ParentLocationCorrection && ! force) return;

			PlaylistBehaviour.EnsureCorrectParentLocation(this, location_type_is_environment);
		}

		public void Initialize2()
		{
			HandleVisual();
		}




		private void HandleVisual()
		{
			switch (MyComponent.ComponentType)
			{
				case ComponentType.MissionZone:
				case ComponentType.EnvironmentZone:
				{
					SetZoneVisual();
					SetupVehicleInZonePreview();

					break;
				}
				case ComponentType.Vehicle:
				{
					HandleVehicle();
					break;
				}

				default:
				{
					// Fallback option.
					meshFilter.sharedMesh = Globals.Meshes.Sphere;

					meshCollider = meshCollider ?? gameObject.AddComponent<MeshCollider>();
					meshCollider.sharedMesh = meshFilter.sharedMesh;
					meshRenderer.material = Globals.Materials.PlaylistZone;
					break;
				}
			}
		}

		private void SetZoneVisual()
		{
			meshFilter.sharedMesh = Globals.WireFrames.Cube;
			meshRenderer.material = Globals.Materials.PlaylistZone;

			Vector3 size = MyComponent.WrappedComponent.spawn_bounds.Size.ToUnity();
			transform.localScale = size;

			boxCollider = boxCollider ?? gameObject.AddComponent<BoxCollider>();
			boxCollider.size = Vector3.one;
		}

		private static readonly Regex potentialSpawnPredicateRegex = new Regex("([^,=]+)=([^,=]+)");
		private void SetupVehicleInZonePreview()
		{
			if (!EditorSettingsManager.Settings.EnablePlaylistVehicleInZonePreview) return;

			var tags = MyComponent.WrappedComponent.cargo_zone_import_cargo_type;
			if (tags == null)
			{
				if (vehicleLoader != null)
				{
					vehicleLoader.vehiclePath = null;
				}
				return;
			}

			var matches = potentialSpawnPredicateRegex.Matches(tags);

			bool hasVehicle = false;
			foreach(Match match in matches)
			{
				string category = match.Groups[1].Value;
				string variant  = match.Groups[2].Value;
				string full = $"{category}={variant}";

				if(PlaylistBehaviour.spawnableVehicles.TryGetValue(full, out var component) && component.component_type == ComponentType.Vehicle)
				{
					vehicleInZone_match = full;
					SetupVehicleInZonePreview(component);
					Console.WriteLine($"Loaded vehicle in zone preview. Zone id: {MyComponent.Id}, vehicle component id: {VehicleInZone_Component.Id}, match: {full}");
					hasVehicle = true;
					break;
				}
			}

			if(! hasVehicle && vehicleLoader != null)
			{
				vehicleLoader.vehiclePath = null;
			}
		}

		private void SetupVehicleInZonePreview(PComponent component)
		{
			VehicleInZone_Component = new Vehicle(component);

			HandleVehicle();
		}

		private void SetupVehicleGameObject()
		{
			vehicleScaleCompensator = new GameObject("VehicleScaleCompensator");
			vehicleScaleCompensator.transform.SetParent(transform, false);

			vehicleContainer = new GameObject("VehicleContainer");
			vehicleContainer.transform.SetParent(vehicleScaleCompensator.transform, false);

			string name = VehicleInZone_Component == null ? $"Vehicle: {MyComponent.WrappedComponent.vehicle_file_name}" : $"Vehicle-in-zone-preview: {vehicleInZone_match}";

			vehicleLoaderGameObject = new GameObject(name);
			vehicleLoaderGameObject.transform.SetParent(vehicleContainer.transform, false);
			vehicleLoader = vehicleLoaderGameObject.AddComponent<VehicleLoader>();

			vehicleLoader.Position = VehicleLoader.PositionMode.Bounds;

			vehicleLoader.Loaded += VehicleLoader_Loaded;
		}

		private void VehicleLoader_Loaded(DataModel.Vehicles.Vehicle obj)
		{
			if (MyComponent.ComponentType == ComponentType.Vehicle)
			{

				var old_bounds = MyComponent.WrappedComponent.spawn_bounds;
				var new_bounds = obj.voxelBoundsF;
				if (old_bounds != new_bounds)
				{
					// This should only get triggered when the vehicle was spawned new by the Editor.
					transform.localScale = new_bounds.Size.ToUnity();

					// Move the object by the difference in bottom of bounds, so the bottom ends up at the surface.
					var diff = old_bounds.Size - new_bounds.Size;

					diff.X = 0;
					diff.Z = 0;

					transform.position -= diff.ToUnity() / 2f;

					FixVehicleScale();
				}
			}
		}

		private void FixVehicleScale()
		{
			if (null == vehicleScaleCompensator)
				return;

			Vector3 scale = transform.localScale;

			// Invert the scale factor to end back up at one.
			vehicleScaleCompensator.transform.localScale =
				new Vector3(1f / scale.x, 1f / scale.y, 1f / scale.z);
		}

		private void HandleVehicle()
		{
			if (null == vehicleLoaderGameObject)
			{
				SetupVehicleGameObject();
			}

			var vehicleComponent = (MyComponent as Vehicle) ?? VehicleInZone_Component;
			if (null == vehicleComponent)
			{
				throw new InvalidOperationException($"Invalid Playlist Component Type, {nameof(Vehicle)} required.");
			}

			

			SetZoneVisual();
			meshRenderer.material = vehicleComponent.IsStatic ? Globals.Materials.EditAreaStatic : Globals.Materials.EditAreaDynamic;


			if (VehicleInZone_Component != null)
			{
				VehicleInZone_Component.Transform.SetDataOn(vehicleContainer.transform, true);
			}
			else
			{
				name = $"PC#{vehicleComponent.Id} [{vehicleComponent.ComponentType}] '{Path.GetFileName(vehicleComponent.RelativeFilePath)}'";
			}
			FixVehicleScale();

			vehicleLoader.vehiclePath = vehicleComponent.WrappedComponent.parentPlaylist.GetVehiclePath(vehicleComponent.RelativeFilePath);

			// todo: need to ensure vehicle is fully enclosed in it's bounding box.
			// VehicleLoader just puts it at the origin.

		}
	}
}
