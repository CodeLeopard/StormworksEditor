﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using DataModel.Missions;

using Playlists;

using Shared.Serialization;

using Tiles;
using Tools;
using UnityEngine;
using UnityEngine.Tilemaps;

using PComponent = DataModel.Missions.Component;

namespace Playlists
{
	/// <summary>
	/// Handles a single playlist
	/// </summary>
	public class PlaylistBehaviour : MonoBehaviour
	{
		public PlaylistManager Manager;
		protected TileLoader tileLoader => Manager.tileLoader;

		public HashSet<string> vehiclesFilesPendingDeletion = new HashSet<string>();


		public Playlist activePlaylist;

		public Dictionary<Location, GameObject> Location2GameObject = new Dictionary<Location, GameObject>();
		public Dictionary<GameObject, Location> GameObject2Location = new Dictionary<GameObject, Location>();

		private SpawnableComponentsContext spawnableComponentsContext;

		public Dictionary<string, PComponent> spawnableVehicles => spawnableComponentsContext.spawnableVehicles;

		public void Initialize(PlaylistManager manager, Playlist playlist)
		{
			Console.WriteLine("Start initializing playlist.");
			Manager = manager ?? throw new ArgumentNullException(nameof(manager));
			activePlaylist = playlist ?? throw new ArgumentNullException(nameof(playlist));

			spawnableComponentsContext = new SpawnableComponentsContext();
			spawnableComponentsContext.CheckForSpawnableComponents(activePlaylist);

			foreach (var location in activePlaylist.Locations)
			{
				var tileName = location.tileName;

				var locationGO = GetOrCreateLocationGameObject(location);
				if(null == locationGO)
				{
					continue;}

				foreach (var component in location.components)
				{
					var wrapper = ComponentWrapper.Create(component);

					var go = new GameObject($"PC#{wrapper.Id} [{wrapper.ComponentType}] '{wrapper.DisplayName}'");
					go.transform.SetParent(locationGO.transform, false);
					var behaviour = go.AddComponent<PlaylistComponent>();
					behaviour.Initialize(this, wrapper);
					behaviour.Initialize2();
				}
			}

			Console.WriteLine("Done initializing playlist.");
		}

		private GameObject GetOrCreateLocationGameObject(Location location)
		{
			var tileName = location.tileName;

			if (string.IsNullOrWhiteSpace(tileName))
			{
				Debug.LogWarning($"Playlist '{activePlaylist.name}' contains a location '{location.name}' that does not specify a tile."
							   + $" The location will not be loaded."
							   + $" Saving the playlist will work properly.");
				return null;
			}

			if (!tileLoader.MapTileNameToTile.TryGetValue(tileName, out var tileWrapper))
			{
				Debug.LogWarning
					(
					 $"Playlist '{activePlaylist.name} contains a location '{location.name}' that refers to tile '{location.tileName}' which was not be found."
				   + " The location will not be loaded."
				   + " Saving the playlist will work properly."
					);
				return null;
			}

			if (! Location2GameObject.TryGetValue(location, out GameObject locationGO))
			{
				locationGO = new GameObject(tileName);
				Location2GameObject.Add(location, locationGO);
				GameObject2Location[locationGO] = location;

				locationGO.transform.position = tileWrapper.tileRoot.transform.position;
				locationGO.transform.SetParent(transform, true);

				locationGO.AddComponent<TileLOD>();
			}

			return locationGO;
		}

		public void EnsureCorrectParentLocation(PlaylistComponent b, bool? location_type_is_environment = null)
		{
			var component = b.MyComponent;

			var nearestTile = tileLoader.GetNearestTile(b.transform.position);

			var tileName = nearestTile.tile.tileName;
			var tileRomPath = nearestTile.tile.meta_romPath;

			Location oldLocation = component.WrappedComponent.parentLocation;
			Location location;

			bool is_env = false;
			if (location_type_is_environment!= null)
			{
				is_env = location_type_is_environment.Value;
			}
			else
			{
				is_env = component.IsEnvironment; ;
			}

			if (is_env)
			{
				if (!activePlaylist.MapTileNameToEnvLocation.TryGetValue(tileName, out location))
				{
					location = activePlaylist.CreateLocation(tileRomPath, true);
				}
			}
			else
			{
				if (!activePlaylist.MapTileNameToLocation.TryGetValue(tileName, out location))
				{
					location = activePlaylist.CreateLocation(tileRomPath, false);
				}
			}

			// Put the component in the correct container, in case it was moved.
			component.MoveToLocation(location);

			var newLocationGO = GetOrCreateLocationGameObject(location);

			b.transform.SetParent(newLocationGO.transform, true);

			if(oldLocation != location)
			{
				FixLoDAfterChangeParent(b);
			}	
		}

		private void FixLoDAfterChangeParent(PlaylistComponent b)
		{
			var go = b.gameObject;

			var vehicleLoader = go.GetComponentInChildren<VehicleLoader>();
			if (vehicleLoader != null)
			{
				vehicleLoader.MaybeTileChanged();
			}
		}

		public void ApplyPendingVehicleFilehandling()
		{
			foreach(var path in vehiclesFilesPendingDeletion)
			{
				if (File.Exists(path))
				{
					Console.WriteLine($"Applying pending playlist vehicle deletion: {path}");
					File.Delete(path);
				}
			}

			vehiclesFilesPendingDeletion.Clear();
		}
	}
}
