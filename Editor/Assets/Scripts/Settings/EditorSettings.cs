﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections.Generic;
using System.Xml.Serialization;

using CurveGraph;

using Shared;

using Tiles;

namespace Tools
{
	[Serializable]
	public class EditorSettings : SettingsBase
	{
		[XmlIgnore]
		public override bool ChangedSinceLastSaved
		{
			get => changedSinceLastSaved || TileLoadSettings.ChangedSinceLastSaved | TileExportSettings.ChangedSinceLastSaved | CurveGraphSettings.ChangedSinceLastSaved;
			set
			{
				changedSinceLastSaved = value;
				TileLoadSettings.ChangedSinceLastSaved = value;
				TileExportSettings.ChangedSinceLastSaved = value;
				CurveGraphSettings.ChangedSinceLastSaved = value;
			}
		}

		[XmlIgnore]
		public override bool ChangedSinceLastLoaded
		{
			get => changedSinceLastLoaded || TileLoadSettings.ChangedSinceLastLoaded;
			set
			{
				changedSinceLastLoaded = value;
				TileLoadSettings.ChangedSinceLastLoaded = value;
			}
		}


		public List<StormworksInstallEntry> StormworksInstalls = new List<StormworksInstallEntry>();

		private bool _enablePlaylistVehicleInZonePreview = true;
		[VisibleProperty]
		public bool EnablePlaylistVehicleInZonePreview
		{
			get => _enablePlaylistVehicleInZonePreview;
			set => SetWithMarkChanged(ref _enablePlaylistVehicleInZonePreview, value);
		}

		private int _numberOfLightsToRender = 12;
		[VisibleProperty]
		public int NumberOfLigthsToRender
		{
			get => _numberOfLightsToRender;
			set => SetWithMarkChanged(ref _numberOfLightsToRender, Math.Clamp(value, 1, 256));
		}

		private bool _placeNewObjectsAtCameraFocus = false;
		[VisibleProperty]
		public bool PlaceNewObjectsAtCameraFocus
		{
			get => _placeNewObjectsAtCameraFocus;
			set => SetWithMarkChanged(ref _placeNewObjectsAtCameraFocus, value);
		}

		[VisibleProperty]
		public TileLoadSettings TileLoadSettings = new TileLoadSettings();
		[VisibleProperty]
		public TileExportSettings TileExportSettings = new TileExportSettings();
		[VisibleProperty]
		public CurveGraphSettings CurveGraphSettings = new CurveGraphSettings();


		[Serializable]
		public class StormworksInstallEntry
		{
			public string Name;
			public string Path;

			private StormworksInstallEntry() { }

			public StormworksInstallEntry(string name, string path)
			{
				Name = name;
				Path = path;
			}
		}
	}
}
