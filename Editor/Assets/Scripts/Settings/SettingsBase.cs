// Copyright 2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections.Generic;
using System.Xml.Serialization;

using Shared;

namespace Tiles
{
	/// <summary>
	/// Case for settings classes.
	/// </summary>
	[Serializable]
	public abstract class SettingsBase
	{
		protected bool changedSinceLastLoaded = false;

		protected bool changedSinceLastSaved = false;

		/// <summary>
		/// Ware these settings changed since they ware last used during loading of tiles.
		/// </summary>
		[XmlIgnore]
		[VisibleProperty(isReadonly:true)]
		public virtual bool ChangedSinceLastLoaded
		{
			get => changedSinceLastLoaded;
			set => changedSinceLastLoaded = value;
		}

		/// <summary>
		/// Ware these settings changed since they ware last saved to file.
		/// </summary>
		[XmlIgnore]
		[VisibleProperty(isReadonly: true)]
		public virtual bool ChangedSinceLastSaved
		{
			get => changedSinceLastLoaded;
			set => changedSinceLastLoaded = value;
		}


		/// <summary>
		/// Sets a value and automatically sets the ChangedSinceLast* values.
		/// </summary>
		/// <typeparam name="TData"></typeparam>
		/// <param name="current"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		protected virtual bool SetWithMarkChanged<TData>(ref TData current, TData value)
		{
			if (EqualityComparer<TData>.Default.Equals(current, value)) return false;

			current = value;
			changedSinceLastLoaded = true;
			changedSinceLastSaved = true;

			return true;
		}

		public virtual void OnLoad() { }

		public virtual void Apply() { }
	}
}
