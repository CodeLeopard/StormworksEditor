﻿// Copyright 2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/




using System;
using System.Collections.Generic;
using Tiles;

namespace Assets.Scripts.Settings
{
	[Serializable]
	public class InstanceSettings
	{
		public List<TileCollectionEntry> TileCollections;

		public static IEnumerable<TileCollectionEntry> InitializeList()
		{
			foreach (var c in TileCollection.DefaultCollections(null))
			{
				yield return new TileCollectionEntry(c.name, true);
			}
		}

		public bool IsTileCollectionEnabled(string name)
		{
			foreach(var t in TileCollections)
			{
				if(t.Name == name) return t.Enabled;
			}

			return true;
		}

		internal void Default()
		{
			TileCollections = new List<TileCollectionEntry>(InitializeList());
		}
	}

	public class TileCollectionEntry
	{
		public string Name;
		public bool Enabled;

		private TileCollectionEntry() { }

		internal TileCollectionEntry(string name, bool enabled)
		{
			Name = name;
			Enabled = enabled;
		}
	}
}
