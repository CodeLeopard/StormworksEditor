﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.IO;
using System.Text;
using Assets.Scripts.Settings;
using Behaviours;

using Control;

using GUI;

using Shared.Serialization;

using UnityEngine;

namespace Tools
{
	[DefaultExecutionOrder(ExecutionOrder.SettingsManager)]
	public class EditorSettingsManager : M0noBehaviour, IInspectorPanelContentProvider
	{
		public static EditorSettingsManager Instance { get; private set; }

		/// <summary>
		/// The currently active <see cref="EditorSettings"/>
		/// </summary>
		public static EditorSettings Settings { get; private set; }



		/// <inheritdoc />
		protected override void OnAwake()
		{
			if (null != Instance)
			{
				Debug.LogError($"[{nameof(EditorSettingsManager)}] Multiple instances of singleton {nameof(EditorSettingsManager)} detected!", this);
				enabled = false;
				return;
			}

			Instance = this;

			Initialize();
		}


		private static readonly string settingsRelativePath = "settings.xml";
		private static string settingsPath => Path.Combine(Application.persistentDataPath, settingsRelativePath);

		private static readonly StringBuilder sb = new StringBuilder();

		/// <summary>
		/// Load existing settings, or generate fresh ones.
		/// </summary>
		private void Initialize()
		{
			if (File.Exists(settingsPath))
			{
				LoadSettings();
				return;
			}

			Settings = new EditorSettings();
			SettingsOnLoad();

			// Write the default settings to the file so the user can edit them if they wish.
			try
			{
				SerializationHelper.Pure.SaveToFile(Settings, settingsPath);
			}
			catch (Exception e) // Best-Effort only
			{
				Debug.LogException(e, this);
			}
		}

		/// <summary>
		/// Load the settings from file and apply them.
		/// </summary>
		private void LoadSettings()
		{
			try
			{
				Settings = SerializationHelper.Pure.LoadFromFile<EditorSettings>(settingsPath);

				sb.Clear();
				sb.AppendLine($"[{nameof(EditorSettingsManager)}] Loaded Settings:");
				DebugSerializer.Dump(sb, Settings, includeProperties: true);
				Console.WriteLine(sb);
				sb.Clear();

				SettingsOnLoad();
			}
			catch (Exception e)
			{
				Settings = new EditorSettings();
				SettingsOnLoad();

				throw new Exception("Failed to load settings. Defaults have been loaded instead."
				                  + " Either fix your settings with a text editor, or save them to overwrite with the defaults."
				                  + $" (Settings file is located at: '{settingsPath}')"
				                  , e);
			}
		}

		private void SettingsOnLoad()
		{
			Settings.ChangedSinceLastLoaded  = false;
			Settings.ChangedSinceLastSaved = false;

			Settings.TileLoadSettings.OnLoad();
			Settings.TileExportSettings.OnLoad();

			ApplyStartupSettings();
		}

		private void ApplyStartupSettings()
		{
			ApplyImmediateSettings();
		}

		private void ApplyImmediateSettings()
		{
			QualitySettings.pixelLightCount = Settings.NumberOfLigthsToRender;
		}

		/// <summary>
		/// Save settings back to file.
		/// </summary>
		public void SaveSettings()
		{
			try
			{
				SerializationHelper.Pure.SaveToFile(Settings, settingsPath);
				Settings.ChangedSinceLastSaved = false;

				sb.Clear();
				sb.AppendLine($"[{nameof(EditorSettingsManager)}] Saved settings:");
				DebugSerializer.Dump(sb, Settings, includeProperties: true);
				Console.WriteLine(sb);
				sb.Clear();

				ApplyImmediateSettings();
			}
			catch (Exception e)
			{
				throw new Exception("Error while saving Settings, changes may not be saved.", e);
			}
		}

		/// <summary>
		/// Open the settings in the SidePanel.
		/// </summary>
		public void OpenSettings()
		{
			SelectionManager.Instance.ClearAndAddTarget(transform, false);
		}

		/// <inheritdoc />
		object IInspectorPanelContentProvider.GetInspectorContent()
		{
			return Settings;
		}
	}
}
