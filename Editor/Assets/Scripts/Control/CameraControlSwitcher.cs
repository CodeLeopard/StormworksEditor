﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections.Generic;

using UnityEngine;

using Behaviours;

using DataTypes.Attributes;
using DataTypes.Extensions;

namespace Control
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(Camera))]
	[DefaultExecutionOrder(-1)]
	public class CameraControlSwitcher : M0noBehaviour
	{
		public CameraController DefaultController;

		[SerializeField]
		[InspectorReadonly]
		public CameraController CurrentController;


		public KeyCode CycleControllerKey = KeyCode.O;

		public List<ControlSWRecord> KeyBinds;


		[Serializable]
		public class ControlSWRecord
		{
			public KeyCode Key;
			public CameraController SwitchTo;
		}


		[HideInInspector]
		public bool WindowsIsFocused => Application.isFocused;

		[SerializeField]
		[InspectorReadonly]
		private bool _mouseIsPresent = true;

		public bool MouseIsPresent => _mouseIsPresent;


		private new Camera camera;


		private CameraController[] allControllers;

		#region UnityMessages

		/// <inheritdoc />
		protected override void OnAwake()
		{
			camera = GetRequiredComponent<Camera>();
			allControllers = GetComponents<CameraController>();

			InputManagement.TextInputFocusChanged += OnTextInputFocusChanged;
		}


		/// <inheritdoc />
		protected override void OnEnableAndAfterStart()
		{
			SwitchToController(DefaultController);
		}

		void Update()
		{
			MousePresence();
			HandleSwitchCameraInput();
		}

		#endregion UnityMessages

		private void OnTextInputFocusChanged(bool textFocus)
		{
			CurrentController.SetEnabled(! textFocus);
		}


		private void MousePresence()
		{
			var view = camera.ScreenToViewportPoint(Input.mousePosition);
			bool newpresence = !(view.x < 0 || view.x > 1 || view.y < 0 || view.y > 1);
			if(newpresence != _mouseIsPresent) {
				Console.WriteLine($"[CameraControlSwitcher] Mouse presence changed to: {(newpresence ? "Present" : "Absent")}.");
			}
			_mouseIsPresent = newpresence;
		}


		private void HandleSwitchCameraInput()
		{
			if (!WindowsIsFocused) return;
			//if (! Input.anyKeyUp) return; // :( does not exist.

			if (InputManagement.TextInputFocused) return;

			if (Input.GetKeyUp(CycleControllerKey))
			{
				int current = allControllers.IndexOf(CurrentController);
				current++;
				if (current < 0) current = 0;
				if (! (current < allControllers.Length)) current = 0;

				if (CurrentController == allControllers[current]) return;

				SwitchToController(allControllers[current]);

				return;
			}


			foreach (var bind in KeyBinds)
			{
				if (Input.GetKeyUp(bind.Key))
				{
					if (CurrentController != bind.SwitchTo) SwitchToController(bind.SwitchTo);
					else SwitchToController(DefaultController);

					break;
				}
			}
		}

		public void SwitchToController(CameraController newController)
		{
			foreach (var controller in allControllers)
			{
				controller.SetEnabled(false);
			}

			newController.SetEnabled(true);
			CurrentController = newController;
		}
	}
}
