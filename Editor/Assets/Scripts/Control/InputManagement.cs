﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Linq;
using TMPro;

using Tools;

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Control
{
	/// <summary>
	/// Responsible for managing input, most notably ensuring that typing into an
	/// InputField does not trigger keybinds.
	/// </summary>
	[DefaultExecutionOrder(ExecutionOrder.InputManagement)]
	public class InputManagement : MonoBehaviour
	{
		private EventSystem system;

		private static bool textInputFocused = false;

		/// <summary>
		/// True if any method of entering Text is currently selected.
		/// </summary>
		public static bool TextInputFocused
		{
			get => textInputFocused;
			private set
			{
				if (value == textInputFocused)
					return;

				textInputFocused = value;
				TextInputFocusChanged?.Invoke(value);
			}
		}

		public static bool AnyModifierKeyPressed { get; private set; }

		public static bool AnyNeverRespondModifierKeyPressed { get; private set; }

		/// <summary>
		/// Fires when <see cref="TextInputFocused"/> has changed.
		/// </summary>
		public static event Action<bool> TextInputFocusChanged;


		private static readonly KeyCode[] neverRespondKeyCodes = new[]
		{
			KeyCode.LeftCommand, KeyCode.RightCommand,
			KeyCode.LeftWindows, KeyCode.RightWindows,
			KeyCode.LeftApple,   KeyCode.RightApple,
		};

		private static readonly KeyCode[] anyModifierKeyCodes = neverRespondKeyCodes.Concat(new[]
		{
			KeyCode.LeftAlt,     KeyCode.RightAlt,
			KeyCode.LeftShift,   KeyCode.RightShift,
			KeyCode.LeftControl, KeyCode.RightControl,
		}).ToArray();

		#region UnityMessages

		private void Update()
		{
			PollKeyboard();
			PollFocus();
		}
		#endregion UnityMessages

		private void PollKeyboard()
		{
			AnyNeverRespondModifierKeyPressed = false;

			foreach(var code in neverRespondKeyCodes)
			{
				if (Input.GetKey(code))
				{
					AnyNeverRespondModifierKeyPressed = true;
					break;
				}
			}

			if (AnyNeverRespondModifierKeyPressed)
			{
				AnyModifierKeyPressed = true;
				return;
			}


			AnyModifierKeyPressed = false;

			foreach (var code in anyModifierKeyCodes)
			{
				if (Input.GetKey(code))
				{
					AnyModifierKeyPressed = true;
					break;
				}
			}
		}

		private void PollFocus()
		{
			if (system == null)
			{
				system = EventSystem.current;
				if (system == null)
				{
					TextInputFocused = false;
					return;
				}
			}

			GameObject currentObject = system.currentSelectedGameObject;
			if (null == currentObject)
			{
				TextInputFocused = false;
				return;
			}

			InputField inputField = currentObject.GetComponent<InputField>();
			if (inputField != null)
			{
				TextInputFocused = inputField.isFocused;
				return;
			}

			TMP_InputField tmpInput = currentObject.GetComponent<TMP_InputField>();
			if (tmpInput != null)
			{
				TextInputFocused = tmpInput.isFocused;
				return;
			}
		}
	}
}
