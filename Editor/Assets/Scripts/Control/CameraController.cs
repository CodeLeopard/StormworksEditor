﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using UnityEngine;

using Behaviours;
using System;

namespace Control
{
	/// <summary>
	/// Base class for Camera controllers.
	/// Contains shared functionality and provides controller switching.
	/// </summary>
	[RequireComponent(typeof(Camera))]
	[RequireComponent(typeof(CameraControlSwitcher))]
	public class CameraController : M0noBehaviour
	{
		protected bool IsFocused => switcher.WindowsIsFocused;

		protected bool MousePresent => switcher.MouseIsPresent;

		public const int MousePrimary_Left = 0;
		public const int MouseSecondary_Right = 1;
		public const int MouseMiddle_ScrollWheel = 2;

		[SerializeField]
		[HideInInspector]
		protected CameraControlSwitcher switcher;

		protected new Camera camera { get; private set; }

		protected override void OnAwake()
		{
			camera = GetRequiredComponent<Camera>();
			switcher = GetRequiredComponent<CameraControlSwitcher>();
		}

		/// <summary>
		/// Set the <see cref="enabled"/> property, and call the appropriate <see cref="OnEnableAndAfterStart()"/> or <see cref="OnDisableIfStarted"/>
		/// </summary>
		/// <param name="isNowEnabled"></param>
		public void SetEnabled(bool isNowEnabled)
		{
			if (enabled == isNowEnabled) return;

			enabled = isNowEnabled;

			if (enabled)
			{
				Console.WriteLine($"Camera controller enabled: {this.GetType().Name}");

				// Now enabled
				OnEnableAndAfterStart();
			}
			else
			{
				// Now disabled
				OnDisableIfStarted();
			}
		}

	}
}
