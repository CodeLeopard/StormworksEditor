﻿// Copyright 2023-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using UnityEngine;

using Behaviours;

namespace Control
{
	[RequireComponent(typeof(Camera))]
	public class CameraFeatureControls : M0noBehaviour
	{
		private Camera camera;

		private Light light;

		[HideInInspector]
		public bool WindowsIsFocused => Application.isFocused;

		protected override void OnAwake()
		{
			camera = GetRequiredComponent<Camera>();
			light = GetComponentInChildren<Light>();
		}

		public void Update()
		{
			if (!WindowsIsFocused) return;
			if (null == light) return;
			if (InputManagement.AnyModifierKeyPressed) return;
			if (InputManagement.TextInputFocused) return;

			if (Input.GetKeyDown(KeyCode.L))
			{
				light.enabled = !light.enabled;
			}

			// todo: camera FoV
		}
	}
}
