﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System.Collections;

using UnityEngine;

using Behaviours;

using DataTypes.Attributes;
using DataTypes.Extensions;

using GUI;
using GUI.World;

using UnityEditor;

namespace Control {
	[DisallowMultipleComponent]
	[RequireComponent(typeof(Camera))]
	public class OrbitCamera : CameraController
	{
		public GameObject focusMarkerPrefab;

		[SerializeField]
		[HideInInspector]
		private GameObject focusMarker;

		[SerializeField]
		[InspectorReadonly]
		private bool HasTarget = false;

		[SerializeField]
		private bool AutoRotate = true;

		[Range(0.0001f, 1000)]
		public float distanceTarget = 10;

		[Range(0.0001f, 10f)]
		public float chaseSpeed = 2f;

		[Range(0.0001f, 10f)]
		public float rotateSpeed = 1.25f;

		[Range(0f, 90f)]
		public float verticalViewAngle = 45f;


		[Range(0.01f, 1_000)]
		public float RotationSpeed_Mouse = 4f;


		[Range(0.01f, 100)]
		public float DistanceScrollMultiplier = 4f;


		[Range(0.01f, 20)]
		public float MinimumSpeed = 0.1f;

		[Range(0.01f, 1f)]
		public float ContinuousMovementDelaySeconds = 0.1f;

		[InspectorReadonly]
		public float Speed;

		[InspectorReadonly]
		public float Speed2;

		[InspectorReadonly]
		public bool FinalMovementMode;

		[InspectorReadonly]
		public bool MovementLocked;


		public bool MouseHorizontalInvert = false;
		public bool MouseVerticalInvert = false;

		[HideInInspector]
		public LayerMask rayCastMask;


		public static Vector3 ActiveFocusPosition
		{
			get;
			private set;
		}


		protected override void OnAwake()
		{
			base.OnAwake();

			HasTarget = false; // Inspector can cause it to be true by copy-paste of values.

			if (!focusMarker) focusMarker = Instantiate(focusMarkerPrefab);
			focusMarker.name = $"{name}:FocusMarker";
			focusMarker.hideFlags |= HideFlags.DontSaveInEditor | HideFlags.DontSaveInBuild;
			focusMarker.GetComponentInChildren<FocusMarkerText>().cameraTr = this.transform;

			rayCastMask = LayerMaskExt.NameToMask("Terrain") | LayerMaskExt.NameToMask("Tracks");
		}

		protected override void OnEnableAndAfterStart()
		{
			focusMarker.SetActive(HasTarget);
		}

		protected override void OnDisableIfStarted()
		{
			focusMarker.SetActive(false);
		}

		// Update is called once per frame
		void Update()
		{
			Update_Input();
		}

		void LateUpdate()
		{
			Update_Movement();
		}


		private void Update_Movement()
		{
			if (! HasTarget) return;

			var deltaTime = Time.deltaTime; // todo: add setting to used realWorld delta time instead, to be unaffected by pause etc.

			// Rotation #######################################################

			Vector3 actualFocusPoint = transform.position + (transform.forward * distanceTarget);

			Vector3 cameraToTarget = transform.position - focusMarker.transform.position;

			if (AutoRotate)
			{
				var targetRotation = Quaternion.LookRotation(-cameraToTarget, Vector3.up);
				{
					var temp = targetRotation.eulerAngles;
					temp.x = verticalViewAngle;
					targetRotation.eulerAngles = temp;
				}

				transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotateSpeed * deltaTime);
			}

			// re-evaluate because rotation has changed
			Vector3 deltaToTarget = actualFocusPoint - focusMarker.transform.position;

			// Movement #######################################################
			// todo: The final few units take too long to travel, making it inconvenient to interact with things, because the camera is moving.

			if (deltaToTarget.sqrMagnitude < 0.0005f)
			{
				transform.position -= deltaToTarget;
				MovementLocked = true;
				return;
			}
			else
			{
				MovementLocked = false;
			}

			// Move chaseSpeed fraction of the distance to to the marker each second.
			// Camera will not while paused.
			Vector3 move = deltaToTarget * chaseSpeed;
			Speed = move.magnitude;
			if (move.magnitude < MinimumSpeed)
			{
				FinalMovementMode = true;
				move.Normalize();
				move *= Mathf.Min(MinimumSpeed, deltaToTarget.magnitude / deltaTime);
			}
			else
			{
				FinalMovementMode = false;
			}


			move *= deltaTime;

			Speed2 = move.magnitude;

			transform.position -= move;
		}

		private void Update_Input()
		{
			if (!IsFocused) return;

			if (Input.GetMouseButtonDown(MouseSecondary_Right))
			{
				StartCoroutine(MouseDrag());
			}

			if (Input.GetKeyUp(KeyCode.F)
			 && ! InputManagement.TextInputFocused
			 && ! InputManagement.AnyModifierKeyPressed
			 && SelectionManager.Instance.FirstTarget != null)
			{
				SetFocusPosition(SelectionManager.Instance.FirstTarget.position);
			}

			if (Input.GetKeyUp(KeyCode.F)
				&& ! InputManagement.TextInputFocused
				&& (Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt))
				&& ! (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl) || Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
			) {
				AutoRotate = !AutoRotate;
			}

			Update_Input_Rotation();
			Update_Input_MouseWheel();
		}

		private void MouseRaycast()
		{
			var ray = camera.ScreenPointToRay(Input.mousePosition);

			if (Physics.Raycast(ray, out var hitInfo, float.MaxValue, rayCastMask))
			{
				OnRayHit(ray, hitInfo);
			}
		}

		private IEnumerator MouseDrag()
		{
			MouseRaycast();
			yield return new WaitForSeconds(ContinuousMovementDelaySeconds);

			while (IsFocused && Input.GetMouseButton(MouseSecondary_Right))
			{
				MouseRaycast();
				yield return null;
			}
		}

		private void Update_Input_Rotation()
		{
			if (! Input.GetMouseButton(MouseMiddle_ScrollWheel)) return;

			// Rotate around the current focus point:
			// Store the current location
			var currentFocusPoint = transform.position + distanceTarget * transform.forward;


			var mouseDelta = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

			float invert = MouseHorizontalInvert ? 1 : -1;

			transform.Rotate
			(
				Vector3.up
				, mouseDelta.x / Screen.width * camera.fieldOfView * RotationSpeed_Mouse * invert
				, Space.World
			);


			invert = MouseVerticalInvert ? 1 : -1;
			transform.Rotate
			(
				Vector3.right
				, mouseDelta.y / Screen.height * camera.fieldOfView * RotationSpeed_Mouse * invert
				, Space.Self
			);

			verticalViewAngle = transform.rotation.eulerAngles.x;


			// Move the camera so that it remains focused on the focus point after rotation.
			var newFocusPoint = transform.position + distanceTarget * transform.forward;

			var focusDelta = currentFocusPoint - newFocusPoint;

			transform.position += focusDelta;
		}

		private void Update_Input_MouseWheel()
		{
			if (! IsFocused) return;
			if (! MousePresent) return;
			if (! MouseEvents.MouseOverWorld) return;
			Vector2 mouse_wheel_delta = Input.mouseScrollDelta;
			float primaryWheel_delta = mouse_wheel_delta.y;
			if (primaryWheel_delta == 0) return;

			if (primaryWheel_delta < 0)
				distanceTarget *= DistanceScrollMultiplier;
			else
				distanceTarget /= DistanceScrollMultiplier;

			distanceTarget = Mathf.Clamp(distanceTarget, 0.001f, 100_000f);
		}

		public void SetFocusPosition(Vector3 worldPosition)
		{
			focusMarker.transform.position = worldPosition;

			HasTarget = true;
			focusMarker.SetActive(true);

			ActiveFocusPosition = worldPosition;
		}

		private void OnRayHit(Ray ray, RaycastHit hitInfo)
		{
			SetFocusPosition(hitInfo.point);
		}
	}
}
