﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using UnityEngine;

namespace Control
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(Camera))]
	public class FlyCamera : CameraController
	{
		/// <summary>
		/// Lock the Up vector for rotations of the camera to Scene.Up
		/// </summary>
		public bool LockUpVector = true;

		/// <summary>
		/// Invert mouse vertically
		/// </summary>
		public bool MouseVerticalInvert = false;

		/// <summary>
		/// The default (and reset) move speed of the camera
		/// </summary>
		[Range(0, 100_000)]
		public float DefaultMoveSpeed = 10f; // Units (meters) / s

		[SerializeField]
		private float CurrentMoveSpeed = 10f; // Units (meters) / s

		/// <summary>
		/// Flat Multiplier to speed when associated key (default shift) is pressed
		/// </summary>
		[Range(1, 100_000)]
		public float MoveFastMultiplier = 2f;

		/// <summary>
		/// The change in move speed per scroll increment of the mouse wheel, as a multiplier
		/// </summary>
		[Range(0, 100)]
		public float ScrollWheelSpeedMultiplier = 1.2f;

		/// <summary>
		/// Rotation speed with keyboard, in degrees / second
		/// </summary>
		[Range(0.01f, 1_000)]
		public float RotationSpeedKeyboard = 30f; // Degrees / sec

		/// <summary>
		/// Mouse sensitivity
		/// </summary>
		[Range(0.01f, 1_000)]
		public float RotationSpeedMouse = 4f; // Unit: no idea lol.

		/// <summary>
		/// Zoom sensitivity
		/// </summary>
		[Range(0.01f, 1_000)]
		public float ZoomSpeed = 22f;

		/// <summary>
		/// Allow rotating in arbitrary 3D orientation
		/// </summary>
		public bool Do3DRotation = true;




		void Update()
		{
			HeldDownKeys();
			PressedKeys();
			MouseMove();
			MouseWheel();
		}


		protected override void OnEnableAndAfterStart()
		{
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
		}

		private void OnDisable()
		{
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = true;
		}

		#region Keyboard

		private void HeldDownKeys()
		{
			if (! IsFocused) return;
			if (! MousePresent) return;

			// scaleCompensation (not used atm)
			float sc = 1;


			var moveSpeed = this.CurrentMoveSpeed * sc;
			if (Input.GetKey(KeyCode.LeftShift)) moveSpeed *= MoveFastMultiplier;


			// Forward
			Vector3 translation = new Vector3();
			if (Input.GetKey(KeyCode.W))
			{
				translation += transform.forward * moveSpeed;
			}
			else if (Input.GetKey(KeyCode.S))
			{
				translation -= transform.forward * moveSpeed;
			}

			// Sideways
			if (Input.GetKey(KeyCode.D))
			{
				translation += transform.right * moveSpeed;
			}
			else if (Input.GetKey(KeyCode.A))
			{
				translation -= transform.right * moveSpeed;
			}

			// Vertical
			if (Input.GetKey(KeyCode.E))
			{
				translation += transform.up * moveSpeed;
			}
			else if (Input.GetKey(KeyCode.Q))
			{
				translation -= transform.up * moveSpeed;
			}

			// Zoom
			if (Input.GetKey(KeyCode.KeypadPlus))
			{
				camera.fieldOfView = Mathf.Clamp(camera.fieldOfView - ZoomSpeed * Time.deltaTime, 10f, 170f);
			}
			else if (Input.GetKey(KeyCode.KeypadMinus))
			{
				camera.fieldOfView = Mathf.Clamp(camera.fieldOfView + ZoomSpeed * Time.deltaTime, 10f, 170f);
			}

			transform.localPosition += translation * Time.deltaTime;



			if (Do3DRotation)
			{
				// Up/Down
				if (Input.GetKey(KeyCode.UpArrow))
				{
					transform.Rotate(Vector3.right, RotationSpeedKeyboard * Time.deltaTime, Space.Self);
				}
				else if (Input.GetKey(KeyCode.DownArrow))
				{
					transform.Rotate(Vector3.right, -RotationSpeedKeyboard * Time.deltaTime, Space.Self);
				}

				// Left/Right
				var space = LockUpVector ? Space.World : Space.Self;
				if (Input.GetKey(KeyCode.LeftArrow))
				{
					transform.Rotate(Vector3.up, -RotationSpeedKeyboard * Time.deltaTime, space);
				}
				else if (Input.GetKey(KeyCode.RightArrow))
				{
					transform.Rotate(Vector3.up, RotationSpeedKeyboard * Time.deltaTime, space);
				}
			}

			// Tilt (Around Forward Vector)
			if (Input.GetKey(KeyCode.Z))
			{ // AntiClock
				transform.Rotate(Vector3.forward, RotationSpeedKeyboard * Time.deltaTime, Space.Self);
			}
			else if (Input.GetKey(KeyCode.X))
			{ // Clock
				transform.Rotate(Vector3.forward, -RotationSpeedKeyboard * Time.deltaTime, Space.Self);
			}
		}


		private void PressedKeys()
		{
			if (! IsFocused) return;

			if (Input.GetKey(KeyCode.LeftWindows) || Input.GetKey(KeyCode.RightWindows))
				return; // So we do not respond to common shortcuts, including [Windows] + [Escape] (To close the magnifier)



			if (Input.GetKeyUp(KeyCode.I))
			{
				MouseVerticalInvert = ! MouseVerticalInvert;
			}


			if (Input.GetKeyUp(KeyCode.U))
			{
				LockUpVector = !LockUpVector;
			}

			if (Input.GetKeyUp(KeyCode.P))
			{
				Do3DRotation = !Do3DRotation;
				if (!Do3DRotation)
				{ // Turn camera back into 2.5D perspective
					var pos = transform.position;
					var forward = transform.up;
					transform.LookAt(pos + Vector3.down, forward);
					Debug.Log("Locked to 2.5D perspective.");
				}
				else
					Debug.Log("Allowing 3D rotation.");
			}

			if (Input.GetKeyUp(KeyCode.R))
			{
				CurrentMoveSpeed = DefaultMoveSpeed;
			}
		}

		#endregion



		#region Mouse


		private void MouseMove()
		{
			if (! IsFocused) return;

			var space = LockUpVector ? Space.World : Space.Self;


			var mouseDelta = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

			// Rotation around Up vector (turn left/right)  
			transform.Rotate
				(Vector3.up, mouseDelta.x / Screen.width * camera.fieldOfView * RotationSpeedMouse, space);

			float mouseInvert = MouseVerticalInvert ? 1 : -1;
			// Rotation around Right vector, (turn up/down)
			// todo: correct for aspect ratio?
			transform.Rotate
			(
				Vector3.right
				, mouseDelta.y / Screen.height * camera.fieldOfView * RotationSpeedMouse * mouseInvert
				, Space.Self
			);
		}

		private void MouseWheel()
		{
			if (! IsFocused) return;
			if (! MousePresent) return;

			Vector2 mouse_wheel_delta = Input.mouseScrollDelta;
			float primaryWheel_delta = mouse_wheel_delta.y;
			if (primaryWheel_delta == 0) return;

			if (primaryWheel_delta > 0)
				CurrentMoveSpeed *= ScrollWheelSpeedMultiplier;
			else
				CurrentMoveSpeed /= ScrollWheelSpeedMultiplier;

			CurrentMoveSpeed = Mathf.Clamp(CurrentMoveSpeed, 0.001f, 10_000f);
		}


		#endregion
	}
}
