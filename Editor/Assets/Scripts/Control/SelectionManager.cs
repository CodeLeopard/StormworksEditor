﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using UnityEngine;

using RuntimeGizmos;

using OutlineFx;

using Behaviours;

using GUI;
using GUI.SidePanel;

using Shared;

using VectorExt = DataTypes.Extensions.VectorExt;

namespace Control
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(Camera))]
	[RequireComponent(typeof(TransformGizmo))]
	public class SelectionManager : M0noBehaviour
	{

		public KeyCode AddSelection => transformGizmo.AddSelection;
		public KeyCode RemoveSelection => transformGizmo.RemoveSelection;

		public LayerMask selectionMask => transformGizmo.selectionMask;


		[SerializeField]
		private GameObject selectedItemPanel;

		public static GameObject SelectedItemPanel => Instance.selectedItemPanel;


		private Camera myCamera;
		private OutlineRenderer selectionOutlineRenderer;


		private TransformGizmo transformGizmo;


		#region Target Management
		private Transform firstTarget => targetRootsOrdered.Count > 0 ? targetRootsOrdered[0] : null;
		public Transform FirstTarget => firstTarget;

		/// <summary>
		/// Targets, but not their children.
		/// </summary>
		private HashSet<Transform> targetRoots = new HashSet<Transform>();

		/// <summary>
		/// Targets, in order, but not their children.
		/// </summary>
		private List<Transform> targetRootsOrdered = new List<Transform>();

		/// <summary>
		/// All (recursive) children of the <see cref="targetRoots"/>.
		/// </summary>
		private HashSet<Transform> children = new HashSet<Transform>();

		/// <summary>
		/// Buffer for when a temporary list is needed, this re-use prevents allocations.
		/// </summary>
		private List<Transform> transformBuffer = new List<Transform>();


		/// <summary>
		/// Current selection, but not children.
		/// </summary>
		public IReadOnlyCollection<Transform> Selection { get; private set; }

		#endregion Target Management



		public TransformPivot Pivot => transformGizmo.pivot;

		public Vector3 PivotPoint => transformGizmo.pivotPoint;

		private readonly HashSet<Func<Transform, bool>> selectionAllowDeciders = new HashSet<Func<Transform, bool>>();

		/// <summary>
		/// Event that fires when the current Selection has changed to Nothing.
		/// </summary>
		[field: NonSerialized]
		public event Action SelectionChanged_None;

		/// <summary>
		/// Event that fires when the current selection has changed to a single object.
		/// </summary>
		[field: NonSerialized]
		public event Action<Transform> SelectionChanged_Single;

		/// <summary>
		/// Event that fires when the current selection has changed and multiple objects are selected.
		/// </summary>
		[field: NonSerialized]
		public event Action<IReadOnlyCollection<Transform>> SelectionChanged_Multiple;


		/// <summary>
		/// Event that fires when the current selection changes in any way.
		/// </summary>
		[field: NonSerialized]
		public event Action SelectionChanged;


		public static SelectionManager Instance { get; private set; }


		private static bool doNotSelectThisFrame = false;


		protected override void OnAwake()
		{
			Selection = new ReadOnlyCollection<Transform>(targetRootsOrdered);

			if (null == Instance)
			{
				Instance = this;


				// Needs to start enabled so Awake is called.
				// But should not be enabled while empty.
				SetActiveNextAfterFrame(SelectedItemPanel, false);
			}

			myCamera = GetRequiredComponent<Camera>();
			selectionOutlineRenderer = GetComponent<OutlineRenderer>();
			transformGizmo = this.GetRequiredComponent<TransformGizmo>();
		}

		/// <inheritdoc />
		protected override void OnEnableAndAfterStart()
		{
			if (transformGizmo.CustomGetTarget != null)
				throw new Exception("SelectionManager needs Exclusive control over target handling");
			transformGizmo.CustomGetTarget = GetTarget;
		}

		/// <inheritdoc />
		protected override void OnDisableIfStarted()
		{
			transformGizmo.CustomGetTarget = null;
		}

		/// <summary>
		/// Get a target from user input.
		/// </summary>
		private void GetTarget()
		{
			if (doNotSelectThisFrame)
			{
				doNotSelectThisFrame = false;
				return;
			}
			// Note: this is invoked by the Transform Gizmo because that's how the refactor ended up working.
			// But idk if that is actually needed for it to work like that (timing wise).

			if (! transformGizmo.mouseIsOverGizmo
			 && MouseEvents.MouseDown && MouseEvents.MouseLeft)
			{
				bool isAdding = Input.GetKey(AddSelection);
				bool isRemoving = Input.GetKey(RemoveSelection);

				if (Physics.Raycast(myCamera.ScreenPointToRay(Input.mousePosition),
				                    out RaycastHit hitInfo,
				                    Mathf.Infinity,
				                    selectionMask))
				{
					Transform target = hitInfo.transform;

					if (! SelectionAllowedOrSubstitute(ref target, out bool _))
					{
						OnRayMiss();
						return;
					}

					if (isAdding)
					{
						AddTarget(target);
					}
					else if (isRemoving)
					{
						RemoveTarget(target);
					}
					else if (!isAdding && !isRemoving)
					{
						ClearAndAddTarget(target);
					}
				}
				else
				{
					OnRayMiss();
				}

				void OnRayMiss()
				{
					if (!isAdding && !isRemoving)
					{
						ClearTargets();
					}
				}
			}
		}


		#region Target Management


		#region Public Interface
		// todo: decide what exactly Add/Remove/etc-Target should return:
		// true only when something was actually changed (the target wasn't already selected)?
		// false only when the target did not end up in the selection (which really only is when it's null or a child)
		// Or return an enum instead (even a bool is at least a byte anyway)


		private void ThrowIfNullOrDestroyed(Transform target)
		{
			if (null == target)
			{
				// Expression is not always true due to Unity logic:
				// Destroyed == null
				if (ReferenceEquals(null, target))
				{
					throw new ArgumentNullException(nameof(target));
				}
				else
				{
					throw new ArgumentNullException(nameof(target), "The GameObject is Destroyed.");
				}
			}
		}


		/// <summary>
		/// Add a target.
		/// </summary>
		/// <param name="target"></param>
		/// <param name="makeUndoEntry"></param>
		/// <returns>The target was added.</returns>
		public bool AddTarget(Transform target, bool makeUndoEntry = true)
		{
			ThrowIfNullOrDestroyed(target);

			if (targetRoots.Contains(target)) return false;
			if (children.Contains(target)) return false;

			if (makeUndoEntry)
			{
				//UndoRedoManager.Insert(new AddTargetCommand(this, target, targetRootsOrdered));
			}

			AddTargetRoot(target);

			MaybeFireSelectionChanged();

			return true;
		}

		/// <summary>
		/// Remove a target.
		/// </summary>
		/// <param name="target"></param>
		/// <param name="makeUndoEntry"></param>
		/// <param name="fireSelectionChanged"></param>
		/// <returns>The target was removed.</returns>
		public bool RemoveTarget(Transform target, bool makeUndoEntry = true, bool fireSelectionChanged = true)
		{
			// We don't use ThrowIfNullOrDestroyed(target);
			// Because a Destroyed object should be removable.
			if (ReferenceEquals(null, target)) throw new ArgumentNullException(nameof(target));

			if (!targetRoots.Contains(target)) return false;

			if (makeUndoEntry)
			{
				//UndoRedoManager.Insert(new RemoveTargetCommand(this, target));}
			}

			RemoveTargetRoot(target);

			if(fireSelectionChanged)
				MaybeFireSelectionChanged();

			return true;
		}

		/// <summary>
		/// Remove all targets
		/// </summary>
		/// <param name="makeUndoEntry"></param>
		public void ClearTargets(bool makeUndoEntry = true)
		{
			ClearTargetsInternal(makeUndoEntry);

			MaybeFireSelectionChanged();
		}

		/// <summary>
		/// Remove all targets and select a new one.
		/// </summary>
		/// <param name="target"></param>
		/// <param name="makeUndoEntry"></param>
		/// <returns>The target was selected.</returns>
		public bool ClearAndAddTarget(Transform target, bool makeUndoEntry = true)
		{
			ThrowIfNullOrDestroyed(target);

			if (firstTarget == target && targetRoots.Count == 1)
				return false; // todo: decide what the return value actually means.

			if (makeUndoEntry)
			{
				//UndoRedoManager.Insert(new ClearAndAddTargetCommand(this, target, targetRootsOrdered));
			}

			ClearTargetsInternal(false);
			bool result = AddTarget(target, false);

			if (! result)
			{
				// AddTarget will have called MaybeFireSelectionChanged for the true case.
				MaybeFireSelectionChanged();
			}

			return result;
		}

		#endregion Public Interface

		#region Implementation

		private void ClearTargetsInternal(bool makeUndoEntry = true)
		{
			if (makeUndoEntry)
			{
				//UndoRedoManager.Insert(new ClearTargetsCommand(this, targetRootsOrdered));
			}

			transformBuffer.Clear();
			foreach (Transform t in targetRootsOrdered)
			{
				transformBuffer.Add(t);
			}
			targetRoots.Clear();
			targetRootsOrdered.Clear();
			children.Clear();

			foreach (Transform t in transformBuffer)
			{
				t.GetComponent<SelectionEventReceiver>()?.FireDeselect();
			}

			selectionOutlineRenderer?.ClearAllOutlinedObjects();

			transformGizmo.ClearTargets(false);
		}


		private void AddTargetRoot(Transform targetRoot)
		{
			targetRoots.Add(targetRoot);
			targetRootsOrdered.Add(targetRoot);

			AddAllChildren(targetRoot);

			targetRoot.GetComponent<SelectionEventReceiver>()?.FireSelect();

			selectionOutlineRenderer?.AddOutlinedObject(targetRoot);

			transformGizmo.AddTarget(targetRoot, false);
		}

		private void RemoveTargetRoot(Transform targetRoot)
		{
			if (targetRoots.Remove(targetRoot))
			{
				targetRootsOrdered.Remove(targetRoot);

				RemoveAllChildren(targetRoot);

				targetRoot.GetComponent<SelectionEventReceiver>()?.FireDeselect();

				selectionOutlineRenderer?.RemoveOutlinedObject(targetRoot);

				transformGizmo.RemoveTarget(targetRoot, false);
			}
		}

		#endregion Implementation

		#region Child management

		private void AddAllChildren(Transform target)
		{
			transformBuffer.Clear();
			target.GetComponentsInChildren<Transform>(true, transformBuffer);
			transformBuffer.Remove(target);

			for (int i = 0; i < transformBuffer.Count; i++)
			{
				Transform child = transformBuffer[i];
				children.Add(child);
				RemoveTargetRoot(child); // We do this in case we selected child first and then the parent.
			}

			transformBuffer.Clear();
		}

		private void RemoveAllChildren(Transform target)
		{
			transformBuffer.Clear();
			target.GetComponentsInChildren<Transform>(true, transformBuffer);
			transformBuffer.Remove(target);

			for (int i = 0; i < transformBuffer.Count; i++)
			{
				children.Remove(transformBuffer[i]);
			}

			transformBuffer.Clear();
		}

		#endregion Child management


		#region Events


		private void MaybeFireSelectionChanged()
		{
			// todo: check that this doesn't fire duplicate events.

			if (targetRoots.Count == 0)
			{
				OnSelectNothing();
			}
			else if (targetRoots.Count == 1)
			{
				OnSelectSingle();
			}
			else if (targetRoots.Count > 1)
			{
				OnSelectMultiple();
			}
		}

		private void OnSelectNothing()
		{
			SelectionChanged_None?.Invoke();
			SelectionChanged?.Invoke();

			SidePanelManager.Instance.ClearTarget();
		}

		private void OnSelectSingle()
		{
			SelectionChanged_Single?.Invoke(firstTarget);
			SelectionChanged?.Invoke();

			SidePanelManager.Instance.SetTarget(firstTarget);
		}

		private void OnSelectMultiple()
		{
			// todo: support multiple targets.
			SidePanelManager.Instance.ClearTarget();

			// Multiple target, not supported completely.
			SelectionChanged_Multiple?.Invoke(Selection);
			SelectionChanged?.Invoke();
		}

		#endregion Events

		#endregion Target Management

		private bool SelectionAllowedOrSubstitute(ref Transform target, out bool wasSubstituted)
		{
			var originalTarget = target;

			wasSubstituted = false;

			// Unity Logic: InParent actually means "in this object or ANY object in the parent hierarchy."
			var ser = target.parent?.GetComponentInParent<SelectionEventReceiver>();
			if (ser != null)
			{
				bool selectable = ser.Selectable && (null == ser.SelectionAllowed || ser.SelectionAllowed(target));

				if (ser.gameObject.transform == target)
				{
					return selectable;
				}
				else if(ser.SelectThisFromChildRayHit)
				{
					target = ser.gameObject.transform;
					wasSubstituted = true;
					return selectable;
				}
			}

			return SelectionAllowed(target);
		}

		private bool SelectionAllowed(Transform arg)
		{
			var ser = arg.GetComponent<SelectionEventReceiver>();
			if (ser != null)
			{
				return ser.Selectable && (null == ser.SelectionAllowed || ser.SelectionAllowed(arg));
			}

			var ter = arg.GetComponent<TransformGizmoEventReceiver>();
			if (null != ter) return true;

			var icp = arg.GetComponent<IInspectorPanelContentProvider>(); // todo: does this actually work?
			if (null != icp) return true;

			foreach (var func in selectionAllowDeciders)
				if (func.Invoke(arg)) return true;

			return false;
		}

		public bool AddDecider(Func<Transform, bool> decider)
		{
			return selectionAllowDeciders.Add(decider);
		}

		public bool RemoveDecider(Func<Transform, bool> decider)
		{
			return selectionAllowDeciders.Remove(decider);
		}


		public void DestroyAllTargets()
		{
			// todo: UndoRedo


			transformBuffer.Clear();

			// todo: overloads
			foreach (Transform t in targetRoots)
			{
				transformBuffer.Add(t);
			}

			// Create single event
			ClearTargets();

			// All targets still exist when the event is fired
			// so subscribers can interact with each other if needed.
			foreach (Transform t in transformBuffer)
			{
				t.GetComponent<SelectionEventReceiver>()?.FireBeforeDelete();

				// todo: Separation of concerns: Delete event is not the responsibility of GizmoEventReceiver.
				// move users over to SelectionEventReceiver.
				t.GetComponent<TransformGizmoEventReceiver>()?.FireGizmoDelete();
			}

			foreach (Transform t in transformBuffer)
			{
				Destroy(t.gameObject);
			}

			transformBuffer.Clear();
		}

		public static void TransformPivotRespectingValueChange(Transform target, Vector3 position, Quaternion rotation, Vector3 scale)
		{
			// todo: we need to keep in mind the pivot point from the Gizmo.
			// todo: refactor so this lives in the gizmo.
			var t = target;

			t.localPosition = position;

			if (Instance.Pivot == TransformPivot.Origin)
			{
				t.localRotation = rotation;
				t.localScale = scale;
			}
			else // gizmo.pivot == TransformPivot.Center
			{
				// The gizmo keeps the pivot in mind, and SW meshes can have wacko origin.
				var pivot = Instance.PivotPoint;

				if (t.localRotation != rotation)
				{
					var rotationDelta = rotation * Quaternion.Inverse(t.localRotation);

					rotationDelta.ToAngleAxis(out float angle, out Vector3 axis);

					if (VectorExt.IsFinite(axis) && axis != Vector3.zero && angle.IsFinite())
					{
						t.RotateAround(pivot, axis, angle);
					}
				}

				if (t.localScale != scale) t.SetScaleFrom(pivot, scale);
			}
		}

		/// <summary>
		/// Tell the <see cref="SelectionManager"/> to not run it's user interaction code
		/// this frame. Use this if you have custom logic responding to clicks.
		/// </summary>
		public static void DoNotSelectThisFrame()
		{
			doNotSelectThisFrame = true;
		}
	}
}
