﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;

using UnityEngine;

namespace Control
{
	public class SelectionEventReceiver : MonoBehaviour
	{
		/// <summary>
		/// Simple bool to make the object not selectable.
		/// </summary>
		public bool Selectable = true;

		/// <summary>
		/// Select the <see cref="Transform"/> that this component is attached to when a child receives the raycast hit.
		/// </summary>
		public bool SelectThisFromChildRayHit = true;


		/// <summary>
		/// Func to check if the object should be selectable.
		/// Will only be queried when <see cref="Selectable"/> is <see langword="true"/>.
		/// Use this if the select-ability depends on external state or is otherwise complicated.
		/// </summary>
		public Func<Transform, bool> SelectionAllowed;

		/// <summary>
		/// Fires when this object is selected.
		/// </summary>
		[field: NonSerialized]
		public event Action<Transform> OnSelect;

		/// <summary>
		/// Fires when this object is deselected.
		/// Does not fire when <see cref="OnBeforeDelete"/> is fired.
		/// </summary>
		[field: NonSerialized]
		public event Action<Transform> OnDeSelect;

		/// <summary>
		/// Is copying of this object allowed.
		/// </summary>
		public bool AllowCopy = false;


		/// <summary>
		/// Handler that should replace the behaviour of Instantiate.
		/// </summary>
		public Func<Transform, Transform> OverrideCopyCreation;

		/// <summary>
		/// Event that fires when a copy of this object has been made. The argument is the copy.
		/// </summary>
		[field: NonSerialized]
		public event Action<Transform> OnCopyCreated;


		/// <summary>
		/// Is deleting of this object allowed.
		/// </summary>
		public bool AllowDelete = false;

		/// <summary>
		/// Event that fires when this object is about to be deleted.
		/// </summary>
		[field: NonSerialized]
		public event Action<Transform> OnBeforeDelete;


		#region Implementation

		internal void FireSelect()
		{
			OnSelect?.Invoke(transform);
		}

		internal void FireDeselect()
		{
			OnDeSelect?.Invoke(transform);
		}

		internal void FireCopyCreated(Transform copy)
		{
			OnCopyCreated?.Invoke(copy);
		}

		internal void FireBeforeDelete()
		{
			OnBeforeDelete?.Invoke(transform);
		}

		#endregion Implementation

		private void OnDestroy()
		{
			// We make neither undoEntry nor fire event because that should be handled
			// by the delete operation itself.
			// This is just here to ensure stuff does not break spectacularly.
			if (SelectionManager.Instance.RemoveTarget(transform, false, false))
			{
				Console.WriteLine
					(
					 $"[{nameof(SelectionEventReceiver)}]/{nameof(OnDestroy)} '{name}' was still selected "
				   + $"while being destroyed. This means whatever operation caused the deletion "
				   + $"is not handling it correctly."
					);
				// A selected object should be deselected before being destroyed, otherwise
				// it sticks around and can cause all kinds of problems for users of the current selection.

				// Cannot provide StackTrace because Unity calls OnDestroy so there would be nothing to see.
			}
		}
	}
}
