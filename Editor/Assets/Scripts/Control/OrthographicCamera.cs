﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections;
using System.IO;

using UnityEngine;

using Tiles;
using UnityEngine.Serialization;

namespace Control
{
	public class OrthographicCamera : CameraController
	{
		/// <summary>
		/// The default (and reset) move speed of the camera
		/// </summary>
		[Range(0, 100_000)]
		public float DefaultMoveSpeed = 10f; // Units (meters) / s

		[SerializeField]
		private float CurrentMoveSpeed = 10f; // Units (meters) / s

		/// <summary>
		/// Flat Multiplier to speed when associated key (default shift) is pressed
		/// </summary>
		[Range(1, 100_000)]
		public float MoveFastMultiplier = 2f;


		/// <summary>
		/// The change in move speed per scroll increment of the mouse wheel, as a multiplier
		/// </summary>
		[Range(0, 100)]
		public float ScrollWheelSpeedMultiplier = 1.2f;

		/// <summary>
		/// Rotation speed with keyboard, in degrees / second
		/// </summary>
		[Range(0.01f, 1_000)]
		public float RotationSpeedKeyboard = 30f; // Degrees / sec



		/// <summary>
		/// Zoom sensitivity
		/// </summary>
		[Range(0.01f, 1_000)]
		public float OrthoSizeSpeed = 22f;

		[Range(0.1f, 1_000_000f)]
		public float OrthoSizeMin = 10f;

		[Range(0.1f, 1_000_000f)]
		public float OrthoSizeMax = 100_000f;


		const int ScreenShotResolution_Min = 5;
		const int ScreenShotResolution_Max = 1_000_000;
		[Range(ScreenShotResolution_Min, ScreenShotResolution_Max)]
		[FormerlySerializedAs("ScreenShotResolution")]
		public int screenShotResolution = 500;

		public int ScreenShotResolution
		{
			get => screenShotResolution;
			set
			{
				SubmitResolution(value);
			}
		}

		private GameObject canvas;

		private Coroutine allTilesCoroutine;

		/// <inheritdoc />
		protected override void OnAwake()
		{
			base.OnAwake();
			canvas = GameObject.Find("Canvas");
		}


		/// <inheritdoc />
		protected override void OnEnableAndAfterStart()
		{
			// Ensure the camera is looking straight down.
			var angles = transform.localEulerAngles;
			angles.x = 90;
			angles.y = 0;
			angles.z = 0;
			transform.localEulerAngles = angles;

			var position = transform.localPosition;
			if (position.y < 5000) position.y = 5000;
			transform.localPosition = position;

			camera.orthographic = true;
		}

		void Update()
		{
			HeldDownKeys();
			PressedKeys();
			MouseWheel();
		}


		void HeldDownKeys()
		{
			if (!IsFocused) return;
			if (!MousePresent) return;


			var moveSpeed = this.CurrentMoveSpeed;
			if (Input.GetKey(KeyCode.LeftShift)) moveSpeed *= MoveFastMultiplier;


			// Forward
			Vector3 translation = new Vector3();
			if (Input.GetKey(KeyCode.W))
			{
				translation += transform.up * moveSpeed;
			}
			else if (Input.GetKey(KeyCode.S))
			{
				translation -= transform.up * moveSpeed;
			}

			// Sideways
			if (Input.GetKey(KeyCode.D))
			{
				translation += transform.right * moveSpeed;
			}
			else if (Input.GetKey(KeyCode.A))
			{
				translation -= transform.right * moveSpeed;
			}


			float currentOrthoSpeed = OrthoSizeSpeed * moveSpeed;
			// Zoom
			if (Input.GetKey(KeyCode.KeypadPlus))
			{
				camera.orthographicSize = Mathf.Clamp(camera.orthographicSize - currentOrthoSpeed * Time.deltaTime, OrthoSizeMin, OrthoSizeMax);
			}
			else if (Input.GetKey(KeyCode.KeypadMinus))
			{
				camera.orthographicSize = Mathf.Clamp(camera.orthographicSize + currentOrthoSpeed * Time.deltaTime, OrthoSizeMin, OrthoSizeMax);
			}

			transform.localPosition += translation * Time.deltaTime;

			// Rotate (Around Forward Vector)
			if (Input.GetKey(KeyCode.Z))
			{ // AntiClock
				transform.Rotate(Vector3.forward, RotationSpeedKeyboard * Time.deltaTime, Space.Self);
			}
			else if (Input.GetKey(KeyCode.X))
			{ // Clock
				transform.Rotate(Vector3.forward, -RotationSpeedKeyboard * Time.deltaTime, Space.Self);
			}
		}

		void PressedKeys()
		{
			if (!IsFocused) return;

			if (Input.GetKey(KeyCode.LeftWindows) || Input.GetKey(KeyCode.RightWindows))
				return; // So we do not respond to common shortcuts, including [Windows] + [Escape] (To close the magnifier)

			if (Input.GetKeyUp(KeyCode.R))
			{
				CurrentMoveSpeed = DefaultMoveSpeed;
			}

			if (Input.GetKeyUp(KeyCode.T))
			{
				// Setup for tile photo:
				AlignWithTile();

				if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
				{
					Screen.SetResolution(Screen.currentResolution.width, Screen.currentResolution.height, FullScreenMode.FullScreenWindow);

					canvas?.SetActive(true);
				}
				else if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
				{
					StartAllTilesScreenShots();
				}
				else
				{
					StartCoroutine(DoCaptureScreenShot());
				}
			}

			const float movement = 1000;
			if (Input.GetKeyUp(KeyCode.LeftArrow))
			{
				transform.position += new Vector3(-movement, 0, 0);
			}
			if (Input.GetKeyUp(KeyCode.RightArrow))
			{
				transform.position += new Vector3(movement, 0, 0);
			}
			if (Input.GetKeyUp(KeyCode.UpArrow))
			{
				transform.position += new Vector3(0, 0, movement);
			}
			if (Input.GetKeyUp(KeyCode.DownArrow))
			{
				transform.position += new Vector3(0, 0, -movement);
			}
		}

		private void MouseWheel()
		{
			if (!IsFocused) return;
			if (!MousePresent) return;

			Vector2 mouse_wheel_delta = Input.mouseScrollDelta;
			float primaryWheel_delta = mouse_wheel_delta.y;
			if (primaryWheel_delta == 0) return;

			if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
				MouseWheel_OrthoSize(primaryWheel_delta);
			else
				MouseWheel_Speed(primaryWheel_delta);
		}

		private void MouseWheel_OrthoSize(float primaryWheel_delta)
		{
			float newValue = camera.orthographicSize;
			if (primaryWheel_delta > 0)
				newValue *= ScrollWheelSpeedMultiplier;
			else
				newValue /= ScrollWheelSpeedMultiplier;

			camera.orthographicSize = Mathf.Clamp(newValue, OrthoSizeMin, OrthoSizeMax);
		}

		private void MouseWheel_Speed(float primaryWheel_delta)
		{
			if (primaryWheel_delta > 0)
				CurrentMoveSpeed *= ScrollWheelSpeedMultiplier;
			else
				CurrentMoveSpeed /= ScrollWheelSpeedMultiplier;

			CurrentMoveSpeed = Mathf.Clamp(CurrentMoveSpeed, 0.001f, 10_000f);
		}

		/// <inheritdoc />
		protected override void OnDisableIfStarted()
		{
			camera.orthographic = false;
		}

		public void SubmitResolution(string s)
		{
			if (! int.TryParse(s, out int i)) return;

			SubmitResolution(i);
		}

		public void SubmitResolution(int i)
		{
			if (i < ScreenShotResolution_Min) return;
			if (i >= ScreenShotResolution_Max) return;

			screenShotResolution = i;
		}

		public void AlignWithTile()
		{
			camera.orthographicSize = 500;
			var angle = transform.localEulerAngles;
			angle.x = 90;
			angle.y = 0;
			angle.z = 0;
			transform.localEulerAngles = angle;

			// Align with tile
			var pos = transform.position;

			pos.x = Mathf.RoundToInt((pos.x - 500) / 1000f) * 1000 + 500;
			pos.z = Mathf.RoundToInt((pos.z - 500) / 1000f) * 1000 + 500;

			transform.position = pos;
		}

		public string ScreenshotFolder()
		{
			string dataPath = Application.dataPath;

			if (Application.isEditor)
			{
				dataPath = dataPath.Replace("Assets", "Build");
			}
			string imageDirectory = Path.Combine(dataPath, $"TileCaptures/{ScreenShotResolution}");


			// Silently does nothing if it exists.
			Directory.CreateDirectory(imageDirectory);

			return imageDirectory;
		}

		public void StartAllTilesScreenShots()
		{
			allTilesCoroutine ??= StartCoroutine(CaptureAllTiles());
		}

		private IEnumerator DoCaptureScreenShot()
		{
			// Set square screen.
			Screen.SetResolution(ScreenShotResolution, ScreenShotResolution, false);

			bool? canvasState = canvas?.activeSelf;
			canvas?.SetActive(false);

			string screenshotFolder = ScreenshotFolder();

			yield return null; // Wait for next frame.


			var nearestTile = TileLoader.instance.GetNearestTile(transform.position);
			if (null == nearestTile)
			{
				int x = Mathf.RoundToInt(transform.position.x / 500f);
				int y = Mathf.RoundToInt(transform.position.z / 500f);
				ScreenCapture.CaptureScreenshot(Path.Combine(screenshotFolder, $"Unknown_Tile_{x}_{y}.png"));
			}
			else
			{
				ScreenCapture.CaptureScreenshot(Path.Combine(screenshotFolder, $"{nearestTile.tile.fileName}.png"));
			}

			yield return null;
			canvas?.SetActive(canvasState.Value);
		}

		private IEnumerator CaptureAllTiles()
		{
			var oldController = switcher.CurrentController;
			switcher.SwitchToController(this);

			bool? canvasState = canvas?.activeSelf;
			try
			{
				canvas?.SetActive(false);
			}
			catch(Exception e)
			{
				Debug.LogException(e);
			}

			// Try to not create a window size that's too big for the screen it's on.
			int resolution = ScreenShotResolution;
			int multiplier = 1;
			while (resolution > Math.Max(Screen.currentResolution.width, Screen.currentResolution.height))
			{
				resolution /= 2;
				multiplier *= 2;
			}


			// Set square window.
			FullScreenMode oldScreenMode = Screen.fullScreenMode;
			int oldWidth = Screen.width;
			int oldHeight = Screen.height;
			Screen.SetResolution(resolution, resolution, FullScreenMode.Windowed);

			float oldOrthographicSize = camera.orthographicSize;
			camera.orthographic = true;
			camera.orthographicSize = 500;

			string screenShotFolder = ScreenshotFolder();

			yield return null; // Wait for changes to happen.


			foreach (var kvp in TileLoader.instance.MapTileNameToTile)
			{
				string tileName = kvp.Key;
				var wrapper = kvp.Value;

				GameObject tileRoot = wrapper.tileRoot;

				var pos = tileRoot.transform.position;
				pos.y = transform.position.y;
				transform.position = pos;

				// Wait for any LOD updates that may need to happen in response to our position change.
				yield return null;

				// The ScreenShot is Queued for the end of the frame or something because it works without waiting.
				ScreenCapture.CaptureScreenshot(Path.Combine(screenShotFolder, $"{tileName}.png"), multiplier);

				yield return null; // We do need to wait for the screenShot to be taken though.
			}

			try
			{
				canvas?.SetActive(canvasState.Value);
			}
			catch (Exception e)
			{
				Debug.LogException(e);
			}


			if (oldScreenMode == FullScreenMode.FullScreenWindow || oldScreenMode == FullScreenMode.ExclusiveFullScreen)
			{
				// For some reason we can get in a 'fullScreen' state that is not the correct resolution.
				Screen.SetResolution(Screen.currentResolution.width, Screen.currentResolution.height, FullScreenMode.FullScreenWindow);
			}
			else
			{
				// Revert to whatever it was before.
				Screen.SetResolution(oldWidth, oldHeight, oldScreenMode);
			}

			camera.orthographicSize = oldOrthographicSize;

			allTilesCoroutine = null;

			switcher.SwitchToController(oldController);

			// Reveal the folder with the screenshots.
			System.Diagnostics.Process.Start(screenShotFolder);
		}
	}
}
