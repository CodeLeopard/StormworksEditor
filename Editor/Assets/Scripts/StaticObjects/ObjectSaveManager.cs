﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections.Generic;

using Tools;

using UnityEngine;

namespace StaticObjects
{
	//[Obsolete("Use direct XML editing instead.")]
	public class ObjectSaveManager : ThingSaveManager<StaticObject>
	{
		void Reset()
		{
			saveFilePath = "Objects";
			thingName_Single = "StaticObject";
			thingName_Multiple = "StaticObjects";
			thingsContainerName = "StaticObjects";


			AllowLoad = true;
			AllowSave = true;
		}

		/// <inheritdoc />
		protected override void OnAwake()
		{
			defaultSerializer = Serializer.SystemXml;

			defaultDeserializer = Serializer.SystemXml;

			// Will load from unPrefixed path.
			deserializeFallbackOrder = new[] { Serializer.SystemXml, };

			base.OnAwake();
		}

		public override List<StaticObject> GetDataFromWorld()
		{
			var list = new List<StaticObject>();
			foreach (Transform t in thingsContainer.transform)
			{
				if (StaticObject.GetDataFrom(t.gameObject, out StaticObject result))
				{
					list.Add(result);
				}
			}

			return list;
		}

		public void LoadAdditive(string fileName)
		{
			string oldSaveFilePath = saveFilePath;
			saveFilePath = fileName;
			try
			{
				Load();
			}
			finally
			{
				saveFilePath = oldSaveFilePath;
			}
		}

		/// <inheritdoc />
		public override void Clear()
		{
			// Remove any existing.
			foreach (Transform t in thingsContainer.transform)
			{
				DestroyOrImmediate(t.gameObject);
			}
		}

		public override void SetDataToWorld(List<StaticObject> data)
		{
			foreach (var staticObject in data)
			{
				try
				{
					var go = Instantiate(RestorePrefab);
					go.SetActive(false);
					go.transform.parent = thingsContainer.transform;
					if (!staticObject.SetDataOn(go))
					{
						DestroyOrImmediate(go);
						continue;
					}

					SetActiveNextAfterFrame(go, true, StaticObjectBehaviour.ColliderCursedTickDelay);
				}
				catch(Exception e)
				{
					Console.WriteLine(e.ToString());

					Debug.LogError($"An {e.GetType().Name} occurred during object instantiation, this could mean that your saved StaticObject data is broken or files referenced by it could not be found. Look at the log for more details and carefully consider if saving in this state may cause data loss due to (potentially) invalid objects getting saved.");
				}
			}
		}
	}
}
