﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.IO;

using Behaviours;

using BinaryDataModel.Converters;

using Control;

using DataTypes.Attributes;
using DataTypes.Extensions;

using OpenTK.Mathematics;

using Shared;

using SimpleFileBrowser;

using Tools;

using UnityEngine;

using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

using static Tools.NewObjectPosition;

namespace StaticObjects
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(Camera))]
	[RequireComponent(typeof(SelectionManager))]
	public class ObjectInteraction : M0noBehaviour
	{
		[InspectorReadonly]
		public string startingFolder;

		private new Camera camera;

		private LayerMask terrain;

		public GameObject objectParent { get; protected set; }
		public GameObject newObjectPrefab;

		/// <inheritdoc />
		protected override void OnStart()
		{
			camera = GetComponent<Camera>();

			terrain = LayerMaskExt.NameToMask("Terrain");
			//startingFolder = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
			startingFolder = StormworksPaths.meshes;

			{
				var worldContainer = GameObject.FindGameObjectWithTag("WorldContainer");
				var saveManager = worldContainer.GetComponent<ObjectSaveManager>();

				objectParent = saveManager.thingsContainer;

				if (null == objectParent)
				{
					Debug.LogError($"Didn't get {nameof(objectParent)} from SaveManager ({nameof(saveManager.thingsContainer)})");
				}

				newObjectPrefab = saveManager.RestorePrefab;

				if (null == newObjectPrefab)
				{
					Debug.LogError($"Didn't get {nameof(newObjectPrefab)} from SaveManager ({nameof(saveManager.RestorePrefab)})");
				}
			}
		}

		private void Update()
		{
			KeyboardInput();
		}


		private void KeyboardInput()
		{
			if (InputManagement.TextInputFocused) return;
			if (Input.GetKeyUp(KeyCode.F)
			 && ! InputManagement.AnyModifierKeyPressed
			 && SelectionManager.Instance.FirstTarget == null)
			{
				Action_SelectFileToImport();
			}

			if (Input.GetKeyUp(KeyCode.C) && ! InputManagement.AnyModifierKeyPressed)
			{
				Action_DeleteSelection();
			}
		}


		private void Action_DeleteSelection()
		{
			// Only allow destroy on things that we manage.
			//if(GizmoSelectionAllowed(selectionManager.CurrentTarget))

			// todo: Implement the above commented line
			// Note: Other components depend on the current broken behaviour.

			SelectionManager.Instance.DestroyAllTargets();
		}


		public bool TerrainRayCast(out Ray ray, out RaycastHit hit)
		{
			return RayCast(camera, terrain, out ray, out hit);
		}

		private Vector3 selectedPosition;
		void Action_SelectFileToImport()
		{
			if (Position(camera, terrain, out selectedPosition))
			{
				FileBrowser.SetFilters(false, ".mesh");

				if (! FileBrowser.ShowLoadDialog
				(
					OnFilesSelected
					, OnFileSelectionCanceled
					, FileBrowser.PickMode.Files
					, false
					, startingFolder
					, null
					, "Select .mesh to load"
				))
				{
					Debug.LogError($"Failed to open the SelectFileDialog.");
				}
			}
		}

		private void OnFilesSelected(string[] selection)
		{
			if (null == selection || selection.Length == 0)
			{
				Debug.Log("No file was selected");
				return;
			}

			if (selection.Length > 1)
			{
				Debug.LogError("Multiple selection is not supported.");
				return;
			}


			var filePath = selection[0];
			var fileName = Path.GetFileName(filePath);
			var folder = Path.GetDirectoryName(filePath);
			var physPath = Path.Combine(folder, $"{Path.GetFileNameWithoutExtension(filePath)}.phys");

			Console.WriteLine($"Loading {filePath}");

			startingFolder = folder; // todo: make this somewhat smart, so that it only switches when a folder is used multiple times.

			var meshInfo = Binary.LoadMesh(filePath).ToUnityMesh();
			if (! File.Exists(physPath))
			{
				// Yes
				physPath = Path.Combine(folder, $"{Path.GetFileNameWithoutExtension(filePath)}_phys.phys");

				if (! File.Exists(physPath))
				{
					Debug.LogWarning("Did not find a matching .phys file, no physics will be loaded for the object.");
					physPath = null;
				}
			}

			RunOnMainThread.Enqueue(MakeGameObject);


			return;

			void MakeGameObject()
			{
				if (RunOnMainThread.CancelableTasksShouldCancel) return;

				var go = Instantiate(newObjectPrefab);
				go.SetActive(false);
				go.hideFlags = HideFlags.DontSaveInBuild | HideFlags.DontSaveInEditor;

				go.transform.parent = objectParent.transform;

				// Setup the StaticObject.
				var obj = new StaticObject();
				obj.name = fileName;
				obj.transform = Matrix4.CreateTranslation((selectedPosition + new Vector3(0, meshInfo.Mesh.bounds.extents.y, 0)).ToOpenTK());
				obj.meshFilePath = PathHelper.GetRelativePath(StormworksPaths.rom, filePath);
				if(! string.IsNullOrWhiteSpace(physPath))
					obj.physMeshFilePath = PathHelper.GetRelativePath(StormworksPaths.rom, physPath);

				obj.SetDataOn(go, meshInfo);

				// This stupid nonsense is needed to make the collider update (even though it works just fine for the extrusion stuff)
				SetActiveNextAfterFrame(go, true, StaticObjectBehaviour.ColliderCursedTickDelay);
			}
		}

		private void OnFileSelectionCanceled()
		{
			Console.WriteLine($"User canceled file selection");
		}
	}
}
