﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.IO;
using System.Linq;

using BinaryDataModel;
using BinaryDataModel.Converters;

using GUI.SidePanel;

using OpenTK.Mathematics;

using Shared;
using Shared.Exceptions;

using UnityEngine;

namespace StaticObjects
{
	[Serializable]
	public class StaticObject
	{
		/// <summary>
		/// id in xml
		/// </summary>
		public string id;

		/// <summary>
		/// Arbitrary name, NOT in xml
		/// </summary>
		public string name;


		private string _meshFilePath;

		/// <summary>
		/// Path, relative to rom to fetch mesh from.
		/// </summary>
		public string meshFilePath
		{
			get => _meshFilePath;
			set => SetPath(ref _meshFilePath, value);
		}

		/// <summary>
		/// Create a physMesh from the mesh if: <see cref="physMeshFilePath"/> is null, empty, or points to a file that does not exist.
		/// </summary>
		public bool createPhys = true;

		private string _physMeshFilePath;

		/// <summary>
		/// Optional: Path, relative to rom to fetch .phys from.
		/// If <see cref="createPhys"/> is <see langword="true"/> the .phys will be stored at the specified path.
		/// If this path is null or empty it will be inferred from <see cref="meshFilePath"/>.
		/// </summary>
		public string physMeshFilePath
		{
			get => _physMeshFilePath;
			set => SetPath(ref _physMeshFilePath, value);
		}

		/// <summary>
		/// The transform.
		/// </summary>
		public Matrix4 transform;


		private void SetPath(ref string current, string value)
		{
			if (string.IsNullOrWhiteSpace(value)) value = null;
			if (current == value) return;

			current = value != null ? PathHelper.NormalizeDirectorySeparator(value) : value;

			MeshPathChanged?.Invoke(this);
		}

		[field: NonSerialized]
		public event Action<StaticObject> MeshPathChanged;


		/// <summary>
		/// Get object from a GameObject
		/// </summary>
		/// <param name="go"></param>
		/// <param name="result"></param>
		/// <returns></returns>
		public static bool GetDataFrom(GameObject go, out StaticObject result)
		{
			result = go.GetComponent<StaticObjectBehaviour>()?.myObject;
			if (null == result) return false;

			result.transform = go.transform.ToOpenTK(local:true);

			return true;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="go"></param>
		/// <returns>success</returns>
		public bool SetDataOn(GameObject go, UnityMesh.MeshWithMaterialInfo meshInfo = null)
		{
			go.name = name;

			transform.SetDataOn(go.transform);

			if (string.IsNullOrWhiteSpace(meshFilePath))
			{
				Debug.LogError($"StaticObject '{id}' with null or empty {nameof(meshFilePath)}, skipping.", go);
				return false;
			}

			string MeshNotFoundString()
			{
				return $"StaticObject '{id}' could not find {nameof(meshFilePath)}: '{meshFilePath}', skipping.";
			}

			try
			{
				meshInfo = meshInfo ?? MeshCache.GetOrLoadMesh(Path.Combine(StormworksPaths.rom, meshFilePath)).ToUnityMesh();
			}
			catch (FileNotFoundException)
			{
				Debug.LogError(MeshNotFoundString(), go);
				return false;
			}
			catch (FileInteractionException e)
			{
				if (e.InnerException is FileNotFoundException)
				{
					Debug.LogError(MeshNotFoundString(), go);
					return false;
				}

				throw;
			}

			var filter = go.GetComponent<MeshFilter>();
			filter.sharedMesh = meshInfo.Mesh;

			var renderer = go.GetComponent<MeshRenderer>();
			renderer.materials = meshInfo.Materials.ToArray();


			var collider = go.GetComponent<MeshCollider>();

			collider.sharedMesh = meshInfo.Mesh;

			var container = go.GetComponent<StaticObjectBehaviour>();
			container.myObject = this;

			return true;
		}

		static StaticObject()
		{
			EditPanel.MkSpec(typeof(StaticObject), new EditPanel.SpecElement[]
			{
				new EditPanel.SpecElement(nameof(id), isReadonly:true),
				new EditPanel.SpecElement(nameof(name)),
				new EditPanel.SpecElement(nameof(meshFilePath)),
				new EditPanel.SpecElement(nameof(createPhys)),
				new EditPanel.SpecElement(nameof(physMeshFilePath)),
			});
		}
	}
}
