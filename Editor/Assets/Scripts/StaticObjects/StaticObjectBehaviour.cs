﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;

using Behaviours;

using GUI;

using RuntimeGizmos;

using Tools;

using UnityEngine;

namespace StaticObjects
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(TransformGizmoEventReceiver))]
	public class StaticObjectBehaviour : M0noBehaviour, IInspectorPanelContentProvider
	{
		private StaticObject _myObject;

		/// <summary>
		/// For some Unity reason the collider dies if it's enabled too soon because Unity.
		/// Previously it was 1 but Unity happened and now it needs to be 2.
		/// </summary>
		public const int ColliderCursedTickDelay = 60;

		public StaticObject myObject
		{
			get => _myObject;
			set => SetMyObject(value);
		}


		[SerializeField]
		[HideInInspector]
		protected TransformGizmoEventReceiver gizmoEvents;


		public static event Action<StaticObjectBehaviour> Removed;

		#region UnityEvents

		/// <inheritdoc />
		protected override void OnAwake()
		{
			gizmoEvents = GetRequiredComponent<TransformGizmoEventReceiver>();
		}

		private void OnDestroy()
		{
			Removed?.Invoke(this);
		}

		#endregion UnityEvents

		/// <inheritdoc />
		object IInspectorPanelContentProvider.GetInspectorContent()
		{
			return _myObject;
		}


		private void SetMyObject(StaticObject value)
		{
			if (_myObject == value) return;

			if(null != _myObject)
				_myObject.MeshPathChanged -= MeshPathChanged;
			_myObject = value;
			_myObject.MeshPathChanged += MeshPathChanged;
		}

		private void MeshPathChanged(StaticObject obj)
		{
			// todo: make dedicated method that won't break.

			RunOnMainThread.EnqueueAndWaitForCompletion(() => obj.SetDataOn(gameObject));
		}
	}
}
