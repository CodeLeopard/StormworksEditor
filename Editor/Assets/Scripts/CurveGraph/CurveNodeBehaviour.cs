﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections;
using System.Collections.Generic;

using Behaviours;

using BinaryDataModel.Converters;

using Control;

using CurveGraph.RuntimeEditor;

using GUI;
using GUI.SidePanel;

using RuntimeGizmos;

using Shared;

using UnityEngine;

namespace CurveGraph
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(MaterialColorTransition))]
	[RequireComponent(typeof(SelectionEventReceiver))]
	[RequireComponent(typeof(TransformGizmoEventReceiver))]
	public class CurveNodeBehaviour : M0noBehaviour, IInspectorPanelContentProvider
	{
		public CurveNode node = null;
		private InspectorContentProvider inspectorContentProvider;


		private const float minTransformMagnitude = 0.001f;
		private const float maxTransformMagnitude = 10_000f;

		public static float maxLength = 2500f;


		public static event Action<CurveNode> NodeCreated;
		public static event Action<CurveNode> NodeRemoved;

		public static event Action<CurveNodeBehaviour> OnSelect;
		public static event Action<CurveNodeBehaviour> OnDeSelect;


		private MaterialColorTransition _colorTransitionThingy;
		private SelectionEventReceiver selectionEvents;
		private TransformGizmoEventReceiver transformEvents;



		private bool IsSelected;
		public Color IdleColor;
		public Color SelectedColor;
		public Color JunctionCandidateColor;
		public Color JunctionRefusedColor;

		public Color StartNodeColor;
		public Color EndNodeColor;

		#region UnityMessages

		/// <inheritdoc />
		protected override void OnAwake()
		{
			// Unity insists on serializing the node into the prefab, and that breaks things.
			// But we can't put NonSerialized, because then it won't show up in the Inspector.
			node = new CurveNode();
			// Will be called immediately after Awake.
			node.NodeValuesChanged += NodeValuesChanged;

			inspectorContentProvider = _newInspectorContentProvider(this);


			_colorTransitionThingy = GetRequiredComponent<MaterialColorTransition>();

			selectionEvents = GetRequiredComponent<SelectionEventReceiver>();
			selectionEvents.AllowCopy = false;
			selectionEvents.AllowDelete = true;

			transformEvents = GetRequiredComponent<TransformGizmoEventReceiver>();
			transformEvents.AllowedScaleAxis = Axis.None; // todo: make this the magnitude.
			transformEvents.GizmoInteract    += GizmoInteract;
			transformEvents.GizmoEndInteract += GizmoEndInteract;
		}

		/// <inheritdoc />
		protected override void OnStart()
		{
			nodeBehaviourMap[node] = this;

			node.ConnectionsChanged += ConnectionsChanged;

			// Must be after Awake, otherwise it recurses
			// forever when the newly created node is auto-selected.
			selectionEvents.OnSelect   += Event_OnSelect;
			selectionEvents.OnDeSelect += Event_OnDeselect;

			NodeCreated?.Invoke(node);
		}

		private void OnValidate()
		{
			Editor_InspectorConnections();

			// The node could have been edited in the Inspector but it can't notice that by itself.
			node.RaiseNodeValuesChanged(true);
		}


		private void OnDestroy()
		{
			nodeBehaviourMap.Remove(node);
			node.ClearConnections();
			NodeRemoved?.Invoke(node);
		}


		#endregion UnityMessages

		public void SetNode(CurveNode newNode)
		{
			if (null != node)
			{
				node.NodeValuesChanged -= NodeValuesChanged;
				node.ConnectionsChanged -= ConnectionsChanged;

				if (nodeBehaviourMap.ContainsKey(node)) nodeBehaviourMap.Remove(node);
			}

			newNode.NodeValuesChanged += NodeValuesChanged;
			newNode.ConnectionsChanged += ConnectionsChanged;

			nodeBehaviourMap[newNode] = this;

			node = newNode;

			OnNodeValuesChanged();
		}



		#region Events

		private void Event_OnSelect(Transform _)
		{
			SetSelected(true);

			OnNodeValuesChanged(); // Read forwardMagnitude from the node;

			OnSelect?.Invoke(this);
		}

		private void Event_OnDeselect(Transform _)
		{
			SetSelected(false);

			OnDeSelect?.Invoke(this);
		}

		private void GizmoInteract(Transform _)
		{
			LimitCurveLength();

			node.position = transform.localPosition.ToOpenTK();
			node.direction = transform.forward.ToOpenTK() * node.direction.Length;
			node.NodeValuesChanged -= NodeValuesChanged;
			node.RaiseNodeValuesChanged(true);
			node.NodeValuesChanged += NodeValuesChanged;

			OnNodeValuesChanged(unsetHasChanged: false);

			CurveGraphRTEditor.Instance.HighlightPotentialJunctions(this);
		}

		private void GizmoEndInteract(Transform _)
		{
			LimitCurveLength();
			if (TransformEditPanel.GizmoInteractEventIsTransformEditPanel) return;
			CurveGraphRTEditor.Instance.MaybeMakeJunction(this);
		}

		private void ConnectionsChanged(CurveNode.ConnectionsChangedEventArgs _)
		{
			if (!isActiveAndEnabled) return;
			StartCoroutine(DeleteIfNoConnections());
		}

		private IEnumerator DeleteIfNoConnections()
		{
			yield return null;

			if (null == gameObject) yield break;

			if (node.Connections.Count == 0)
				Destroy(gameObject);
		}

		private void NodeValuesChanged(CurveNode _)
		{
			OnNodeValuesChanged();
		}

		private void OnNodeValuesChanged(bool unsetHasChanged = true)
		{
			gameObject.transform.position = node.Position.ToUnity();
			gameObject.transform.forward  = node.Direction.ToUnity().normalized;

			if(unsetHasChanged) gameObject.transform.hasChanged = false;
		}

		#endregion Events

		private void LimitCurveLength()
		{
			float squaredMaxLength = maxLength * maxLength;
			var position = transform.position.ToOpenTK();

			int averageDivisor = 0;
			OpenTK.Mathematics.Vector3 adjustedPosition = OpenTK.Mathematics.Vector3.Zero;

			bool any = false;
			foreach (var kvp in node.Connections)
			{
				var other = kvp.Key;
				var connection = kvp.Value;

				var toPosition = position - other.Position;
				OpenTK.Mathematics.Vector3 adusted = position;
				if ((toPosition).LengthSquared > squaredMaxLength)
				{
					any = true;
					adusted = other.Position + (toPosition.Normalized() * maxLength);
				}

				adjustedPosition += adusted;
				averageDivisor++;
			}

			if (! any)
			{
				// This exists mostly so you can put a breakpoint here.
				// For the math it wouldn't matter.
				return;
			}

			adjustedPosition /= averageDivisor;

			transform.position = adjustedPosition.ToUnity();
		}

		/// <inheritdoc />
		object IInspectorPanelContentProvider.GetInspectorContent()
		{
			return inspectorContentProvider;
		}
		/// <inheritdoc />
		void IInspectorPanelContentProvider.OnInteract()
		{
			OnNodeValuesChanged();
		}


		public void UpdateToMatchNodeValues()
		{
			NodeValuesChanged(null);
		}


		#region HighLight
		public void SetSelected(bool newState)
		{
			if (IsSelected == newState) return;
			IsSelected = newState;

			if (IsSelected)
			{
				_colorTransitionThingy.SetTarget(SelectedColor);
			}
			else
			{
				_colorTransitionThingy.SetTarget(IdleColor);
			}
		}

		public void SetHighlight_JunctionCandidate()
		{
			_colorTransitionThingy.SetTarget(JunctionCandidateColor);
		}


		public void SetHighlight_JunctionNotAllowed()
		{
			_colorTransitionThingy.SetTarget(JunctionRefusedColor);
		}

		public void SetHighlight_Idle()
		{
			_colorTransitionThingy.SetTarget(IdleColor);
		}

		public void SetHighlight_StartNode()
		{
			_colorTransitionThingy.SetTarget(StartNodeColor);
		}

		public void SetHighlight_EndNode()
		{
			_colorTransitionThingy.SetTarget(EndNodeColor);
		}

		#endregion HighLight

		#region SidePanel

		private static Func<CurveNodeBehaviour, InspectorContentProvider> _newInspectorContentProvider;

		private class InspectorContentProvider
		{
			private readonly CurveNodeBehaviour behaviour;
			private CurveNode node => behaviour.node;


			[VisibleProperty]
			public bool AutoSmooth
			{
				get => node.AutoSmooth;
				set => node.AutoSmooth = value;
			}

			[VisibleProperty]
			public float Curvature
			{
				get => node.Curvature;
				set => node.Curvature = value;
			}

			[VisibleProperty]
			public Vector3 Tangent
			{
				get => node.Direction.ToUnity();
				set => node.Direction = value.ToOpenTK();
			}

			[VisibleProperty]
			public float TangentMagnitude
			{
				get => node.Direction.Length;
				set
				{
					if (value < minTransformMagnitude) return;
					if (value > maxTransformMagnitude) return;
					var newValue = node.Direction.Normalized() * value;
					node.Direction = newValue;
				}
			}

			[VisibleProperty]
			public float Roll
			{
				get => node.Roll;
				set => node.Roll = value;
			}

			[VisibleProperty]
			public Vector2 Scale
			{
				get => node.Scale.ToUnity();
				set => node.Scale = value.ToOpenTK();
			}

			private InspectorContentProvider(CurveNodeBehaviour b)
			{
				behaviour = b;
			}

			static InspectorContentProvider()
			{
				_newInspectorContentProvider = (value) => new InspectorContentProvider(value);
			}
		}
		static CurveNodeBehaviour()
		{
			System.Runtime.CompilerServices.RuntimeHelpers.RunClassConstructor(typeof(InspectorContentProvider).TypeHandle);
		}

		#endregion SidePanel


		private static Dictionary<CurveNode, CurveNodeBehaviour> nodeBehaviourMap =
			new Dictionary<CurveNode, CurveNodeBehaviour>();

		public static IReadOnlyDictionary<CurveNode, CurveNodeBehaviour> NodeBehaviourMap => nodeBehaviourMap;


#if UNITY_EDITOR
		// unity Editor stuff #################################################
		// Allow dragging a CurveNode into an inspector property to create/remove connection


		[SerializeField]
		private CurveNodeBehaviour InspectorConnectToThisNode;

		[SerializeField]
		private CurveNodeBehaviour InspectorDisconnectThisNode;
		private void Editor_InspectorConnections()
		{
			if (null != InspectorConnectToThisNode)
			{
				try
				{
					var toNode = InspectorConnectToThisNode.node;
					if (null != toNode && toNode != node && ! node.IsConnectedTo(toNode))
						node.CreateConnection(toNode);
				}
				finally
				{
					InspectorConnectToThisNode = null;
				}
			}

			if (null != InspectorDisconnectThisNode)
			{
				try
				{
					var toNode = InspectorDisconnectThisNode.node;
					if (null != toNode && toNode != node && node.IsConnectedTo(toNode))
						node.RemoveConnection(toNode);
				}
				finally
				{
					InspectorDisconnectThisNode = null;
				}
			}
		}
#else
		// Exists so that we don't need to #if around the invocation.
		// Release build will optimize it away.
		private void Editor_InspectorConnections() {}
#endif
	}
}
