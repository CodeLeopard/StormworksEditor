﻿// Copyright 2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using LuaIntegration;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace CurveGraph
{
	/// <summary>
	/// Used to keep a consistent unique identofier on Connections so the export process is deterministic.
	/// </summary>
	public class ConnectionUIDProvider
	{
		public static readonly ConnectionUIDProvider Instance = new ConnectionUIDProvider();

		private uint _nextID = 0;

		/// <summary>
		/// Returns the next ConnectionID and increments the interlan counter.
		/// </summary>
		private uint NextConnectionID()
		{
			_nextID++;
			return _nextID;
		}

		/// <summary>
		/// Peeks the next ConnectionID, but does not increment the internal counter.
		/// </summary>
		private uint PeekNextConnectionID
		{
			get
			{
				return _nextID + 1;
			}
		}

		/// <summary>
		/// Reset internal state.
		/// </summary>
		public void Reset()
		{
			_nextID = 0;
		}

		/// <summary>
		/// Signal that new Connections have been loaded, Reset the internal sttate and import the new data.
		/// </summary>
		/// <param name="cs"></param>
		public void Loaded(IEnumerable<Connection> cs)
		{
			foreach (Connection connection in cs)
			{
				if (connection.data is ConnectionCustomData cd)
				{
					_nextID = Math.Max(_nextID, cd._id);
				}
				else if (connection.data != null)
				{
					Debug.LogError($"Encountered unexpected object '{connection.data.GetType().Name}' where '{nameof(ConnectionCustomData)}' was expected.");
				}
				else
				{
					connection.data = new ConnectionCustomData();
				}
			}

			Console.WriteLine($"NextConnectionID (initial): {PeekNextConnectionID}");

			foreach (Connection connection in cs)
			{
				if (connection.data is ConnectionCustomData cd && cd._id == 0)
				{
					cd._id = NextConnectionID();
				}
			}

			Console.WriteLine($"NextConnectionID (final)  : {PeekNextConnectionID}");
		}

		/// <summary>
		/// Ensure the given <see cref="Connection"/> has an id and return it.
		/// </summary>
		/// <param name="c"></param>
		/// <returns></returns>
		public uint EnsureHasID(Connection c)
		{
			if (c.data == null)
			{
				c.data = new ConnectionCustomData();
			}

			if (c.data is not ConnectionCustomData)
			{
				Debug.LogError($"Encountered unexpected object '{c.data.GetType().Name}' where '{nameof(ConnectionCustomData)}' was expected.");
				return 0;
			}

			var cd = c.data as ConnectionCustomData;

			if (cd._id == 0)
			{
				cd._id = NextConnectionID();
			}

			return cd._id;
		}

		/// <summary>
		/// Ensure the provided <see cref="Connection"/>s all have an ID.
		/// </summary>
		/// <param name="cs"></param>
		public void EnsureHasID(IEnumerable<Connection> cs)
		{
			foreach (Connection connection in cs)
			{
				EnsureHasID(connection);
			}
		}

		/// <summary>
		/// Retrieve the ID of the given <see cref="Connection"/>, an id value 0 is not valid.
		/// </summary>
		/// <param name="c"></param>
		/// <returns></returns>
		public uint GetID(Connection c)
		{
			if (c.data is ConnectionCustomData cd)
			{
				return cd._id;
			}
			else
			{
				return 0;
			}

		}

		/// <summary>
		/// Retrieve the Lowest ID of any <see cref="Connection"/> that starts at this <see cref="Node"/>. This will be a Unique number.
		/// </summary>
		/// <param name="n"></param>
		/// <returns></returns>
		private uint GetID(CurveNode n)
		{
			uint lowest = uint.MaxValue;
			foreach(var connection in n.Connections.Values)
			{
				if (connection.EndNode == n) continue;
				uint id = EnsureHasID(connection);
				lowest = Math.Min(lowest, id);
			}

			return lowest;
		}
	}
}
