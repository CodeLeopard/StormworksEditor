﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

using Shared;
using Tiles;

namespace CurveGraph
{
	[Serializable]
	public class CurveGraphSettings : SettingsBase
	{
		private string _defaultCurveSpecPath = "CSSpec/StandardG";

		[VisibleProperty]
		public string DefaultCurveSpecPath
		{
			get => _defaultCurveSpecPath;
			set => SetWithMarkChanged(ref _defaultCurveSpecPath, value);
		}


		private bool _saveFileBackupBinary = true;
		[VisibleProperty]
		public bool SaveFileBackupBinary
		{
			get => _saveFileBackupBinary;
			set => SetWithMarkChanged(ref _saveFileBackupBinary, value);
		}
	}
}
