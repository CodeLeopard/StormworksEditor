﻿// Copyright 2022-2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using Behaviours;
using BinaryDataModel.Converters;
using DataTypes.Extensions;
using LuaIntegration;
using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using Tools;
using UnityEngine;

namespace CurveGraph
{
	/// <summary>
	/// Keeps track of <see cref="Connection"/>s in the <see cref="CurveNode"/> graph and manages <see cref="GameObject"/>s
	/// to represent the <see cref="Connection"/>s.
	/// </summary>
	[DisallowMultipleComponent]
	public class CurveGraphConnectionManager : M0noBehaviour
	{
		private readonly Dictionary<Connection, ConnectionBehaviour> connectionMap =
			new Dictionary<Connection, ConnectionBehaviour>();

		/// <summary>
		/// The prefab used to <see cref="Instantiate"/> new connection visualizations.
		/// </summary>
		public GameObject ConnectionPrefab;

		/// <summary>
		/// The parent object of the connection representations.
		/// </summary>
		public Transform ConnectionParent;

		private FileSystemWatcher fileWatcher;


		/// <inheritdoc />
		protected override void OnAwake()
		{
			Instance = this;
			ConnectionMap = new ReadOnlyDictionary<Connection, ConnectionBehaviour>(connectionMap);

			SetupWatcher();
		}

		private void SetupWatcher()
		{
			try
			{
				// Create the target if it does not exist already.
				Directory.CreateDirectory(StormworksPaths.CreatorToolKitData.curveSpec);

				if (fileWatcher != null)
				{
					fileWatcher.Dispose();
					fileWatcher = null;
				}

				fileWatcher = new FileSystemWatcher();
				fileWatcher.Path = StormworksPaths.CreatorToolKitData.curveSpec;
				fileWatcher.IncludeSubdirectories = true;
				fileWatcher.NotifyFilter = NotifyFilters.LastWrite;
				fileWatcher.Changed += FileWatcher_Changed;
				fileWatcher.Error += FileWatcher_Error;
				fileWatcher.EnableRaisingEvents = true;
			}
			catch (Exception e)
			{
				Debug.LogException(e, this);
			}
		}

		private void FileWatcher_Error(object sender, ErrorEventArgs e)
		{
			Debug.LogWarning($"FileWatcher reported error, it will be restarted soon.");

			RunOnMainThread.Enqueue(Action);

			void Action()
			{
				Debug.Log("Restarting fileWatcher...");
				SetupWatcher();
			}
		}

		/// <inheritdoc />
		protected override void OnEnableAndAfterStart()
		{
			Connection.Created -= Connection_Created;
			Connection.Created += Connection_Created;

			CurveSaveState.Loaded -= CurveSaveState_Loaded;
			CurveSaveState.Loaded += CurveSaveState_Loaded;
		}

		/// <inheritdoc />
		protected override void OnDisableIfStarted()
		{
			Connection.Created -= Connection_Created;

			CurveSaveState.Loaded -= CurveSaveState_Loaded;

			try
			{
				fileWatcher?.Dispose();
			}
			catch (Exception e)
			{
				Debug.LogException(e, this);
			}
		}


		private void Connection_Created(Connection connection)
		{
			CreateConnectionGO(connection);
		}

		private GameObject CreateConnectionGO(Connection connection)
		{
			if (connectionMap.TryGetValue(connection, out ConnectionBehaviour cb))
			{
				// todo: complain?
				return cb.gameObject;
			}

			var go = Instantiate
				(ConnectionPrefab, connection.Position.ToUnity(), Quaternion.identity, ConnectionParent);

			cb = go.GetRequiredComponent<ConnectionBehaviour>();
			cb.manager = this;

			cb.AssignConnection(connection);

			connectionMap[connection] = cb;

			connection.Removed -= Connection_Removed;
			connection.Removed += Connection_Removed;

			return go;
		}

		public void Clear(){
			foreach (Transform t in ConnectionParent)
			{
				Destroy(t.gameObject);
			}
			connectionMap.Clear();
		}

		private void CurveSaveState_Loaded(List<CurveNode> loadedData)
		{
			if (loadedData.Count == 0) return;

			foreach (var connection in loadedData[0].NetworkConnections(loadedData))
			{
				CreateConnectionGO(connection);
			}
		}

		private void Connection_Removed(Connection connection)
		{
			if (connectionMap.TryGetValue(connection, out var behaviour))
			{
				connection.Removed -= Connection_Removed;

				if (null != behaviour && null != behaviour.gameObject)
				{
					Destroy(behaviour.gameObject);
				}

				connectionMap.Remove(connection);
			}
		}

		public void ReloadCrossSections()
		{
			CurveSpecCache.Instance.Clear(); // todo: outdated only.
			foreach (ConnectionBehaviour behaviour in connectionMap.Values)
			{
				try
				{
					var data = behaviour.Connection.data as ConnectionCustomData;
					if (null == data) continue;

					var path = data._curveSpecFilePath;
					if (null == data) continue;

					data._curveSpecFilePath = null;

					// Force null so the missing/error specs are used if the file went missing.
					data._curveSpec = null;

					data.CurveSpecFilePath = path;

					// The above property will refuse paths with errors to be set.
					// This should only happen when the user is typing it in, not on reload
					// because that affects all curves and thus could destroy work.
					data._curveSpecFilePath = path;

					// Force update
					data.RaiseChanged();
				}

				catch (Exception e)
				{
					Debug.LogException(e, behaviour);
					return;
				}
			}
		}

		private void FileWatcher_Changed(object sender, FileSystemEventArgs e)
		{
			string eventPath = e.FullPath.Replace(StormworksPaths.CreatorToolKitData.curveSpec, "");
			Console.WriteLine($"Updating CurveSpec due to changed file: {eventPath}");
			RunOnMainThread.Enqueue(Action);
			void Action()
			{
				ReloadCrossSections();
			}

			// todo: Ideally we would know the dependency graph of the lua files and only update the affected CurveSpecs.
		}

		public IEnumerator SetMeshVisible(bool newValue)
		{
			foreach (var connectionBehaviour in connectionMap.Values)
			{
				connectionBehaviour.MeshRenderer.gameObject.SetActive(newValue);

				yield return null;
			}
		}

		public IEnumerator SetPhysVisible(bool newValue)
		{
			foreach (var connectionBehaviour in connectionMap.Values)
			{
				connectionBehaviour.PhysRenderer.gameObject.SetActive(newValue);

				yield return null;
			}
		}

		#region Static

		public static CurveGraphConnectionManager Instance { get; private set; }

		public static IReadOnlyDictionary<Connection, ConnectionBehaviour> ConnectionMap { get; private set; }


		static CurveGraphConnectionManager()
		{
			CurveSpecCache.Instance.OnException += Instance_OnException;
		}

		private static void Instance_OnException(string arg1, Exception arg2)
		{
			// Logging the Exception as an Exception seems to alter the message.
			RunOnMainThread.LogError(arg2.ToString());
		}

		#endregion Static
	}
}
