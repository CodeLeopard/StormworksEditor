﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Linq;
using System.Threading.Tasks;

using UnityEngine;

using Behaviours;

using BinaryDataModel.Converters;

using DataTypes.Extensions;

using Extrusion;
using Extrusion.Generator;

using LuaIntegration;

using Manage;

using Tools;
using System.Collections.Generic;
using static Tools.Globals;
using Assets.Scripts.CurveGraph;

namespace CurveGraph
{
	[DisallowMultipleComponent]
	public class ConnectionRenderer : M0noBehaviour
	{
		public bool IsPhysics;

		public ConnectionBehaviour ConnectionBehaviour;
		public Connection Connection => ConnectionBehaviour.Connection;

		private Task meshTask;

		private List<ConnectionRendererElement> renderElements = new List<ConnectionRendererElement>();

		public bool updateMesh;

		/// <inheritdoc />
		protected override void OnEnableAndAfterStart()
		{
			ConnectionBehaviour = transform.parent.GetRequiredComponent<ConnectionBehaviour>();

			if (IsPhysics)
			{
				ConnectionBehaviour.PhysRenderer = this;
			}
			else
			{
				ConnectionBehaviour.MeshRenderer = this;
			}

			Connection.Changed -= Curve_Changed;
			Connection.Changed += Curve_Changed;

			if (IsPhysics)
			{
				updateMesh = ViewManager.Instance.PhysVisible;
			}
			else
			{
				updateMesh = ViewManager.Instance.MeshVisible;
			}
			gameObject.SetActive(updateMesh);
		}

		private void Update()
		{
			if(updateMesh) StartTask();
		}

		private void StartTask()
		{
			if (Connection?.curve == null) return; // We hope that it'll be set soon.
			if (null              != meshTask) return; // Task already running. Wait for it to complete before starting another.

			var connection = Connection.CloneForMultiThreadedReading();
			updateMesh = false;

			if (Globals.Constants.ForceSingleThreaded)
			{
				meshTask = Task.Run(() => RunOnMainThread.EnqueueAndWaitForCompletion(() => Task_DoMesh(connection)));
			}
			else
			{
				meshTask = Task.Run(() => Task_DoMesh(connection));
			}
		}


		private void Curve_Changed()
		{
			updateMesh = true;
		}

		private void Curve_Changed(Connection curve)
		{
			Curve_Changed();
		}

		private void Curve_Changed(CurveSpec spec)
		{
			Curve_Changed();
		}

		private void Task_DoMesh(Connection connection)
		{
			try
			{
				// todo: if data not present just return.
				var curve = connection.curve;
				var data = connection.data as ConnectionCustomData ?? Globals.Defaults.DefaultConnectionCustomData;

				CurveSpec spec;
				try
				{
					spec = data.CurveSpec ?? Globals.Defaults.FallbackCurveSpec;
				}
				catch (Exception e)
				{
					Debug.LogException(e);
					spec = Globals.Defaults.FallbackCurveSpec;
				}

				Tasked_GenerateMesh(curve, spec);
			}
			catch (Exception e)
			{
				// Unity does not log unhandled exceptions in Tasks by default.
				Debug.LogException(e, this);
				throw;
			}
			finally
			{
				meshTask = null;
			}
		}


		private void UnSetMesh()
		{
			foreach(var elem in renderElements)
			{
				elem.UnSetMesh();
			}
		}

		private static Material[] physicsMaterialsArray = null;
		private static Material[] PhysicsMaterialsArray
		{
			get
			{
				if(physicsMaterialsArray == null)
				{
					physicsMaterialsArray = new Material[] { Globals.Materials.Physics };
				}
				return physicsMaterialsArray;
			}
		}

		private void Tasked_GenerateMesh_Visual(CubicBezierCurve curve, CurveSpec spec)
		{
			if (!spec.WillGenerateMesh)
			{
				return;
			}

			var resultData = GenerateMesh(curve, spec);

			int index = 0;
			foreach (var meshResult in resultData.Meshes)
			{
				var mesh = meshResult.Mesh.ToUnityMesh(UnityMesh.MeshConvertOptions.None);
				var unityMesh = mesh.Mesh;
				var materials = mesh.Materials.ToArray();

				RunOnMainThread.EnqueueAndWaitForCompletion(Apply);
				void Apply()
				{
					var element = GetOrCreateElement(index);
					element.SetMesh(unityMesh, materials);
				}
				index++;
			}
			DisableElementsAtOrAfterIndex(index);
		}

		private void Tasked_GenerateMesh_Physics(CubicBezierCurve curve, CurveSpec spec)
		{
			if (!spec.WillGeneratePhys)
			{
				return;
			}

			var resultData = GeneratePhys(curve, spec);

			int index = 0;
			foreach (var meshResult in resultData.Physes)
			{
				var unityMesh = meshResult.Phys.ToUnityMesh();
				var materials = PhysicsMaterialsArray;

				RunOnMainThread.EnqueueAndWaitForCompletion(Apply);
				void Apply()
				{
					var element = GetOrCreateElement(index);
					element.SetMesh(unityMesh, materials);
				}
				index++;
			}
			DisableElementsAtOrAfterIndex(index);
		}

		private void DisableElementsAtOrAfterIndex(int index)
		{
			// If the index is equal to the count then we don't need to do anything as all elements are in use.
			if (renderElements.Count == index)
				return;

			RunOnMainThread.EnqueueAndWaitForCompletion(Disable);
			void Disable()
			{
				foreach (var element in renderElements.Skip(index))
				{
					element.UnSetMesh();
				}
			}
		}

		private ConnectionRendererElement GetOrCreateElement(int index)
		{
			if(renderElements.Count > index)
			{
				return renderElements[index];
			}

			var element = CreateElement();
			renderElements.Add(element);
			return element;
		}

		private ConnectionRendererElement CreateElement()
		{
			var go = new GameObject();
			go.name = $"Element #{renderElements.Count}";
			go.transform.parent = transform;
			go.transform.localPosition = Vector3.zero;
			go.transform.localScale = Vector3.one;
			go.transform.localRotation = Quaternion.identity;

			var element = go.AddComponent<ConnectionRendererElement>();

			return element;
		}

		private void Tasked_GenerateMesh(CubicBezierCurve curve, CurveSpec spec)
		{
			try
			{
				RunOnMainThread.EnqueueAndWaitForCompletion(SetPosition);
				void SetPosition()
				{
					if (null == gameObject) return; // Destroyed while running

					gameObject.transform.parent.position = curve.Position.ToUnity();
				}

				if (IsPhysics)
				{
					Tasked_GenerateMesh_Physics(curve, spec);
				}
				else
				{
					Tasked_GenerateMesh_Visual(curve, spec);
				}
			}
			catch (Exception e)
			{
				if (spec == Globals.Defaults.FallbackCurveSpec)
				{
					// Failed even with the fallback spec, something has gone really wrong.
					throw new Exception($"Failed to generate data for curve even with Fallback CurveSpec.", e);
				}
				else if (spec == Globals.Defaults.ErrorCurveSpec)
				{
					// Failed even with the fallback spec, something has gone really wrong.
					throw new Exception($"Failed to generate data for curve even with Error CurveSpec.", e);
				}
				else
				{
					// User spec could be incomplete or nonsensical.
					// Try using the fallback spec so that there is at least something to click on.

					Debug.LogException(e, this);

					Tasked_GenerateMesh(curve, Globals.Defaults.ErrorCurveSpec);
				}
			}
		}

		private CurveDecorator.ResultData GenerateMesh(CubicBezierCurve curve, CurveSpec spec)
		{
			CurveDecorator.ResultData DoStuff()
			{
				var decorator = new CurveDecorator();
				decorator.GenerateMesh = true;
				decorator.GeneratePhys = false;
				decorator.GenerateTrack = false;
				var result = decorator.Generate(curve, spec, "ConnectionRenderer");

				return result;
			}

			if (Globals.Constants.ForceSingleThreaded)
			{
				return RunOnMainThread.EnqueueAndWaitForCompletion(DoStuff);
			}
			else
			{
				return DoStuff();
			}
		}

		private CurveDecorator.ResultData GeneratePhys(CubicBezierCurve curve, CurveSpec spec)
		{
			CurveDecorator.ResultData DoStuff()
			{
				var decorator = new CurveDecorator();
				decorator.GenerateMesh = false;
				decorator.GeneratePhys = true;
				decorator.GenerateTrack = false;
				var result = decorator.Generate(curve, spec, "ConnectionRenderer");

				return result;
			}

			if (Globals.Constants.ForceSingleThreaded)
			{
				return RunOnMainThread.EnqueueAndWaitForCompletion(DoStuff);
			}
			else
			{
				return DoStuff();
			}
		}
	}
}
