﻿// Copyright 2022-2025 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections.Generic;

using BinaryDataModel.Converters;

using LuaIntegration;

using Tools;

using Shared.Serialization;

using UnityEngine;

using Vector2 = OpenTK.Mathematics.Vector2;
using Vector3 = OpenTK.Mathematics.Vector3;
using System.Linq;
using CurveGraph.Interface;

namespace CurveGraph
{
	[DisallowMultipleComponent]
	public class CurveSaveState : ThingSaveManager<CurveNode>
	{
		private ConnectionUIDProvider ConnectionUIDProvider => ConnectionUIDProvider.Instance;

		void Reset()
		{
			saveFilePath = "Curves";
			thingName_Single = "Curve";
			thingName_Multiple = "Curves";
			thingsContainerName = "CurveGraphs";

			AllowLoad = true;
			AllowSave = true;
		}

		/// <inheritdoc />
		protected override void OnAwake()
		{
			defaultSerializer = Serializer.Custom;

			if (EditorSettingsManager.Settings.CurveGraphSettings.SaveFileBackupBinary)
			{
				backupSerializer = Serializer.BinaryFormatter;
			}

			defaultDeserializer = Serializer.Custom;
			deserializeFallbackOrder = new[] { Serializer.DataContract, Serializer.BinaryFormatter, };

			base.OnAwake();

			SerializationHelper.DContract.AddKnownTypes
				(typeof(List<CurveNode>), new[] { typeof(ConnectionCustomData) });

			spec.AddType(typeof(ConnectionCustomData));
			spec.TypeSpecifications[typeof(ConnectionCustomData)].SerializedName = "CurveGraph.ConnectionCustomData";
			spec.TypeSpecifications[typeof(CurveNode)].ByRef = true;
			spec.TypeSpecifications[typeof(Connection)].ByRef = true;
			spec.AddType(typeof(ConnectionDecoratorData));
		}

		/// <summary>
		/// Called before clearing the current state in preparation for loading.
		/// </summary>
		public static event Action BeforeClearState;

		/// <inheritdoc />
		public override void Clear()
		{
			BeforeClearState?.Invoke();
			// Remove any existing.
			foreach (Transform t in thingsContainer.transform)
			{
				DestroyOrImmediate(t.gameObject);
			}

			CurveGraphConnectionManager.Instance.Clear();
			ConnectionUIDProvider.Reset();
		}

		public void LoadAdditive(string fileName)
		{
			string oldSaveFilePath = saveFilePath;
			saveFilePath = fileName;
			try
			{
				Load();
			}
			finally
			{
				saveFilePath = oldSaveFilePath;
			}
		}

		/// <inheritdoc />
		public override List<CurveNode> GetDataFromWorld()
		{
			var list = new List<CurveNode>();

			foreach (Transform t in thingsContainer.transform)
			{
				var child = t.gameObject;

				var nodeBehaviour = child.GetComponent<CurveNodeBehaviour>();

				if (null == nodeBehaviour) continue;

				list.Add(nodeBehaviour.node);
			}

			try
			{
				int counter = 0;
				if (list.Count > 0)
				{
					foreach (var conn in list[0].NetworkConnections(list))
					{
						counter++;

						ConnectionUIDProvider.EnsureHasID(conn);

						if (conn.data is ConnectionCustomData data)
						{
							data.CurveSpec = data.CurveSpec ?? Globals.Defaults.FallbackCurveSpec;
						}
					}
				}

				Console.WriteLine($"[{nameof(CurveSaveState)}] Saving {list.Count} nodes and {counter} connections.");
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.ToString());
			}

			// Sort by distance to origin, to force a consistent order in the serialized data.
			int Comparer(CurveNode a, CurveNode b)
			{
				int r = a.position.LengthSquared.CompareTo(b.position.LengthSquared);
				if (r != 0) return r;

				// Compare components
				r = a.position.X.CompareTo(b.position.X);
				if (r != 0) return r;

				r = a.position.Y.CompareTo(b.position.Y);
				if (r != 0) return r;

				r = a.position.Z.CompareTo(b.position.Z);
				if (r != 0) return r;

				// Give up.
				return 0;
			}

			list.Sort(Comparer);

			return list;
		}

		/// <inheritdoc />
		public override void SetDataToWorld(List<CurveNode> data)
		{
#if DEBUG
			int counter = 0;
			if (data.Count > 0)
			{
				foreach (var conn in data[0].NetworkConnections(data))
				{
					counter++;
				}
			}

			Console.WriteLine($"[{nameof(CurveSaveState)}] Loaded {data.Count} nodes and {counter} connections.");
#endif

			int removeCount = 0;

			int count = 0;
			foreach (var node in data)
			{
				if (! Shared.VectorExt.IsFinite(node.Position)
				 || ! Shared.VectorExt.IsFinite(node.Direction)
				 || node.Direction == Vector3.Zero
				 || ! Shared.VectorExt.IsFinite(node.Up)
				 || node.Up == Vector3.Zero
				 || ! Shared.VectorExt.IsFinite(node.Scale)
				 || node.Scale == Vector2.Zero
				 || ! float.IsFinite(node.Roll))
				{
					removeCount++;

					Console.WriteLine($"Ignoring invalid CurveNode because it has NaN, Infinity or invalid 0. '{node}'");

					node.ClearConnections();
					continue;
				}

				var go = Instantiate(RestorePrefab, thingsContainer.transform);
				go.name = $"{thingName_Single}_{count++}";
				var behaviour = go.GetComponent<CurveNodeBehaviour>();
				behaviour.node = node;

				go.transform.localPosition = node.Position.ToUnity();
				go.transform.forward = node.Direction.ToUnity(); // todo: normalize?
			}

			if (removeCount > 0)
			{
				Debug.LogError($"Removed {removeCount} invalid CurveNodes ({data.Count} total) because they had NaN, Infinity or invalid 0. See the log for more details.", this);
			}

			// the samples of the curves aren't serialized, so we need to trigger their generation.
			if (data.Count != 0)
			{
				foreach (Connection connection in data[0].NetworkConnections(data))
				{
					connection.FixupAfterDeserialization(null);
				}

				ConnectionUIDProvider.Loaded(data[0].NetworkConnections(data));
			}
			else
			{
				ConnectionUIDProvider.Loaded(Enumerable.Empty<Connection>());
			}
		}
	}
}
