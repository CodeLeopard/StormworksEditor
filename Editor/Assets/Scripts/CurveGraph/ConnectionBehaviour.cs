﻿// Copyright 2022-2025 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;

using Behaviours;

using Control;

using CurveGraph.Interface;

using GUI;

using Shared;

using Tools;

using LuaIntegration;

using UnityEngine;

namespace CurveGraph
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(SelectionEventReceiver))]
	public class ConnectionBehaviour : M0noBehaviour, IInspectorPanelContentProvider
	{
		private InspectorContentProvider inspectorContentProvider;

		private SelectionEventReceiver selectionEvents;

		[SerializeField]
		[HideInInspector]
		internal CurveGraphConnectionManager manager;

		public Connection Connection { get; private set; }


		private CurveNodeBehaviour startNodeBehaviour;
		private CurveNodeBehaviour endNodeBehaviour;

		public CurveNodeBehaviour StartNodeBehaviour => startNodeBehaviour;
		public CurveNodeBehaviour EndNodeBehaviour => endNodeBehaviour;

		internal ConnectionRenderer MeshRenderer;
		internal ConnectionRenderer PhysRenderer;

		public int UniqueId { get; private set; } = uniqueIdCounter++;


		private static int uniqueIdCounter = 0;


		/// <inheritdoc />
		protected override void OnAwake()
		{
			selectionEvents = GetRequiredComponent<SelectionEventReceiver>();
			selectionEvents.AllowCopy = false;
			selectionEvents.AllowDelete = true;

			selectionEvents.OnSelect += SelectionEvents_OnSelect;
			selectionEvents.OnDeSelect += SelectionEvents_OnDeSelectOrDestroy;
			selectionEvents.OnBeforeDelete += SelectionEvents_OnDeSelectOrDestroy;

			inspectorContentProvider = _newInspectorContentProvider(this);
		}

		public void AssignConnection(Connection connection)
		{
			Connection = connection;

			gameObject.name = $"Connection#{ConnectionUIDProvider.Instance.EnsureHasID(connection)}";
		}


		private void SelectionEvents_OnSelect(Transform obj)
		{
			if (null == startNodeBehaviour || null == endNodeBehaviour)
			{
				GetNodes();
			}

			startNodeBehaviour?.SetHighlight_StartNode();
			endNodeBehaviour?.SetHighlight_EndNode();
		}

		private void SelectionEvents_OnDeSelectOrDestroy(Transform obj)
		{
			startNodeBehaviour?.SetHighlight_Idle();
			endNodeBehaviour?.SetHighlight_Idle();
		}


		private void GetNodes()
		{
			CurveNodeBehaviour.NodeBehaviourMap.TryGetValue(Connection.StartNode, out startNodeBehaviour);
			CurveNodeBehaviour.NodeBehaviourMap.TryGetValue(Connection.EndNode, out endNodeBehaviour);
		}

		/// <inheritdoc />
		protected override void OnEnableAndAfterStart()
		{
			OnValidate();
		}


		void OnValidate()
		{
			// todo: understaand why this is needed.
			if (Connection == null) return;

			if (! Application.isPlaying) return;
			var data = Connection.data as ConnectionCustomData ?? Globals.Defaults.DefaultConnectionCustomData;
			// Globals will return a new instance each time.


			Connection.data = data;

			if(null != Connection.curve)
				Connection.StartNode?.RaiseNodeValuesChanged(false);
		}


		private void OnDestroy()
		{
			if (! Connection.TryRemove())
			{
				// todo: why does this fail? could point to a real issue.
				//Debug.LogWarning($"Failed to remove connection '{name}' in OnDestroy. This could mean things are broken in the {nameof(CurveGraphConnectionManager)} or elsewhere.", this);
			}
		}


		/// <inheritdoc />
		object IInspectorPanelContentProvider.GetInspectorContent()
		{
			return inspectorContentProvider;
		}

		#region SidePanel

		private static Func<ConnectionBehaviour, InspectorContentProvider> _newInspectorContentProvider;

		private class InspectorContentProvider
		{
			private readonly ConnectionBehaviour behaviour;
			private Connection connection => behaviour.Connection;
			private CubicBezierCurve curve => connection.curve;


			[VisibleProperty(isReadonly: true)]
			public ConnectionNodeSettings GreenNodeSettings => connection.startNodeSettings;

			[VisibleProperty(isReadonly: true)]
			public ConnectionNodeSettings RedNodeSettings => connection.endNodeSettings;

			[VisibleProperty(isReadonly:true)]
			public object Data => connection.data;

			[VisibleProperty(isReadonly: true)]
			public CubicBezierCurve Curve => connection.curve;

			[VisibleProperty]
			public float GreenMagnitudeOverride
			{
				get
				{
					if (curve.StartMagnitudeOverride is > 0)
					{
						return curve.StartMagnitudeOverride.Value;
					}

					return 0;
				}
				set
				{
					if (value > 0)
					{
						curve.StartMagnitudeOverride = value;
					}
					else
					{
						curve.StartMagnitudeOverride = null;
					}
				}
			}

			[VisibleProperty]
			public float RedMagnitudeOverride
			{
				get
				{
					if (curve.EndMagnitudeOverride is > 0)
					{
						return curve.EndMagnitudeOverride.Value;
					}

					return 0;
				}
				set
				{
					if (value > 0)
					{
						curve.EndMagnitudeOverride = value;
					}
					else
					{
						curve.EndMagnitudeOverride = null;
					}
				}
			}


			private const float ms_2_kmh = 3.6f;
			private const float kmh_2_ms = 1 / ms_2_kmh;

			[VisibleProperty(nameOverride: "Track speed (km/h)")]
			public float TrackSpeed_KMH
			{
				get
				{
					if (connection.decoratorData?.TrackSpeed_ms == null) return 0;
					return connection.decoratorData.TrackSpeed_ms.Value * ms_2_kmh;
				}
				set
				{
					if (value != 0 && connection.decoratorData == null)
					{
						connection.decoratorData = new ConnectionDecoratorData();
					}

					if (connection.decoratorData != null)
					{
						connection.decoratorData.TrackSpeed_ms = value * kmh_2_ms;
					}

					MaybeClearDecoratorData();
				}
			}

			[VisibleProperty(nameOverride: "Superelevation (mm)")]
			public float SuperElevationMM
			{
				get
				{
					if (connection.decoratorData?.SuperElevation_mm == null) return 0;
					return connection.decoratorData.SuperElevation_mm.Value;
				}
				set
				{
					if (value != 0 && connection.decoratorData == null)
					{
						connection.decoratorData = new ConnectionDecoratorData();
					}

					if (connection.decoratorData != null)
					{
						connection.decoratorData.SuperElevation_mm = value;
					}

					MaybeClearDecoratorData();
				}
			}


			[VisibleProperty(nameOverride: "Track Name")]
			public string TrackName
			{
				get => connection.decoratorData?.TrackName ?? string.Empty;
				set
				{
					bool nullOrEmpty = string.IsNullOrEmpty(value);

					if (!nullOrEmpty && connection.decoratorData == null)
					{
						connection.decoratorData = new ConnectionDecoratorData();
					}

					if (!nullOrEmpty)
					{
						connection.decoratorData.TrackName = value;
					}

					MaybeClearDecoratorData();
				}
			}


			[VisibleProperty(nameOverride: "Catenary Group")]
			public string CatenarySpec
			{
				get => connection.decoratorData?.CatenarySpec ?? string.Empty;
				set
				{
					bool nullOrEmpty = string.IsNullOrEmpty(value);

					if (!nullOrEmpty && connection.decoratorData == null)
					{
						connection.decoratorData = new ConnectionDecoratorData();
					}

					if (!nullOrEmpty)
					{
						connection.decoratorData.CatenarySpec = value;
					}

					MaybeClearDecoratorData();
				}
			}

			private void MaybeClearDecoratorData()
			{
				if (connection.decoratorData == null) return;
				if (connection.decoratorData.TrackSpeed_ms != 0
					|| connection.decoratorData.SuperElevation_mm != 0
				) return;

				connection.decoratorData = null;
			}

			private InspectorContentProvider(ConnectionBehaviour b)
			{
				behaviour = b;
			}

			static InspectorContentProvider()
			{
				_newInspectorContentProvider = (value) => new InspectorContentProvider(value);
			}
		}
		static ConnectionBehaviour()
		{
			System.Runtime.CompilerServices.RuntimeHelpers.RunClassConstructor(typeof(InspectorContentProvider).TypeHandle);
		}

		#endregion SidePanel
	}
}
