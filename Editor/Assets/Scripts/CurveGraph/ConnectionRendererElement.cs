﻿using Behaviours;
using BinaryDataModel.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.CurveGraph
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(MeshFilter))]
	[RequireComponent(typeof(MeshRenderer))]
	[RequireComponent(typeof(MeshCollider))]
	public class ConnectionRendererElement : M0noBehaviour
	{
		private MeshFilter meshFilter;
		private MeshRenderer meshRenderer;
		private MeshCollider meshCollider;

		protected override void OnAwake()
		{
			meshFilter = GetRequiredComponent<MeshFilter>();
			meshRenderer = GetRequiredComponent<MeshRenderer>();
			meshCollider = GetRequiredComponent<MeshCollider>();
		}


		public void UnSetMesh()
		{
			if(null == gameObject) return; // Destroyed while task was running.

			meshFilter.sharedMesh = null;
			meshCollider.sharedMesh = null;
			gameObject.SetActive(false);
		}

		public void SetMesh(Mesh mesh, Material[] materials)
		{
			if (null == gameObject) return; // Destroyed while task was running.

			meshFilter.sharedMesh = mesh;
			meshRenderer.materials = materials;
			meshCollider.sharedMesh = mesh;
			gameObject.SetActive(true);
		}
	}
}
