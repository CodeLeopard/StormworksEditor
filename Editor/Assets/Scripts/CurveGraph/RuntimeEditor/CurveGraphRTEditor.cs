﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections;
using System.Diagnostics;
using System.Linq;

using Behaviours;

using BinaryDataModel.Converters;

using Control;

using DataTypes.Extensions;

using LuaIntegration;

using GUI;
using GUI.SidePanel;

using RuntimeGizmos;

using Shared;

using Tools;

using UnityEditor;

using UnityEngine;

using Debug = UnityEngine.Debug;

using S = GUI.SidePanel.EditPanel.SpecElement;

namespace CurveGraph.RuntimeEditor
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(Camera))]
	public class CurveGraphRTEditor : M0noBehaviour
	{
		private new Camera camera;

		[SerializeField]
		private GameObject _selectedItemPanel;
		private EditPanel _editPanel;

		private TransformGizmo transformGizmo;

		private Vector3 draggedNodeOffset;
		private float draggedNodeVerticalOffset;
		private float draggedNodeDistanceAboveTerrain;

		private byte _nextNodeID;
		private byte nextNodeID => unchecked(_nextNodeID++);




		private LayerMask mask_terrain;
		private int layer_terrain;

		private LayerMask mask_nodeHandle;
		private int layer_nodeHandle;

		private LayerMask mask_track;
		private int layer_track;


		private CurveNodeBehaviour activeHandle;
		private CurveNode activeNode => activeHandle.node;


		private const KeyCode NewNodeKey = KeyCode.LeftAlt;
		private const KeyCode SplitNodeKey = KeyCode.V;
		private const KeyCode SplitTrackKey = KeyCode.H;


		[HideInInspector]
		public GameObject NewSplineParent;


		[SerializeField]
		public GameObject NewCurveNodePrefab;


		[SerializeField]
		public bool DefaultAutoSmooth = true;

		[SerializeField]
		[Range(float.Epsilon, 1)]
		public float DefaultCurvature = 0.5f;


		[SerializeField]
		public float JunctionCreationDistance = 1f;

		[SerializeField]
		public float NewNodeHeightOffset = 2f; // todo: define this in the CrossSectionSpec instead.




		public const string CurveHandleTag = "CurveNodeHandle";

		public static CurveGraphRTEditor Instance { get; protected set; }


		#region UnityMessages
		protected override void OnAwake()
		{
			Instance = this;

			camera = GetRequiredComponent<Camera>();
			transformGizmo = GetRequiredComponent<TransformGizmo>();

			if (null != _selectedItemPanel)
			{
				// if is for testing only, should always be present
				// but in some testing Scenes we don't need it.

				_editPanel = _selectedItemPanel.GetComponentInChildren<EditPanel>();
			}

			NewSplineParent = GameObject.FindGameObjectWithTag("WorldContainer").GetComponent<CurveSaveState>()?.thingsContainer;
			if (null == NewSplineParent)
			{
				Debug.LogWarning($"{nameof(CurveGraphRTEditor)} could not find the parent for new Curve Nodes.", this);
			}

			LayerMaskExt.NameToMask("Terrain", out mask_terrain,    out layer_terrain);
			LayerMaskExt.NameToMask("3DGUI",   out mask_nodeHandle, out layer_nodeHandle);
			LayerMaskExt.NameToMask("Tracks",  out mask_track,      out layer_track);
		}

		protected override void OnEnableAndAfterStart()
		{
			MouseEvents.OnWorldMouseDown += MouseEvents_OnWorldMouseDown;

			CurveNodeBehaviour.OnSelect += OnNodeSelect;
			CurveNodeBehaviour.OnDeSelect += OnNodeDeSelect;
		}


		private void Update()
		{
			if (InputManagement.AnyModifierKeyPressed) return;
			if (InputManagement.TextInputFocused) return;
			// Todo: is window focussed.

			if (Input.GetKey(SplitTrackKey))
			{
				StartSplitCurveInteractionWithSelectedCurve();
			}
		}


		/// <inheritdoc />
		protected override void OnDisableIfStarted()
		{
			MouseEvents.OnWorldMouseDown -= MouseEvents_OnWorldMouseDown;

			CurveNodeBehaviour.OnSelect -= OnNodeSelect;
			CurveNodeBehaviour.OnDeSelect -= OnNodeDeSelect;
		}


		#endregion UnityMessages

		#region EventHandlers

		private void MouseEvents_OnWorldMouseDown()
		{
			if (!MouseEvents.MouseLeft) return;

			if (MyCustomRayCast(out Ray ray, out var hit, mask_nodeHandle | mask_terrain | mask_track, QueryTriggerInteraction.Collide))
			{
				var hitGO = hit.transform.gameObject;
				var layer = hitGO.layer;

				if (layer == layer_nodeHandle)
				{
					// SelectionManager will select it and trigger an event we listen for.
					// SelectionManager can delete it.

					// SelectionManager will not fire event for re-selecting the same object.
					if (Input.GetKey(NewNodeKey)
					 && (activeHandle?.gameObject == hitGO
					    || SelectionManager.Instance.FirstTarget?.gameObject == hitGO)
					    )
					{
						SelectNode(hitGO.GetComponent<CurveNodeBehaviour>());
						AddNodeOrJunction();
					}
				}
				else if (layer == layer_terrain)
				{
					ClickedTerrain();
				}
				else if (layer == layer_track)
				{
					// SelectionManager will select it.
					// SelectionManager can delete it.
				}


				return; // #### #### #### #### #### #### #### #### #### #### #### #

				void ClickedTerrain()
				{
					if (Input.GetKey(NewNodeKey))
					{
						if (activeHandle == null)
						{
							NewSpline();
						}
						else
						{
							AddNodeOrJunction();
						}
					}
				}
			}
		}

		private void OnNodeSelect(CurveNodeBehaviour nodeHandle)
		{
			if (!nodeHandle) return;
			activeHandle = nodeHandle;

			if (Input.GetKey(NewNodeKey))
			{
				AddNodeOrJunction(); // todo: currently doesn't make junctions, inserts node instead.
			}
			else if (Input.GetKey(SplitNodeKey))
			{
				SplitNode();
			}
		}


		private void OnNodeDeSelect(CurveNodeBehaviour nodeHandle)
		{
			// When SelectNode is used, this event will fire for the previously
			// selected node, so we should only deselect here when the deselected node
			// was the active one.
			if (activeHandle == nodeHandle)
			{
				activeHandle = null;
			}
		}


		#endregion EventHandlers


		#region HighlightNearby
		/// <summary>
		/// Responsible for UnHighlighting <see cref="highlighted"/> when
		/// <see cref="HighlightPotentialJunctions"/> is not getting called anymore.
		/// It is started by that method and monitors <see cref="lastFrameCount"/> as trigger.
		/// </summary>
		private Coroutine highlightExpireWatchdogRoutine;

		/// <summary>
		/// The indicator used to notice that <see cref="HighlightPotentialJunctions"/>
		/// is not getting called anymore.
		/// This will signal to <see cref="highlightExpireWatchdogRoutine"/> that it needs to
		/// set the color back to idle.
		/// </summary>
		private int lastFrameCount;

		/// <summary>
		/// The NodeBehaviour that is currently highlighted.
		/// </summary>
		private CurveNodeBehaviour highlighted;

		/// <summary>
		/// Highlight the node that would junction with <paramref name="node"/>.
		/// Also Un-Highlights the potential junction if it went out of range again.
		/// </summary>
		/// <param name="node"></param>
		public void HighlightPotentialJunctions(CurveNodeBehaviour node)
		{
			if (null == node) return;

			lastFrameCount = Time.frameCount;
			highlightExpireWatchdogRoutine ??= StartCoroutine(HighlightExpireWatchdog());

			var lastHighlighted = highlighted;

			highlighted = GetJunctionCandidate(node);

			if (lastHighlighted != null
			 && lastHighlighted != highlighted)
			{
				lastHighlighted.SetHighlight_Idle();
			}

			if (null == highlighted)
			{
				return;
			}

			if (IsJunctionAllowedWith(node, highlighted))
			{
				highlighted.SetHighlight_JunctionCandidate();
			}
			else
			{
				highlighted.SetHighlight_JunctionNotAllowed();
			}
		}

		private IEnumerator HighlightExpireWatchdog()
		{
			while (Time.frameCount <= lastFrameCount + 1)
			{
				yield return null;
			}

			if (null != highlighted)
			{
				highlighted.SetHighlight_Idle();
			}

			highlighted = null;
			highlightExpireWatchdogRoutine = null;
		}

		#endregion HighlightNearby

		#region Spline Interactions


		[DebuggerStepThrough]
		private void SelectNode(CurveNodeBehaviour newSelection)
		{
			activeHandle = newSelection;
			if (null != newSelection)
			{
				SelectionManager.Instance.ClearAndAddTarget(newSelection.transform);
			}
		}

		public void InstantiateCurveNode(out CurveNode node, out CurveNodeBehaviour handle)
		{
			var go = Instantiate(NewCurveNodePrefab, NewSplineParent.transform);
			go.name = $"CurveNode #{nextNodeID}";

			handle = go.GetRequiredComponent<CurveNodeBehaviour>();
			node = handle.node;

			node.autoSmooth = DefaultAutoSmooth;
			node.curvature = DefaultCurvature;
		}


		private void NewSpline()
		{
			if (!MyCustomRayCast(out var ray, out var hit, mask_terrain, QueryTriggerInteraction.Ignore))
				return;

			InstantiateCurveNode(out var nodeA, out var handleA);
			InstantiateCurveNode(out var nodeB, out var handleB);

			var startPosition = hit.point + Vector3.up * NewNodeHeightOffset;
			nodeA.Position = startPosition.ToOpenTK();

			nodeB.Position = (startPosition + new Vector3(0, 0, 0.01f)).ToOpenTK(); // offset to prevent problems with curve.

			handleA.UpdateToMatchNodeValues();
			handleB.UpdateToMatchNodeValues();

			nodeA.CreateConnection(nodeB);

			draggedNodeDistanceAboveTerrain = GetHeightOffset(startPosition);
			SelectNode(handleB);

			NewNodeCommon();
		}

		private void AddNodeOrJunction()
		{
			// todo: behaviour needs to be:
			// - [*] Add node at end
			// - [ ] Or make junction at selected node.

			if (!MyCustomRayCast(out var ray, out var hit, mask_terrain, QueryTriggerInteraction.Ignore))
				return;

			draggedNodeDistanceAboveTerrain = GetHeightOffset();
			var newNodePosition = hit.point + Vector3.up * draggedNodeDistanceAboveTerrain;
			var direction = activeNode.Direction;

			InstantiateCurveNode(out var newNode, out var newHandle);
			newNode.position = newNodePosition.ToOpenTK();
			newNode.direction = direction;
			newNode.RaiseNodeValuesChanged(false);

			// Try to copy properties from the existing node.
			var conn = activeNode.Connections.Values.FirstOrDefault();

			var newConn = activeNode.CreateConnection(newNode);
			newConn.data ??= Globals.Defaults.DefaultConnectionCustomData;

			if (null != conn
			 && conn.data is ConnectionCustomData from
			 && newConn.data is ConnectionCustomData to)
			{
				to.CurveSpecFilePath     = from.CurveSpecFilePath;
			}

			newNode.autoSmooth = activeNode.AutoSmooth;
			newNode.curvature = activeNode.Curvature;


			SelectNode(newHandle);

			NewNodeCommon();
		}

		private void NewNodeCommon()
		{
			// The comment below is how it's supposed to work.
			//transformGizmo.ForceStartRaycastDrag();
			// Below is what we actually need to do to make it work.
			// Because we are clicking the transformGizmo will start transforming on it's own accord.
			// todo: make this not confusing and strange.
			transformGizmo.transformType = TransformType.RayCast;

			SelectionManager.DoNotSelectThisFrame();
		}

		private void SplitNode()
		{
			activeNode.Split();
		}


		#region SplitCurve

		private class SplitCurveInfoThing : IInspectorPanelContentProvider
		{
			private GameObject SplitPlaneVis;

			public Connection Connection { get; private set; }

			internal event Action TimeChanged;
			internal event Action Apply;

			[VisibleProperty(nameOverride: "Apply")]
			public bool applyBool
			{
				get => false;
				set
				{
					if (value)
					{
						InvokeApply();
					}
				}
			}

			private float distance;

			[VisibleProperty]
			public float Distance
			{
				get => distance;
				set
				{
					float safe = Mathf.Clamp(value, 0, Connection.curve.Length);
					if (safe == distance) return;

					distance = safe;

					time = Connection.curve.GetSampleAtDistance(distance).timeInCurve;

					InvokeTimeChanged();
				}
			}


			private float time;

			[VisibleProperty]
			public float Time
			{
				get => time;
				set
				{
					float safe = Mathf.Clamp(value, 0, 1);
					if(safe == time) return;

					time = safe;

					distance = Connection.curve.GetSample(time).distanceInCurve;

					InvokeTimeChanged();
				}
			}

			[VisibleProperty]
			public Vector3 Position => Connection.curve.GetSample(time).location.ToUnity();

			[VisibleProperty]
			public Vector3 Velocity => Connection.curve.GetSample(time).velocity.ToUnity();

			[VisibleProperty]
			public float TangentMagnitude => Connection.curve.GetSample(time).velocity.Length;

			internal SplitCurveInfoThing(Connection c)
			{
				Connection = c;


				SplitPlaneVis = GameObject.CreatePrimitive(PrimitiveType.Sphere);
				SplitPlaneVis.name = "Curve Split Plane";
				var ren = SplitPlaneVis.GetRequiredComponent<MeshRenderer>();
				ren.material = Globals.Materials.SplitPlaneVis;
				var col = SplitPlaneVis.GetRequiredComponent<SphereCollider>();
				col.enabled = false;

				SplitPlaneVis.transform.localScale = new Vector3(5, 5, 0.1f);

				Time = 0.5f;
			}

			~SplitCurveInfoThing()
			{
				Cancel();
			}

			internal void Cancel()
			{
				if (null != SplitPlaneVis)
					Destroy(SplitPlaneVis);
			}

			private void UpdateVis()
			{
				SplitPlaneVis.transform.position = Position;
				SplitPlaneVis.transform.forward = Velocity.normalized;
			}

			private void InvokeTimeChanged()
			{
				UpdateVis();

				TimeChanged?.Invoke();
			}

			private void InvokeApply()
			{
				Destroy(SplitPlaneVis);

				Apply?.Invoke();
			}


			/// <inheritdoc />
			public object GetInspectorContent()
			{
				return this;
			}
		}

		private SplitCurveInfoThing SplitCurveActivity = null;

		private void StartSplitCurveInteractionWithSelectedCurve()
		{
			if (SelectionManager.Instance.FirstTarget == null) return;

			var b = SelectionManager.Instance.FirstTarget.GetComponent<ConnectionBehaviour>();
			if (null == b) return;
			if (null != SplitCurveActivity)
			{
				return;
			}

			SplitCurveActivity = new SplitCurveInfoThing(b.Connection);
			SplitCurveActivity.Apply += SplitCurveApply;

			SidePanelManager.Instance.Target = SplitCurveActivity;

			SelectionManager.Instance.SelectionChanged -= Instance_SelectionChanged;
			SelectionManager.Instance.SelectionChanged += Instance_SelectionChanged;
		}

		private void Instance_SelectionChanged()
		{
			if (SplitCurveActivity != null)
			{
				SplitCurveActivity.Cancel();
				SplitCurveActivity = null;
			}
		}

		private void SplitCurveApply()
		{
			var c = SplitCurveActivity.Connection;

			(var node, var conn) = c.SplitAtTime(SplitCurveActivity.Time);

			InstantiateCurveNode(out _, out var nodeBehaviour);

			nodeBehaviour.SetNode(node);

			// Copy CurveSpec etc.
			conn.data = c.data.CloneForMultiThreadedReading();

			if(CurveGraphConnectionManager.ConnectionMap.TryGetValue(conn, out ConnectionBehaviour behaviour))
			{

			}
			else
			{

			}

			SelectionManager.Instance.SelectionChanged -= Instance_SelectionChanged;
			SelectionManager.Instance.ClearAndAddTarget(nodeBehaviour.transform);
#if UNITY_EDITOR
			Selection.activeGameObject = nodeBehaviour.gameObject;
#endif
			SplitCurveActivity = null;
		}



		#endregion SplitCurve


		private CurveNodeBehaviour GetJunctionCandidate(CurveNodeBehaviour targetHandle)
		{
			var nearby = FindNearbyWithTag(
			                               targetHandle.transform.position,
			                               CurveHandleTag,
			                               JunctionCreationDistance
			                              ).Where(g => g != targetHandle.gameObject);

			foreach (var go in nearby)
			{
				if (go == targetHandle.gameObject) continue;

				var handle = go.GetComponent<CurveNodeBehaviour>();

				if (null == handle) continue;

				// We need the behaviour to be consistent with HighlightPotentialJunction
				if (!IsJunctionAllowedWith(targetHandle, handle)) break;


				return handle;
			}

			return null;
		}

		/// <summary>
		/// Try to make a junction at <paramref name="heldHandle"/>
		/// using the closest nearby <see cref="CurveNode"/>.
		/// If a junction is made <paramref name="heldHandle"/> is <see cref="CurveNode.Absorb"/>ed into
		/// the junction and the <paramref name="heldHandle"/> is <see cref="UnityEngine.Object.Destroy"/>ed.
		/// </summary>
		/// <param name="heldHandle"></param>
		/// <returns>true if a junction was made, false otherwise.</returns>
		public bool MaybeMakeJunction(CurveNodeBehaviour heldHandle)
		{
			var junctionHandle = GetJunctionCandidate(heldHandle);
			if (null == junctionHandle) return false;

			junctionHandle.node.Absorb(heldHandle.node);

			RunOnMainThread.Enqueue(NextTick, -1000);
			void NextTick()
			{
				SelectNode(junctionHandle);

				// Not needed: the ConnectionManager will destroy the orphan node.
				//Destroy(heldHandle.gameObject);
			}

			return true;
		}

		/// <summary>
		/// Check if, according to the logic of Stormworks a junction can be made out of the specified nodes.
		/// </summary>
		/// <param name="ba">Behaviour a</param>
		/// <param name="bb">Behaviour B</param>
		/// <returns>true if a junction may be made, false otherwise.</returns>
		public static bool IsJunctionAllowedWith(CurveNodeBehaviour ba, CurveNodeBehaviour bb)
		{
			var a = ba.node;
			var b = bb.node;

			if (!a.CanConnectTo(b)) return false;

			if (a.Connections.Count > 2 || b.Connections.Count > 2)
			{
				// CurveNode.CanConnectTo() would allow multiple connections, but we cannot.
				return false;
			}

			return true;
		}


		#endregion Spline Interactions

		#region Misc Tools

		public bool MyCustomRayCast(out Ray ray, out RaycastHit hit, LayerMask mask, QueryTriggerInteraction triggerInteraction)
		{
			ray = camera.ScreenPointToRay(Input.mousePosition);

			if (!Physics.Raycast(ray, out hit, float.MaxValue, mask, triggerInteraction))
				return false;
			else
				return true;
		}

		public RaycastHit[] MyCustomRayCastMany(out Ray ray, LayerMask mask, QueryTriggerInteraction triggerInteraction)
		{
			ray = camera.ScreenPointToRay(Input.mousePosition);

			var hits = Physics.RaycastAll(ray, float.MaxValue, mask, triggerInteraction);

			Array.Sort(hits, (a, b) => a.distance.CompareTo(b.distance));

			return hits;
		}


		private float GetHeightOffset(Vector3? _abovePos = null)
		{
			const float upDist = 10;
			Vector3 abovePos;
			if (_abovePos.HasValue)
				abovePos = _abovePos.Value;
			else
				abovePos = activeHandle.transform.position;

			var rayStart = abovePos + (upDist * Vector3.up);
			var ray = new Ray(rayStart, Vector3.down);

			if (!Physics.Raycast(ray, out var hit, float.MaxValue, mask_terrain))
				return NewNodeHeightOffset;

			return hit.distance - upDist;
		}

		#endregion Misc Tools

		#region Static

		static CurveGraphRTEditor()
		{
			/*
			EditPanel.MkSpec
				(
				 typeof(CurveGraph.Connection)
			   , new EditPanel.SpecElement[]
				 {
					 new S(nameof(CurveGraph.Connection.startNodeSettings), "StartSettings")
				   , new S(nameof(CurveGraph.Connection.endNodeSettings),   "EndSettings")
				   , new S(nameof(CurveGraph.Connection.data),              "Data",  isReadonly: true)
				   , new S(nameof(CurveGraph.Connection.curve),             "Curve", isReadonly: true)
				 }
				);
			*/

			EditPanel.MkSpec
				(
				 typeof(CurveGraph.CubicBezierCurve)
			   , new S[]
				 {
					 new S(nameof(CurveGraph.CubicBezierCurve.Length), "Length", isReadonly: true),
				 }
				);

			EditPanel.MkSpec(typeof(CurveGraph.ConnectionNodeSettings), new EditPanel.SpecElement[]
			{
				new S(nameof(CurveGraph.ConnectionNodeSettings.SmoothingWeight))
			});
		}


		#endregion Static
	}
}
