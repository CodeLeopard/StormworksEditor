﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

using CustomSerialization.Specification;

using DataTypes.Attributes;
using DataTypes.SerializationSurrogates;

using Shared;
using Shared.Serialization;

using UnityEngine;

namespace Tools
{
	/// <summary>
	/// Framework for other save/load managers.
	/// </summary>
	public abstract class ThingSaveManager<TData> : AbstractThingSaveManager
	{
		public enum Serializer : byte
		{
			NONE,
			BinaryFormatter,
			SystemXml,
			DataContract,
			Custom,
		}


		protected const string defaultSaveFilePath = "Things";
		protected const string defaultThingName_Single = "Thing";
		protected const string defaultThingName_Multiple = "Things";
		protected const string defaultThinsContainerName = "ThingsContainer";

		protected const string binaryExtension = ".sav";
		protected const string xmlExtension = ".xml";

		protected const string systemXmlExtensionPrefix = ".sys";
		protected const string dataContractExtensionPrefix = ".dc";
		protected const string customSerializerExtensionPrefix = ".cs";

		public Serializer defaultSerializer = Serializer.NONE;
		public Serializer backupSerializer = Serializer.NONE;
		public Serializer defaultDeserializer = Serializer.NONE;
		public Serializer[] deserializeFallbackOrder = Array.Empty<Serializer>();

		public string saveFilePath = defaultSaveFilePath;
		public string thingName_Single = defaultThingName_Single;
		public string thingName_Multiple = defaultThingName_Multiple;
		public string thingsContainerName = defaultThinsContainerName;

		public string saveFileRoot => StormworksPaths.creatorToolkitData;
		public string saveFileFullPath => Path.Combine(saveFileRoot, saveFilePath);


		[InspectorReadonly]
		public GameObject thingsContainer;

		public GameObject RestorePrefab;

#if UNITY_EDITOR
		public bool DoSave = false;
		public bool DoClear = false;
		public bool DoLoad = false;
#endif

		public static event Action<List<TData>> Loaded;

		private static readonly BinaryFormatter bf = CreateFormatter();

		protected static GraphSerializationSpecification spec;

		#region UnityMessages

		void Reset()
		{
			saveFilePath        = defaultSaveFilePath;
			thingName_Single    = defaultThingName_Single;
			thingName_Multiple  = defaultThingName_Multiple;
			thingsContainerName = defaultThinsContainerName;

			AllowLoad = true;
			AllowSave = true;
		}


		protected override void OnAwake()
		{
			if (null == thingsContainer)
			{
				var t = transform.Find(thingsContainerName);
				if (null != t)
					thingsContainer = t.gameObject;
				else
				{
					thingsContainer = new GameObject(thingsContainerName);
					thingsContainer.hideFlags |= HideFlags.DontSaveInBuild | HideFlags.DontSaveInEditor;
					thingsContainer.transform.parent = transform;
				}
			}

			if (defaultSerializer   == Serializer.Custom
			 || defaultDeserializer == Serializer.Custom
			 || deserializeFallbackOrder.Contains(Serializer.Custom))
			{
				spec = SpecGenerator.CreateGraphSerializationSpecification(typeof(List<TData>));
			}
		}


		/// <inheritdoc />
		protected override void OnEnableAndAfterStart()
		{
			if (!Application.isPlaying) return;
			if (AllowLoad)
			{
				Load();
			}
		}


		/// <inheritdoc />
		protected override void OnDisableIfStarted()
		{
			if (!Application.isPlaying) return;
			if (AllowSave)
			{
				Save();
			}
		}

#if UNITY_EDITOR
		private void Update()
		{
			if (DoSave)
			{
				DoSave = false;
				Save();
			}

			if (DoClear)
			{
				DoClear = false;
				Clear();
			}

			if (DoLoad)
			{
				DoLoad = false;
				Load();
			}
		}
#endif

#endregion UnityMessages


		/// <summary>
		/// Create a instance of <see cref="BinaryFormatter"/> with <see cref="DefaultSerializationSurrogateProvider"/> already applied.
		/// </summary>
		/// <returns></returns>
		public static BinaryFormatter CreateFormatter()
		{
			var bf = new BinaryFormatter();
			bf.AddDefaultSurrogatesSelector();

			return bf;
		}


		private string GetSaveFilePath(Serializer serializer, bool prefix)
		{
			string extension;
			switch (serializer)
			{
				case Serializer.NONE:
				{
					throw new ArgumentException($"Invalid serializer selection '{serializer}'.");
				}
				case Serializer.BinaryFormatter:
				{
					extension = binaryExtension;
					break;
				}
				case Serializer.SystemXml:
				{
					extension = (prefix ? systemXmlExtensionPrefix : "") + xmlExtension;
					break;
				}
				case Serializer.DataContract:
				{
					extension = (prefix ? dataContractExtensionPrefix : "") + xmlExtension;
					break;
				}
				case Serializer.Custom:
				{
					extension = (prefix ? customSerializerExtensionPrefix : "") + xmlExtension;
					break;
				}
				default:
				{
					throw new ArgumentException($"Unknown {nameof(Serializer)} enum value: '{serializer}'.");
				}
			}

			return saveFileFullPath + extension;
		}

		#region Loading

		public abstract void SetDataToWorld(List<TData> data);


		/// <inheritdoc />
		public override void Load()
		{
			string fullPath = "<<<Error path not decided>>>";
			try
			{
				fullPath = GetSaveFilePath(defaultDeserializer, true);

				if (File.Exists(fullPath))
				{
					LoadFromFile(defaultDeserializer, fullPath);
					Console.WriteLine($"Loaded {thingName_Multiple} from '{fullPath}' using {defaultSerializer}.");
					return;
				}


				foreach (var serializer in deserializeFallbackOrder)
				{
					fullPath = GetSaveFilePath(serializer, false);

					if (! File.Exists(fullPath))
					{
						continue;
					}

					LoadFromFile(serializer, fullPath);
					Debug.Log
						($"Loaded {thingName_Multiple} from fallback option '{fullPath}' (using: {serializer}).", this);
					return;
				}

				Debug.LogWarning
					($"Could not load {thingName_Multiple} because the file (and fallbacks) could not be found.", this);
			}
			catch
			{
				AllowSave = false;

				Debug.LogWarning
					(
					 $"Loading of {thingName_Multiple} from '{fullPath}' failed. Auto-Saving is disabled to prevent accidental data loss."
				   , this
					);
				throw;
			}
		}

		private void LoadFromFile(Serializer serializer, string fullPath)
		{
			switch (serializer)
			{
				case Serializer.NONE:
				{
					throw new ArgumentException($"Invalid serializer selection '{serializer}'.");
				}
				case Serializer.BinaryFormatter:
				{
					LoadFromFileBinary(fullPath);
					return;
				}
				case Serializer.SystemXml:
				{
					LoadFromFile_SystemXml(fullPath);
					return;
				}
				case Serializer.DataContract:
				{
					LoadFromFile_DataContract(fullPath);
					return;
				}
				case Serializer.Custom:
				{
					LoadFromFile_Custom(fullPath);
					return;
				}
				default:
				{
					throw new ArgumentException
						($"Unknown {nameof(Serializer)} enum value: '{serializer}'.");
				}
			}
		}

		protected void LoadFromFileBinary(string fullPath)
		{
			using (var file = File.OpenRead(fullPath))
			{
				var list = (List<TData>)bf.Deserialize(file);

				SetDataToWorld(list);

				Loaded?.Invoke(list);
			}
		}


		protected void LoadFromFile_SystemXml(string fullPath)
		{
			using (var file = File.OpenRead(fullPath))
			{
				var list = SerializationHelper.Pure.LoadFromStream<List<TData>>(file, fullPath);

				SetDataToWorld(list);

				Loaded?.Invoke(list);
			}
		}

		protected void LoadFromFile_DataContract(string fullPath)
		{
			using (var file = File.OpenRead(fullPath))
			{
				var list = SerializationHelper.DContract.LoadFromStream<List<TData>>(file, fullPath);

				SetDataToWorld(list);

				Loaded?.Invoke(list);
			}
		}

		protected void LoadFromFile_Custom(string fullPath)
		{
			using (var file = File.OpenRead(fullPath))
			{
				var list = CustomSerialization.Serializer.Deserialize<List<TData>>(file, spec, new StreamingContext(StreamingContextStates.Persistence | StreamingContextStates.File));

				SetDataToWorld(list);

				Loaded?.Invoke(list);
			}
		}



		#endregion Loading

		#region Saving

		public abstract List<TData> GetDataFromWorld();

		/// <inheritdoc />
		public override void Save()
		{
			try
			{
				Save(defaultDeserializer);
			}
			finally
			{
				if (backupSerializer != Serializer.NONE)
				{
					Save(backupSerializer);
				}
			}
		}

		protected void Save(Serializer serializer)
		{
			using (var stream = new MemoryStream())
			{
				switch (serializer)
				{
					case Serializer.BinaryFormatter:
					{
						SaveToStream_Binary(stream);
						break;
					}
					case Serializer.SystemXml:
					{
						SaveToStream_SystemXml(stream);
						break;
					}
					case Serializer.DataContract:
					{
						SaveToStream_DataContract(stream);
						break;
					}
					case Serializer.Custom:
					{
						SaveToStream_Custom(stream);
						break;
					}
					default:
					{
						throw new ArgumentException($"unknown serializer '{defaultSerializer}'");
					}
				}

				// This construction prevents the file from being opened (erased),
				// then an exception occurring, and the file ending up empty or invalid.
				// Either it's written completely, ot isn't opened at all.
				string filepath = GetSaveFilePath(serializer, true);
				// Ensure directory exists.
				Directory.CreateDirectory(Path.GetDirectoryName(filepath));

				using (var file = File.Open(filepath, FileMode.Create))
				{
					stream.Position = 0;
					stream.WriteTo(file);
				}
			}
		}



		protected void SaveToStream_Binary(Stream stream)
		{
			var list = GetDataFromWorld();

			bf.Serialize(stream, list);
			// If "not serializable" error on type expected to be serializable:
			// ensure [field:NonSerialized] is on events.
		}


		protected void SaveToStream_SystemXml(Stream stream)
		{
			var list = GetDataFromWorld();

			SerializationHelper.Pure.SaveToStream(stream, list);
		}

		protected void SaveToStream_DataContract(Stream stream)
		{
			var list = GetDataFromWorld();

			SerializationHelper.DContract.SaveToStream(stream, list);
		}

		protected void SaveToStream_Custom(Stream stream)
		{
			var list = GetDataFromWorld();

			CustomSerialization.Serializer.Serialize(stream, list, spec, new StreamingContext(StreamingContextStates.File | StreamingContextStates.Persistence));
		}

		#endregion Saving
	}
}
