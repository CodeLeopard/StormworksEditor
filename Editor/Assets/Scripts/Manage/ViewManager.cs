﻿// Copyright 2022-2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections;
using System.Runtime.CompilerServices;

using Behaviours;

using CurveGraph;
using DataTypes.Extensions;
using Tiles;

using Tools;

using UnityEngine;

namespace Manage
{
	/// <summary>
	/// Manages the visibility for the View Menu item.
	/// </summary>
	[DefaultExecutionOrder(ExecutionOrder.ViewManager)]
	public class ViewManager : M0noBehaviour
	{
		[SerializeField]
		private TileLoader tileLoader;
		[SerializeField]
		private CurveGraphConnectionManager connectionManager;

		[SerializeField]
		private int itemPerTick = 1000;

		[SerializeField]
		private GameObject Sea;

		[SerializeField]
		private GameObject SeaFloor;


		[NonSerialized]
		public bool MeshVisible;
		[NonSerialized]
		public bool PhysVisible;
		[NonSerialized]
		public bool FlattenTerrain;

		[NonSerialized]
		public bool LightSelectorEnabled;
		[NonSerialized]
		public bool LightEmissionEnabled;
		[NonSerialized]
		public bool OmniLightsVisible;
		[NonSerialized]
		public bool SpotLightsVisible;
		[NonSerialized]
		public bool TubeLightsVisible;

		[NonSerialized]
		public bool EditAreasVisible;
		[NonSerialized]
		public bool EditAreasWithVehicleVisible;
		[NonSerialized]
		public bool EditAreasWithVehicleBoundsVisible;
		[NonSerialized]
		public bool InteractablesVisible;
		[NonSerialized]
		public bool TrackLinesVisible;
		[NonSerialized]
		public bool SeaVisible;
		[NonSerialized]
		public bool SeaRaycast;
		[NonSerialized]
		public bool SeaFloorVisible;

		/// <summary>
		/// The target state for tree visibility.
		/// </summary>
		[NonSerialized]
		public bool TreesVisible;

		public static ViewManager Instance;


		/// <inheritdoc />
		protected override void OnAwake()
		{
			Instance = this;

			tileLoader.Settings.SeaFloorHeightChanged += Settings_SeaFloorHeightChanged;
			Settings_SeaFloorHeightChanged(tileLoader.Settings.SeaFloorHeight);
		}

		private void Settings_SeaFloorHeightChanged(float newValue)
		{
			var p = SeaFloor.transform.localPosition;
			p.y = newValue;
			SeaFloor.transform.localPosition = p;
		}

		public void SetTreesVisible(bool newValue)
		{
			if (newValue == TreesVisible) return;
			StartCoroutine(Enumerator_SetVisible(TileBehaviour.SetTreesActive, newValue));
			TreesVisible = newValue;
		}

		public void SetMeshVisible(bool newValue)
		{
			if (newValue == MeshVisible) return;
			StartCoroutine(Enumerator_SetVisible(TileBehaviour.SetMeshActive, newValue));
			StartCoroutine(connectionManager.SetMeshVisible(newValue));
			MeshVisible = newValue;
		}

		public void SetPhysVisible(bool newValue)
		{
			if (newValue == PhysVisible) return;
			StartCoroutine(Enumerator_SetVisible(TileBehaviour.SetPhysActive, newValue));
			StartCoroutine(connectionManager.SetPhysVisible(newValue));
			PhysVisible = newValue;
		}

		public bool IsFlattening { get; private set; }
		public Coroutine SetFlattenTerrain(bool newValue)
		{
			if (newValue == FlattenTerrain) return null;

			// Adjust sea level slightly to avoid z fighting.
			var pos = Sea.transform.localPosition;
			pos.y = newValue ? -0.005f : 0;
			Sea.transform.localPosition = pos;

			var co = StartCoroutine(Enumerator_SetFlatten(TileBehaviour.SetFlattenTerrain, newValue));
			FlattenTerrain = newValue;

			return co;
		}

		public void SetFlattenTerrain_void(bool newValue)
		{
			SetFlattenTerrain(newValue);
		}

		#region Lights
		public void SetLightSelectorEnabled(bool newValue)
		{
			if (newValue == LightSelectorEnabled) return;
			StartCoroutine(Enumerator_SetVisible(TileBehaviour.SetLightSelectorEnabled, newValue));
			LightSelectorEnabled = newValue;
		}
		public void SetLighsEmissionEnabled(bool newValue)
		{
			if (newValue == LightEmissionEnabled) return;
			StartCoroutine(Enumerator_SetVisible(TileBehaviour.SetLightEmissionEnabled, newValue));
			LightEmissionEnabled = newValue;
		}

		public void SetOmniLighsVisible(bool newValue)
		{
			if (newValue == OmniLightsVisible) return;
			StartCoroutine(Enumerator_SetVisible(TileBehaviour.SetOmniLightActive, newValue));
			OmniLightsVisible = newValue;
		}

		public void SetSpotLightsVisible(bool newValue)
		{
			if (newValue == SpotLightsVisible) return;
			StartCoroutine(Enumerator_SetVisible(TileBehaviour.SetSpotLightActive, newValue));
			SpotLightsVisible = newValue;
		}

		public void SetTubeLightsVisible(bool newValue)
		{
			if (newValue == TubeLightsVisible) return;
			StartCoroutine(Enumerator_SetVisible(TileBehaviour.SetTubeLightsActive, newValue));
			TubeLightsVisible = newValue;
		}
		#endregion Lights

		public void SetEditAreasVisible(bool newValue)
		{
			if (newValue == EditAreasVisible) return;
			StartCoroutine(Enumerator_SetVisible(TileBehaviour.SetEditAreasActive, newValue));
			EditAreasVisible = newValue;
		}


		public void SetEditAreasWithVehicleVisible(bool newValue)
		{
			if (newValue == EditAreasWithVehicleVisible) return;
			StartCoroutine(Enumerator_SetVisible(TileBehaviour.SetEditAreasWithVehicleActive, newValue));
			EditAreasWithVehicleVisible = newValue;
		}

		public void SetEditAreasForceBoundsVisible(bool newValue)
		{
			if (newValue == EditAreasWithVehicleBoundsVisible) return;
			StartCoroutine(Enumerator_SetVisible(TileBehaviour.SetEditAreasForceBoundsVisible, newValue));
			EditAreasWithVehicleBoundsVisible = newValue;
		}


		public void SetInteractablesVisible(bool newValue)
		{
			if (newValue == InteractablesVisible) return;
			StartCoroutine(Enumerator_SetVisible(TileBehaviour.SetInteractablesActive, newValue));
			InteractablesVisible = newValue;
		}

		public void SetTrackLinesVisible(bool newValue)
		{
			if (newValue == TrackLinesVisible) return;
			StartCoroutine(Enumerator_SetVisible(TileBehaviour.SetTrackLinesActive, newValue));
			TrackLinesVisible = newValue;
		}

		public void SetSeaVisible(bool newValue)
		{
			Sea.SetActive(newValue);
		}

		public void SetSeaRaycast(bool newValue)
		{
			Sea.GetRequiredComponent<MeshCollider>().enabled = newValue;
		}

		public void SetSeaFloorVisible(bool newValue)
		{
			if (newValue == SeaFloorVisible) return;
			StartCoroutine(Enumerator_SetVisible(TileBehaviour.SetSeaFloorActive, newValue));
			SeaFloorVisible = newValue;
		}


		private IEnumerator Enumerator_SetVisible(Action<TileBehaviour, bool> method, bool newValue, [CallerMemberName] string callerMemberName = "unknown")
		{
			Console.WriteLine($"[{nameof(ViewManager)}] Changeing visibility of {callerMemberName} to {newValue}.");
			int counter = 0;
			foreach (GameObject go in tileLoader.MapGameObjectToTile.Keys)
			{
				method.Invoke(go.GetComponent<TileBehaviour>(), newValue);

				if (counter++ > itemPerTick)
				{
					counter = 0;
					yield return null;
				}
			}
			Console.WriteLine($"[{nameof(ViewManager)}] Changed visibility of {callerMemberName} to {newValue}.");
		}

		private IEnumerator Enumerator_SetFlatten(Action<TileBehaviour, bool> method, bool newValue)
		{
			IsFlattening = true;
			Console.WriteLine($"[{nameof(ViewManager)}] Changeing Flattening to {newValue}.");
			int counter = 0;
			foreach (GameObject go in tileLoader.MapGameObjectToTile.Keys)
			{
				method.Invoke(go.GetComponent<TileBehaviour>(), newValue);

				if (counter++ > itemPerTick)
				{
					yield return null;
				}
			}
			Console.WriteLine($"[{nameof(ViewManager)}] Changed Flattening to {newValue}.");

			IsFlattening = false;
		}
	}
}
