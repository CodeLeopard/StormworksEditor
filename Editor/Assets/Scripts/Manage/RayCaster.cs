﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using UnityEngine;

namespace Tools
{
	[RequireComponent(typeof(Camera))]
	public class RayCaster : MonoBehaviour
	{
		[SerializeField]
		[HideInInspector]
		private Camera myCamera;

		[SerializeField]
		[HideInInspector]
		private bool IsFocused;

		public GameObject hitMarkerPrefab;
		public GameObject hitMarkerInstance;

		void Start()
		{
			myCamera = GetComponent<Camera>();

			if (null == hitMarkerInstance)
			{
				hitMarkerInstance = Instantiate(hitMarkerPrefab);
			}
			hitMarkerInstance.SetActive(false);
		}

		void OnApplicationFocus(bool isNowInFocus)
		{
			IsFocused = isNowInFocus;
		}

		void Update()
		{
			MouseInteraction();
		}

		private void MouseInteraction()
		{
			if (!IsFocused) return;
			if (Input.GetMouseButton(0))
			{
				var ray = myCamera.ScreenPointToRay(Input.mousePosition);

				if (Physics.Raycast(ray, out var hitInfo))
				{
					OnRayHit(ray, hitInfo);
				}
			}
		}

		private void OnRayHit(Ray ray, RaycastHit hitInfo)
		{
			var parentName = "root";
			if (null != hitInfo.transform.parent) parentName = hitInfo.transform.parent.name;
			//Debug.Log($"{parentName} / {hitInfo.transform.name} @ {hitInfo.point}");

			hitMarkerInstance.transform.position = hitInfo.point;
			hitMarkerInstance.SetActive(true);
		}

	}
}
