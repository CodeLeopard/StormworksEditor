﻿using System;
using UnityEditor;
using UnityEngine;

namespace Manage.GameStates
{
	internal class Menu : SceneHandler
	{
		public void Exit()
		{
			Console.WriteLine("User pressed Exit button.");
#if UNITY_EDITOR
			EditorApplication.ExitPlaymode();
#else
			Application.Quit(0);
#endif
		}
	}
}
