﻿// Copyright 2021-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/




using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Control;

using GUI;
using GUI.MenuBar;

using Tiles;

using Tools;

using UnityEngine;

namespace Manage.GameStates
{
	internal class World : SceneHandler
	{
		public List<AbstractThingSaveManager> Savers;

		private void Update()
		{
			if (InputManagement.TextInputFocused) return;
			if (Input.GetKeyUp(KeyCode.S) && (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)))
			{
				Save();
			}
		}


		public async void Save()
		{
			bool anyFailed = false;
			foreach (var saver in Savers)
			{
				try
				{
					saver.Save();
				}
				catch (Exception e)
				{
					Debug.LogException(e);

					anyFailed = true;
				}
			}

			try
			{
				await TileExporter.Instance.Export();
			}
			catch (Exception e)
			{
				Debug.LogException(e);

				anyFailed = true;
			}

			if (anyFailed)
			{
				var popup = ConfirmationPopupPanel.Create(null);
				popup.HeaderText = "Saving failed";
				popup.BodyText = "Saving did not succeed, you may be able to find out why using the log and fix the problem.";
				popup.ConfirmText = "LogFile";
				popup.CancelText = "Dismiss";

				bool? result = await popup.WaitForInteraction();

				if (result == true)
				{
					AboutButtons.LogFileClick();
				}
			}
		}


		public async void SaveAndQuit()
		{
			Console.WriteLine($"[{nameof(GameStateManager)}] Save-Quit requested.");

			try
			{
				await TileExporter.Instance.Export();

				Console.WriteLine($"[{nameof(GameStateManager)}] Save-Quit: Saving completed, will now quit.");
				InternalQuit();
			}
			catch // The exception should be logged by the task in which it occurred.
			{
				Debug.LogError($"Will not exit because saving was requested but did not succeed.");

				var popup = ConfirmationPopupPanel.Create(null);
				popup.HeaderText = "Saving failed";
				popup.BodyText = "Saving did not succeed, you may be able to find out why using the log (About -> LogFile) and fix the problem.\n"
							   + "Do you want to quit and loose your changes or stay in the program?";
				popup.ConfirmText = "Discard & Quit";
				popup.CancelText = "Cancel & Keep changes";

				bool? result = await popup.WaitForInteraction();

				if (result is true)
				{
					Console.WriteLine($"[{nameof(GameStateManager)}] Save-Quit: Saving failed, user requested exit anyway.");
					InternalQuit();
					return;
				}

				Console.WriteLine($"[{nameof(GameStateManager)}] Discard & Quit aborted.");
			}
		}

		public async void DiscardAndQuit()
		{
			Console.WriteLine($"[{nameof(GameStateManager)}] Discard & Quit requested.");

			var popup = ConfirmationPopupPanel.Create(null);
			popup.HeaderText = "Discard and Exit";
			popup.BodyText = "Unsaved changes will be lost. Are you sure you want to quit without saving?";
			popup.ConfirmText = "Discard & Quit";

			bool? result = await popup.WaitForInteraction();

			if (!result.HasValue || result.Value == false)
			{
				Console.WriteLine($"[{nameof(GameStateManager)}] Discard & Quit aborted.");
				return;
			}

			Internal_DiscardAndQuit();
		}

		private void Internal_DiscardAndQuit()
		{
			foreach (var saver in Savers)
			{
				saver.AllowSave = false;
			}

			InternalQuit();
		}

		public async void DiscardAndReload()
		{
			Console.WriteLine($"[{nameof(GameStateManager)}] Discard & Reload requested.");

			var popup = ConfirmationPopupPanel.Create(null);
			popup.HeaderText = "Discard and Reload";
			popup.BodyText = "Unsaved changes will be lost. Are you sure you want to reload?";
			popup.ConfirmText = "Discard & Reload";

			bool? result = await popup.WaitForInteraction();

			if (!result.HasValue || result.Value == false)
			{
				Console.WriteLine($"[{nameof(GameStateManager)}] Discard & Reload aborted.");
				return;
			}


			foreach (var saver in Savers)
			{
				saver.Clear();
			}

			await TileLoader.instance.StartReloadTiles();

			foreach (var saver in Savers)
			{
				saver.Load();
			}

			Console.WriteLine($"[{nameof(GameStateManager)}] Discard and Reload completed.");
		}

		private void InternalQuit()
		{
			GameStateManager.LeaveGameState(this);
		}
	}
}
