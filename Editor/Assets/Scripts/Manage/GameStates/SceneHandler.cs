﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Behaviours;

using UnityEngine;
using UnityEngine.SceneManagement;

namespace Manage.GameStates
{
	/// <summary>
	/// Base class for the singleton handler of a Scene.
	/// </summary>
	public abstract class SceneHandler : MonoBehaviour
	{
		public Scene Scene => gameObject.scene;
#if UNITY_EDITOR
		public string SceneName;
#endif
		public bool UnloadWhenSwitchToAnotherScene = true;

#if UNITY_EDITOR
		private void Awake()
		{
			SceneName = Scene.name;
		}
#endif
	}
}
