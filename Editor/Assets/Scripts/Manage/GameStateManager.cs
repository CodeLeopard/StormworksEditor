﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using Assets.Scripts.Tools;
using Behaviours;


using DataTypes;

using GUI;

using Manage.GameStates;

using UnityEditor;

using UnityEngine;
using UnityEngine.SceneManagement;

namespace Tools
{
	public class GameStateManager : M0noBehaviour
	{
#if UNITY_EDITOR
		public SceneAsset MenuScene;
		public SceneAsset LoadingScene;
		void OnValidate()
		{
			if (MenuScene != null && !string.IsNullOrEmpty(MenuScene.name))
			{
				MenuSceneName = MenuScene.name;
			}
			if (LoadingScene != null && !string.IsNullOrEmpty(LoadingScene.name))
			{
				LoadingSceneName = LoadingScene.name;
			}

		}
#endif
		[SerializeField]
		private string MenuSceneName;
		[SerializeField]
		private string LoadingSceneName;

		public bool useLoading = false;

		private Scene? CurrentScene;

		public static GameStateManager Instance { get; private set; }

		/// <inheritdoc />
		protected override void OnAwake()
		{
			Instance = this;
		}

		/// <inheritdoc />
		protected override void OnEnableAndAfterStart()
		{
			CheckBuildInfo();
			DecideStartupScene();

			ThreadPool.GetMaxThreads(out int workerThreads, out int cpt);
			ThreadPool.SetMaxThreads(Environment.ProcessorCount * 2, cpt);
		}


		private void CheckBuildInfo()
		{
			try
			{
				var info = BuildAndVersionInfomanager.GetBuildAndVersionInfo();
				Console.WriteLine(info);
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
			}
		}



		private const string StartupCommandParameter = "StartupScene";
		private static Regex StartupCommandRegex = new Regex
			("[-/\\\\]StartupScene[ =](\"([^\"]+)\"|([^ \n\t]+))", RegexOptions.Compiled);
		private void DecideStartupScene()
		{
			try
			{
				string commandLine = Environment.CommandLine;

				var m = StartupCommandRegex.Match(commandLine);

				if (! m.Success)
				{
					Console.WriteLine($"Loading startup scene: '{MenuSceneName}'.");
					GotoScene(MenuSceneName);
					return;
				}

				Console.WriteLine($"Looking for Startup scene in: {commandLine}\n-->{m.Value}");

				string result;
				if (m.Groups[2].Success)
				{
					result = m.Groups[2].Value;
				}
				else
				{
					result = m.Groups[3].Value;
				}

				if (string.IsNullOrEmpty(result))
				{
					Console.WriteLine($"Could not find argument in '{m.Value}'. Loading default startup scene: '{MenuSceneName}'.");
					GotoScene(MenuSceneName);
					return;
				}

				Console.WriteLine($"Overriding startup scene with CommandLine argument: '{result}'.");
				GotoScene(result);
			}
			catch (Exception e)
			{
				Debug.LogException(e, this);

				Console.WriteLine($"Falling back to default startup scene: '{MenuSceneName}' due to error (see previous log messages).");
				GotoScene(MenuSceneName);
			}
		}

		public static void GotoScene(Scene scene)
		{
			GotoScene(scene.name);
		}

		public static void GotoScene(string name)
		{
			Instance.GotoSceneInternal(name);
		}

		private void GotoSceneInternal(string name)
		{
			StartCoroutine(GotoSceneEnumerator(name));
		}


		private IEnumerator GotoSceneEnumerator(string name)
		{
			Console.WriteLine($"[ChangeScene] Changing to Scene: '{name}'");

			Scene? loadingProgressScene = null;
			if (useLoading)
			{
				var loadingProgressSceneAsync = SceneManager.LoadSceneAsync(LoadingSceneName, LoadSceneMode.Additive);
				while (! loadingProgressSceneAsync.isDone)
				{
					yield return null;
				}

				loadingProgressScene = SceneManager.GetSceneByName(LoadingSceneName);
			}

			var maybeSceneToUnload = CurrentScene;

			AsyncOperation unloadAsync = null;
			if (maybeSceneToUnload.HasValue)
			{
				LoadingScreen.StartFadeIn();
				Console.WriteLine($"[ChangeScene] Unloading Scene: '{maybeSceneToUnload.Value.name}'");
				unloadAsync = SceneManager.UnloadSceneAsync(maybeSceneToUnload.Value);
				unloadAsync.priority = 1;
			}

			var loadAsync = SceneManager.LoadSceneAsync(name, LoadSceneMode.Additive);
			loadAsync.priority = 0;


			while (true)
			{
				if (unloadAsync != null && ! unloadAsync.isDone)
				{
					Console.WriteLine($"[ChangeScene] Progress - Unload: {unloadAsync.progress * 100f}%");
				}
				if (! loadAsync.isDone)
				{
					Console.WriteLine($"[ChangeScene] progress - Load: {loadAsync.progress * 100f}%");
				}

				float progress = unloadAsync == null ? loadAsync.progress : (unloadAsync.progress + loadAsync.progress) / 2;

				LoadingScreen.SetProgress(progress);

				if ((unloadAsync == null || unloadAsync.isDone) && loadAsync.isDone)
				{
					break;
				}

				yield return null;
			}

			CurrentScene = SceneManager.GetSceneByName(name);
			SceneManager.SetActiveScene(CurrentScene.Value);

			if (loadingProgressScene.HasValue)
			{
				LoadingScreen.SetProgress(1f);
				yield return new WaitForSeconds(LoadingScreen.Instance.FadeTime);
				SceneManager.UnloadSceneAsync(loadingProgressScene.Value);
			}

			Console.WriteLine($"[ChangeScene] Done Loading Scene: '{CurrentScene.Value.name}'");
		}

		/// <summary>
		/// leave a game state and go to the appropriate menu state.
		/// </summary>
		/// <param name="state"></param>
		/// <exception cref="ArgumentNullException"></exception>
		public static void LeaveGameState(SceneHandler state)
		{
			_ = state ?? throw new ArgumentNullException(nameof(state));

			if (state.UnloadWhenSwitchToAnotherScene)
			{
				GotoScene(Instance.MenuSceneName);
			}
		}

		public static void LeaveProgram()
		{
			Instance.InternalQuit();
		}


		private void OnDestroy()
		{
			HourTracker.SaveTimePlayed();
		}

		/// <summary>
		/// Exit the program gracefully, calling OnDisable on Components and triggering Savers to Save() if set to do so.
		/// </summary>
		private void InternalQuit()
		{
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#else
			Application.Quit(0);
#endif
		}
	}
}
