﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using Behaviours;

namespace Tools
{
	public abstract class AbstractThingSaveManager : M0noBehaviour
	{
		/// <summary>
		/// Allow Automatic loading from file.
		/// </summary>
		public bool AllowLoad = true;

		/// <summary>
		/// Allow Automatic saving to file.
		/// </summary>
		public bool AllowSave = true;

		/// <summary>
		/// Clear the data currently loaded in the world.
		/// </summary>
		public abstract void Clear();

		/// <summary>
		/// Load using the default method from the default file.
		/// </summary>
		public abstract void Load();

		/// <summary>
		/// Save using the default method to the default file.
		/// </summary>
		public abstract void Save();
	}
}
