﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;

using UnityEngine;
using UnityEditor;

using DataTypes.Attributes;

namespace DataTypes
{
	[CreateAssetMenu(fileName = "ScriptableObject_", menuName = "ScriptableObject_")]
	public class ScriptableObject_ : ScriptableObject, IComparable<ScriptableObject_>
	{
		[SerializeField]
		[InspectorReadonly]
		[Tooltip("The GUID of this instance")]
		private string guid;

		public string GUID => guid;

		[SerializeField]
		[InspectorReadonly]
		[Tooltip("The Path of this instance")]
		private string path;


		public string Path => path;

		void Reset()
		{
			//Debug.Log($"ScriptableObject_.Reset '{guid}'");
		}

		void Awake()
		{
			OnAwake();
		}

		public void UpdateAssetPath()
		{
			TrySetGUID();
		}

		public void HolderAwake()
		{
			TrySetGUID();

			OnHolderAwake();
		}

		protected virtual void OnAwake()
		{

		}

		protected virtual void OnHolderAwake()
		{

		}

		private void TrySetGUID()
		{
#if UNITY_EDITOR
			var newPath = AssetDatabase.GetAssetPath(this);
			//Debug.Log($"ScriptableObject_.TrySetGUID() GetAssetPath: '{newPath}'", this);

			if (! string.IsNullOrWhiteSpace(newPath)) path = newPath;

			if (string.IsNullOrWhiteSpace(guid))
			{
				if (string.IsNullOrWhiteSpace(path)) return; 
				// Path is not set immediately after creation, because user has not finished typing it.


				guid = AssetDatabase.AssetPathToGUID(path);
				Debug.Log($"ScriptableObject_.TrySetGUID() new GUID: '{guid}', path: '{path}'", this);
			}
#endif

			if (string.IsNullOrWhiteSpace(guid))
			{
				//Debug.LogError("Did not get a valid GUID", this);
			}
			else
			{
				ScriptableObjectDB.AddOrUpdate(this);
			}
		}

		void OnEnable()
		{
			TrySetGUID();
		}

		void OnValidate()
		{
			TrySetGUID();
		}

		/// <inheritdoc />
		public int CompareTo(ScriptableObject_ other)
		{
			return String.Compare(this.GUID, other.GUID, StringComparison.Ordinal);
		}
	}
}
