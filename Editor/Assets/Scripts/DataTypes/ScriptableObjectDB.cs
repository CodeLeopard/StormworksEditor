﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

using DataTypes.Attributes;

namespace DataTypes
{
	/// <summary>
	/// Note that using the DB will cause the ScriptableObjects to be referenced at all times
	/// </summary>
	//[CreateAssetMenu(fileName = "ScriptableObjectDB", menuName = "ScriptableObjectDB")]
	public class ScriptableObjectDB : ScriptableObject, ISerializationCallbackReceiver
	{
		private static ScriptableObjectDB instance;
		private static HashSet<ScriptableObject_> temp;

		[SerializeField]
		[HideInInspector]
		public Dictionary<string, ScriptableObject_> guid_SO;

		[SerializeField]
		[HideInInspector]
		public Dictionary<ScriptableObject_, string> SO_guid;

		[SerializeField]
		[InspectorReadonly]
		private List<string> records;


		static ScriptableObjectDB()
		{
			temp = new HashSet<ScriptableObject_>();
		}

		private void Initialize()
		{
			Console.WriteLine($"guid_SO: == null: {guid_SO == null}");
			Console.WriteLine($"SO_guid: == null: {SO_guid == null}");

			guid_SO = guid_SO ?? new Dictionary<string, ScriptableObject_>();
			SO_guid = SO_guid ?? new Dictionary<ScriptableObject_, string>();

			instance = this;

			Console.WriteLine($"[ScriptableObjectDB.Initialize()] Entries in temp: {temp.Count}");
			foreach (var object_ in temp)
			{
				AddOrUpdate(object_);
			}

			Console.WriteLine($"[ScriptableObjectDB.Initialize()] Entries: {SO_guid.Count}");
			foreach (var object_ in SO_guid.Keys)
			{
				Console.WriteLine($"\t{object_.name,-10} : {object_.GUID}");
			}
		}

		protected void Awake()
		{
			if (null == instance)
			{
				Initialize();
			}
			else if(Application.isPlaying)
			{
				Debug.LogWarning($"An instance of {nameof(ScriptableObjectDB)} already exists!");
			}
			else
			{
				// Editor could have multiple scenes open, etc. so it's probably fine.
			}
		}

		protected void OnEnable()
		{
			if (null == instance)
			{
				Initialize();
				//Debug.LogWarning($"Not properly initialized", this);
			}
		}

		protected void OnValidate()
		{
			if (null == instance)
			{
				Initialize();
				//Debug.LogWarning($"Not properly initialized", this);
			}
		}

		public static void AddOrUpdate(ScriptableObject_ obj)
		{
			if (null != instance)
			{
				instance.guid_SO[obj.GUID] = obj;
				instance.SO_guid[obj] = obj.GUID;
			}
			else
			{
				temp.Add(obj);
			}
		}

		public static void UpdatePaths()
		{
			if (null == instance) return;
			foreach (var kvp in instance.guid_SO.ToArray())
			{
				if (null == kvp.Value)
				{
					instance.guid_SO.Remove(kvp.Key);
					instance.SO_guid.Remove(kvp.Value);
				}
				kvp.Value.UpdateAssetPath();
			}
		}

		public static ScriptableObject_ Get(string guid)
		{
			try
			{
				return instance.guid_SO[guid];
			}
			catch (KeyNotFoundException e)
			{
				throw new KeyNotFoundException($"Key '{guid}' was not found.", e);
			}
		}

		public static List<ScriptableObject_> GetAll()
		{
			return instance.SO_guid.Keys.ToList();
		}

		public static TResult Get<TResult>(string guid) where TResult : ScriptableObject_
		{
			return instance.guid_SO[guid] as TResult;
		}

		public static bool TryGet(string guid ,out ScriptableObject_ result)
		{
			return instance.guid_SO.TryGetValue(guid, out result);
		}

		public static bool TryGet<TResult>(string guid, out TResult result) where TResult : ScriptableObject_
		{
			var found = instance.guid_SO.TryGetValue(guid, out ScriptableObject_ untyped);
			result = untyped as TResult;

			return found;
		}

		/// <inheritdoc />
		public void OnBeforeSerialize()
		{
			records = guid_SO.Values.Select(s => $"{s.name,-10} : {s.GUID}").ToList();
			records.Sort(); // Ensure consistent serialization order (to prevent phantom changes in git)
		}

		/// <inheritdoc />
		public void OnAfterDeserialize()
		{
			records = null;
		}
	}
}
