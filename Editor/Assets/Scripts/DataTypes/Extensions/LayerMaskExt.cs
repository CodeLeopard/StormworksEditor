﻿using UnityEngine;

namespace DataTypes.Extensions
{
	public static class LayerMaskExt
	{
		public static LayerMask NameToMask(string name)
		{
			return 1 << LayerMask.NameToLayer(name);
		}

		public static void NameToMask(string name, out LayerMask mask, out int layer)
		{
			layer = LayerMask.NameToLayer(name);
			mask = 1 << layer;
		}
	}
}
