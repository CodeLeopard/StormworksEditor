﻿using System;

using UnityEngine;

namespace DataTypes.Extensions
{
	/// <summary>
	/// Extensions to <see cref="UnityEngine.Component"/>
	/// </summary>
	public static class ComponentExt
	{
		/// <summary>
		/// Get a component of type <typeparamref name="TComponent"/> or throw if not found.
		/// </summary>
		/// <typeparam name="TComponent"></typeparam>
		/// <returns></returns>
		/// <exception cref="NullReferenceException">Component not found</exception>
		public static TComponent GetRequiredComponent<TComponent>(this Component instance) where TComponent : Component
		{
			return instance.gameObject.GetRequiredComponent<TComponent>();
		}

		/// <summary>
		/// Get a component of type <typeparamref name="TComponent"/> or throw if not found.
		/// </summary>
		/// <typeparam name="TComponent"></typeparam>
		/// <returns></returns>
		/// <exception cref="NullReferenceException">Component not found</exception>
		public static TComponent GetRequiredComponent<TComponent>(this GameObject instance) where TComponent : Component
		{
			var result = instance.GetComponent<TComponent>();

			if (null == result)
			{
				throw new NullReferenceException($"Required Component {typeof(TComponent).FullName} could not be retrieved.");
			}

			return result;
		}


		/// <summary>
		/// Get a <see cref="Component"/> of type <typeparamref name="TComponent"/> from the closest parent with such component, or null.
		/// </summary>
		/// <typeparam name="TComponent"></typeparam>
		/// <param name="instance"></param>
		/// <param name="includeInactive"></param>
		/// <returns></returns>
		public static TComponent GetComponentInParents<TComponent>(this Component instance, bool includeInactive = true) where TComponent : Component
		{
			return instance.gameObject.GetComponentInParents<TComponent>(includeInactive);
		}

		/// <summary>
		/// Get a <see cref="Component"/> of type <typeparamref name="TComponent"/> from the closest parent with such component, or null.
		/// </summary>
		/// <typeparam name="TComponent"></typeparam>
		/// <param name="instance"></param>
		/// <param name="includeInactive"></param>
		/// <returns></returns>
		public static TComponent GetComponentInParents<TComponent>(this GameObject instance, bool includeInactive = true) where TComponent : Component
		{
			// Yes, this exists, but it's name implies it looks only at the direct parent, while it does look at the entire tree.
			return instance.GetComponentInParent<TComponent>(includeInactive);
		}


		/// <summary>
		/// Get a <see cref="Component"/> of type <typeparamref name="TComponent"/> from the closest parent with such component, or throw.
		/// </summary>
		/// <typeparam name="TComponent"></typeparam>
		/// <param name="instance"></param>
		/// <param name="includeInactive"></param>
		/// <returns></returns>
		/// <exception cref="NullReferenceException">Component not found</exception>
		public static TComponent GetRequiredComponentInParents<TComponent>(this Component instance, bool includeInactive = true) where TComponent : Component
		{
			return instance.gameObject.GetRequiredComponentInParents<TComponent>(includeInactive);
		}

		/// <summary>
		/// Get a <see cref="Component"/> of type <typeparamref name="TComponent"/> from the closest parent with such component, or throw.
		/// </summary>
		/// <typeparam name="TComponent"></typeparam>
		/// <param name="instance"></param>
		/// <param name="includeInactive"></param>
		/// <returns></returns>
		/// <exception cref="NullReferenceException">Component not found</exception>
		public static TComponent GetRequiredComponentInParents<TComponent>(this GameObject instance, bool includeInactive = true) where TComponent : Component
		{
			var result = instance.GetComponentInParents<TComponent>(includeInactive);

			if (null != result) return result;

			throw new NullReferenceException($"Required Component {typeof(TComponent).FullName} could not be retrieved.");
		}

		/// <summary>
		/// Get a <see cref="Component"/> of type <typeparamref name="TComponent"/> from the closest child with such component, or throw.
		/// </summary>
		/// <typeparam name="TComponent"></typeparam>
		/// <param name="instance"></param>
		/// <param name="includeInactive"></param>
		/// <returns></returns>
		/// <exception cref="NullReferenceException">Component not found</exception>
		public static TComponent GetRequiredComponentInChildren<TComponent>(this Component instance, bool includeInactive = true) where TComponent : Component
		{
			return instance.gameObject.GetRequiredComponentInChildren<TComponent>(includeInactive);
		}

		/// <summary>
		/// Get a <see cref="Component"/> of type <typeparamref name="TComponent"/> from the closest child with such component, or throw.
		/// </summary>
		/// <typeparam name="TComponent"></typeparam>
		/// <param name="instance"></param>
		/// <param name="includeInactive"></param>
		/// <returns></returns>
		/// <exception cref="NullReferenceException">Component not found</exception>
		public static TComponent GetRequiredComponentInChildren<TComponent>(this GameObject instance, bool includeInactive = true) where TComponent : Component
		{
			var result = instance.GetComponentInChildren<TComponent>(includeInactive);

			if (null != result) return result;

			throw new NullReferenceException($"Required Component {typeof(TComponent).FullName} could not be retrieved.");
		}
	}
}
