﻿using System.Collections.Generic;

using Priority_Queue;

namespace DataTypes.Extensions
{
	public static class QueueExtensions
	{
		public static IDictionary<TPriority, int> GetItemCountByPriority<TData, TPriority>(this SimplePriorityQueue<TData, TPriority> queue)
			where TPriority : System.IComparable<TPriority>
		{
			var d = new SortedDictionary<TPriority, int>();

			foreach (TData data in queue)
			{
				if (queue.TryGetPriority(data, out var priority))
				{
					d.IncrementOrCreateKey(priority);
				}
			}

			return d;
		}
	}
}
