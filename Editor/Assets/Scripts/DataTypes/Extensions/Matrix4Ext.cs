﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;

using OpenTK.Mathematics;

namespace DataTypes.Extensions
{
	public static class Matrix4Ext
	{
		public static Vector3 Position(this in Matrix4 matrix)
		{ return matrix.Row3.Xyz; }

		public static Vector3 Forward(this Matrix4 matrix)
		{ return new Vector3(matrix.Row2.Xyz); }

		public static Vector3 Backward(this Matrix4 matrix)
		{ return new Vector3(-matrix.Row2.Xyz); }


		public static Vector3 Right(this Matrix4 matrix)
		{ return new Vector3(matrix.Row0.Xyz); }


		public static Vector3 Left(this Matrix4 matrix)
		{ return new Vector3(-matrix.Row0.Xyz); }


		public static Vector3 Up(this Matrix4 matrix)
		{ return new Vector3(matrix.Row1.Xyz); }


		public static Vector3 Down(this Matrix4 matrix)
		{ return new Vector3(-matrix.Row1.Xyz); }


		#region Aliases
		public static Vector3 Translation(this in Matrix4 matrix)
		{ return matrix.Position(); }

		#endregion Aliases



		/// <summary>
		/// Create a matrix world transform. Rotation and scale4 are performed such that translation is unaffected.
		/// </summary>
		/// <param name="translation"></param>
		/// <param name="scale"></param>
		/// <param name="rotation"></param>
		/// <returns></returns>
		[Obsolete("This is broken, don't use it")]
		public static Matrix4 CreateTransScaleRot(Vector3 translation, Vector3 scale, Vector3 rotation)
		{
			return CreateTransScaleRot(translation, scale, Quaternion.FromEulerAngles(rotation));
		}

		/// <summary>
		/// Create a matrix world transform. Rotation and scale4 are performed such that translation is unaffected.
		/// </summary>
		/// <param name="translation"></param>
		/// <param name="scale"></param>
		/// <param name="rotation"></param>
		/// <returns></returns>
		[Obsolete("This is broken, don't use it")]
		public static Matrix4 CreateTransScaleRot(Vector3 translation, Vector3 scale, Quaternion rotation)
		{
			var sca = Matrix4.CreateScale(scale);
			var tra = Matrix4.CreateTranslation(translation);
			var rot = Matrix4.CreateFromQuaternion(rotation);
			// 1: scale around the origin.
			// 2: rotate the scaled stuff around the origin.
			// 3: translate the result of the previous operations.
			return sca * rot * tra;
		}

		/// <summary>
		/// Create a matrix that sits at <paramref name="origin"/> and has it's forward vector pointing towards <paramref name="target"/>.
		/// </summary>
		/// <param name="origin"></param>
		/// <param name="target"></param>
		/// <returns></returns>
		public static Matrix4 PointAt(this in Matrix4 origin, Vector3 target)
		{
			return CreatePointAt(origin.Translation(), target, origin.Up());
		}

		/// <summary>
		/// Create a matrix that sits at <paramref name="position"/> and has it's forward vector pointing towards <paramref name="target"/>.
		/// </summary>
		/// <param name="position"></param>
		/// <param name="target"></param>
		/// <param name="up">up vector</param>
		/// <returns></returns>
		public static Matrix4 CreatePointAt(Vector3 position, Vector3 target, Vector3 up)
		{
			var direction = (target - position).Normalized();
			Vector3 xAxis = Vector3.Cross(up, direction).Normalized();

			Vector3 yAxis = Vector3.Cross(direction, xAxis).Normalized();

			var result = new Matrix4(
				xAxis.X, xAxis.Y, xAxis.Z, position.X,
				yAxis.X, yAxis.Y, yAxis.Z, position.Y,
				direction.X, direction.Y, direction.Z, position.Z,
				0, 0, 0, 1);

			return result;
		}
	}
}
