﻿using System;

using UnityEngine;

namespace DataTypes.Extensions
{
	public static class Matrix4x4Ext
	{
		/// <summary>
		/// Extract the position component of this matrix
		/// </summary>
		/// <param name="matrix"></param>
		/// <returns></returns>
		public static Vector3 Position(this in Matrix4x4 matrix)
		{
			var t = matrix.GetRow(3);
			return new Vector3(t.x, t.y, t.z);
		}

		public static Vector3 Forward(this Matrix4x4 matrix)
		{
			var t = matrix.GetRow(2);
			return new Vector3(t.x, t.y, t.z);
		}

		public static Vector3 Backward(this Matrix4x4 matrix)
		{
			var t = matrix.GetRow(2);
			return - new Vector3(t.x, t.y, t.z);
		}


		public static Vector3 Right(this Matrix4x4 matrix)
		{
			var t = matrix.GetRow(0);
			return new Vector3(t.x, t.y, t.z);
		}


		public static Vector3 Left(this Matrix4x4 matrix)
		{
			var t = matrix.GetRow(0);
			return - new Vector3(t.x, t.y, t.z);
		}


		public static Vector3 Up(this Matrix4x4 matrix)
		{
			var t = matrix.GetRow(1);
			return new Vector3(t.x, t.y, t.z);
		}


		public static Vector3 Down(this Matrix4x4 matrix)
		{
			var t = matrix.GetRow(1);
			return - new Vector3(t.x, t.y, t.z);
		}

		public static Vector3 UniformScale(this Matrix4x4 matrix)
		{
			var Row0 = matrix.GetRow(0).Xyz();
			var Row1 = matrix.GetRow(1).Xyz();
			var Row2 = matrix.GetRow(2).Xyz();
			return new Vector3(Row0.magnitude, Row1.magnitude, Row2.magnitude);
		}

        /// <summary>
        /// Returns the rotation component of this instance. Quite slow.
        /// </summary>
        /// <param name="rowNormalize">
        /// Whether the method should row-normalize (i.e. remove scale from) the Matrix. Pass false if
        /// you know it's already normalized.
        /// </param>
        /// <returns>The rotation.</returns>
        public static Quaternion ExtractRotation(this Matrix4x4 m, bool rowNormalize = true)
        {
            var row0 = m.GetRow(0).Xyz();
            var row1 = m.GetRow(1).Xyz();
            var row2 = m.GetRow(2).Xyz();

            if (rowNormalize)
            {
                row0 = row0.normalized;
                row1 = row1.normalized;
                row2 = row2.normalized;
            }

            // code below adapted from Blender
            var q = default(Quaternion);
            var trace = 0.25 * (row0[0] + row1[1] + row2[2] + 1.0);

            if (trace > 0)
            {
                var sq = Math.Sqrt(trace);

                q.w = (float)sq;
                sq = 1.0 / (4.0 * sq);
                q.x = (float)((row1[2] - row2[1]) * sq);
                q.y = (float)((row2[0] - row0[2]) * sq);
                q.z = (float)((row0[1] - row1[0]) * sq);
            }
            else if (row0[0] > row1[1] && row0[0] > row2[2])
            {
                var sq = 2.0 * Math.Sqrt(1.0 + row0[0] - row1[1] - row2[2]);

                q.x = (float)(0.25 * sq);
                sq = 1.0 / sq;
                q.w = (float)((row2[1] - row1[2]) * sq);
                q.y = (float)((row1[0] + row0[1]) * sq);
                q.z = (float)((row2[0] + row0[2]) * sq);
            }
            else if (row1[1] > row2[2])
            {
                var sq = 2.0 * Math.Sqrt(1.0 + row1[1] - row0[0] - row2[2]);

                q.y = (float)(0.25 * sq);
                sq = 1.0 / sq;
                q.w = (float)((row2[0] - row0[2]) * sq);
                q.x = (float)((row1[0] + row0[1]) * sq);
                q.z = (float)((row2[1] + row1[2]) * sq);
            }
            else
            {
                var sq = 2.0 * Math.Sqrt(1.0 + row2[2] - row0[0] - row1[1]);

                q.z = (float)(0.25 * sq);
                sq = 1.0 / sq;
                q.w = (float)((row1[0] - row0[1]) * sq);
                q.x = (float)((row2[0] + row0[2]) * sq);
                q.y = (float)((row2[1] + row1[2]) * sq);
            }

            q.Normalize();
            return q;
        }


    }
}
