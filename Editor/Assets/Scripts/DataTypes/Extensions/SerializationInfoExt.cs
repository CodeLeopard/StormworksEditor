﻿using System.Runtime.Serialization;

namespace DataTypes.Extensions
{
	public static class SerializationInfoExt
	{
		public static TValue GetValue<TValue>(this SerializationInfo info, string name)
		{
			return (TValue) info.GetValue(name, typeof(TValue));
		}
	}
}
