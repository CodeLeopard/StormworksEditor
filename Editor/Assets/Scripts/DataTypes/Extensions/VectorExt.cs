﻿// Copyright 2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using UnityEngine;

namespace DataTypes.Extensions
{
	public static class VectorExt
	{
		#region Downgrades

		public static Vector3 Xyz(this Vector4 v)
		{
			return new Vector3(v.x, v.y, v.z);
		}

		public static Vector2 Xy(this Vector4 v)
		{
			return new Vector2(v.x, v.y);
		}

		public static Vector2 Xy(this Vector3 v)
		{
			return new Vector2(v.x, v.y);
		}

		public static Vector2 Xz(this Vector3 v)
		{
			return new Vector2(v.x, v.z);
		}

		public static Vector2 Xz(this Vector4 v)
		{
			return new Vector2(v.x, v.z);
		}

		#endregion Downgrades

		#region Upgrades

		public static Vector3 WithZ(this Vector2 instance, float z)
		{
			return new Vector3(instance.x, instance.y, z);
		}

		public static Vector4 WithW(this Vector3 instance, float w)
		{
			return new Vector4(instance.x, instance.y, instance.z, w);
		}

		#endregion Upgrades

		#region With X Y Z W

		/// <summary>
		/// Returns a new vector with the X component set to the specified value.
		/// </summary>
		/// <param name="instance"></param>
		/// <param name="x"></param>
		/// <returns></returns>
		public static Vector3 WithX(this Vector3 instance, float x)
		{
			instance.x = x; // Copy by value, so input unaffected.
			return instance;
		}
		/// <summary>
		/// Returns a new vector with the Y component set to the specified value.
		/// </summary>
		/// <param name="instance"></param>
		/// <param name="y"></param>
		/// <returns></returns>
		public static Vector3 WithY(this Vector3 instance, float y)
		{
			instance.y = y; // Copy by value, so input unaffected.
			return instance;
		}
		/// <summary>
		/// Returns a new vector with the Z component set to the specified value.
		/// </summary>
		/// <param name="instance"></param>
		/// <param name="z"></param>
		/// <returns></returns>
		public static Vector3 WithZ(this Vector3 instance, float z)
		{
			instance.z = z; // Copy by value, so input unaffected.
			return instance;
		}

		#endregion With X Y Z W

		#region Checks

		/// <summary>
		/// Returns true if any component of the vector is NaN
		/// </summary>
		/// <param name="v"></param>
		/// <returns></returns>
		public static bool HasNan(Vector3 v)
		{
			return float.IsNaN(v.x) || float.IsNaN(v.y) || float.IsNaN(v.z);
		}

		/// <summary>
		/// Returns true if all components of the vector are finite, that is: not infinite, NaN or otherwise not a valid number.
		/// </summary>
		/// <param name="v"></param>
		/// <returns></returns>
		public static bool IsFinite(Vector3 v)
		{
			return float.IsFinite(v.x) && float.IsFinite(v.y) && float.IsFinite(v.z);
		}


		#endregion Checks

	}
}
