﻿using System.Threading.Tasks;

namespace DataTypes.Extensions
{
	public static class TaskExt
	{
		public static bool IsCompletedSuccessfully(this Task instance)
		{
			return instance.IsCompleted && ! instance.IsCanceled && ! instance.IsFaulted;
		}

		/// <summary>
		/// Returns true if the <see cref="Task"/> is waiting to start, running or waiting for children.
		/// </summary>
		/// <param name="instance"></param>
		/// <returns></returns>
		public static bool IsWaitingOrRunning(this Task instance)
		{
			// Logical pattern  (is ... or ... or ...) not supported by Unity
			return instance.Status == TaskStatus.Created
			    || instance.Status == TaskStatus.WaitingForActivation
			    || instance.Status == TaskStatus.WaitingToRun
			    || instance.Status == TaskStatus.Running
			    || instance.Status == TaskStatus.WaitingForChildrenToComplete;
		}
	}
}
