﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Runtime.Serialization;
using System.Text;

using Shared;

namespace DataTypes
{
	/// <summary>
	/// A General Exception used for any operation that involves a Tile during the export process.
	/// The <see cref="itemName"/> will appear in the exception message.
	/// The actual exception is typically stored inside.
	/// </summary>
	[Serializable]
	public class TileItemExportException : Exception
	{
		public readonly string itemName;

		public TileItemExportException()
		{
		}

		public TileItemExportException(string itemName)
		{
			this.itemName = itemName;
		}

		/// <summary>
		/// Create a new <see cref="TileExportException"/> with auto generated message:
		/// $"{innerException.GetType().Name} occurred reading '{filePath}'."
		/// </summary>
		/// <param name="itemName"></param>
		/// <param name="innerException"></param>
		public TileItemExportException(string itemName, Exception innerException) : base
			($"{innerException.GetType().Name} occurred interacting with '{itemName}'.", innerException)
		{
			this.itemName = itemName;
		}

		public TileItemExportException(string itemName, string message, Exception innerException) : base(message, innerException)
		{
			this.itemName = itemName;
		}

		protected TileItemExportException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		private void CustomToStringPart(StringBuilder sb)
		{
			if (null == itemName)
			{
				sb.AppendLine("ItemName was null.");
				return;
			}

			sb.Append(" ItemName: ");
			sb.AppendLine($"'{itemName}'");
		}

		/// <inheritdoc />
		public override string ToString()
		{
			return this.ExceptionToString(CustomToStringPart);
		}
	}
}