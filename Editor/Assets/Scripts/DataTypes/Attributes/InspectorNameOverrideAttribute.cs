﻿// From: http://answers.unity.com/answers/1383657/view.html

using UnityEngine;
using UnityEditor;

namespace DataTypes.Attributes {
	/// <summary>
	/// <see cref="System.Attribute"/> that changes the label of the item it's applied to in the UnityEditor Inspector window.
	/// </summary>
	public class InspectorNameOverrideAttribute : PropertyAttribute
	{
		public readonly string label;
		public InspectorNameOverrideAttribute(string label)
		{
			this.label = label;
		}

#if UNITY_EDITOR
		[CustomPropertyDrawer(typeof(InspectorNameOverrideAttribute))]
		public class ThisPropertyDrawer : PropertyDrawer
		{
			public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
			{
				try
				{
					var propertyAttribute = this.attribute as InspectorNameOverrideAttribute;
					if (IsItBloodyArrayTho(property) == false)
					{
						label.text = propertyAttribute.label;
					}
					else
					{
						Debug.LogWarningFormat(
							"{0}(\"{1}\") doesn't support arrays ",
							typeof(InspectorNameOverrideAttribute).Name,
							propertyAttribute.label
						);
					}
					EditorGUI.PropertyField(position, property, label);
				}
				catch (System.Exception ex) { Debug.LogException(ex); }
			}

			bool IsItBloodyArrayTho(SerializedProperty property)
			{
				string path = property.propertyPath;
				int idot = path.IndexOf('.');
				if (idot == -1) return false;
				string propName = path.Substring(0, idot);
				SerializedProperty p = property.serializedObject.FindProperty(propName);
				return p.isArray;
				//CREDITS: https://answers.unity.com/questions/603882/serializedproperty-isnt-being-detected-as-an-array.html
			}
		}
#endif
	}
}
