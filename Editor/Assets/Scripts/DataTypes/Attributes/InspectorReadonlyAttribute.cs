﻿using UnityEngine;

namespace DataTypes.Attributes
{
	/// <summary>
	/// This property marks the property as read-only for the Inspector panel in the Unity Editor
	/// </summary>
	public class InspectorReadonlyAttribute : PropertyAttribute
	{
	}
}
