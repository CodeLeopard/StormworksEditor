﻿using UnityEngine;

namespace DataTypes.Attributes
{
	/// <summary>
	/// Use this attribute to override the name of a property in the Unity Inspector.
	/// </summary>
	public class InspectorCustomNameAttribute : PropertyAttribute
	{
		public string Name { get; private set; }

		public InspectorCustomNameAttribute(string name)
		{
			Name = name;
		}
	}
}
