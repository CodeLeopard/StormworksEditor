﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DataTypes
{
	/// <summary>
	/// A wrapper for an array that exposes only a subset of that array.
	/// </summary>
	public static class ArrayRange
	{
		/// <summary>
		/// Create a new ArrayRange, but without having to specify the type parameter explicitly.
		/// </summary>
		/// <typeparam name="TData"></typeparam>
		/// <param name="data"></param>
		/// <returns></returns>
		public static ArrayRange<TData> New<TData>(IEnumerable<TData> data)
		{
			return new ArrayRange<TData>(data.ToArray());
		}

		/// <summary>
		/// Create a new ArrayRange, but without having to specify the type parameter explicitly.
		/// </summary>
		/// <typeparam name="TData"></typeparam>
		/// <param name="data"></param>
		/// <returns></returns>
		public static ArrayRange<TData> New<TData>(TData[] data)
		{
			return new ArrayRange<TData>(data);
		}

		/// <summary>
		/// Create a new ArrayRange, but without having to specify the type parameter explicitly.
		/// </summary>
		/// <typeparam name="TData"></typeparam>
		/// <param name="data"></param>
		/// <param name="start"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		public static ArrayRange<TData> New<TData>(TData[] data, int start, int length)
		{
			return new ArrayRange<TData>(data, start, length);
		}
	}

	/// <summary>
	/// A wrapper for an array that exposes only a subset of that array.
	/// </summary>
	/// <typeparam name="TData"></typeparam>
	public class ArrayRange<TData> : IEnumerable<TData>, IReadOnlyList<TData>
	{
		private readonly TData[] data;
		public readonly int Start;
		public readonly int Length;

		public ArrayRange(TData[] data)
		{
			this.data = data;
			this.Start = 0;
			this.Length = data.Length;
		}

		public ArrayRange(TData[] data, int start, int length)
		{
			this.data = data;
			this.Start = start;
			this.Length = length;
		}


		public TData this[int i]
		{
			get
			{
				if(i < 0) throw new IndexOutOfRangeException();
				if(i >= Length) throw new IndexOutOfRangeException();
				return data[i + Start];
			}
			set
			{
				if (i < 0) throw new IndexOutOfRangeException();
				if (i >= Length) throw new IndexOutOfRangeException();
				data[i + Start] = value;
			}
		}

		public TData[] Data => this.AsEnumerable().ToArray();

		public static implicit operator TData[](ArrayRange<TData> range) => range.Data;

		/// <inheritdoc />
		public IEnumerator<TData> GetEnumerator()
		{
			for (int i = Start; i < Start + Length; i++)
				yield return data[i];
		}

		/// <inheritdoc />
		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		/// <inheritdoc />
		public int Count => Length;
	}

}
