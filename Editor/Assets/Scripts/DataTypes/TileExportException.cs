﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Runtime.Serialization;
using System.Text;

using Shared;

namespace DataTypes
{
	/// <summary>
	/// A General Exception used for any operation that involves a Tile during the export process.
	/// The <see cref="tileName"/> will appear in the exception message.
	/// The actual exception is typically stored inside.
	/// </summary>
	[Serializable]
	public class TileExportException : Exception
	{
		protected readonly bool messageHasFilePath = false;
		public readonly string tileName;

		public TileExportException()
		{
		}

		public TileExportException(string tileName)
		{
			this.tileName = tileName;
		}

		/// <summary>
		/// Create a new <see cref="TileExportException"/> with auto generated message:
		/// $"{innerException.GetType().Name} occurred reading '{filePath}'."
		/// </summary>
		/// <param name="tileName"></param>
		/// <param name="innerException"></param>
		public TileExportException(string tileName, Exception innerException) : base
			($"{innerException.GetType().Name} occurred interacting with '{tileName}'.", innerException)
		{
			this.tileName = tileName;
			messageHasFilePath = true;
		}

		public TileExportException(string tileName, string message, Exception innerException) : base(message, innerException)
		{
			this.tileName = tileName;
			messageHasFilePath = message.Contains(tileName);
		}

		protected TileExportException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		private void CustomToStringPart(StringBuilder sb)
		{
			if (messageHasFilePath) return;

			sb.Append(" Filepath: ");
			if (null != tileName)
				sb.Append($"'{tileName}'");
			else
				sb.Append($"null");
		}

		/// <inheritdoc />
		public override string ToString()
		{
			return this.ExceptionToString(CustomToStringPart);
		}
	}
}