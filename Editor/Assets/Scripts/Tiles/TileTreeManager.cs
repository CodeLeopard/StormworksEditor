// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;

using Behaviours;

using BinaryDataModel;
using BinaryDataModel.Converters;

using Control;

using Shared.Serialization;

using DataTypes.Extensions;

using OpenTK.Mathematics;

using RuntimeGizmos;

using Shared;
using Shared.Exceptions;

using Tools;

using UnityEngine;

using Debug = UnityEngine.Debug;
using Mesh = UnityEngine.Mesh;
using Random = UnityEngine.Random;
using Tree = DataModel.Tiles.Tree;
using Vector3 = UnityEngine.Vector3;

namespace Tiles
{
	/// <summary>
	/// Manages the trees on a single tile, including LODs
	/// </summary>
	public class TileTreeManager : M0noBehaviour
	{
		private static float priority_main => TileLoader.instance.ThreadingSettings.treeLoaderPriority;
		private static uint treesPerTick => TileLoader.instance.ThreadingSettings.treeInstancesPerTick;
		private static float priority_sharedMesh => TileLoader.instance.ThreadingSettings.treeBakePriority;


		private TileLoader tileLoader;
		private TileLOD _myTileLod;


		private string instancesPath;

		[SerializeField]
		internal GameObject IndividualTrees;

		[SerializeField]
		internal GameObject MergedTrees;

		internal MeshFilter MergedTreesMeshFilter;
		internal MeshRenderer MergedTreesMeshRenderer;


		private bool _unsavedChanges;

		/// <summary>
		/// Changes have been made that are not yet saved to instances file.
		/// </summary>
		public bool UnsavedChanges
		{
			get => _unsavedChanges;
			private set => _unsavedChanges = value; // Only for Debugger to have somewhere to put breakPoint.
		}

		/// <inheritdoc cref="MergedMeshOutdated"/>
		private bool _mergedMeshOutdated = false;

		/// <summary>
		/// The Merged Mesh is not up to date with changes to the individuals.
		/// </summary>
		public bool MergedMeshOutdated
		{
			get => _mergedMeshOutdated;
			internal set => SetDirty(value);
		}

		private void SetDirty(bool value)
		{
			UnsavedChanges |= value;

			if (value != _mergedMeshOutdated)
			{
				_mergedMeshOutdated = value;
				if (value)
					SetDirty();
			}
		}

		private Task SetDirty()
		{
			_mergedMeshOutdated = true;
			UnsavedChanges = true;

			if (null == bakeTask || !bakeTask.IsWaitingOrRunning())
			{
				bakeTask = Task.Run(Task_GenerateMergedMesh);
			}

			return bakeTask;
		}

		private bool individualsOutdated = false;





		[SerializeField]
		private Mesh mergedTreeMesh;



		internal List<Tree> trees;
		internal int treeCounter;


		private bool treesMapReady = false;
		private Dictionary<Transform, Tree> treesMap = new Dictionary<Transform, Tree>();

		public int TreeCount => treesMap.Count;


		private bool ShouldAbortLoading => RunOnMainThread.CancelableTasksShouldCancel;



		private int treeLayer;

		internal Task loadTask { get; private set; }
		internal Task bakeTask {get;private set;}
		internal Task saveTask {get;private set;}
		private Coroutine individualsLoadRoutine;

		private byte currentLod;



		public const float minScale =    0.01f;
		public const float maxScale = 1000.00f;

		public TileLoader.TileLoadState LoadState { get; private set; } = TileLoader.TileLoadState.NotStarted;
		public TileLoader.TileLoadState IndividualTreesState { get; private set; } = TileLoader.TileLoadState.NotStarted;
		public TileLoader.TileLoadState BakeState { get; private set; } = TileLoader.TileLoadState.NotStarted;


		/// <summary>
		/// TileLoader will check these task for success and wait for them when measuring loading time.
		/// </summary>
		private SynchronizedHashSet<Task> additionalWaitTasks => TileLoader.instance.LoadingTasksToWaitFor;


		/// <inheritdoc />
		protected override void OnAwake()
		{
			treeLayer = LayerMask.NameToLayer("Trees");

			tileLoader = GetRequiredComponentInParents<TileLoader>();

			if (! staticDataLoaded)
				LoadStaticData();


			_myTileLod = GetRequiredComponentInParents<TileLOD>();
			_myTileLod.LODChanged += MyTileLodLodChanged;

			IndividualTrees = new GameObject("Individuals");
			IndividualTrees.transform.parent = transform;
			IndividualTrees.transform.localPosition = Vector3.zero;
			IndividualTrees.SetActive(false);

			MergedTrees = new GameObject("MergedTrees");
			MergedTrees.transform.parent = transform;
			MergedTrees.transform.localPosition = Vector3.zero;
			MergedTreesMeshFilter = MergedTrees.AddComponent<MeshFilter>();

			MergedTreesMeshRenderer = MergedTrees.AddComponent<MeshRenderer>();
		}

		protected void OnDestroy()
		{
			_myTileLod.LODChanged -= MyTileLodLodChanged;
		}



		private void MyTileLodLodChanged(byte newLOD)
		{
			currentLod = newLOD;
			if (newLOD == 0)
			{
				LoadIndividualsIfDirty();

				IndividualTrees.SetActive(true);
				MergedTrees.SetActive(false);
			}
			else
			{
				IndividualTrees.SetActive(false);
				MergedTrees.SetActive(true);
			}
		}

		/// <summary>
		/// Set the Initial data and begins the loading process in a <see cref="Task"/> which is returned.
		/// </summary>
		/// <param name="instancesPath"></param>
		/// <returns></returns>
		public Task SetInitialData(string instancesPath)
		{
			this.instancesPath = instancesPath;

			return LoadFromFile();
		}

		/// <summary>
		/// Begins the loading process in a <see cref="Task"/>, which is returned.
		/// </summary>
		/// <returns></returns>
		public Task LoadFromFile()
		{
			if (File.Exists(instancesPath))
			{
				LoadState = TileLoader.TileLoadState.Started;
				loadTask = Task.Run(Task_LoadFromFile);
				return loadTask;
			}
			else
			{
				treesMapReady = true; // Remains empty.

				individualsOutdated = false;

				LoadState = TileLoader.TileLoadState.Completed;

				return Task.CompletedTask;
			}
		}

		private void Task_LoadFromFile()
		{
			try
			{
				var timer = new Stopwatch();
				timer.Start();
				LoadState = TileLoader.TileLoadState.Loading;

				XDocument doc = XMLHelper.LoadFromFile(instancesPath);

				trees = Tree.ParseTrees(doc);

				var myBakeTask = SetDirty();
				UnsavedChanges = false; // Was set to true by SetDirty.

				individualsOutdated = true;

				RunOnMainThread.Enqueue(LoadIndividualsIfVisible, priority_main);


				if (TileLoader.instance.ThreadingSettings.treeLoadingCompletionWaitForBake)
				{
					additionalWaitTasks?.Add(myBakeTask);
				}

				timer.Stop();
				//Console.WriteLine($"[{nameof(TileTreeManager)}] Loading completed after {timer.Elapsed} for '{instancesPath}'");

				LoadState = TileLoader.TileLoadState.Completed;
			}
			catch (OperationCanceledException)
			{
				LoadState = TileLoader.TileLoadState.Canceled;
				// We don't care about the exception so don't log.
				throw; // We need the Task object to know it failed.
			}
			catch (FileInteractionException e)
			{
				// This Exception Type would already have the path, no need to wrap it again.
				LoadState = TileLoader.TileLoadState.Failed;
				RunOnMainThread.LogException(e, this);
				throw; // We need the Task object to know it failed.
			}
			catch (Exception e)
			{
				LoadState = TileLoader.TileLoadState.Failed;
				var f = new FileInteractionException(instancesPath, e);
				RunOnMainThread.LogException(f, this);
				throw f; // We need the Task object to know it failed.
			}
		}

		public void SetFromList(List<Tree> newTrees)
		{
			trees = newTrees;

			UnsavedChanges = true;

			individualsOutdated = true;

			LoadIndividualsIfVisible();
		}


		/// <summary>
		/// Clear the individuals, note! this does NOT change the list of trees.
		/// </summary>
		private IEnumerator Co_ClearIndividuals()
		{
			// This seems to perform well enough that we don't need a loop.
			int counter = IndividualTrees.transform.childCount;
			DestroyOrImmediate(IndividualTrees);
			IndividualTrees = new GameObject("Individuals");
			IndividualTrees.transform.SetParent(transform, false);

			if (counter > 0)
			{
				MergedMeshOutdated = true;
			}

			yield return null;
		}

		private void LoadIndividualsIfVisible()
		{
			if (currentLod != 0) return;
			LoadIndividualsIfDirty();
		}

		private void LoadIndividualsIfDirty()
		{
			if (!individualsOutdated) return;
			LoadIndividualsNow();
		}

		private void LoadIndividualsNow()
		{
			if (! gameObject.activeInHierarchy) return;
			if (null != individualsLoadRoutine) StopCoroutine(individualsLoadRoutine);
			IndividualTreesState = TileLoader.TileLoadState.Started;
			individualsLoadRoutine = StartCoroutine(Co_LoadIndividuals());
		}

		private IEnumerator Co_LoadIndividuals()
		{
			IndividualTreesState = TileLoader.TileLoadState.Waiting;
			yield return StartCoroutine(Co_ClearIndividuals());

			treeCounter = 0;

			while (null == trees)
			{
				// not loaded yet, should be soon
				yield return null;
			}

			IndividualTreesState = TileLoader.TileLoadState.Loading;

			// Get a copy of the to be loaded data, in case it's edited while running.
			var data = trees.ToArray();
			individualsOutdated = false;

			foreach (var tree in data)
			{
				if (ShouldAbortLoading) yield break;

				InstantiateTree(tree);

				// One per tick is too slow.
				if(treeCounter % treesPerTick == 0)
					yield return null;
			}

			treesMapReady = true;
			IndividualTreesState = TileLoader.TileLoadState.Completed;
		}

		private void InstantiateTree(Tree tree)
		{
			var go = new GameObject($"#{treeCounter++}");
			go.transform.parent = IndividualTrees.transform;

			go.layer = treeLayer;

			go.transform.localPosition = tree.position.ToUnity();
			go.transform.localScale = Vector3.one * tree.scale;

			var filter = go.AddComponent<MeshFilter>();
			filter.mesh = singleTreeMeshInfo.Mesh;
			var renderer = go.AddComponent<MeshRenderer>();
			renderer.materials = singleTreeMeshInfo.Materials.ToArray();

			var selectionEventReceiver = go.AddComponent<SelectionEventReceiver>();
			selectionEventReceiver.AllowCopy = false;
			selectionEventReceiver.AllowDelete = true;

			var eventReceiver = go.AddComponent<TransformGizmoEventReceiver>();
			eventReceiver.GizmoEndInteract += EventReceiver_GizmoInteraction;
			eventReceiver.GizmoDetach += EventReceiver_GizmoInteraction;
			eventReceiver.GizmoDelete += EventReceiver_GizmoDelete;

			eventReceiver.AllowedMovementAxis = Axis.All;
			eventReceiver.AllowedRotateAxis = Axis.None;
			eventReceiver.AllowedScaleAxis = Axis.Uniform;
			eventReceiver.AllowRaycastMove = true;

			if (tileLoader.CreatePhysicsTrees)
			{
				var collider = go.AddComponent<MeshCollider>();
				collider.convex = true;
				collider.cookingOptions = MeshColliderCookingOptions.EnableMeshCleaning
				                        | MeshColliderCookingOptions.WeldColocatedVertices;
				collider.sharedMesh     = singleTreePhysicMeshInfo.Mesh;
				collider.sharedMaterial = treePhysicMaterial;
			}

			treesMap[go.transform] = tree;
		}

		internal void CreateTreeAtWorldPos(Vector3 worldPosition)
		{
			// Tree sizes: min: 0.167379, max: 1.499466.
			// Sizes ware determined empirically with the TreesTest in StormworksData/Tests Project.

			// Have some randomness, but not too much.
			float size = Random.Range(0.9f, 1.1f);

			Tree t = new Tree((worldPosition - transform.position).ToOpenTK(), size);

			InstantiateTree(t);
			MergedMeshOutdated = true;
		}

		/// <summary>
		/// Add a existing Tree <see cref="GameObject"/>, for example because it was moved between tiles.
		/// </summary>
		/// <param name="g"></param>
		internal void AddTree(GameObject g)
		{
			Transform t = g.transform;
			t.localScale = Vector3.one;

			var eulerAngles = t.localEulerAngles;
			eulerAngles.x = 0;
			eulerAngles.z = 0;
			t.localEulerAngles = eulerAngles;


			Tree tree = new Tree(t.localPosition.ToOpenTK(), t.localScale.magnitude);

			treesMap[t] = tree;
			MergedMeshOutdated = true;
		}

		/// <summary>
		/// Remove a existing Tree <see cref="GameObject"/>, for example because it was moved between tiles.
		/// </summary>
		/// <param name="g"></param>
		internal void RemoveTree(GameObject g)
		{
			EventReceiver_GizmoDelete(g.transform);
		}

		private void EventReceiver_GizmoInteraction(Transform t)
		{
			// Sanitize the transform, because trees only allow translation and uniform scale

			float getScale(Vector3 v)
			{
				float average = (v.x + v.y + v.z) / 3;

				return Mathf.Clamp(average, minScale, maxScale);
			}

			float size = getScale(t.localScale);

			t.localScale = Vector3.one * size;

			// Rotation is not saved in the format.
			t.localEulerAngles = Vector3.zero;

			// todo: Moving trees between tiles ughguhguhuhuguhg

			if (treesMap.TryGetValue(t, out Tree tree))
			{
				tree.position = t.localPosition.ToOpenTK();
				tree.scale = size;

				treesMap[t] = tree; // Struct, so has to be assigned back.

				MergedMeshOutdated = true;
			}
			else
			{
				Debug.LogWarning($"Unknown tree (Transform) for 'GizmoDetach' event.", t);
			}
		}

		private void EventReceiver_GizmoDelete(Transform t)
		{
			bool existed = treesMap.Remove(t);
			if (existed)
			{
				MergedMeshOutdated = true;
			}
			else
			{
				Debug.LogWarning($"Unknown tree (Transform) for 'GizmoDelete' event.", t);
			}
		}

		private void Task_GenerateMergedMesh()
		{
			try
			{
				if (ShouldAbortLoading) return;
				BakeState = TileLoader.TileLoadState.Loading;
				var timer = new Stopwatch();
				timer.Start();


				Tree[] inputData = treesMapReady ? treesMap.Values.ToArray() : trees.ToArray();

				MergedMeshOutdated = false; // We want any change after copying the input data to still trigger a refresh.

				var mergeData = new MeshCombiner.DataItem[inputData.Length];
				int i = 0;
				foreach (Tree tree in inputData)
				{
					if (ShouldAbortLoading) return;

					var t = Matrix4.CreateScale(tree.scale);
					t.Column3 = tree.position.WithW(1);

					var item = new MeshCombiner.DataItem(toBeMergedTreeMesh, t);
					mergeData[i++] = item;
				}

				var result = MeshCombiner.Combine(mergeData, true);
				var meshInfo = result.ToUnityMesh();

				// Wait for completion so the Task reports completion only when the mesh has actually been loaded.
				RunOnMainThread.EnqueueAndWaitForCompletion(MainThreadAction, priority_sharedMesh);
				void MainThreadAction()
				{
					if (ShouldAbortLoading) return;
					MergedTreesMeshFilter.sharedMesh = meshInfo.Mesh;
					MergedTreesMeshRenderer.materials = meshInfo.Materials.ToArray();
				}

				timer.Stop();
				//Console.WriteLine($"[{nameof(TileTreeManager)}] Baking completed after {timer.Elapsed} for '{instancesPath}'");

				if (MergedMeshOutdated)
				{
					// If the state became dirty again restart the process.

					// todo?: should wait until lod changes before starting?

					bakeTask = Task.Run(Task_GenerateMergedMesh);
					return;
				}

				BakeState = TileLoader.TileLoadState.Completed;
			}
			catch (Exception e)
			{
				RunOnMainThread.LogException(e, this);
				throw;
			}
		}

		/// <summary>
		/// Starts a task to save to file.
		/// </summary>
		public void Enqueue_SaveToFile()
		{
			// In both these cases the user can't possibly have edited the trees on this tile, so don't bother saving anything.
			if (! staticDataLoaded) return;
			if (! treesMapReady) return;

			if (! UnsavedChanges) return;

			saveTask = Task.Run(Task_SaveToFile);
		}

		private void Task_SaveToFile()
		{
			try
			{
				XDocument doc = Tree.WriteTreesDocument(treesMap.Values);

				XMLHelper.SaveToFile(doc, instancesPath);

				UnsavedChanges = false;
			}
			catch (Exception e)
			{
				RunOnMainThread.LogException(e, this);
				throw;
			}
		}


		#region Static
		private static bool staticDataLoaded;

		private static string treesFolder => StormworksPaths.Meshes.trees;

		private const string treeMeshName_LOD_0 = "fir_tree.mesh";
		private const string treeMeshName_LOD_1 = "fir_tree_lod_1.mesh";
		private const string treeMeshName_LOD_2 = "fir_tree_lod_2.mesh";

		private static BinaryDataModel.DataTypes.Mesh treeBMesh_LOD_0;
		private static BinaryDataModel.DataTypes.Mesh treeBMesh_LOD_1;
		private static BinaryDataModel.DataTypes.Mesh treeBMesh_LOD_2;

		private static UnityMesh.MeshWithMaterialInfo treeMeshInfo_LOD_0;
		private static UnityMesh.MeshWithMaterialInfo treeMeshInfo_LOD_1;
		private static UnityMesh.MeshWithMaterialInfo treeMeshInfo_LOD_2;


		private static UnityMesh.MeshWithMaterialInfo singleTreeMeshInfo;
		private static UnityMesh.MeshWithMaterialInfo singleTreePhysicMeshInfo;
		private static BinaryDataModel.DataTypes.Mesh toBeMergedTreeMesh;

		private static Material treeMaterial;
		private static PhysicMaterial treePhysicMaterial;

		private static void LoadStaticData()
		{
			// todo: set LODs on individual trees?

			treeMaterial = Globals.Materials.Tree;
			treePhysicMaterial = Globals.PhysicMaterials.Tree;

			(BinaryDataModel.DataTypes.Mesh bMesh, UnityMesh.MeshWithMaterialInfo uMesh) Load(string _name)
			{
				var treeFilePath = Path.Combine(treesFolder, _name);
				var bMesh = MeshCache.GetOrLoadMesh(treeFilePath);
				var uMesh = bMesh.ToUnityMesh();
				return (bMesh, uMesh);
			}

			(treeBMesh_LOD_0, treeMeshInfo_LOD_0) = Load(treeMeshName_LOD_0);
			(treeBMesh_LOD_1, treeMeshInfo_LOD_1) = Load(treeMeshName_LOD_1);
			(treeBMesh_LOD_2, treeMeshInfo_LOD_2) = Load(treeMeshName_LOD_2);


			singleTreeMeshInfo       = treeMeshInfo_LOD_0;
			singleTreePhysicMeshInfo = treeMeshInfo_LOD_2;

			// More detail creates huge meshes that lag when loading.
			toBeMergedTreeMesh = treeBMesh_LOD_2;


			Console.WriteLine($"[{nameof(TileTreeManager)}] Tree static data loaded.");

			staticDataLoaded = true;
		}

		#endregion Static
	}
}
