// Copyright 2022-2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/




using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using UnityEngine;

namespace Tiles
{
	/// <summary>
	/// Encapsulates a collection of tiles that belong together
	/// </summary>
	[Serializable]
	internal class TileCollection
	{
		#region DataTypes

		public enum PosMethod
		{
			/// <summary>
			/// Tiles are placed according to the first and second group of a Regex.Match.
			/// </summary>
			Connected,
			/// <summary>
			/// Tiles are placed with one tile gap between them based on load order.
			/// </summary>
			Separate,
			/// <summary>
			/// Isle of Donkk, this is a special case of <see cref="Connected"/> because the game uses special rules.
			/// </summary>
			Donkk,
			/// <summary>
			/// Arctic Islands, this is a special case of <see cref="Connected"/> because the game uses special rules.
			/// </summary>
			Arctic,
		}

		#endregion DataTypes



		public readonly string name;
		public readonly Vector3 rootPosition;
		public readonly Regex itemNameRegex;

		[NonSerialized]
		public readonly GameObject container;

		public readonly PosMethod PositionParserKind;
		[NonSerialized]
		public readonly Func<Match, Vector3> PositionParser;

		public bool skipLoading;

		internal TileCollection(
			GameObject collectionContainer,
			string name,
			Vector3 rootPosition,
			Regex itemNameRegex,
			PosMethod posMethod,
			bool skipLoading = false)
		{
			this.name = name;
			this.rootPosition = rootPosition;
			this.itemNameRegex = itemNameRegex;
			PositionParserKind = posMethod;

			switch (posMethod)
			{
				case PosMethod.Connected:
					{
						PositionParser = Position_Connected;
						break;
					}
				case PosMethod.Separate:
					{
						PositionParser = Position_Separate;
						break;
					}
				case PosMethod.Donkk:
					{
						PositionParser = Position_Donkk;
						break;
					}
				case PosMethod.Arctic:
					{
						PositionParser = Position_Arctic;
						break;
					}
			}

			if (collectionContainer != null)
			{
				container = collectionContainer.transform.Find(name)?.gameObject;

				if (null == container)
				{
					container = new GameObject(name);
					container.transform.parent = collectionContainer.transform;
				}

				container.hideFlags |= HideFlags.DontSaveInBuild | HideFlags.DontSaveInEditor;
				container.transform.localPosition = rootPosition;
			}
			this.skipLoading = skipLoading;
		}

		internal static IEnumerable<TileCollection> DefaultCollections(GameObject collectionContainer)
		{
			var c = collectionContainer;
			yield return new TileCollection(c,
				"Sawyer",
				new Vector3(0, 0, 0),
				new Regex("^mega_island_(\\d+)_(\\d+)$"),
				PosMethod.Connected);

			yield return new TileCollection(c,
				"Volcanic",
				new Vector3(0, 0, -11000),
				new Regex("^volcanic_(\\d+)_(\\d+)$"),
				PosMethod.Connected,
				skipLoading: true);

			yield return new TileCollection(c,
				"Arid",
				new Vector3(0,0,-24000),
				new Regex("^arid_island_(\\d+)_(\\d+)$"),
				PosMethod.Connected,
				skipLoading: true);

			yield return new TileCollection(c,
				"Donkk",
				new Vector3(-1000, 0, 14000),
				new Regex("^island_33_tile_((\\d)(\\d))$|^island_33_tile_end(_(\\d))?$"),
				PosMethod.Donkk);

			yield return new TileCollection(c,
				"Arctic",
				new Vector3(11000, 0, 18000),
				new Regex("^arctic_tile_(\\d)(\\d)(_oilrig)?$|arctic_island_playerbase$"),
				PosMethod.Arctic,
				skipLoading: true);

			yield return new TileCollection(c,
				"Arctic_Volcano",
				new Vector3(15000, 0, 18000),
				new Regex("^island_42_volcano_tile_(\\d)(\\d)$"),
				PosMethod.Arctic,
				skipLoading: true);

			yield return new TileCollection(c,
				"Arctic_Islands",
				new Vector3(11000, 0, 13000),
				new Regex("^arctic_island_(\\w+)$|^island_37_container_depot$|^island_41_underwater_mining$"),
				PosMethod.Separate,
				skipLoading: true);

			yield return new TileCollection(c,
				"Arctic_Iceburgs",
				new Vector3(21000, 0, 19000),
				new Regex("^ice_shelf_(\\d+)(_rot_\\d)?$"),
				PosMethod.Separate,
				skipLoading: true);

			yield return new TileCollection(c,
				"Tracks",
				new Vector3(21000, 0, 11000),
				new Regex("^track_"),
				PosMethod.Separate);

			yield return new TileCollection(c,
				"Moon",
				new Vector3(-34000, 2000, -18000),
				new Regex("^moon_surface_(\\d+)_(\\d+)$"),
				PosMethod.Connected,
				skipLoading: true);

			yield return new TileCollection(c,
				"Other",
				new Vector3(21000, 0, 3000),
				new Regex("."),
				PosMethod.Separate,
				skipLoading: true)
				{ gridMaxX = 20 };
		}

		public override string ToString()
		{
			return $"{{{typeof(TileCollection).FullName}: '{name}'}}";
		}

		#region MatchFunctions

		public static Vector3 Position_Connected(Match match)
		{
			if (!match.Success) throw new ArgumentException($"{nameof(match)} should be successful");


			int x = int.Parse(match.Groups[1].Value);
			int y = 0;
			int z = int.Parse(match.Groups[2].Value);

			return new Vector3(x * 1000, y * 1000, z * 1000);
		}

		public static Vector3 Position_Arctic(Match match)
		{
			if (!match.Success) throw new ArgumentException($"{nameof(match)} should be successful");

			// Special case because Stormworks.
			if (match.Value == "arctic_island_playerbase")
			{
				return new Vector3(0 * 1000, 0 * 1000, -1 * 1000);
			}


			int x = int.Parse(match.Groups[2].Value);
			int y = 0;
			int z = int.Parse(match.Groups[1].Value);

			z = -z;

			return new Vector3(x * 1000, y * 1000, z * 1000);
		}

		public static Vector3 Position_Donkk(Match match)
		{
			if (!match.Success) throw new ArgumentException($"{nameof(match)} should be successful");

			// Special case because Stormworks.
			// "island_33_tile_end"
			if (match.Value.Contains("end"))
			{
				if (match.Groups[5].Success)
				{
					// "island_33_tile_end_1"
					int i = int.Parse(match.Groups[5].Value);

					return new Vector3(10 * 1000, 0 * 1000, -i * 1000);
				}

				return new Vector3(9 * 1000, 0 * 1000, -1 * 1000);
			}

			int x = int.Parse(match.Groups[3].Value);
			int y = 0;
			int z = int.Parse(match.Groups[2].Value);

			z = -z;

			return new Vector3(x * 1000, y * 1000, z * 1000);
		}

		private int gridX = 0;
		private int gridY = 0;

		public int gridMaxX = 20;

		public Vector3 Position_Separate(Match match)
		{
			if (!match.Success) throw new ArgumentException($"{nameof(match)} should be successful");

			if (gridX >= gridMaxX)
			{
				gridX = 0;
				gridY++;
			}

			int x = gridX++ * 2;
			int y = 0;
			int z = gridY * -2;


			return new Vector3(x * 1000, y * 1000, z * 1000);
		}

		#endregion MatchFunctions
	}
}
