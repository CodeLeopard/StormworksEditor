// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

using Behaviours;

using BinaryDataModel;
using BinaryDataModel.Converters;

using DataTypes.Extensions;

using Shared;

using Tools;

using Unity.Profiling;

using UnityEngine;

using Debug = UnityEngine.Debug;
using Exception = System.Exception;
using SWMesh = BinaryDataModel.Converters.Binary;

namespace Tiles
{
	/// <summary>
	/// Loads tile data (and trees todo: Separation of Concerns )
	/// </summary>
	[DisallowMultipleComponent]
	public class TileLoader : M0noBehaviour
	{
		private static readonly ProfilerMarker profilerMarker_InstantiateTileRoot =
			new ProfilerMarker($"{nameof(TileLoader)}.InstantiateTileRoot");

		public KeyCode ToggleTreesKey_Down = KeyCode.T;
		public KeyCode ToggleTreesKey_Hold = KeyCode.LeftAlt;



		public event Action LoadingComplete;


		#region Private
		[NonSerialized]
		public Task tileLoader;

		[NonSerialized]
		public GameObject tileCollectionParent;
		public const string tileCollectionParentName = "Tiles";


		private static string romFolder => StormworksPaths.rom;
		private static string tilesFolder => StormworksPaths.Data.tiles;


		private int terrainLayer;


		public Dictionary<GameObject, TileWrapper> MapGameObjectToTile;
		public Dictionary<string, TileWrapper> MapTileNameToTile;


		/// <summary>
		/// TileLoader will check these task for success and wait for them when measuring loading time.
		/// </summary>
		public readonly SynchronizedHashSet<Task> LoadingTasksToWaitFor = new SynchronizedHashSet<Task>();


		private readonly CancellationTokenSource cancelSource = new CancellationTokenSource();
		public CancellationToken CancelToken { get; private set; }

		#endregion Private


		[NonSerialized]
		public bool AnyTileFailed;

		#region Options

		public TileLoadSettings Settings => EditorSettingsManager.Settings.TileLoadSettings;


		public bool LoadTrees => Settings.LoadTrees;


		public bool CreatePhysicsTrees => Settings.LoadTreesPhys;

		// Loading options
		public bool LoadExtraTiles => Settings.LoadAllTiles;

		public bool LoadSubObjects => Settings.LoadAllMesh;


		public bool LoadEditAreas => Settings.LoadEditArea;

		public bool LoadInteractables => Settings.LoadInteractable;

		public bool LoadTrackLines => Settings.LoadTracks;


		public Material TerrainMaterial;
		public PhysicMaterial TerrainPhysMaterial;



		[Serializable]
		internal class _ThreadingSettings
		{
			[SerializeField]
			internal int TileThreads = 16;

			[SerializeField] internal float tileLoaderPriority = 20;
			[SerializeField] internal float tileInitialObjectPriority = 30;
			[SerializeField] internal float tileInitialSubsequentObjectsPriority = 40;
			[SerializeField] internal float tileOtherObjectsPriority = 50;


			[SerializeField] internal bool tileCompletionWaitForTreeLoad = true;


			[SerializeField] internal float treeLoaderPriority = 60;
			[SerializeField] internal uint treeInstancesPerTick = 20;
			[SerializeField] internal float treeBakePriority = 61;
			[SerializeField] internal bool treeLoadingCompletionWaitForBake = true;
		}

		[SerializeField]
		internal _ThreadingSettings ThreadingSettings = new _ThreadingSettings();

		public enum TileLoadState : byte { NotStarted, Canceled, Failed, Started, Waiting, Loading, Completed_Waiting, Completed, }

		[SerializeField]
		internal GameObject TrackXmlContainerPrefab;

		[SerializeField]
		internal GameObject TrackXmlNodePrefab;

		[SerializeField]
		internal GameObject TrackXmlLinePrefab;


		#endregion Options

		public static string Status { get; private set; } = "Not started yet.";


		/// <summary>
		/// Should loading be aborted.
		/// </summary>
		private bool ShouldAbortLoading => _ShouldAbortLoading();

		private bool _ShouldAbortLoading()
		{
			return cancelSource.IsCancellationRequested || RunOnMainThread.CancelableTasksShouldCancel;
		}

		public static void ThrowIfAbortLoading()
		{
			if(null == instance || instance.ShouldAbortLoading)
				throw new OperationCanceledException();
		}

		public void Clear()
		{
			Destroy(tileCollectionParent);
		}

		public static TileLoader instance { get; private set; }


		#region UnityMessages



		protected override void OnAwake()
		{
			instance = this;

			Settings.Apply();

			terrainLayer = LayerMask.NameToLayer("Terrain");

			MapGameObjectToTile = new Dictionary<GameObject, TileWrapper>();
			MapTileNameToTile = new Dictionary<string, TileWrapper>();

			tileCollectionParent = transform.Find(tileCollectionParentName)?.gameObject;

			if (null == tileCollectionParent)
			{
				tileCollectionParent = new GameObject(tileCollectionParentName);
				tileCollectionParent.hideFlags |= HideFlags.DontSaveInBuild | HideFlags.DontSaveInEditor;
				tileCollectionParent.transform.localPosition = new Vector3(500, 0, 500);
				tileCollectionParent.transform.parent = gameObject.transform;
			}
		}

		protected override void OnEnableAndAfterStart()
		{
			if (!Application.isPlaying) return;

			RunOnMainThread.EnsureInitialized();
			Load_Tiles();
		}



		private void OnDestroy()
		{
			// To prevent things from breaking during shutdown we need to wait for the tasks to stop.

			cancelSource.Cancel();

			if (null != tileLoader)
			{
				try
				{
					// The loading process depends on RunOnMainThread so we need to keep that alive while we are waiting.
					// If we don't cycle RunOnMainThread we could deadlock.
					bool keepWaiting = true;
					while (keepWaiting)
					{
						RunOnMainThread.instance.RunCycle();

						bool didComplete = tileLoader.Wait(0);
						keepWaiting = !didComplete;
					}
				}
				catch
				{
					// don't care.
				}
			}

			if (null != LoadingTasksToWaitFor)
			{
				// ToArray -> WaitAll could miss a task added by a task we ware waiting on.
				while (LoadingTasksToWaitFor.Count > 0)
				{
					var arr = LoadingTasksToWaitFor.ToArray();
					try
					{
						Task.WaitAll(arr);
					}
					catch
					{
						// don't care.
					}

					foreach (Task task in arr)
					{
						if (task.IsCompleted)
						{
							LoadingTasksToWaitFor.Remove(task);
						}
					}
				}
			}
		}




		#endregion UnityMessages


		public TileWrapper GetNearestTile(OpenTK.Mathematics.Vector3 target)
		{
			return GetNearestTile(target.ToUnity());
		}

		/// <summary>
		/// Searches for the nearest tile to the given world position.
		/// </summary>
		/// <param name="target"></param>
		/// <returns></returns>
		public TileWrapper GetNearestTile(Vector3 target)
		{
			TileWrapper best = null;
			float bestSquaredDistance = float.PositiveInfinity;
			foreach (var wrapper in MapTileNameToTile.Values)
			{
				// beware: worldPosition is cached.
				var rootPosition = wrapper.worldPosition;

				var distSq = (target - rootPosition).sqrMagnitude;

				if (distSq < bestSquaredDistance)
				{
					best = wrapper;
					bestSquaredDistance = distSq;
				}
			}

			return best;
		}

		public bool TryGetCurrentTile(OpenTK.Mathematics.Vector3 worldPos, out TileWrapper result)
		{
			return TryGetCurrentTile(worldPos.ToUnity(), out result);
		}

		public bool TryGetCurrentTile(Vector3 worldPos, out TileWrapper result)
		{
			result = GetNearestTile(worldPos);

			if (null == result) return false;

			var distance = result.worldPosition - worldPos;
			const float closeEnough = 1000 * 1000f;
			if (distance.sqrMagnitude < closeEnough) // todo: this is WRONG, but for the current use-case it's good enough.
			{
				return true;
			}

			result = null;
			return false;
		}

		public Task StartReloadTiles()
		{
			Console.WriteLine($"[{nameof(TileLoader)}]::{nameof(Button_ReloadTiles)} Starting reloading of tiles. {DateTime.Now}");
			var reloadTask = Task.Run(Task_ReloadTiles, CancelToken);
			return reloadTask;
		}

		public void Button_ReloadTiles()
		{
			StartReloadTiles();
		}

		public void Button_DirtyTiles()
		{
			foreach (var wrapper in MapGameObjectToTile.Values)
			{
				wrapper.tile.ChangedSinceLastSaved = true;
			}
		}

		public void Button_ReloadOutdatedVehicles()
		{
			VehicleLoader.ClearOutdated();
		}

		public void Button_ReloadAllVehicles()
		{
			VehicleLoader.ClearCache();
		}

		public void Button_ClearMeshCache()
		{
			MeshCache.Clear();
		}

		public void StartReloadTrackLines()
		{
			foreach (var wrapper in MapTileNameToTile.Values)
			{
				wrapper.tileBehaviour.ReloadTracksOnly();
			}
		}

		private void Task_ReloadTiles()
		{
			try
			{
				Stopwatch timer = new Stopwatch();
				timer.Start();
				bool forceReload = Settings.ChangedSinceLastLoaded;

				string prefix = forceReload ? "Reloading all" : "Reloading changed";

				Status = $"{prefix}...";


				// Also cause vehicles to be re-loaded in case they ware changed.
				// (Even if the tile itself wasn't changed.)
				VehicleLoader.ClearOutdated();

				int total = MapGameObjectToTile.Keys.Count;
				int counter = 0;
				var tasks = new List<Task>();
				foreach (GameObject go in MapGameObjectToTile.Keys)
				{
					if (ShouldAbortLoading) return;


					Status = $"{prefix} ({counter++}/{total})";

					// Note: we could use an ennumerator here
					// but that could overload the main thread with work
					// and freeze/timeout the RunOnMainThread activity.

					// So we use a Task and RunOnMainThread with wait and default priority
					// so that we don't overload it.

					RunOnMainThread.EnqueueAndWaitForCompletion(StartReload);
					void StartReload()
					{
						Task maybeTask;
						if (forceReload)
						{
							maybeTask = go.GetRequiredComponent<TileBehaviour>().StartReloadAlways();
						}
						else
						{
							maybeTask = go.GetRequiredComponent<TileBehaviour>().StartReloadIfFileChangedOrDirty();
						}

						if (null != maybeTask)
						{
							tasks.Add(maybeTask);
						}
					}
				}

				Status = $"Reloading, waiting for {tasks.Count} subTasks...";

				if (! tasks.Any())
				{
					Console.WriteLine
						($"[{nameof(TileLoader)}]:[{nameof(Task_ReloadTiles)}] No tiles needed reloading.");
					return;
				}

				var array = tasks.ToArray();
				Task.WaitAll(array, CancelToken);


				Settings.ChangedSinceLastLoaded = false;


				timer.Stop();
				var elapsed = timer.Elapsed;

				Status = $"Reloading Done after {elapsed}.";
				Console.WriteLine
					(
					 $"[{nameof(TileLoader)}]:[{nameof(Task_ReloadTiles)}] All tiles reloaded after {elapsed}."
					);
			}
			catch (Exception e)
			{
				RunOnMainThread.LogException(e, this);
				throw;
			}
		}



		private List<TileCollection> tileCollections;
		private void SetupCollections()
		{
			tileCollections = new List<TileCollection>(TileCollection.DefaultCollections(tileCollectionParent));

			var instanceSettings = InstanceSettingsManager.Settings;

			foreach (var tileCollection in tileCollections)
			{
				bool skipLoading = !instanceSettings.IsTileCollectionEnabled(tileCollection.name);
				tileCollection.skipLoading = skipLoading;
			}
		}

		public void Load_Tiles()
		{
			Status = "Prepare to Start Task...";
			SetupCollections();

			AnyTileFailed = false;

			Status = "Starting Task...";
			tileLoader = Task.Run(Task_TileLoader);
		}

		private void Task_TileLoader()
		{
			try
			{
				ThrowIfAbortLoading();
				var timer = new Stopwatch();
				timer.Start();

				var primaryTileTasks = new SynchronizedHashSet<Task>();

				Status = "Task running.";

				if (Settings.LoadTrees && ! Settings.LoadTreesPhys)
				{
					Debug.LogWarning("Loading Trees without physics, they will not be selectable!");
				}

				Console.WriteLine($"[{nameof(TileLoader)}] Started asynchronous loading of tiles.\n\n\n\n");

				var paths = Directory.GetFiles(tilesFolder, "mega_island*.xml").AsEnumerable();
				if (LoadExtraTiles) paths = paths.Union(Directory.GetFiles(tilesFolder, "*.xml"));

				foreach(string tileFilePath in paths)
				{
					ThrowIfAbortLoading();

					var fileName = Path.GetFileNameWithoutExtension(tileFilePath);

					if (Regex.IsMatch(fileName, "_instances"))
					{
						// *_instances.xml files contain trees, these are loaded separately.
						continue;
					}

					if (! Settings.LoadRotations && Regex.IsMatch(fileName, "_rot_\\d"))
					{
						// rotations of tiles. They don't show up in the in-game editor, so we won't show them either.
						continue;
					}

					GameObject tileParent = null;
					Vector3 localPosition = Vector3.zero;
					TileCollection tileCollection = null;

					foreach (var collection in tileCollections)
					{
						var match = collection.itemNameRegex.Match(fileName);
						if (match.Success)
						{
							tileParent = collection.container;

							localPosition = collection.PositionParser(match);


#if DEBUG
							if (collection.name == "Any")
							{
								// put breakpoint
							}
#endif
							tileCollection = collection;

							Console.WriteLine($"[{nameof(TileLoader)}] matched tile '{fileName}' to collection '{collection.name}'.");
							break;
						}
					}

					if (null == tileCollection)
					{
						throw new Exception
							(
							 $"No regex matched {name}. "
						   + $"This is not allowed, ensure there are no dangling names with a catch-all regex, or an exclude regex"
							);
					}

					if (tileCollection.skipLoading)
					{
						Console.WriteLine($"[{nameof(TileLoader)}] skipping loading tile '{fileName}' because the collection '{tileCollection.name}' is set to not load it's tiles.");
						continue;
					}

					

					Status = $"Running ({fileName})";

					var tileTask = StartTask_InstantiateTileRoot(fileName, localPosition, tileFilePath, tileParent);
					primaryTileTasks.Add(tileTask);
				}

				Console.WriteLine($"[{nameof(TileLoader)}] Tile Instantiation done after {timer.Elapsed}, now waiting for Tiles to complete loading...");

				WaitForGrowingCollection(primaryTileTasks, CancelToken, "tiles to initialize...");
				primaryTileTasks.Dispose();
				Console.WriteLine($"[{nameof(TileLoader)}] Tile Primary (Terrain, physics) Loading done after {timer.Elapsed}...");


				WaitForGrowingCollection(LoadingTasksToWaitFor, CancelToken, "tiles to finish loading...");
				Console.WriteLine($"[{nameof(TileLoader)}] Tile Secondary (SubObjects, trees, etc.) Loading done after {timer.Elapsed}...");
				Status = "Running Completion Events...";

				RunOnMainThread.EnqueueAndWaitForCompletion(DoLoadingComplete);
				void DoLoadingComplete()
				{
					timer.Stop();
					Status = $"Invoking {nameof(LoadingComplete)}...";
					LoadingComplete?.Invoke();

					Status = $"Done after {timer.Elapsed}";
					Debug.Log($"[{nameof(TileLoader)}] Tile Loading Fully Completed after {timer.Elapsed}.");
				}
			}
			catch (OperationCanceledException)
			{
				// Don't care -> don't log.
				Status = "Loading Canceled";
				throw;
			}
			catch (Exception e)
			{
				Status = $"Loading failed: {e.Message}";
				RunOnMainThread.LogException(e, this);
				throw;
			}
		}

		private static void WaitForGrowingCollection(SynchronizedHashSet<Task> tasks, CancellationToken cancelToken, string message)
		{
			while (tasks.Count > 0)
			{
				Status = $"Waiting for {tasks.Count} {message}";

				var buffer = new Task[tasks.Count];
				// LinQ will throw when an element is added while it's running.
				// So we have to do it manually.
				tasks.CopyTo(buffer, 0, buffer.Length);
				// If only Task.Wait$ could just work on ICollection and not complain when elements ware added/Removed.
				Task.WaitAny(buffer, cancelToken);

				for (int i = 0; i < buffer.Length; i++)
				{
					var t = buffer[i];
					if (t == null) break;

					if (t.IsCompleted)
					{
						tasks.Remove(t);
					}
				}
			}
		}

		private Task StartTask_InstantiateTileRoot(
			string tileName,
			Vector3 localPosition,
			string tileFilePath,
			GameObject tileParent)
		{
			using (profilerMarker_InstantiateTileRoot.Auto())
			{
				return RunOnMainThread.EnqueueAndWaitForCompletion(MakeTileRoot);
				Task MakeTileRoot()
				{
					ThrowIfAbortLoading();

					var _tileRoot = new GameObject(tileName);
					_tileRoot.transform.parent = tileParent.transform;
					_tileRoot.transform.localPosition = localPosition;

					var tileBehaviour = _tileRoot.AddComponent<TileBehaviour>();
					return tileBehaviour.Initialize(tileName, tileFilePath);
				}
			}
		}
	}
}
