﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Behaviours;

using BinaryDataModel.Converters;

using DataTypes.Attributes;

using Shared;
using Tools;

using UnityEngine;

using Debug = UnityEngine.Debug;

namespace Tiles
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(TileLoader))]
	public class SpecialTileLoader : M0noBehaviour
	{
		const float priority = 200;



		public Task loader;

		private Stopwatch loadTimer = new Stopwatch();


		private TileLoader myTileLoader;

		Regex regex = new Regex("^arid_island_(\\d+)_(\\d+)$");

		private int terrainLayer;

		public GameObject tileCollectionRoot => myTileLoader.tileCollectionParent;

		[InspectorReadonly]
		public GameObject container;


		public Material TerrainMaterial => myTileLoader.TerrainMaterial;
		public PhysicMaterial TerrainPhysMaterial => myTileLoader.TerrainPhysMaterial;


		private static string meshesFolder => StormworksPaths.meshes;


		/// <inheritdoc />
		protected override void OnEnableAndAfterStart()
		{
			myTileLoader = GetRequiredComponent<TileLoader>();

			terrainLayer = LayerMask.NameToLayer("Terrain");

			if (!Application.isPlaying) return;

			RunOnMainThread.EnsureInitialized();

			Load_Tiles();
		}


		private bool alreadyComplainedAboutLoader;

		public void Update()
		{
			if (!alreadyComplainedAboutLoader && loader?.Exception != null)
			{
				alreadyComplainedAboutLoader = true;
				Debug.LogException(loader.Exception, this);
			}
		}


		private void Load_Tiles()
		{
			loader = Task.Run(Task_Load_Tiles);
		}

		public static Vector3 TilePositionArid(Match match)
		{
			if (!match.Success) throw new ArgumentException($"{nameof(match)} should be successful");


			int x = int.Parse(match.Groups[1].Value);
			int y = 0;
			int z = int.Parse(match.Groups[2].Value);

			return new Vector3(x * 1000, y * 1000, z * 1000);
		}

		public void Task_Load_Tiles()
		{
			try
			{
				const string folder = "leaked_arid_mainland";
				if (! Directory.Exists(Path.Combine(meshesFolder,folder)))
				{
					Debug.Log($"[{nameof(SpecialTileLoader)}] Folder does not exist, skipping");
					return;
				}


				Debug.Log($"[{nameof(SpecialTileLoader)}] Start");
				loadTimer.Start();


				void EnsureContainer()
				{
					if (null == container)
					{
						container = new GameObject("Leaked Arid Island");
						container.transform.parent = tileCollectionRoot.transform;
						container.transform.position = new Vector3(0, 0, -37_000);
					}
				}

				RunOnMainThread.EnqueueAndWaitForCompletion(EnsureContainer);


				var paths = Directory.GetFiles(Path.Combine(meshesFolder, folder), "arid_island*.mesh")
				                     .AsEnumerable();


				foreach (string meshFilePath in paths)
				{
					if (RunOnMainThread.CancelableTasksShouldCancel) return;
					var fileName = Path.GetFileNameWithoutExtension(meshFilePath);

					if (Regex.IsMatch(fileName, "_instances"))
					{
						// *_instances.xml files contain trees.
						continue;
					}

					if (Regex.IsMatch(fileName, "_rot_\\d"))
					{
						// rotations of tiles. They don't show up in the in-game editor, so we won't show them either.
						continue;
					}

					var match = regex.Match(fileName);
					if (match.Success)
					{
						var localPosition = TilePositionArid(match);

						var meshData = Binary.LoadMesh(meshFilePath);

						var meshInfo = meshData.ToUnityMesh();

						void MainThreadAction()
						{
							var tileRoot = new GameObject(name);
							tileRoot.layer = terrainLayer;
							tileRoot.transform.parent = container.transform;
							tileRoot.transform.localPosition = localPosition;

							var filter = tileRoot.AddComponent<MeshFilter>();
							filter.sharedMesh = meshInfo.Mesh;

							var renderer = tileRoot.AddComponent<MeshRenderer>();
							renderer.materials = meshInfo.Materials.ToArray();

							 var collider = tileRoot.AddComponent<MeshCollider>();
							 collider.convex = false;
							 collider.cookingOptions =
								 MeshColliderCookingOptions.EnableMeshCleaning
							   | MeshColliderCookingOptions.WeldColocatedVertices;
							 collider.sharedMesh = meshInfo.Mesh;
							 collider.sharedMaterial = TerrainPhysMaterial;
						}

						RunOnMainThread.Enqueue(MainThreadAction, priority);
					}
				}


				void LoadDone()
				{
					loadTimer.Stop();
					Debug.Log($"[{nameof(SpecialTileLoader)}] Done");
				}

				RunOnMainThread.EnqueueAndWaitForCompletion(LoadDone, priority);
			}
			catch (Exception e)
			{
				RunOnMainThread.LogException(e, this);
				throw;
			}
		}
	}
}
