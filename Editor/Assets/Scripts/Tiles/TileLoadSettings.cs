// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

using Manage;

using Shared;

namespace Tiles
{
	[Serializable]
	public class TileLoadSettings : SettingsBase
	{
		private bool _overrideStormworksDirectory = false;
		[VisibleProperty]
		public bool OverrideStormworksDirectory
		{
			get => _overrideStormworksDirectory;
			set => SetWithMarkChanged(ref _overrideStormworksDirectory, value);
		}

		private string _stormworksDirectory = StormworksPaths.Install;
		[VisibleProperty]
		public string StormworksDirectory
		{
			get => _stormworksDirectory;
			set => SetWithMarkChanged(ref _stormworksDirectory, value);
		}


		private bool _loadAllTiles = true;
		[VisibleProperty]
		public bool LoadAllTiles
		{
			get => _loadAllTiles;
			set => SetWithMarkChanged(ref _loadAllTiles, value);
		}


		private bool _loadRotations = true;
		/// <summary>
		/// Load rotated variants of tiles as stand-alone tiles.
		/// </summary>
		[VisibleProperty]
		public bool LoadRotations
		{
			get => _loadRotations;
			set => SetWithMarkChanged(ref _loadRotations, value);
		}


		#region LoadWhatParts


		private bool _loadMesh = true;
		[VisibleProperty]
		public bool LoadMesh
		{
			get => _loadMesh;
			set => SetWithMarkChanged(ref _loadMesh, value);
		}

		private bool _loadPhys = false;
		[VisibleProperty]
		public bool LoadPhys
		{
			get => _loadPhys;
			set => SetWithMarkChanged(ref _loadPhys, value);
		}


		private bool _loadAllMesh = true;
		[VisibleProperty]
		public bool LoadAllMesh
		{
			get => _loadAllMesh;
			set => SetWithMarkChanged(ref _loadAllMesh, value);
		}


		#region Lights
		private bool _loadOmniLight = true;
		[VisibleProperty]
		public bool LoadOmniLights
		{
			get => _loadOmniLight;
			set => SetWithMarkChanged(ref _loadOmniLight, value);
		}

		private bool _loadSpotLight = true;
		[VisibleProperty]
		public bool LoadSpotLights
		{
			get => _loadSpotLight;
			set => SetWithMarkChanged(ref _loadSpotLight, value);
		}

		private bool _loadTubeLight = true;
		[VisibleProperty]
		public bool LoadTubeLights
		{
			get => _loadTubeLight;
			set => SetWithMarkChanged(ref _loadTubeLight, value);
		}
		#endregion Lights


		private bool _loadEditArea = true;
		[VisibleProperty]
		public bool LoadEditArea
		{
			get => _loadEditArea;
			set => SetWithMarkChanged(ref _loadEditArea, value);
		}


		private bool _loadInteractable = true;
		[VisibleProperty]
		public bool LoadInteractable
		{
			get => _loadInteractable;
			set => SetWithMarkChanged(ref _loadInteractable, value);
		}


		private bool _loadTracks = false;
		[VisibleProperty]
		public bool LoadTracks
		{
			get => _loadTracks;
			set => SetWithMarkChanged(ref _loadTracks, value);
		}



		private bool _loadTrees = true;
		[VisibleProperty]
		public bool LoadTrees
		{
			get => _loadTrees;
			set => SetWithMarkChanged(ref _loadTrees, value);
		}

		private bool _loadTreesPhys = true;
		[VisibleProperty(nameOverride: "Tree Physics (Makes them selectable)")]
		public bool LoadTreesPhys
		{
			get => _loadTreesPhys;
			set => SetWithMarkChanged(ref _loadTreesPhys, value);
		}


		private float _seafloorHeight = -300;
		[VisibleProperty]
		public float SeaFloorHeight
		{
			get => _seafloorHeight;
			set
			{
				if (SetWithMarkChanged(ref _seafloorHeight, value))
				{
					SeaFloorHeightChanged?.Invoke(value);
				}
			}
		}

		public event Action<float> SeaFloorHeightChanged;


		#endregion LoadWhatParts

		#region InitialVisibility

		private const string prefix = "Startup visibility of ";

		private bool _showMesh = true;
		[VisibleProperty(nameOverride: prefix + "Mesh")]
		public bool ShowMesh
		{
			get => _showMesh;
			set => SetWithMarkChanged(ref _showMesh, value);
		}

		private bool _showPhys = true;
		[VisibleProperty(nameOverride: prefix + "Phys")]
		public bool ShowPhys
		{
			get => _showPhys;
			set => SetWithMarkChanged(ref _showPhys, value);
		}



		#region Lights

		private bool _showLightSelector = true;
		[VisibleProperty(nameOverride: prefix + "Light Selector")]
		public bool ShowLightSelector
		{
			get => _showLightSelector;
			set => SetWithMarkChanged(ref _showLightSelector, value);
		}

		private bool _showLightEmission = true;
		[VisibleProperty(nameOverride: prefix + "Light Emission")]
		public bool ShowLightEmission
		{
			get => _showLightEmission;
			set => SetWithMarkChanged(ref _showLightEmission, value);
		}


		private bool _showOmniLight = true;
		[VisibleProperty(nameOverride: prefix + "OmniLight")]
		public bool ShowOmniLights
		{
			get => _showOmniLight;
			set => SetWithMarkChanged(ref _showOmniLight, value);
		}

		private bool _showSpotLight = true;
		[VisibleProperty(nameOverride: prefix + "SpotLight")]
		public bool ShowSpotLights
		{
			get => _showSpotLight;
			set => SetWithMarkChanged(ref _showSpotLight, value);
		}

		private bool _showTubeLight = true;
		[VisibleProperty(nameOverride: prefix + "TubeLight")]
		public bool ShowTubeLights
		{
			get => _showTubeLight;
			set => SetWithMarkChanged(ref _showTubeLight, value);
		}
		#endregion Lights


		private bool _showEditArea = true;
		[VisibleProperty(nameOverride: prefix + "Edit Area")]
		public bool ShowEditArea
		{
			get => _showEditArea;
			set => SetWithMarkChanged(ref _showEditArea, value);
		}

		private bool _showEditAreaWithVehicle = true;
		[VisibleProperty(nameOverride: prefix + "Vehicle (Edit Area)")]
		public bool ShowEditAreaWithVehicle
		{
			get => _showEditAreaWithVehicle;
			set => SetWithMarkChanged(ref _showEditAreaWithVehicle, value);
		}

		private bool _showEditAreaWithVehicleBounds = false;
		[VisibleProperty(nameOverride: prefix + "Vehicle Bounds (Edit Area)")]
		public bool ShowEditAreaWithVehicleBounds
		{
			get => _showEditAreaWithVehicleBounds;
			set => SetWithMarkChanged(ref _showEditAreaWithVehicleBounds, value);
		}


		private bool _showInteractable = true;
		[VisibleProperty(nameOverride: prefix + "Interactable")]
		public bool ShowInteractable
		{
			get => _showInteractable;
			set => SetWithMarkChanged(ref _showInteractable, value);
		}


		private bool _showTracks = true;
		[VisibleProperty(nameOverride: prefix + "Xml Tracks")]
		public bool ShowTracks
		{
			get => _showTracks;
			set => SetWithMarkChanged(ref _showTracks, value);
		}


		private bool _showTrees = true;
		[VisibleProperty(nameOverride: prefix + "Trees")]
		public bool ShowTrees
		{
			get => _showTrees;
			set => SetWithMarkChanged(ref _showTrees, value);
		}

		private bool _showSea = true;
		[VisibleProperty(nameOverride: prefix + "Sea")]
		public bool ShowSea
		{
			get => _showSea;
			set => SetWithMarkChanged(ref _showSea, value);
		}

		private bool _showSeaFloor = true;
		[VisibleProperty(nameOverride: prefix + "Sea Floor")]
		public bool ShowSeaFloor
		{
			get => _showSeaFloor;
			set => SetWithMarkChanged(ref _showSeaFloor, value);
		}

		#endregion InitialVisibility

		private bool _seaRaycast = false;
		[VisibleProperty(nameOverride: "Initial state: Sea Level Raycast")]
		public bool SeaRaycast
		{
			get => _seaRaycast;
			set => SetWithMarkChanged(ref _seaRaycast, value);
		}

		#region WhatIsTerrain

		public List<string> TerrainIDRegexPatterns = new List<string>()
		{
			"mesh_\\d+(_phys)?",
			"Plate_part_\\d+(_phys)?"
		};
		//public List<string> MeshIDRegexPatterns = new List<string>();


		[XmlIgnore]
		public List<Regex> TerrainIDRegex;

		#endregion WhatIsTerrain



		public override void OnLoad()
		{
			if (OverrideStormworksDirectory)
			{
				StormworksPaths.Install = StormworksDirectory;

				Console.WriteLine($"Overridden Stormworks directory to '{StormworksDirectory}' as specified in settings.");
			}

			TerrainIDRegex = new List<Regex>(TerrainIDRegexPatterns.Count);
			foreach (string pattern in TerrainIDRegexPatterns)
			{
				TerrainIDRegex.Add(new Regex(pattern, RegexOptions.Compiled));
			}
		}

		public override void Apply()
		{
			var manager = ViewManager.Instance;

			// Some settings only specify initial values
			// but runtime changes are not automatically saved back.
			manager.MeshVisible = ShowMesh;
			manager.PhysVisible = ShowPhys;
			manager.LightSelectorEnabled = ShowLightSelector;
			manager.LightEmissionEnabled = ShowLightEmission;
			manager.OmniLightsVisible    = ShowOmniLights;
			manager.SpotLightsVisible    = ShowSpotLights;
			manager.TubeLightsVisible    = ShowTubeLights;
			manager.EditAreasVisible                  = ShowEditArea;
			manager.EditAreasWithVehicleVisible       = ShowEditAreaWithVehicle;
			manager.EditAreasWithVehicleBoundsVisible = ShowEditAreaWithVehicleBounds;
			manager.InteractablesVisible = ShowInteractable;
			manager.TrackLinesVisible    = ShowTracks;
			manager.TreesVisible         = ShowTrees;
			manager.SeaVisible           = ShowSea;
			manager.SeaFloorVisible      = ShowSeaFloor;
			manager.SetSeaRaycast(SeaRaycast);
		}
	}
}
