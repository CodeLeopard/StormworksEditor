﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

using Behaviours;

using BinaryDataModel.Converters;

using Control;

using DataModel.Tiles;

using DataTypes.Extensions;

using GUI.ItemSelector;
using GUI.MenuBar;
using Manage;
using RuntimeGizmos;
using Shared;
using SimpleFileBrowser;
using Tiles.Content;
using Tools;
using UnityEngine;

using Vector3 = OpenTK.Mathematics.Vector3;

namespace Tiles
{
	[RequireComponent(typeof(Camera))]
	public class TileInteraction : M0noBehaviour
	{


		private new Camera camera;
		private TransformGizmo transformGizmo;

		private LayerMask terrain;


		public static TileInteraction Instance;



		private GameObject CursorHeldObject;


		[NonSerialized]
		public bool selectTerrain = false;
		public static bool SelectTerrain
		{
			get => Instance.selectTerrain;
			set => Instance.selectTerrain = value;
		}

		#region UnityMessages

		/// <inheritdoc />
		protected override void OnAwake()
		{
			Instance = this;
			camera = GetRequiredComponent<Camera>();
			transformGizmo = GetRequiredComponent<TransformGizmo>();

			terrain = LayerMaskExt.NameToMask("Terrain");
		}


		private async void Update()
		{
			await keyboardInput();
		}


		#endregion UnityMessages

		public bool TerrainRayCast(out Ray ray, out RaycastHit hit)
		{
			ray = camera.ScreenPointToRay(Input.mousePosition);
			hit = default;

			if (!Physics.Raycast(ray, out hit, float.MaxValue, terrain, QueryTriggerInteraction.Ignore))
				return false;
			else
				return true;
		}


		private async Task keyboardInput()
		{
			if (InputManagement.TextInputFocused) return;
			if (InputManagement.AnyModifierKeyPressed) return;

			if (Input.GetKeyUp(KeyCode.G))
			{
				await TriggerPlacementMenu();
			}
		}

		private ButtonGrid PlacementMenu;


		string startingFolder = StormworksPaths.meshes;
		private async Task<string?> FilePicker(string message)
		{
			var semafore = new SemaphoreSlim(0, 1);
			string result = null;

			bool opened = FileBrowser.ShowLoadDialog(
				OnFilesSelected
				, OnFileSelectionCanceled
				, FileBrowser.PickMode.Files
				, false
				, startingFolder
				, null
				, message
				);

			if (!opened)
			{
				return await Task.FromResult<string>(null);
			}

			await semafore.WaitAsync();
			return await Task.FromResult(result);
			

			void OnFilesSelected(string[] selection)
			{
				if (null == selection || selection.Length == 0)
				{
					Debug.Log("No file was selected");
					return;
				}

				if (selection.Length > 1)
				{
					Debug.LogError("Multiple selection is not supported.");
					return;
				}

				result = selection[0];
				semafore.Release();
			}
			void OnFileSelectionCanceled()
			{
				Console.WriteLine($"User canceled file selection");
				semafore.Release();
			}
		}

		private async Task TriggerPlacementMenu()
		{
			if (PlacementMenu != null) return;

			if (! NewObjectPosition.Position(camera, terrain, out UnityEngine.Vector3 position))
			{
				Debug.LogWarning($"Could not spawn a TileObject because mouse is not over terrain, unable to find position to place the object", this);
				return;
			}

			var t = TileLoader.instance.GetNearestTile(position);

			if (null == t)
			{
				Debug.LogWarning($"Could not spawn a TileObject because there is no tile at the current mouse position.", this);
				return;
			}


			PlacementMenu = ButtonGrid.Create(null);
			PlacementMenu.gameObject.name = "NewTileItemSelector";
			PlacementMenu.Layout.cellSize = new Vector2(200, 50);
			PlacementMenu.AddCancelButton();

			GameObject meshGameObject = null;

			PlacementMenu.AddButton("Mesh&Phys", MeshPhys);
			void MeshPhys()
			{
				FileBrowser.SetFilters(false, ".mesh");
				var filePickerTask = FilePicker("Select a .mesh");
				filePickerTask.ContinueWith(OnSelect);

				async Task OnSelect(Task<string?> task)
				{
					var meshPath = await task;

					if (string.IsNullOrEmpty(meshPath))
					{
						Debug.LogWarning("User did not select a .mesh");
						return;
					}

					var folder = Path.GetDirectoryName(meshPath);
					var physPath = Path.Combine(folder, $"{Path.GetFileNameWithoutExtension(meshPath)}.phys");

					if (!File.Exists(physPath))
					{
						physPath = Path.Combine(folder, $"{Path.GetFileNameWithoutExtension(meshPath)}_phys.phys");
					}

					if (!File.Exists(physPath))
					{
						Debug.Log("Did not find a matching .phys file, prompting the user...");

						Task<string?> phystPathTask = null;
						void MT()
						{
							FileBrowser.SetFilters(false, ".phys");
							string mesh_name = Path.GetFileNameWithoutExtension(meshPath);
							phystPathTask = FilePicker($"Did not find a matching phys file for '{mesh_name}'. Select a .phys");
						}
						RunOnMainThread.EnqueueAndWaitForCompletion(MT);
						physPath = await phystPathTask;
					}

					if (string.IsNullOrEmpty(physPath))
					{
						Debug.LogWarning("User did not select a .phys");
						return;
					}

					// Convert absolute to relative to rom
					meshPath = StormworksPaths.GetRelativePath(meshPath);
					physPath = StormworksPaths.GetRelativePath(physPath);

					RunOnMainThread.EnqueueAndWaitForCompletion(MainThread);
					void MainThread()
					{
						var p = position - t.worldPosition;
						p.y += 0.5f;

						//---------------------------------------------

						var meshInfo = new MeshInfo();
						var tempTransform = meshInfo.LTransform;
						tempTransform.Row3 = p.WithW(1).ToOpenTK();
						meshInfo.LTransform = tempTransform;

						meshInfo.id = "mesh";
						meshInfo.file_name = meshPath;

						t.tile.Add(meshInfo);

						var physInfo = new PhysInfo();
						physInfo.LTransform = meshInfo.LTransform;
						physInfo.id = meshInfo.id + "_phys";
						physInfo.file_name = physPath;

						t.tile.Add(physInfo);

						//-------------------------------------------

						meshGameObject = new GameObject(meshInfo.id);
						var meshBehaviour = meshGameObject.AddComponent<MeshOrPhysBehaviour>();
						meshBehaviour.Initialize(meshInfo);

						t.tileBehaviour.Add(meshGameObject);

						var physGameObject = new GameObject(physInfo.id);
						var physBehaviour = physGameObject.AddComponent<MeshOrPhysBehaviour>();
						physBehaviour.Initialize(physInfo);

						t.tileBehaviour.Add(physGameObject);

						//-------------------------------------------

						meshBehaviour.SetupSameObjectLinks();
						physBehaviour.SetupSameObjectLinks();

						meshBehaviour.InitComplete();
						physBehaviour.InitComplete();

						if (!ViewManager.Instance.MeshVisible && !ViewManager.Instance.PhysVisible)
						{
							Debug.Log($"Setting Mesh to visible because an object was added while visibility for that object was disabled.");
							ViewManager.Instance.SetMeshVisible(true);
						}

						// Because of the asyncyness WaitForInteraction won't actually wait for us.
						// Se we do those actions ourselves.
						SelectionManager.Instance.ClearAndAddTarget(meshGameObject.transform);
						SelectionManager.DoNotSelectThisFrame();
					}
				}
			}

			PlacementMenu.AddButton("EditArea", MkEditArea);
			void MkEditArea()
			{
				var p = position - t.worldPosition;
				p.y += 0.5f;

				var dataModel = new EditArea();
				dataModel.size = Vector3.One;
				var tempTransform = dataModel.LTransform;
				tempTransform.Row3 = p.WithW(1).ToOpenTK();
				dataModel.LTransform = tempTransform;
				dataModel.id = "edit";

				t.tile.Add(dataModel);

				meshGameObject = new GameObject(dataModel.id);
				var b = meshGameObject.AddComponent<EditAreaBehaviour>();
				b.Initialize(dataModel);

				t.tileBehaviour.Add(meshGameObject);

				b.InitComplete();

				if (!ViewManager.Instance.EditAreasVisible)
				{
					Debug.Log($"Setting EditArea to visible because an object was added while visibility for that object was disabled.");
					ViewManager.Instance.SetEditAreasVisible(true);
				}
			}

			PlacementMenu.AddButton("Interactable", MkInteractable);
			void MkInteractable()
			{
				var p = position - t.worldPosition;
				p.y += 0.5f;

				var dataModel = new Interactable();
				dataModel.size = Vector3.One;
				var tempTransform = dataModel.LTransform;
				tempTransform.Row3 = p.WithW(1).ToOpenTK();
				dataModel.LTransform = tempTransform;
				dataModel.id = "interact";

				t.tile.Add(dataModel);

				meshGameObject = new GameObject(dataModel.id);
				var b = meshGameObject.AddComponent<InteractableBehaviour>();
				b.Initialize(dataModel);

				t.tileBehaviour.Add(meshGameObject);

				b.InitComplete();

				if (!ViewManager.Instance.InteractablesVisible)
				{
					Debug.Log($"Setting Interactable to visible because an object was added while visibility for that object was disabled.");
					ViewManager.Instance.SetInteractablesVisible(true);
				}
			}

			#region Lights
			PlacementMenu.AddButton("OmniLight", MkOmniLight);
			void MkOmniLight()
			{
				var p = position - t.worldPosition;
				p.y += 0.5f;

				var dataModel = new OmniLight();
				dataModel.color = new Vector3(5, 5, 5);
				dataModel.radius = 5;

				var tempTransform = dataModel.LTransform;
				tempTransform.Row3 = p.WithW(1).ToOpenTK();
				dataModel.LTransform = tempTransform;
				dataModel.id = "omni";

				t.tile.Add(dataModel);

				meshGameObject = new GameObject(dataModel.id);
				var b = meshGameObject.AddComponent<OmniLightBehaviour>();
				b.Initialize(dataModel);

				t.tileBehaviour.Add(meshGameObject);

				b.InitComplete();

				if (!ViewManager.Instance.OmniLightsVisible)
				{
					Debug.Log($"Setting OmniLight to visible because an object was added while visibility for that object was disabled.");
					ViewManager.Instance.SetOmniLighsVisible(true);
				}
			}

			PlacementMenu.AddButton("SpotLight", MkSpotLight);
			void MkSpotLight()
			{
				var p = position - t.worldPosition;
				p.y += 0.5f;

				var dataModel = new SpotLight();
				dataModel.color = new Vector3(5, 5, 5);
				dataModel.fov_deg = 40;
				dataModel.range = 20;

				var tempTransform = dataModel.LTransform;
				tempTransform.Row3 = p.WithW(1).ToOpenTK();
				dataModel.LTransform = tempTransform;
				dataModel.id = "spot";

				t.tile.Add(dataModel);

				meshGameObject = new GameObject(dataModel.id);
				var b = meshGameObject.AddComponent<SpotLightBehaviour>();
				b.Initialize(dataModel);

				t.tileBehaviour.Add(meshGameObject);

				b.InitComplete();

				if (!ViewManager.Instance.SpotLightsVisible)
				{
					Debug.Log($"Setting SpotLight to visible because an object was added while visibility for that object was disabled.");
					ViewManager.Instance.SetSpotLightsVisible(true);
				}
			}

			PlacementMenu.AddButton("TubeLight", MkTubeLight);
			void MkTubeLight()
			{
				var p = position - t.worldPosition;
				p.y += 0.5f;

				var dataModel = new TubeLight();
				dataModel.color = new Vector3(5, 5, 5);
				dataModel.radius = 5;
				dataModel.length = 5;

				var tempTransform = dataModel.LTransform;
				tempTransform.Row3 = p.WithW(1).ToOpenTK();
				dataModel.LTransform = tempTransform;
				dataModel.id = "tube";

				t.tile.Add(dataModel);

				meshGameObject = new GameObject(dataModel.id);
				var b = meshGameObject.AddComponent<TubeLightBehaviour>();
				b.Initialize(dataModel);

				t.tileBehaviour.Add(meshGameObject);

				b.InitComplete();

				if (!ViewManager.Instance.TubeLightsVisible)
				{
					Debug.Log($"Setting TubeLight to visible because an object was added while visibility for that object was disabled.");
					ViewManager.Instance.SetTubeLightsVisible(true);
				}
			}
			#endregion Lights

			await PlacementMenu.WaitForInteraction();

			if (null == meshGameObject) return;

			SelectionManager.Instance.ClearAndAddTarget(meshGameObject.transform);
			SelectionManager.DoNotSelectThisFrame();
		}


		private void PlaceEditArea()
		{
			if (! TerrainRayCast(out Ray ray, out RaycastHit hit))
			{
				return;
			}

			var t = TileLoader.instance.GetNearestTile(hit.point);

			if (null == t)
			{
				// todo: complain
				return;
			}

			var p = hit.point - t.worldPosition;
			p.y += 0.5f;

			var editArea = new EditArea();
			editArea.size = Vector3.One;
			var temp = editArea.LTransform;
			temp.Row3 = p.WithW(1).ToOpenTK();
			editArea.LTransform = temp;
			editArea.id = "edit";

			t.tile.Add(editArea);

			var g = new GameObject(editArea.id);
			g.transform.parent = t.tileBehaviour.editAreaRoot.transform;
			var b = g.AddComponent<EditAreaBehaviour>();
			b.Initialize(editArea);
			b.InitComplete();

			SelectionManager.Instance.ClearAndAddTarget(g.transform);
		}

		private void PlaceInteractable()
		{
			if (!TerrainRayCast(out Ray ray, out RaycastHit hit))
			{
				return;
			}

			var t = TileLoader.instance.GetNearestTile(hit.point);

			if (null == t)
			{
				// todo: complain
				return;
			}

			var p = hit.point - t.worldPosition;
			p.y += 0.5f;

			var editArea = new Interactable();
			editArea.size = Vector3.One;
			var tempTransform = editArea.LTransform;
			tempTransform.Row3 = p.WithW(1).ToOpenTK();
			editArea.LTransform = tempTransform;
			editArea.id = "interact";

			t.tile.Add(editArea);

			var g = new GameObject(editArea.id);
			g.transform.parent = t.tileBehaviour.interactableRoot.transform;
			var b = g.AddComponent<InteractableBehaviour>();
			b.Initialize(editArea);
			b.InitComplete();

			SelectionManager.Instance.ClearAndAddTarget(g.transform);
		}

		public void SetSelectable(bool newValue)
		{
			selectTerrain = newValue;
		}
	}
}