﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

using Behaviours;

using DataModel.Tiles;

using DataTypes.Attributes;
using DataTypes.Extensions;

using Manage;

using Shared;
using Shared.Exceptions;
using Shared.Serialization;

using Tiles.Content;

using Tools;

using Unity.Profiling;

using UnityEngine;

namespace Tiles
{
	[DisallowMultipleComponent]
	public class TileBehaviour : M0noBehaviour
	{
		private static readonly ProfilerMarker profilerMarker_InstantiateTile =
			new ProfilerMarker($"{nameof(TileBehaviour)}.Task_InstantiateTile.");

		private static readonly ProfilerMarker profilerMarker_LoadTileXml =
			new ProfilerMarker($"{nameof(TileBehaviour)}.Task_LoadTileXml.");

		private static readonly ProfilerMarker profilerMarker_LoadTileData =
			new ProfilerMarker($"{nameof(TileBehaviour)}.Task_LoadTileData.");

		private static readonly ProfilerMarker profilerMarker_WaitForSubCompletion =
			new ProfilerMarker($"{nameof(TileBehaviour)}.Task_WaitForSubTaskCompletion.");


		private static TileLoader Loader =>TileLoader.instance;
		private static TileLoadSettings Settings => Loader.Settings;
		private static ViewManager ViewManager => ViewManager.Instance;


		private static float basePriority => Loader.ThreadingSettings.tileLoaderPriority;
		private static float initialPriority => Loader.ThreadingSettings.tileInitialObjectPriority;
		private static float subsequentPriority => Loader.ThreadingSettings.tileInitialSubsequentObjectsPriority;
		private static float otherObjectsPriority => Loader.ThreadingSettings.tileOtherObjectsPriority;


		private static Regex noSeaFloorRegex = new Regex("^moon_surface_", RegexOptions.Compiled);

		#region LoadWhat
		private bool LoadSubObjects => Loader.LoadSubObjects;
		private bool LoadMesh => Settings.LoadMesh;
		private bool LoadPhys => Settings.LoadPhys;
		private bool LoadOmniLights => Settings.LoadOmniLights;
		private bool LoadSpotLights => Settings.LoadSpotLights;
		private bool LoadTubeLights => Settings.LoadTubeLights;
		private bool LoadEditAreas => Loader.LoadEditAreas;
		private bool LoadInteractables => Loader.LoadInteractables;
		private bool LoadTrackLines => Loader.LoadTrackLines;
		private bool LoadTrees => Loader.LoadTrees;
		#endregion LoadWhat

		#region Visibility
		private bool TreesVisible => ViewManager.Instance.TreesVisible;


		#endregion Visibility

		private GameObject TrackXmlContainerPrefab => Loader.TrackXmlContainerPrefab;

		private bool AnyTileFailed
		{
			get => Loader.AnyTileFailed;
			set => Loader.AnyTileFailed = value;
		}


		private Dictionary<GameObject, TileWrapper> MapGameObjectToTile => TileLoader.instance.MapGameObjectToTile;
		private Dictionary<string, TileWrapper> MapTileNameToTile => TileLoader.instance.MapTileNameToTile;


		private bool internalTaskAbortRequested;
		private bool externalTaskAbortRequested => RunOnMainThread.CancelableTasksShouldCancel;

		private bool ShouldAbortLoading => internalTaskAbortRequested || externalTaskAbortRequested;

		private int terrainLayer;


		[field: SerializeField, InspectorReadonly]
		public string tileName { get; private set; }

		[field: SerializeField, InspectorReadonly]
		public string tileFilePath { get; private set; }


		public TileWrapper myWrapper { get; private set; }
		public Tile tileData { get; private set; }

		private TileLOD lodBehaviour;

		private GameObject treeRoot;
		private TileTreeManager treeManager;

		private GameObject seaFloor;


		private GameObject staticRoot;

		private GameObject omniLightRoot;
		private GameObject spotLightRoot;
		private GameObject tubeLightRoot;

		public GameObject editAreaRoot;

		public GameObject interactableRoot;

		public IEnumerable<GameObject> TileItemBaseRoots()
		{
			yield return editAreaRoot;
			yield return interactableRoot;
		}





		private GameObject trackRoot;
		private TrackLines trackLines;


		public bool ChangedSinceLastSaved
		{
			get => tileData.ChangedSinceLastSaved;
			set => tileData.ChangedSinceLastSaved = value;
		}

		private Task loadTask;
		private Task treeLoadTask;

		private DateTime xmlLastWriteTime;

		/// <summary>
		/// TileLoader will check these task for success and wait for them when measuring loading time.
		/// </summary>
		private SynchronizedHashSet<Task> loadingTasks => TileLoader.instance.LoadingTasksToWaitFor;


		public TileLoader.TileLoadState LoadState { get; private set; } = TileLoader.TileLoadState.NotStarted;

		#region UnityMessages

		protected override void OnAwake()
		{
			terrainLayer = LayerMask.NameToLayer("Terrain");

			lodBehaviour = gameObject.AddComponent<TileLOD>();
			lodBehaviour.LODChanged += LodBehaviour_LODChanged;
		}

		#endregion UnityMessages


		#region Events


		#endregion Events

		/// <summary>
		/// Set the data needed to load the Tile, and start the Task that will do the loading.
		/// Exceptions from this Task will be logged and swallowed.
		/// </summary>
		/// <param name="tileName"></param>
		/// <param name="tileFilePath"></param>
		/// <returns></returns>
		public Task Initialize(string tileName, string tileFilePath)
		{
			this.tileName = tileName;
			this.tileFilePath = tileFilePath;

			return StartLoading();
		}

		public Task StartLoading()
		{
			if (LoadState != TileLoader.TileLoadState.NotStarted)
			{
				// todo: prevent starting a 2nd task.
			}

			LoadState = TileLoader.TileLoadState.Started;

			loadTask = Task.Run(Task_InstantiateTile);

			return loadTask;
		}

		#region Re-Load

		public void ReloadTracksOnly()
		{
			trackLines?.MarkOutdated();
		}

		public Task StartReloadAlways()
		{
			return StartLoading();
		}

		/// <summary>
		/// If related files changed returns the <see cref="Task"/> that will reload,
		/// otherwise returns <see langword="null"/>.
		/// </summary>
		/// <returns></returns>
		public Task StartReloadIfFileChangedOrDirty()
		{
			if (new FileInfo(tileFilePath).LastWriteTimeUtc > xmlLastWriteTime
			 || ChangedSinceLastSaved)
			{
				Console.WriteLine($"[{nameof(TileBehaviour)}] Xml '{tileName}' updated, reloading...");
				return StartLoading();
			}

			StartCoroutine(ReloadVehicles());
			return null;
		}

		private IEnumerator ReloadVehicles()
		{
			if (null == editAreaRoot) yield break;
			foreach (Transform t in editAreaRoot.transform)
			{
				t.gameObject.GetComponent<EditAreaBehaviour>()?.UpdateVehicle();
				yield return null;
			}
		}


		#endregion Re-Load

		public void ClearTile()
		{
			// todo: abort the task and wait for it to prevent madness.
			if (null != staticRoot) DestroyOrImmediate(staticRoot);

			if (null != omniLightRoot) DestroyOrImmediate(omniLightRoot);
			if (null != spotLightRoot) DestroyOrImmediate(spotLightRoot);
			if (null != tubeLightRoot) DestroyOrImmediate(tubeLightRoot);

			if (null != editAreaRoot) DestroyOrImmediate(editAreaRoot);
			if (null != interactableRoot) DestroyOrImmediate(interactableRoot);

			if (null != trackRoot) DestroyOrImmediate(trackRoot);

			if (null != treeRoot) DestroyOrImmediate(treeRoot);
		}

		/// <summary>
		/// Add the given <see cref="GameObject"/> to the Tile.
		/// This keeps the internal BookKeeping up to date.
		/// It is okay if the represented <see cref="AbstractTileItem"/> has already been added to the <see cref="Tile"/>.
		/// </summary>
		/// <param name="go"></param>
		/// <param name="tileSpace">The GameObject has been placed in tile-relative coordinates.</param>
		/// <exception cref="NotImplementedException"></exception>
		public void Add(GameObject go, bool tileSpace = true)
		{
			bool worldPositionStays = ! tileSpace;

			var b = go.GetComponent<AbstractTileItemBehaviour>();

			tileData.Add(b.Data);
			go.name = b.Data.id;

			if (b is MeshOrPhysBehaviour)
			{
				go.transform.SetParent(staticRoot.transform, worldPositionStays);
			}
			else if (b is OmniLightBehaviour)
			{
				go.transform.SetParent(omniLightRoot.transform, worldPositionStays);
			}
			else if (b is SpotLightBehaviour)
			{
				go.transform.SetParent(spotLightRoot.transform, worldPositionStays);
			}
			else if (b is TubeLightBehaviour)
			{
				go.transform.SetParent(tubeLightRoot.transform, worldPositionStays);
			}
			else if (b is EditAreaBehaviour)
			{
				go.transform.SetParent(editAreaRoot.transform, worldPositionStays);
			}
			else if (b is InteractableBehaviour)
			{
				go.transform.SetParent(interactableRoot.transform, worldPositionStays);
			}
			else
			{
				throw new NotImplementedException($"{b.GetType().Name} is not implemented.");
			}
		}

		#region Set_Thing_Active



		private static readonly List<MeshOrPhysBehaviour> meshOrPhysBuffer = new List<MeshOrPhysBehaviour>();
		private static readonly List<BaseLightBehaviour> baseLightBehaviourBuffer = new List<BaseLightBehaviour>();
		private static readonly List<EditAreaBehaviour> editAreaBuffer = new List<EditAreaBehaviour>();

		public static void SetMeshActive(TileBehaviour instance, bool newValue)
		{
			meshOrPhysBuffer.Clear();
			foreach (Transform t in instance.staticRoot.transform)
			{
				t.GetComponentsInChildren(meshOrPhysBuffer);
				foreach (var b in meshOrPhysBuffer)
				{
					b.SetMeshVisible(newValue);
				}
				meshOrPhysBuffer.Clear();
			}
		}

		public static void SetPhysActive(TileBehaviour instance, bool newValue)
		{
			meshOrPhysBuffer.Clear();
			foreach (Transform t in instance.staticRoot.transform)
			{
				t.GetComponentsInChildren(meshOrPhysBuffer);
				foreach (var b in meshOrPhysBuffer)
				{
					b.SetPhysVisible(newValue);
				}
				meshOrPhysBuffer.Clear();
			}
		}

		public static void SetFlattenTerrain(TileBehaviour instance, bool newValue)
		{
			const float flatScale = 0.00159765468f; // Sentinel value

			meshOrPhysBuffer.Clear();
			foreach (Transform t in instance.staticRoot.transform)
			{
				t.GetComponentsInChildren(meshOrPhysBuffer);
				foreach (var b in meshOrPhysBuffer)
				{
					if (b.IsTerrain)
					{
						var scale = b.transform.localScale;
						if (scale.y == flatScale || scale.y == 1)
						// Only do something if the scale is 1 or the sentinel value.
						// Otherwise we would need to store the scale value somewhere so we can put it back.
						{
							if (newValue)
							{
								scale.y = flatScale;
							}
							else
							{
								scale.y = 1;
							}
						}
						b.transform.localScale = scale;
					}
				}
				meshOrPhysBuffer.Clear();
			}
		}

		#region Lights

		public IEnumerable<Transform> AllBaseLightTransforms()
		{
			if (null != omniLightRoot)
			{
				foreach(Transform t in omniLightRoot.transform)
				{
					yield return t;
				}
			}
			if (null != spotLightRoot)
			{
				foreach (Transform t in spotLightRoot.transform)
				{
					yield return t;
				}
			}
			if (null != tubeLightRoot)
			{
				foreach (Transform t in tubeLightRoot.transform)
				{
					yield return t;
				}
			}
		}

		public static void SetLightSelectorEnabled(TileBehaviour instance, bool newValue)
		{
			baseLightBehaviourBuffer.Clear();
			foreach (Transform t in instance.AllBaseLightTransforms())
			{
				t.GetComponentsInChildren(baseLightBehaviourBuffer);
				foreach (var b in baseLightBehaviourBuffer)
				{
					b.SetLightSelectorVisible(newValue);
				}
				baseLightBehaviourBuffer.Clear();
			}
		}

		public static void SetLightEmissionEnabled(TileBehaviour instance, bool newValue)
		{
			baseLightBehaviourBuffer.Clear();
			foreach (Transform t in instance.AllBaseLightTransforms())
			{
				t.GetComponentsInChildren(baseLightBehaviourBuffer);
				foreach (var b in baseLightBehaviourBuffer)
				{
					b.SetLightEmission(newValue);
				}
				baseLightBehaviourBuffer.Clear();
			}
		}

		public static void SetOmniLightActive(TileBehaviour instance, bool newValue)
		{
			instance.omniLightRoot?.SetActive(newValue);
		}

		public static void SetSpotLightActive(TileBehaviour instance, bool newValue)
		{
			instance.spotLightRoot?.SetActive(newValue);
		}

		public static void SetTubeLightsActive(TileBehaviour instance, bool newValue)
		{
			instance.tubeLightRoot?.SetActive(newValue);
		}


		#endregion Lights

		public static void SetEditAreasActive(TileBehaviour instance, bool newValue)
		{
			if (null == instance.editAreaRoot) return;

			editAreaBuffer.Clear();
			foreach (Transform t in instance.editAreaRoot.transform)
			{
				t.GetComponentsInChildren(editAreaBuffer);
				foreach (var b in editAreaBuffer)
				{
					b.SetVisible_EditArea(newValue);
				}
				editAreaBuffer.Clear();
			}
		}

		public static void SetEditAreasWithVehicleActive(TileBehaviour instance, bool newValue)
		{
			if (null == instance.editAreaRoot) return;

			editAreaBuffer.Clear();
			foreach (Transform t in instance.editAreaRoot.transform)
			{
				t.GetComponentsInChildren(editAreaBuffer);
				foreach (var b in editAreaBuffer)
				{
					b.SetVisible_EditAreaWithVehicle(newValue);
				}
				editAreaBuffer.Clear();
			}
		}

		public static void SetEditAreasForceBoundsVisible(TileBehaviour instance, bool newValue)
		{
			if (null == instance.editAreaRoot) return;

			editAreaBuffer.Clear();
			foreach (Transform t in instance.editAreaRoot.transform)
			{
				t.GetComponentsInChildren(editAreaBuffer);
				foreach (var b in editAreaBuffer)
				{
					b.SetVisible_EditAreaWithVehicleForceBounds(newValue);
				}
				editAreaBuffer.Clear();
			}
		}

		public static void SetInteractablesActive(TileBehaviour instance, bool newValue)
		{
			instance.interactableRoot?.SetActive(newValue);
		}

		public static void SetTrackLinesActive(TileBehaviour instance, bool newValue)
		{
			instance.trackRoot?.SetActive(newValue);
		}


		public static void SetTreesActive(TileBehaviour instance, bool newValue)
		{
			instance.treeRoot?.SetActive(newValue);
		}

		public static void SetSeaFloorActive(TileBehaviour instance, bool newValue)
		{
			instance.seaFloor?.SetActive(newValue);
		}

		#endregion Set_Thing_Active


		private void LodBehaviour_LODChanged(byte lod)
		{
			bool activeState = lod <= 1;

			SetEditAreasActive(this,     activeState && ViewManager.EditAreasVisible);
			SetInteractablesActive(this, activeState && ViewManager.InteractablesVisible);
		}

		private void Task_InstantiateTile()
		{
			try
			{
				profilerMarker_InstantiateTile.Begin();

				LoadState = TileLoader.TileLoadState.Loading;

				TileLoader.ThrowIfAbortLoading();
				RunOnMainThread.EnqueueAndWaitForCompletion(ClearTile, basePriority);
				TileLoader.ThrowIfAbortLoading();
				RunOnMainThread.EnqueueAndWaitForCompletion(SetupContainers, basePriority);

				TileLoader.ThrowIfAbortLoading();
				XDocument doc;
				try
				{
					profilerMarker_LoadTileXml.Begin();
					xmlLastWriteTime = new FileInfo(tileFilePath).LastWriteTimeUtc;
					doc = XMLHelper.LoadFromFile(tileFilePath);
				}
				catch (Exception e)
				{
					var s =
						 $"Error loading tile '{tileName}' from file '{tileFilePath}' during XML loading."
					   + $" The tile will be skipped. Full Exception details:\n{e}";
					throw new Exception(s, e);
				}
				finally
				{
					profilerMarker_LoadTileXml.End();
				}


				var tileRomPath = PathHelper.GetRelativePath(StormworksPaths.rom, tileFilePath);

				TileLoader.ThrowIfAbortLoading();

				try
				{
					profilerMarker_LoadTileData.Begin();
					tileData = new Tile(doc.Element("definition"), tileRomPath);
				}
				catch (Exception e)
				{
					var s =
						$"Error loading tile '{tileName}' from file '{tileFilePath}' during Tile Representation loading."
					  + $" The tile will be skipped. Full Exception details:\n{e}";
					throw new Exception(s, e);
				}
				finally
				{
					profilerMarker_LoadTileData.End();
				}

				myWrapper.tile = tileData;

				loadingTasks.Add(Task_SetupSubObjects());

				LoadState = TileLoader.TileLoadState.Completed;
			}
			catch (OperationCanceledException)
			{
				LoadState = TileLoader.TileLoadState.Canceled;
				// We don't care about the exception so don't log.
				throw; // We need the Task object to know it failed.
			}
			catch (FileInteractionException e)
			{
				AnyTileFailed = true;
				// This Exception Type would already have the path, no need to wrap it again.
				LoadState = TileLoader.TileLoadState.Failed;
				RunOnMainThread.LogException(e, this);
				throw; // We need the Task object to know it failed.
			}
			catch (Exception e)
			{
				AnyTileFailed = true;
				LoadState = TileLoader.TileLoadState.Failed;
				var f = new FileInteractionException(tileFilePath, e);
				RunOnMainThread.LogException(f, this);
				throw f; // We need the Task object to know it failed.
			}
			finally
			{
				profilerMarker_InstantiateTile.End();
			}
		}


		private void SetupContainers()
		{
			TileLoader.ThrowIfAbortLoading();

			if (LoadTrees)
			{
				treeRoot = new GameObject("Trees");
				treeRoot.transform.SetParent(transform, false);

				treeManager = treeRoot.AddComponent<TileTreeManager>();
				treeLoadTask = treeManager.SetInitialData(Tile.GetInstancesPath(tileFilePath));

				treeRoot.SetActive(ViewManager.Instance.TreesVisible);
			}

			if (LoadMesh || LoadPhys)
			{
				staticRoot = new GameObject("Static");
				staticRoot.transform.SetParent(transform, false);
				staticRoot.SetActive(ViewManager.Instance.MeshVisible);
			}

			if (LoadOmniLights)
			{
				omniLightRoot = new GameObject("OmniLights");
				omniLightRoot.transform.SetParent(transform, false);
				omniLightRoot.SetActive(ViewManager.Instance.OmniLightsVisible);
			}

			if (LoadSpotLights)
			{
				spotLightRoot = new GameObject("SpotLights");
				spotLightRoot.transform.SetParent(transform, false);
				spotLightRoot.SetActive(ViewManager.Instance.SpotLightsVisible);
			}

			if (LoadTubeLights)
			{
				tubeLightRoot = new GameObject("TubeLights");
				tubeLightRoot.transform.SetParent(transform, false);
				tubeLightRoot.SetActive(ViewManager.Instance.TubeLightsVisible);
			}

			if (LoadEditAreas)
			{
				editAreaRoot = new GameObject("EditAreas");
				editAreaRoot.transform.SetParent(transform, false);
				editAreaRoot.SetActive(ViewManager.Instance.EditAreasVisible);
			}

			if (LoadInteractables)
			{
				interactableRoot = new GameObject("Interacables");
				interactableRoot.transform.SetParent(transform, false);
				interactableRoot.SetActive(ViewManager.Instance.InteractablesVisible);
			}

			if (LoadTrackLines)
			{
				trackRoot = Instantiate(TrackXmlContainerPrefab, transform.position, Quaternion.identity, transform);
				trackRoot.name = "Track_Lines";

				trackLines = trackRoot.GetRequiredComponent<TrackLines>();

				trackRoot.SetActive(ViewManager.Instance.TrackLinesVisible);
			}

			// Force-Update the lod state
			// on the newly created GameObjects and behaviours.
			lodBehaviour.SetLOD(lodBehaviour.lod, forceFireEvent: true);

			TileWrapper wrapper;
			lock (MapTileNameToTile)
			{
				if (MapGameObjectToTile.TryGetValue(gameObject, out wrapper))
				{
					myWrapper = wrapper;
					return;
				}

				wrapper = new TileWrapper(gameObject);
				MapGameObjectToTile[gameObject] = wrapper;
				MapTileNameToTile[tileName] = wrapper;
			}

			myWrapper = wrapper;
		}

		private Task Task_SetupSubObjects()
		{
			return Task.Run(() =>
			{
				try
				{
					_Tasked_SetupSubObjects();
				}
				catch (OperationCanceledException)
				{
					// Ignored.
				}
				catch (Exception e)
				{
					RunOnMainThread.LogException(e, this);
					throw;
				}
			});
		}

		private void _Tasked_SetupSubObjects()
		{
			TileLoader.ThrowIfAbortLoading();
			if (true)
			{
				RunOnMainThread.EnqueueAndWaitForCompletion(MakeSeaFloorHeight, basePriority);

				void MakeSeaFloorHeight()
				{
					if (noSeaFloorRegex.IsMatch(tileName)) return;

					var localPosition = new Vector3(0, tileData.sea_floor_height, 0);

					if (null != seaFloor)
					{
						seaFloor.transform.localPosition = localPosition;
					}
					else
					{
						seaFloor = Instantiate
							(
							 Globals.Prefabs.SeaFloorApproximation
						   , transform.position + localPosition
						   , Quaternion.identity
						   , transform
							);
					}

					seaFloor.SetActive(ViewManager.Instance.SeaFloorVisible);
				}
			}

			float priority = initialPriority;
			if (LoadMesh || LoadPhys) // Merged objects need both loaded. The items themselves will skip loading their mesh if the setting requires it.
			{
				foreach (var meshInfo in tileData.meshes)
				{
					TileLoader.ThrowIfAbortLoading();

					if (meshInfo.LoadIgnore) continue;

					RunOnMainThread.EnqueueAndWaitForCompletion(Create, priority);
					void Create()
					{
						var go = new GameObject($"{meshInfo.id}");
						go.transform.parent = staticRoot.transform;
						var b = go.AddComponent<MeshOrPhysBehaviour>();
						b.Initialize(meshInfo);
						b.SetMeshVisible(Settings.ShowMesh);
					}

					priority = subsequentPriority;

					if (false == LoadSubObjects) break;
				}
			}

			priority = otherObjectsPriority;
			if (LoadMesh || LoadPhys) // Merged objects need both loaded. The items themselves will skip loading their mesh if the setting requires it.
			{
				foreach (var physInfo in tileData.physicsMeshes)
				{
					TileLoader.ThrowIfAbortLoading();

					if (physInfo.LoadIgnore)
						continue;

					RunOnMainThread.EnqueueAndWaitForCompletion(Create, priority);
					void Create()
					{
						var go = new GameObject($"{physInfo.id}");
						go.transform.parent = staticRoot.transform;
						var b = go.AddComponent<MeshOrPhysBehaviour>();
						b.Initialize(physInfo);
						b.SetPhysVisible(Settings.ShowPhys);
					}

					if (false == LoadSubObjects) break;
				}
			}

			void LoadSimple<TComponent>(bool do_loading, IEnumerable<AbstractTileItem> items, GameObject parent) where TComponent : AbstractTileItemBehaviour
			{
				if (!do_loading) return;
				foreach (var item in items)
				{
					TileLoader.ThrowIfAbortLoading();

					if (item.LoadIgnore)
						continue;

					RunOnMainThread.EnqueueAndWaitForCompletion(Create, priority);

					void Create()
					{
						var go = new GameObject($"{item.id}");
						go.transform.parent = parent.transform;
						var b = go.AddComponent<TComponent>();
						b.Initialize(item);
					}
				}
			}

			LoadSimple<OmniLightBehaviour>(LoadOmniLights, tileData.lights_omni, omniLightRoot);
			LoadSimple<SpotLightBehaviour>(LoadSpotLights, tileData.lights_spot, spotLightRoot);
			LoadSimple<TubeLightBehaviour>(LoadTubeLights, tileData.lights_tube, tubeLightRoot);
			LoadSimple<EditAreaBehaviour>(LoadEditAreas, tileData.edit_areas, editAreaRoot);
			LoadSimple<InteractableBehaviour>(LoadInteractables, tileData.interactables, interactableRoot);


			if (true /* MergeTileObjects */)
			{
				TileLoader.ThrowIfAbortLoading();
				RunOnMainThread.EnqueueAndWaitForCompletion(Do, basePriority);
				void Do()
				{
					foreach (Transform t in staticRoot.transform)
					{
						var b = t.gameObject.GetComponent<AbstractTileItemBehaviour>();
						b.SetupSameObjectLinks();
					}
				}
			}


			if (LoadTrackLines)
			{
				TileLoader.ThrowIfAbortLoading();
				trackLines.Initialize(tileData);
			}

			if (true /* Setup Parent hierarchy */)
			{
				TileLoader.ThrowIfAbortLoading();
				RunOnMainThread.EnqueueAndWaitForCompletion(Do, basePriority);
				void Do()
				{
					foreach (Transform t in staticRoot.transform)
					{
						var b = t.gameObject.GetComponent<AbstractTileItemBehaviour>();
						b.SetupParent();
					}
				}
			}

			if (true /* Initialize objects */)
			{
				TileLoader.ThrowIfAbortLoading();
				RunOnMainThread.EnqueueAndWaitForCompletion(Do, basePriority);
				void Do()
				{
					foreach (Transform t in staticRoot.transform)
					{
						var b = t.gameObject.GetComponent<AbstractTileItemBehaviour>();
						b.InitComplete();
					}
				}
			}

			if (Loader.ThreadingSettings.tileCompletionWaitForTreeLoad
			 && null != treeLoadTask)
			{
				// This ensures that we don't report completion until all xml has been loaded.
				// Note however that the visuals may still be loading.

				loadingTasks?.Add(treeLoadTask);
			}
		}
	}
}
