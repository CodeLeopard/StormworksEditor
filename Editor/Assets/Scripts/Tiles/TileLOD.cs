﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections;

using Behaviours;

using UnityEngine;

namespace Tiles
{
	/// <summary>
	/// Responsible for a single tile, most notable task is LOD
	/// </summary>
	public class TileLOD : M0noBehaviour, ILODEventReceiver, ILODEventSender
	{
		public byte lod { get; private set; } = LOD_NOT_INITIALIZED;

		private CullingGroup cullingGroup;

		private const float tileRadius = 707; // 500 * Sqrt(2)

		private const float range_lod_0         =   1_000;
		private const float range_lod_1         =   5_000;
		private const float range_lod_2         =  10_000;
		private const float range_lod_invisible = 100_000;

		public const byte MAX_LOD = 3;
		public const byte LOD_NOT_INITIALIZED = byte.MaxValue;


		#region UnityMessages

		/// <inheritdoc />
		protected override void OnAwake()
		{
			cullingGroup = new CullingGroup();
			cullingGroup.targetCamera = Camera.main;
			cullingGroup.SetDistanceReferencePoint(Camera.main.transform);

			cullingGroup.SetBoundingSpheres(new BoundingSphere[1]{new BoundingSphere(transform.position, tileRadius)});

			cullingGroup.SetBoundingDistances(new float[]
			{
				range_lod_0,
				range_lod_1,
				range_lod_2,
				range_lod_invisible
			});

			cullingGroup.onStateChanged = OnCullingStateChanged;
		}

		/// <inheritdoc />
		protected override void OnEnableAndAfterStart()
		{
			StartCoroutine(CheckCullingStateNextFrame());
		}

		protected void OnDestroy()
		{
			cullingGroup.Dispose();
			cullingGroup = null;
		}


		#endregion unityMessages
		private void OnCullingStateChanged(CullingGroupEvent sphere)
		{
			SetLOD((byte)sphere.currentDistance);
		}

		private IEnumerator CheckCullingStateNextFrame()
		{
			yield return null; // Wait for next frame.

			if (! cullingGroup.IsVisible(0))
			{
				SetLOD(MAX_LOD);
			}
		}


		[field: NonSerialized]
		public event Action<byte> LODChanged;

		private void DoEvents()
		{
			LODChanged?.Invoke(lod);
		}

		/// <inheritdoc />
		public void SetLOD(byte newLOD)
		{
			SetLOD(newLOD, false);
		}

		public void SetLOD(byte newLOD, bool forceFireEvent = false)
		{
			if (newLOD != lod || forceFireEvent)
			{
				lod = newLOD;
				DoEvents();
			}
		}
	}
}
