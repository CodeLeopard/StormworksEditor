﻿// Copyright 2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;

using BinaryDataModel.Converters;
using DataModel.Tiles;
using DataTypes.Extensions;
using GUI;
using Shared;
using Tiles.Content.Support;
using Tools;

using UnityEngine;

namespace Tiles.Content
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(Light))]
	public abstract class BaseLightBehaviour : AbstractTileItemBehaviour, IInspectorPanelContentProvider
	{
		private BaseLight baseLight => (BaseLight)Data;

		protected new Light light;

		protected LightSelectorBehaviour selector;

		protected override void OnAwake()
		{
			base.OnAwake();
			light = GetRequiredComponent<Light>();

			var go = new GameObject("visualization", typeof(LightSelectorBehaviour));
			go.transform.SetParent(transform, false);
			selector = go.GetRequiredComponent<LightSelectorBehaviour>();
		}

		protected override void DerivedInitialize()
		{
			SetupLight();
			MyUpdateLight();
			UpdateLight();
		}

		protected abstract void SetupLight();

		protected override void OnInteract()
		{
			base.OnInteract();

			MyUpdateLight();
			UpdateLight();
		}

		protected override void OnSubmit()
		{
			// base would save scale into transform, which it should not for this.
			//base.GizmoEndInteract(obj);
			EnsureCorrectParentTile();


			// The Gizmo affects this object, but we need the vehicle to always have scale 1
			UnityEngine.Vector3 scale = transform.localScale;

			// Clamp scale to game-supported values.
			scale.x = Math.Clamp(scale.x, 0, 128);
			scale.y = Math.Clamp(scale.y, 0, 128);
			scale.z = Math.Clamp(scale.z, 0, 128);

			if (scale.sqrMagnitude == 0)
			{
				// Zero dimensional is not clickAble.
				scale = UnityEngine.Vector3.one;
			}
			transform.localScale = scale;

			// Save the actual values.
			Data.LTransform = transform.ToOpenTK(local: true, scale: false);
		}

		private void MyUpdateLight()
		{
			var color = baseLight.color.ToUnityColor(Globals.Materials.LightPositionIndicator.color.a);
			light.color = color;
			selector.color = color;
		}

		protected abstract void UpdateLight();

		public void SetLightSelectorVisible(bool newValue)
		{
			selector.gameObject.SetActive(newValue);
		}

		public void SetLightEmission(bool newValue)
		{
			light.enabled = newValue;
		}


		#region InspectorContent

		protected override void SetupInspectorContent()
		{
			inspectorContentProvider = _newInspectorContentProvider(this);
		}

		/// <inheritdoc />
		public override object GetInspectorContent() => inspectorContentProvider;

		private LightInspectorContentProvider inspectorContentProvider;
		private static Func<AbstractTileItemBehaviour, LightInspectorContentProvider> _newInspectorContentProvider;

		protected class LightInspectorContentProvider : InspectorContentProvider
		{
			private BaseLightBehaviour typedBehaviour => (BaseLightBehaviour)behaviour;

			[VisibleProperty]
			public Vector3 Color
			{
				get => typedBehaviour.baseLight.color.ToUnity();
				set => typedBehaviour.baseLight.color = value.ToOpenTK();
			}

			protected LightInspectorContentProvider(AbstractTileItemBehaviour b) : base(b) { }

			static LightInspectorContentProvider()
			{
				_newInspectorContentProvider = (value) => new LightInspectorContentProvider(value);
			}
		}

		static BaseLightBehaviour()
		{
			System.Runtime.CompilerServices.RuntimeHelpers.RunClassConstructor(typeof(LightInspectorContentProvider).TypeHandle);
		}

		#endregion InspectorContent
	}
}
