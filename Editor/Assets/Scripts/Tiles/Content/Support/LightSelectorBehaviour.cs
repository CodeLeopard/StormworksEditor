﻿// Copyright 2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using Behaviours;
using DataTypes.Extensions;
using Tools;
using UnityEngine;

namespace Tiles.Content.Support
{

	[DisallowMultipleComponent]
	[RequireComponent(typeof(MeshFilter))]
	[RequireComponent(typeof(MeshRenderer))]
	[RequireComponent(typeof(MeshCollider))]
	public class LightSelectorBehaviour : M0noBehaviour
	{
		protected MeshFilter meshFilter;
		protected MeshRenderer meshRenderer;
		protected MeshCollider meshCollider;

		public Color color
		{
			get => meshRenderer.material.color;
			set => meshRenderer.material.color = value;
		}

		private const float tube_size_factor = 4;
		public float tubelength
		{
			get => transform.localScale.y * tube_size_factor;
			set => transform.localScale = transform.localScale.WithY(value / tube_size_factor);
		}

		/// <inheritdoc />
		protected override void OnAwake()
		{
			base.OnAwake();
			meshFilter = GetRequiredComponent<MeshFilter>();
			meshRenderer = GetRequiredComponent<MeshRenderer>();
			meshCollider = GetRequiredComponent<MeshCollider>();

			meshRenderer.material = Globals.Materials.LightPositionIndicator;
		}

		public void SetOmni()
		{
			transform.localScale = Vector3.one;
			transform.localEulerAngles = Vector3.zero;
			meshFilter.sharedMesh = Globals.Meshes.Sphere;
			meshCollider.sharedMesh = Globals.Meshes.Sphere;
		}

		public void SetSpot()
		{
			transform.localScale = Vector3.one;
			transform.localEulerAngles = Vector3.zero;
			meshFilter.sharedMesh = Globals.Meshes.Cone;
			meshCollider.sharedMesh = Globals.Meshes.Cone;
		}

		public void SetTube()
		{
			transform.localScale = new Vector3(0.25f, 1, 0.25f);
			transform.localEulerAngles = new Vector3(90, 0, 0);
			meshFilter.sharedMesh = Globals.Meshes.Cylinder;
			meshCollider.sharedMesh = Globals.Meshes.Cylinder;
		}
	}
}
