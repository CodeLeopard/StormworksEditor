﻿// Copyright 2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using DataModel.Tiles;

using UnityEngine;


namespace Tiles.Content
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(Light))]
	public class OmniLightBehaviour : BaseLightBehaviour
	{
		public OmniLight omniLight => (OmniLight)Data;

		protected override void SetupLight()
		{
			selector.SetOmni();
			light.type = LightType.Point;
		}

		protected override void UpdateLight()
		{
			light.range = omniLight.radius;
		}
	}
}
