﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;

using BinaryDataModel.Converters;

using DataModel.Tiles;

using Tools;

using UnityEngine;

using Vector3 = OpenTK.Mathematics.Vector3;

namespace Tiles.Content
{
	[DisallowMultipleComponent]
	public class InteractableBehaviour : ItemWithMeshBehaviour
	{
		public Interactable interactable => (Interactable)Data;


		protected override void OnSubmit()
		{
			// base would save scale into transform, which it should not for this.
			//base.GizmoEndInteract(obj);
			EnsureCorrectParentTile();


			// The Gizmo affects this object, but we need the vehicle to always have scale 1
			UnityEngine.Vector3 scale = transform.localScale;

			// Clamp scale to game-supported values.
			scale.x = Math.Clamp(scale.x, 0, 128);
			scale.y = Math.Clamp(scale.y, 0, 128);
			scale.z = Math.Clamp(scale.z, 0, 128);

			if (scale.sqrMagnitude == 0)
			{
				// Zero dimensional is not clickAble.
				scale = UnityEngine.Vector3.one;
			}
			transform.localScale = scale;

			// Save the actual values.
			interactable.LTransform = transform.ToOpenTK(local: true, scale: false);
			interactable.size = scale.ToOpenTK();
		}


		protected override void SetupMesh()
		{
			{
				// Fix invisible size setting.
				var size = interactable.size;
				int counter = 0;
				if (size.X == 0) counter++;
				if (size.Y == 0) counter++;
				if (size.Z == 0) counter++;

				if (counter >= 2)
				{
					if (size.X == 0) size.X = 1;
					if (size.Y == 0) size.Y = 1;
					if (size.Z == 0) size.Z = 1;
				}

				interactable.size = size;
			}


			meshFilter.sharedMesh = Globals.WireFrames.Cube;
			meshRenderer.material = Globals.Materials.Interactable;
			meshCollider.sharedMesh = Globals.Meshes.Cube;

			transform.localScale = interactable.size.ToUnity();
		}

		/// <inheritdoc />
		protected override void Tasked_UpdateMesh() { }
	}
}
