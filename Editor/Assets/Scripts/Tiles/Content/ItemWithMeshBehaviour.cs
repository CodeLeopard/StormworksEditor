﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BinaryDataModel.Converters;

using Control;

using DataModel.Tiles;

using Tiles.Content;

using Tools;

using UnityEngine;
using UnityEngine.PlayerLoop;

namespace Tiles.Content
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(MeshFilter))]
	[RequireComponent(typeof(MeshRenderer))]
	[RequireComponent(typeof(MeshCollider))]
	public abstract class ItemWithMeshBehaviour : AbstractTileItemBehaviour
	{
		protected MeshFilter meshFilter;
		protected MeshRenderer meshRenderer;
		protected MeshCollider meshCollider;

		public bool RendererEnabled
		{
			get => meshRenderer?.enabled ?? false;
			set
			{
				if (null == meshRenderer) return; // Cursed Unity crap
				meshRenderer.enabled = value;
			}
		}

		public bool ColliderEnabled
		{
			get => meshCollider?.enabled ?? false;
			set
			{
				if (null == meshCollider) return; // Cursed Unity crap
				meshCollider.enabled = value;
			}
		}


		/// <inheritdoc />
		protected override void OnAwake()
		{
			base.OnAwake();
			meshFilter   = GetRequiredComponent<MeshFilter>();
			meshRenderer = GetRequiredComponent<MeshRenderer>();
			meshCollider = GetRequiredComponent<MeshCollider>();
		}

		/// <inheritdoc />
		/// <inheritdoc />
		protected override void OnStart()
		{
			base.OnStart();
			SetupVisual();
		}



		protected virtual void SetupVisual()
		{
			SetupMesh();
			UpdateMesh();
		}

		protected abstract void SetupMesh();
		protected abstract void Tasked_UpdateMesh();


		/// <inheritdoc />
		protected override void OnInspectorSubmit()
		{
			UpdateMesh();
		}


		private bool meshUpdateNeeded = false;
		protected void UpdateMesh()
		{
			meshUpdateNeeded = true;
			Task.Run(Task_UpdateMesh);
		}

		private void Task_UpdateMesh()
		{
			try
			{
				while (meshUpdateNeeded)
				{
					meshUpdateNeeded = false;
					Tasked_UpdateMesh();
				}
			}
			catch (Exception e)
			{
				RunOnMainThread.LogException(e, this);
			}
		}
	}
}
