// Copyright 2022-2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using Behaviours;
using BinaryDataModel.Converters;
using Control;
using DataModel.Tiles;
using GUI;
using RuntimeGizmos;
using Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using Tools;
using UnityEngine;

namespace Tiles.Content
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(SelectionEventReceiver))]
	[RequireComponent(typeof(TransformGizmoEventReceiver))]
	public abstract class AbstractTileItemBehaviour : M0noBehaviour, IInspectorPanelContentProvider
	{
		protected TransformGizmoEventReceiver gizmoEvents;
		protected SelectionEventReceiver selectionEvents;


		/// <summary>
		/// The <see cref="AbstractTileItem"/> we represent.
		/// </summary>
		public AbstractTileItem Data { get; internal set; }


		public AbstractTileItemBehaviour sameObjectParent { get; internal set; }

		public AbstractTileItemBehaviour child_Phys { get; internal set; }


		/// <summary>
		/// Should Parent Tile Correction be performed.
		/// </summary>
		public bool ParentTileCorrection
		{
			get => Data.AutoSelectParentTile ?? true;
			set => Data.AutoSelectParentTile = value;
		}

		private bool isTerrain_heuristic = false;
		/// <summary>
		/// Is this object part of the terrain.
		/// Used for Selection mask.
		/// </summary>
		public bool IsTerrain
		{
			get => Data.IsTerrain ?? isTerrain_heuristic;
			set => Data.IsTerrain = value;
		}


		public bool IsChild { get; private set; }

		public bool IsCompoundObjectChild { get; private set; }


		public bool ChangedSinceLastSaved
		{
			get => Data.ChangedSinceLastSaved;
			protected set => Data.ChangedSinceLastSaved = value;
		}

		#region UnityMessages

		/// <inheritdoc />
		protected override void OnAwake()
		{
			gizmoEvents = GetRequiredComponent<TransformGizmoEventReceiver>();
			gizmoEvents.GizmoInteract    += GizmoInteract;
			gizmoEvents.GizmoEndInteract += GizmoEndInteract;

			selectionEvents = GetRequiredComponent<SelectionEventReceiver>();
			selectionEvents.AllowCopy   = false;
			selectionEvents.AllowDelete = false;
			selectionEvents.SelectionAllowed = SelectionAllowed;

			SetupInspectorContent();
		}

		protected virtual void SetupInspectorContent()
		{
			inspectorContentProviderSingle   = _newInspectorContentProviderSingle(this);
			inspectorContentProviderCombined = _newInspectorContentProviderCombined(this);
		}


		protected virtual void OnDestroy()
		{
			Data.tile.Remove(Data);
		}

		#endregion UnityMassages

		/// <summary>
		/// Load the <see cref="AbstractTileItem"/> that this object will represent.
		/// </summary>
		/// <param name="item"></param>
		public void Initialize(AbstractTileItem item)
		{
			Data = item ?? throw new ArgumentNullException(nameof(item));

			ItemToBehaviourMap.Add(Data, this);
			Data.LTransform.SetDataOn(transform, true);

			DerivedInitialize();
		}

		/// <summary>
		/// Called when <see cref="Data"/> has been populated.
		/// </summary>
		protected virtual void DerivedInitialize()
		{
			// virtual
		}


		/// <summary>
		/// Called as the last step in loading process, once all other objects in the same tile have been <see cref="Initialize"/>d.
		/// </summary>
		public virtual void InitComplete()
		{
			CheckIsTerrain();
		}

		private void CheckIsTerrain()
		{
			if (Data.GTransform.ExtractTranslation() == OpenTK.Mathematics.Vector3.Zero)
			{
				isTerrain_heuristic = true;
				return;
			}

			foreach (var regex in EditorSettingsManager.Settings.TileLoadSettings.TerrainIDRegex)
			{
				if (regex.IsMatch(Data.id))
				{
					isTerrain_heuristic = true;
					return;
				}
			}
		}


		internal void SetupParent()
		{
			return; // Disabled for now.
#pragma warning disable CS0162 // Unreachable code detected



			if (Data.parent == null)
			{
				return;
			}

			var parentBehaviour = ItemToBehaviourMap[Data.parent];

			// todo: 'worldPositionStays: true' is cursed, but appears to be correct.
			transform.SetParent(parentBehaviour.transform, true);

#pragma warning restore CS0162 // Unreachable code detected
		}


		internal void SetupSameObjectLinks()
		{
			if (Data is not DataModel.Tiles.MeshInfo) return;

			foreach (AbstractTileItem linkedObject in Data.SameObjectLinks)
			{
				if (!ItemToBehaviourMap.TryGetValue(linkedObject, out var behaviour))
				{
					Debug.LogError($"Tile '{Data.tile.fileName}' Did not find Behaviour for linkedObject '{linkedObject.id}'.", this);
					continue;
				}

				if (behaviour.Data is PhysInfo)
				{
					child_Phys = behaviour;
				}
				else
				{
					Console.WriteLine($"Tile '{Data.tile.fileName}' Ignoring non-Phys SameItem '{linkedObject.meta_elementName}' with id '{linkedObject.id}'.");
					continue;
				}

				behaviour.LinkAsSameObject(this);
			}
		}

		private void LinkAsSameObject(AbstractTileItemBehaviour parent)
		{
			if (parent == this) throw new ArgumentException("Cannot set parent to self.");

			sameObjectParent = parent;

			// todo: 'worldPositionStays: true' is cursed, but appears to be correct.
			transform.SetParent(parent.transform, true);

			IsCompoundObjectChild = true;
		}


		#region Events

		private void InternalInteract()
		{
			ChangedSinceLastSaved = true;
			OnInteract();
		}

		private void InternalSubmit()
		{
			ChangedSinceLastSaved = true;

			OnSubmit();
		}

		/// <summary>
		/// General Interaction Event.
		/// Triggered by both TransformGizmo and EditPanel
		/// </summary>
		protected virtual void OnInteract()
		{

		}

		/// <summary>
		/// General Submit/EndInteract Event.
		/// Triggered by both TransformGizmo and EditPanel
		/// </summary>
		protected virtual void OnSubmit()
		{
			EnsureCorrectParentTile();
			Data.LTransform = transform.ToOpenTK(local: true, scale: true);

			if (null != child_Phys)
			{
				child_Phys.EnsureCorrectParentTile();
				child_Phys.Data.LTransform = Data.LTransform; // todo: this is evil, but maybe good enough for now.
			}
		}



		protected virtual void OnGizmoInteract()
		{

		}

		protected virtual void OnGizmoEndInteract()
		{

		}


		protected virtual void OnInspectorInteract()
		{

		}

		protected virtual void OnInspectorSubmit()
		{

		}


		#region Handlers

		private void GizmoInteract(Transform obj)
		{
			InternalInteract();
			OnGizmoInteract();
		}

		private void GizmoEndInteract(Transform obj)
		{
			InternalSubmit();
			OnGizmoEndInteract();
		}



		private bool SelectionAllowed(Transform obj)
		{
			return ! IsTerrain || TileInteraction.SelectTerrain;
		}
		private IEnumerator SelectNextTick()
		{
			yield return null;
			SelectionManager.Instance.AddTarget(sameObjectParent.transform);
		}


		/// <inheritdoc />
		public virtual object GetInspectorContent()
		{
			if (null != child_Phys)
			{
				return inspectorContentProviderCombined;
			}

			return inspectorContentProviderSingle;
		}

		/// <inheritdoc />
		void IInspectorPanelContentProvider.OnInteract()
		{
			InternalInteract();
			OnInspectorInteract();

			// Trigger child's update too.
			(child_Phys as IInspectorPanelContentProvider)?.OnInteract();
		}

		/// <inheritdoc />
		void IInspectorPanelContentProvider.OnSubmit()
		{
			InternalSubmit();
			OnInspectorSubmit();

			// Trigger child's update too.
			(child_Phys as IInspectorPanelContentProvider)?.OnSubmit();
		}

		#endregion Handlers

		#endregion Events

		protected void EnsureCorrectParentTile()
		{
			EnsureCorrectParentTile(transform, Data);
		}

		protected void EnsureCorrectParentTile(Transform t, AbstractTileItem obj, bool force = false)
		{
			if (!ParentTileCorrection && !force) return;

			// If moved outside of tile bounds transfer to the correct tile.
			var wrapper = TileLoader.instance.GetNearestTile(t.position);
			if (wrapper.tile == obj.tile) return;

			wrapper.tile.Add(obj);
			t.SetParent(wrapper.tileRoot.transform, true);
		}


		#region InspectorContentProvider

		private InspectorContentProvider inspectorContentProviderSingle;
		private CombinedObject inspectorContentProviderCombined;

		private static Func<AbstractTileItemBehaviour, InspectorContentProvider> _newInspectorContentProviderSingle;
		private static Func<AbstractTileItemBehaviour, CombinedObject> _newInspectorContentProviderCombined;
		protected class InspectorContentProvider
		{
			protected readonly AbstractTileItemBehaviour behaviour;

			[VisibleProperty]
			public AbstractTileItem Data => behaviour.Data;

			[VisibleProperty]
			public AbstractTileItem Phys => behaviour.child_Phys?.Data;

			[VisibleProperty]
			public bool IsTerrain
			{
				get => behaviour.IsTerrain;
				set => behaviour.IsTerrain = value;
			}

			[VisibleProperty]
			public bool ParentTileCorrection
			{
				get => behaviour.ParentTileCorrection;
				set => behaviour.ParentTileCorrection = value;
			}

			protected InspectorContentProvider(AbstractTileItemBehaviour b)
			{
				behaviour = b;
			}

			static InspectorContentProvider()
			{
				_newInspectorContentProviderSingle = (value) => new InspectorContentProvider(value);
			}
		}

		/// <summary>
		/// Helper to define the InspectorPanel's representation of a combined Mesh and Phys object.
		/// </summary>
		private class CombinedObject
		{
			private readonly AbstractTileItemBehaviour behaviour;

			[VisibleProperty]
			public string MeshId
			{
				get => Mesh.id;
				set => Mesh.id = value;
			}

			[VisibleProperty]
			public string PhysId
			{
				get => Phys.id;
				set => Phys.id = value;
			}

			[VisibleProperty]
			public string Parent_Id
			{
				get => Mesh.parent_id;
				set
				{
					Mesh.parent_id = value;
					Phys.parent_id = value;
				}
			}

			[VisibleProperty]
			public string Purchase_Interactable_Id
			{
				get => Mesh.purchase_interactable_id;
				set
				{
					Mesh.purchase_interactable_id = value;
					Phys.purchase_interactable_id = value;
				}
			}

			[VisibleProperty]
			public AbstractTileItem.SeasonFlags SeasonalFlags
			{
				get => Mesh.seasonal_flags;
				set
				{
					Mesh.seasonal_flags = value;
					Phys.seasonal_flags = value;
				}
			}



			[VisibleProperty]
			public string MeshFile
			{
				get => Mesh.file_name;
				set => Mesh.file_name = value;
			}

			[VisibleProperty]
			public string PhysFile
			{
				get => Phys.file_name;
				set => Phys.file_name = value;
			}

			[VisibleProperty]
			public string Tile => Mesh.TileName;


			[VisibleProperty]
			public bool IsTerrain
			{
				get => behaviour.IsTerrain;
				set
				{
					Mesh.IsTerrain = value;
					Phys.IsTerrain = value;
				}
			}

			[VisibleProperty]
			public bool ParentTileCorrection
			{
				get => behaviour.ParentTileCorrection;
				set
				{
					Mesh.AutoSelectParentTile = value;
					Phys.AutoSelectParentTile = value;
				}
			}

			public MeshInfo Mesh => behaviour.Data as MeshInfo;

			public PhysInfo Phys => behaviour.child_Phys?.Data as PhysInfo;


			private CombinedObject(AbstractTileItemBehaviour b)
			{
				behaviour = b;
			}

			static CombinedObject()
			{
				_newInspectorContentProviderCombined = (value) => new CombinedObject(value);
			}
		}

		#endregion InspectorContentProvider

		static AbstractTileItemBehaviour()
		{
			System.Runtime.CompilerServices.RuntimeHelpers.RunClassConstructor(typeof(InspectorContentProvider).TypeHandle);
			System.Runtime.CompilerServices.RuntimeHelpers.RunClassConstructor(typeof(CombinedObject).TypeHandle);
		}

		protected static Dictionary<AbstractTileItem, AbstractTileItemBehaviour> ItemToBehaviourMap =
			new Dictionary<AbstractTileItem, AbstractTileItemBehaviour>();
	}
}
