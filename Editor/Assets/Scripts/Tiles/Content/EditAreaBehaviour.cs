﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.IO;

using BinaryDataModel.Converters;

using DataModel.Missions;
using DataModel.Tiles;

using GUI.SidePanel;

using Shared;

using Tools;

using UnityEngine;

using Vehicle = DataModel.Vehicles.Vehicle;

namespace Tiles.Content
{
	[DisallowMultipleComponent]
	public class EditAreaBehaviour : ItemWithMeshBehaviour
	{
		public EditArea editArea => (EditArea) Data;

		// We need a separate GameObject for the contained vehicle
		// Otherwise the scale of the edit area bounds would affect the vehicle.
		private GameObject vehicle;
		private VehicleLoader vehicleLoader;

		private bool forceBoundsVisible = false;

		#region UnityMessages

		/// <inheritdoc />
		protected override void OnAwake()
		{
			base.OnAwake();

			selectionEvents.OnSelect += SelectionEvents_OnSelect;
			selectionEvents.OnDeSelect += SelectionEvents_OnDeSelect;
		}

		/// <inheritdoc />
		protected override void OnEnableAndAfterStart()
		{
			base.OnEnableAndAfterStart();

			UpdateVehicle();


		}


		#endregion UnityMessages


		/// <inheritdoc />
		protected override void DerivedInitialize()
		{
			// Mitigate scale sneaking into the transform somehow.
			transform.localScale = Vector3.one;
			editArea.LTransform = transform.ToOpenTK(local: true, scale: false);
			transform.localScale = editArea.size.ToUnity();
		}

		#region InteractionHandlers

		#region Inspectorpanel
		/// <inheritdoc />
		protected override void OnInspectorInteract()
		{
			base.OnInspectorInteract();

			// When the size preset dropdown is used.
			SaveSizeFromEditArea();
			FixVehicleScale();
		}

		/// <inheritdoc />
		protected override void OnInspectorSubmit()
		{
			base.OnInspectorSubmit();

			// When the size preset dropdown is used.
			SaveSizeFromEditArea();
			FixVehicleScale();

			UpdateVehicle();
		}

		#endregion Inspectorpanel

		#region Gizmo


		protected override void OnGizmoInteract()
		{
			base.OnInteract();

			// Gizmo affects transform
			SaveSizeFromTransform();

			FixVehicleScale();
		}

		protected override void OnGizmoEndInteract()
		{
			// base would save scale into transform, which it should not for this.
			//base.OnSubmit();
			EnsureCorrectParentTile();

			// Gizmo affects transform
			SaveSizeFromTransform();

			FixVehicleScale();
		}
		#endregion Gizmo







		#endregion InteractionHandlers

		private void SaveSizeFromEditArea()
		{
			var size = editArea.size;
			SaveSize(size.ToUnity());
		}

		private void SaveSizeFromTransform()
		{
			// The Gizmo affects this object, but we need the vehicle to always have scale 1
			UnityEngine.Vector3 scale = transform.localScale;

			// Clamp scale to game-supported values.
			scale.x = Math.Clamp(scale.x, 0, 128);
			scale.y = Math.Clamp(scale.y, 0, 128);
			scale.z = Math.Clamp(scale.z, 0, 128);

			if (scale.sqrMagnitude == 0)
			{
				// Zero dimensional is not clickAble.
				scale = Vector3.one;
			}
			SaveSize(scale);
		}

		private void SaveSize(Vector3 size)
		{
			transform.localScale = size;

			// Save the actual values, note that scale is disabled.
			editArea.LTransform = transform.ToOpenTK(local: true, scale: false);
			editArea.size = size.ToOpenTK();
		}


		private void FixVehicleScale()
		{
			if (null == vehicle)
				return;

			Vector3 scale = transform.localScale;

			// Invert the scale factor to end back up at one.
			vehicle.transform.localScale =
				new Vector3(1f / scale.x, 1f / scale.y, 1f / scale.z);
		}

		/// <inheritdoc />
		protected override void SetupMesh()
		{
			{
				// Fix invisible size setting.
				var size = editArea.size;
				int counter = 0;
				if (size.X == 0) counter++;
				if (size.Y == 0) counter++;
				if (size.Z == 0) counter++;

				if (counter >= 2)
				{
					if (size.X == 0) size.X = 1;
					if (size.Y == 0) size.Y = 1;
					if (size.Z == 0) size.Z = 1;
				}

				editArea.size = size;
			}

			meshFilter.sharedMesh = Globals.WireFrames.Cube;

			if (editArea.is_static)
			{
				meshRenderer.material = Globals.Materials.EditAreaStatic;
			}
			else
			{
				meshRenderer.material = Globals.Materials.EditAreaDynamic;
			}

			meshCollider.sharedMesh = Globals.Meshes.Cube;

			transform.localScale = editArea.size.ToUnity();
		}

		/// <inheritdoc />
		protected override void Tasked_UpdateMesh() { }


		private void OnVehicleLoaded(Vehicle vehicle)
		{
			bool newState = ! (null != vehicle && ! editArea.is_invisible_vehicle) || forceBoundsVisible;
			RendererEnabled = newState;
			ColliderEnabled = newState;
		}


		private void SelectionEvents_OnDeSelect(Transform obj)
		{
			OnVehicleLoaded(vehicleLoader?.activeVehicle);
		}

		private void SelectionEvents_OnSelect(Transform obj)
		{
			OnVehicleLoaded(null);
		}

		public void SetVisible_EditArea(bool newValue)
		{
			if (string.IsNullOrWhiteSpace(editArea.default_vehicle))
			{
				gameObject.SetActive(newValue);
			}
		}

		public void SetVisible_EditAreaWithVehicle(bool newValue)
		{
			if (! string.IsNullOrWhiteSpace(editArea.default_vehicle))
			{
				gameObject.SetActive(newValue);
			}
		}

		public void SetVisible_EditAreaWithVehicleForceBounds(bool newValue)
		{
			forceBoundsVisible = newValue;
			OnVehicleLoaded(vehicleLoader?.activeVehicle);
		}


		private void SetupVehicleGameObject()
		{
			vehicle = new GameObject($"Vehicle: {name}");
			vehicle.transform.SetParent(transform, false);
			vehicleLoader = vehicle.AddComponent<VehicleLoader>();
			vehicleLoader.Position = VehicleLoader.PositionMode.Origin;
			vehicleLoader.Loaded += OnVehicleLoaded;
		}

		public void UpdateVehicle()
		{
			if (string.IsNullOrWhiteSpace(editArea.default_vehicle)
			|| editArea.is_invisible_vehicle)
			{
				if (! ReferenceEquals(null, vehicle))
				{
					Destroy(vehicle);
				}
				return;
			}

			if (null == vehicle)
			{
				SetupVehicleGameObject();
			}

			FixVehicleScale();


			string vehiclePath = Path.Combine(StormworksPaths.Data.debris, editArea.default_vehicle);

			if (vehicleLoader.vehiclePath != vehiclePath)
			{
				OnVehicleLoaded(null); // Set bounds visible again.
			}

			vehicleLoader.vehiclePath = vehiclePath;
		}
	}
}
