﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

using BinaryDataModel;
using BinaryDataModel.Converters;
using BinaryDataModel.DataTypes;

using DataModel.Tiles;

using Tiles.Content;

using Tools;

using UnityEngine;

using Mesh = UnityEngine.Mesh;

namespace Tiles.Content
{
	[DisallowMultipleComponent]
	public class MeshOrPhysBehaviour : ItemWithMeshBehaviour
	{
		private AbstractMeshOrPhys myInfo => (AbstractMeshOrPhys)Data;

		public bool IsMesh => ! IsPhys;

		public bool IsPhys
		{
			get;
			private set;
		}


		/// <inheritdoc />
		protected override void DerivedInitialize()
		{
			bool isMesh = myInfo is MeshInfo;
			bool isPhys = myInfo is PhysInfo;

			if (! isMesh && ! isPhys)
			{
				throw new /* UnityWTF */ Exception
					(
					 $"{nameof(myInfo)} is neither {nameof(MeshInfo)} nor {nameof(PhysInfo)}. That does not make sense."
					);
			}

			IsPhys = isPhys;
		}

		public void SetMeshVisible(bool value)
		{
			if (!IsMesh) return;

			meshRenderer.enabled = value;
			meshCollider.enabled = value;
		}

		public void SetPhysVisible(bool value)
		{
			if (!IsPhys) return;

			meshRenderer.enabled = value;
			meshCollider.enabled = value;
		}

		/// <inheritdoc />
		protected override void SetupMesh()
		{
			// todo: configurable!
			gameObject.layer = LayerMask.NameToLayer("Terrain");

			meshRenderer.material = IsPhys ? Globals.Materials.Physics : Globals.Materials.Terrain;
		}

		private string currentMeshPath;
		private const float priority = 30;

		/// <inheritdoc />
		protected override void Tasked_UpdateMesh()
		{
			try
			{
				if (myInfo.file_name == currentMeshPath) return;
				currentMeshPath = myInfo.file_name;

				if (IsMesh && ! EditorSettingsManager.Settings.TileLoadSettings.LoadMesh) return;
				if (IsPhys && ! EditorSettingsManager.Settings.TileLoadSettings.LoadPhys) return;

				Mesh colliderMesh;
				if (IsPhys)
				{
					TileLoader.ThrowIfAbortLoading();
					var meshData = MeshCache.GetOrLoadPhys(myInfo.file_name);
					if (null == meshData)
					{
						return;
					}

					var mesh = meshData.ToUnityMesh();

					RunOnMainThread.EnqueueAndWaitForCompletion(ApplyVisual, priority);
					void ApplyVisual()
					{
						TileLoader.ThrowIfAbortLoading();
						meshFilter.sharedMesh = mesh;
						meshRenderer.material = Globals.Materials.Physics;
					}
					colliderMesh = mesh;
				}
				else
				{
					TileLoader.ThrowIfAbortLoading();
					var meshData = MeshCache.GetOrLoadMesh(myInfo.file_name);
					if (null == meshData)
					{
						return;
					}

					var meshInfo = meshData.ToUnityMesh();
					colliderMesh = meshInfo.Mesh;

					RunOnMainThread.EnqueueAndWaitForCompletion(ApplyVisual, priority);
					void ApplyVisual()
					{
						TileLoader.ThrowIfAbortLoading();
						meshFilter.sharedMesh = meshInfo.Mesh;
						meshRenderer.materials = meshInfo.Materials.ToArray();
					}
				}

				TileLoader.ThrowIfAbortLoading();
				RunOnMainThread.EnqueueAndWaitForCompletion(ApplyCollider, priority);
				void ApplyCollider()
				{
					TileLoader.ThrowIfAbortLoading();

					meshCollider.convex = false;
					meshCollider.cookingOptions =
						MeshColliderCookingOptions.EnableMeshCleaning
					  | MeshColliderCookingOptions.WeldColocatedVertices;
					meshCollider.sharedMesh = colliderMesh;
					meshCollider.sharedMaterial = Globals.PhysicMaterials.Terrain;
				}
			}
			catch(RunOnMainThreadException e)
			{
				if (e.InnerException is OperationCanceledException)
				{
					// swallow
				}
				else
				{
					throw;
				}
			}
			catch (OperationCanceledException)
			{
				// Ignored.
			}
			catch (Exception e)
			{
				RunOnMainThread.LogException(e, this);
			}
		}
	}
}
