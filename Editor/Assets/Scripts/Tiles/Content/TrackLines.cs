﻿// Copyright 2022-2025 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

using Behaviours;

using BinaryDataModel.Converters;

using DataModel.Tiles;

using DataTypes.Extensions;

using Manage;

using TMPro;

using Tools;

using UnityEngine;

namespace Tiles.Content
{
	public class TrackLines : M0noBehaviour
	{

		private bool ShouldAbortLoading => RunOnMainThread.CancelableTasksShouldCancel;


		private GameObject TrackXmlContainerPrefab => TileLoader.instance.TrackXmlContainerPrefab;
		private GameObject TrackXmlNodePrefab => TileLoader.instance.TrackXmlNodePrefab;
		private GameObject TrackXmlLinePrefab => TileLoader.instance.TrackXmlLinePrefab;


		private TileLOD _myTileLod;
		private byte currentLod;

		private Tile myTileData;
		private bool initStarted = false;
		private bool IsUpToDate = false;

		private Task loadTask;


		protected override void OnAwake()
		{
			_myTileLod = GetRequiredComponentInParents<TileLOD>();
			_myTileLod.LODChanged += MyTileLodLodChanged;
		}

		protected void OnDestroy()
		{
			_myTileLod.LODChanged -= MyTileLodLodChanged;
		}

		private void MyTileLodLodChanged(byte newLOD)
		{
			currentLod = newLOD;
			if (newLOD == 0
			 && ViewManager.Instance.TrackLinesVisible) // todo: this is a hack, somehow do this properly
			{
				gameObject.SetActive(true);
				Load();
			}
			else
			{
				gameObject.SetActive(false);
			}
		}

		/// <summary>
		/// 
		/// Can be run in a task
		/// </summary>
		/// <param name="tileInfo"></param>
		public void Initialize(Tile tileInfo)
		{
			myTileData = tileInfo ?? throw new ArgumentNullException(nameof(tileInfo));
		}

		public void MarkOutdated()
		{
			IsUpToDate = false;
			if(currentLod == 0)
			{
				Load();
			}
		}

		public void Load()
		{
			if (initStarted || IsUpToDate) return;

			if (null == myTileData)
			{
				return;
				// Can happen during reload tiles when camera causes LOD update.
				throw new InvalidOperationException("Cannot load when tile data is not present yet");
			}

			initStarted = true;
			loadTask = Task.Run(Tasked_Load);
		}

		private void Tasked_Load()
		{
			// Collect uninterrupted lines.
			try
			{
				Console.WriteLine($"[{nameof(TrackLines)}]/{nameof(Initialize)}/TrackXml: [{myTileData.fileName}] Starting loading.");
				var timer = new Stopwatch();
				timer.Start();
				IsUpToDate = true;
				initStarted = true;
				var tileInfo = myTileData;
				var map = new Dictionary<string, TrainTrackNode>();
				var uniqueConnections = new HashSet<TrackLinkItem>();

				RunOnMainThread.ThrowIfCanceled();
				RunOnMainThread.EnqueueAndWaitForCompletion(Clear);
				void Clear()
				{
					RunOnMainThread.ThrowIfCanceled();
					foreach (Transform child in transform)
					{
						Destroy(child.gameObject);
					}
				}

				Console.WriteLine($"[{nameof(TrackLines)}]/{nameof(Initialize)}/TrackXml: [{myTileData.fileName}] Starting Instantiation: Nodes.");
				foreach (var track in tileInfo.trackNodes)
				{
					RunOnMainThread.ThrowIfCanceled();
					if (! map.TryAdd(track.id, track))
					{
						Console.WriteLine
							(
							 $"[{nameof(TrackLines)}]/{nameof(Initialize)}/TrackXml: [{tileInfo.fileName}] Encountered duplicate id '{track.id}'."
							);
					}

					foreach (string linkId in track.LinkIds)
					{
						uniqueConnections.Add(new TrackLinkItem(track.id, linkId));
					}

					RunOnMainThread.ThrowIfCanceled();
					RunOnMainThread.EnqueueAndWaitForCompletion(MakeNode, 1000);

					void MakeNode()
					{
						RunOnMainThread.ThrowIfCanceled();
						var go = Instantiate
							(TrackXmlNodePrefab, track.LPosition.ToUnity(), Quaternion.identity, transform);
						go.name = track.id;
						go.transform.localPosition = track.LPosition.ToUnity();

						var tmp = go.GetRequiredComponentInChildren<TextMeshPro>();
						tmp.text = $"{track.TrackLinks.Count} | {track.id}";
					}
				}

				Console.WriteLine($"[{nameof(TrackLines)}]/{nameof(Initialize)}/TrackXml: [{myTileData.fileName}] Starting Instantiation: Lines.");
				foreach (var item in uniqueConnections)
				{
					RunOnMainThread.ThrowIfCanceled();
					RunOnMainThread.EnqueueAndWaitForCompletion(MakeLink, 1001);

					void MakeLink()
					{
						RunOnMainThread.ThrowIfCanceled();
						if (gameObject == null) return; // If destroyed.

						if (! map.TryGetValue(item.A, out var a))
						{
							Console.WriteLine
								(
								 $"[{nameof(TrackLines)}]/{nameof(Initialize)}/TrackXml: [{tileInfo.fileName}] Did not find link target '{item.A}' for '{item.B}'."
								);
							return;
						}

						if (! map.TryGetValue(item.B, out var b))
						{
							Console.WriteLine
								(
								 $"[{nameof(TrackLines)}]/{nameof(Initialize)}/TrackXml: [{tileInfo.fileName}] Did not find link target '{item.B}' for '{item.A}'."
								);
							return;
						}

						Vector3 pa = a.LPosition.ToUnity();
						Vector3 pb = b.LPosition.ToUnity();
						Vector3 pAverage = (pa + pb) / 2;

						// Adjust the position so the local position ends up in the right location.
						pa -= pAverage;
						pb -= pAverage;

						string labelText = $"{a.id} <-> {b.id}";

						var go = Instantiate(TrackXmlLinePrefab, pAverage, Quaternion.identity, transform);
						go.name = labelText;
						go.transform.localPosition = pAverage;

						var label = go.GetRequiredComponentInChildren<TextMeshPro>();
						label.text = labelText;

						label.gameObject.SetActive(false);

						// todo: use a lineRenderer per continuous segment because this sucks for performance
						var line = go.GetRequiredComponent<LineRenderer>();
						line.useWorldSpace = false;
						line.SetPositions(new[] { pa, pb });
					}
				}
				timer.Stop();

				Console.WriteLine($"[{nameof(TrackLines)}]/{nameof(Initialize)}/TrackXml: [{myTileData.fileName}] Loading completed after {timer.Elapsed}.");
			}
			catch (OperationCanceledException)
			{
				// Don't log, but still crash the task.
				throw;
			}
			catch (Exception e)
			{
				RunOnMainThread.LogException(e, this);
				throw;
			}
			finally
			{
				initStarted = false; // It's done.
			}
		}

		private readonly struct TrackLinkItem : IEquatable<TrackLinkItem>
		{
			public readonly string A;
			public readonly string B;

			internal TrackLinkItem(string a, string b)
			{
				// The order does not matter so we ensure we always use the same order, regardless of how the arguments are given.
				// I choose to compare HashCode instead of 'just' comparing because that will cause all kinds of 'culture' and 'case' madness.
				var ha = a.GetHashCode();
				var hb = b.GetHashCode();

				if (ha < hb)
				{
					A = a;
					B = b;
				}
				else
				{
					A = b;
					B = a;
				}
			}

			public override bool Equals(object obj)
			{
				return obj is TrackLinkItem item && Equals(item);
			}

			public bool Equals(TrackLinkItem other)
			{
				return A == other.A &&
				       B == other.B;
			}

			public override int GetHashCode()
			{
				int hashCode = -1817952719;
				hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(A);
				hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(B);
				return hashCode;
			}

			public static bool operator ==(TrackLinkItem left, TrackLinkItem right)
			{
				return left.Equals(right);
			}

			public static bool operator !=(TrackLinkItem left, TrackLinkItem right)
			{
				return !(left == right);
			}
		}
	}
}
