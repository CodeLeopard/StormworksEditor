﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;

using DataModel.Tiles;

using DataTypes.Extensions;

using UnityEngine;

namespace Tiles
{
	/// <summary>
	/// Helper class for <see cref="TileLoader"/> to store information in.
	/// </summary>
	public class TileWrapper
	{
		public Tile tile;

		public GameObject tileRoot;
		public TileBehaviour tileBehaviour;

		// Stored so that we don't need to call into Unity when searching for the nearest tile.
		public Vector3 worldPosition;

		public TileWrapper(GameObject tileRoot)
		{
			this.tileRoot = tileRoot ?? throw new ArgumentNullException(nameof(tileRoot));

			tileBehaviour = tileRoot.GetRequiredComponent<TileBehaviour>();

			worldPosition = tileRoot.transform.position;
		}
	}
}
