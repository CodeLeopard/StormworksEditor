﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;

using Shared;

namespace Tiles
{
	[Serializable]
	public class TileExportSettings : SettingsBase
	{
		private bool _exportTrainTrack = true;
		[VisibleProperty]
		public bool ExportTrainTrack
		{
			get => _exportTrainTrack;
			set => SetWithMarkChanged(ref _exportTrainTrack, value);
		}


		private bool _exportCurveMesh = true;
		[VisibleProperty]
		public bool ExportCurveMesh
		{
			get => _exportCurveMesh;
			set => SetWithMarkChanged(ref _exportCurveMesh, value);
		}


		private bool _exportCurvePhys = true;
		[VisibleProperty]
		public bool ExportCurvePhys
		{
			get => _exportCurvePhys;
			set => SetWithMarkChanged(ref _exportCurvePhys, value);
		}


		private bool _exportTileItems = true;
		[VisibleProperty]
		public bool ExportTileItems
		{
			get => _exportTileItems;
			set => SetWithMarkChanged(ref _exportTileItems, value);
		}


		private bool _exportTrees = true;
		[VisibleProperty]
		public bool ExportTrees
		{
			get => _exportTrees;
			set => SetWithMarkChanged(ref _exportTrees, value);
		}



		private float _connectedNodeDetectionRadius = 0.5f;
		[VisibleProperty]
		public float ConnectedNodeDetectionRadius
		{
			get => _connectedNodeDetectionRadius;
			set
			{
				if (value < 0 || value > 10)
					return; // This will trigger visualization of validation in SidePanel.

				SetWithMarkChanged(ref _connectedNodeDetectionRadius, value);
			}
		}


		private bool _enableSmartJunctionHandling = true;
		[VisibleProperty]
		public bool EnableSmartJunctionHandling
		{
			get => _enableSmartJunctionHandling;
			set => SetWithMarkChanged(ref _enableSmartJunctionHandling, value);
		}

		private float _smartJunctionHandlingRadius = 2.25f;
		[VisibleProperty]
		public float SmartJunctionHandlingRadius
		{
			get => _smartJunctionHandlingRadius;
			set
			{
				if(value < 0 || value > 10)
					return; // This will trigger visualization of validation in SidePanel.

				SetWithMarkChanged(ref _smartJunctionHandlingRadius, value);
			}
		}

		private bool _enableUnintendedJunctionPrevention = true;
		[VisibleProperty]
		public bool EnableUnintendedJunctionprevention
		{
			get => _enableUnintendedJunctionPrevention;
			set => SetWithMarkChanged(ref _enableUnintendedJunctionPrevention, value);
		}

		private float _unintendedJunctionPreventionRadius = 1.5f;
		[VisibleProperty]
		public float UnintendedJunctionPreventionRadius
		{
			get => _unintendedJunctionPreventionRadius;
			set
			{
				if (value < 0 || value > 10)
					return; // This will trigger visualization of validation in SidePanel.

				SetWithMarkChanged(ref _unintendedJunctionPreventionRadius, value);
			}
		}

		private bool _enableTrackTileBorderMitigation = true;
		[VisibleProperty]
		public bool EnableTrackTileBorderMitigation
		{
			get => _enableTrackTileBorderMitigation;
			set => SetWithMarkChanged(ref _enableTrackTileBorderMitigation, value);
		}
	}
}
