﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

using Behaviours;

using DataTypes.Extensions;

using UnityEngine;

namespace Tiles
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(TileLoader))]
	public class TreeExporter : M0noBehaviour
	{
		private TileLoader loader;

		public static TreeExporter instance { get; private set; }

		public string status { get; private set; } = "Not started";


		/// <inheritdoc />
		protected override void OnAwake()
		{
			instance = this;
			loader = GetRequiredComponent<TileLoader>();
		}


		private Task completionSignal;

		public Task Export()
		{
			completionSignal = new Task(() => { });
			StartCoroutine(Do_Export());

			return completionSignal;
		}

		private IEnumerator Do_Export()
		{
			Console.WriteLine($"[{nameof(TreeExporter)}] Started exporting trees...");
			status = "Working ...";
			int tileCounter = 0;
			int treeCounter = 0;
			Stopwatch timer = new Stopwatch();
			timer.Start();

			var waitTasks = new List<Task>();

			foreach (Transform tileCollection in loader.tileCollectionParent.transform)
			{
				foreach (Transform tileRoot in tileCollection)
				{
					var treesRoot = tileRoot.Find("Trees");
					if (null == treesRoot)
					{
						yield return null;
						continue;
					}

					var manager = treesRoot.GetRequiredComponent<TileTreeManager>();
					if (! manager.UnsavedChanges)
					{
						yield return null;
						continue;
					}

					tileCounter++;

					manager.Enqueue_SaveToFile();
					waitTasks.Add(manager.saveTask);

					treeCounter += manager.TreeCount;

					status = $"Working ... {tileCounter,3} tiles, {treeCounter,5} trees processed.";

					yield return null;
				}
			}

			var waitTasks_ = waitTasks.ToArray();
			while (! Task.WaitAll(waitTasks_, 0))
			{
				yield return null;
			}

			timer.Stop();
			Console.WriteLine($"[{nameof(TreeExporter)}] Completed exporting trees in {timer.Elapsed}. {tileCounter} tiles changed, {treeCounter} trees total.");
			status = $"Done: {tileCounter,3} tiles, {treeCounter,5} trees processed.";

			completionSignal?.RunSynchronously();
		}
	}
}
