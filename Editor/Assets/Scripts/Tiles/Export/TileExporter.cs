// Copyright 2022-2025 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/






using Behaviours;
using BinaryDataModel;
using BinaryDataModel.Converters;
using BinaryDataModel.DataTypes;
using Control;
using CurveGraph;
using DataModel.Tiles;
using DataTypes;
using DataTypes.Extensions;
using Extrusion;
using Extrusion.Generator;
using GUI;
using LuaIntegration;
using Manage;
using OpenTK.Mathematics;
using Shared;
using Shared.Serialization;
using StaticObjects;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Tiles.Export;
using Tiles.Export.Tracks;
using Tools;
using UnityEngine;
using Debug = UnityEngine.Debug;
using DMesh = BinaryDataModel.DataTypes.Mesh;
using Quaternion = OpenTK.Mathematics.Quaternion;
using Tile = DataModel.Tiles.Tile;
using Vector3 = OpenTK.Mathematics.Vector3;

namespace Tiles
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(TileLoader))]
	public class TileExporter : M0noBehaviour
	{
		[SerializeField]
		private bool immediatelyReloadTrackLines = true;

		private bool doTileBorderFixup => Settings.EnableTrackTileBorderMitigation;


		public static TileExporter Instance { get; private set; }


		private CurveSaveState curveSaver;
		private ObjectSaveManager objectSaver;
		private TileLoader loader;


		private Task exportTask;


		[HideInInspector]
		public GameObject CurvesParent;

		[HideInInspector]
		public GameObject ObjectsParent;


		private static string romFolder => StormworksPaths.rom;
		private static string tilesFolder => StormworksPaths.Data.tiles;
		private static string meshesFolder => StormworksPaths.meshes;


		private static readonly ParallelOptions _noThreadingParallelOptions = new ParallelOptions() { MaxDegreeOfParallelism = 1 };
		private static readonly ParallelOptions _threadingParallelOptions   = new ParallelOptions() { MaxDegreeOfParallelism = Environment.ProcessorCount };

		private static ParallelOptions parallelOptions =>
			Globals.Constants.ForceSingleThreaded ? _noThreadingParallelOptions : _threadingParallelOptions;


		public TileExportSettings Settings => EditorSettingsManager.Settings.TileExportSettings;

		public static Task ExportTask;
		public static string Status = "Not started";

		#region UnityEvents

		/// <inheritdoc />
		protected override void OnAwake()
		{
			Instance = this;

			loader = GetComponent<TileLoader>();
			curveSaver = GetComponent<CurveSaveState>();
			objectSaver = GetComponent<ObjectSaveManager>();

			CurveNodeBehaviour.NodeRemoved += NodeRemoved_MarkTileDirty;
			StaticObjectBehaviour.Removed  += StaticObjectRemoved_markTileDirty;
		}


		/// <inheritdoc />
		protected override void OnEnableAndAfterStart()
		{
			CurvesParent = curveSaver.thingsContainer;
			ObjectsParent = objectSaver.thingsContainer;
		}

		protected void Update()
		{
			KeyboardInput();

			if (null != exportTask)
			{
				if (exportTask.IsFaulted && null != exportTask.Exception)
				{
					Debug.LogException(exportTask.Exception, this);
					exportTask = null;
				}
				else if (exportTask.IsCanceled)
				{
					Debug.LogError("Export task was cancelled", this);
					exportTask = null;
				}
				else if (exportTask.IsCompleted)
				{
					// Do nothing, so that other classes can see it's completed status.
				}
			}
		}

		private void OnDestroy()
		{
			CurveNodeBehaviour.NodeRemoved -= NodeRemoved_MarkTileDirty;
			StaticObjectBehaviour.Removed  -= StaticObjectRemoved_markTileDirty;
		}

		#endregion UnityEvents

		private void KeyboardInput()
		{
			if (InputManagement.TextInputFocused) return;

			if (Input.GetKeyDown(KeyCode.B) && Input.GetKey(KeyCode.LeftControl))
			{
				if (curveSaver.AllowSave) curveSaver.Save();
				if (objectSaver.AllowSave) objectSaver.Save();

				Export();
			}
		}


		#region EnsureDirty


		private void NodeRemoved_MarkTileDirty(CurveNode obj)
		{
			var wrapper = loader.GetNearestTile(obj.Position);
			wrapper.tileBehaviour.ChangedSinceLastSaved = true;
		}

		private void StaticObjectRemoved_markTileDirty(StaticObjectBehaviour obj)
		{
			var wrapper = loader.GetNearestTile(obj.transform.position);
			wrapper.tileBehaviour.ChangedSinceLastSaved = true;
		}

		#endregion EnsureDirty

		private void MaybeWaitForTileLoader()
		{
			var tileLoader = loader.tileLoader;
			if (null != tileLoader)
			{
				if (!tileLoader.IsCompleted)
				{
					if (tileLoader.IsCanceled || tileLoader.IsFaulted || loader.AnyTileFailed)
					{
						throw new InvalidOperationException
							($"TileExporter cannot run if the loading of tiles failed");
					}
					else
					{
						Debug.Log($"Waiting for Tiles to finish loading before beginning Export.");
						Status = "Waiting for Tiles to load";
						tileLoader.Wait();
					}
				}
			}
		}

		private void MaybeWaitForUnFlattenTerrain()
		{
			if (!ViewManager.Instance.FlattenTerrain) return;

			var co = RunOnMainThread.EnqueueAndWaitForCompletion(StartUnFlatten);
			Coroutine StartUnFlatten()
			{
				return ViewManager.Instance.SetFlattenTerrain(false);
			}

			if (co == null) return;

			while (ViewManager.Instance.IsFlattening)
			{
				// todo: polling REEEEE
				Thread.Sleep(TimeSpan.FromMilliseconds(100));
			}
		}

		private void CheckForLoadingFailed()
		{
			if (TileLoader.instance.AnyTileFailed)
			{
				ConfirmationPopupPanel MkDialog()
				{
					var d = ConfirmationPopupPanel.Create();
					d.HeaderText = "Are you sure?";
					d.BodyText = "Some tiles failed to load (see About -> LogFile for details)."
					           + "\nSaving in that state could cause inconsistency.";

					return d;
				}

				var dialog = RunOnMainThread.EnqueueAndWaitForCompletion(MkDialog);

				bool? result = dialog.WaitForInteraction().Result;


				if (result != true)
				{
					throw new OperationCanceledException("Exporting was aborted by the user.");
				}
			}
		}

		public Task Export()
		{
			if (null != exportTask && ! exportTask.IsCompleted)
			{
				throw new InvalidOperationException($"Export task already running.");
			}

			var workData = new WorkData();

			Status = "Waiting for Task to start.";


			void T_CollectWorkData()
			{
				Console.WriteLine($"[{nameof(TileExporter)}] Export Task Started.");
				MaybeWaitForTileLoader();
				MaybeWaitForUnFlattenTerrain();

				CheckForLoadingFailed();

				TileDataSearcher.Reload();

				var global = Task.Run(() => CollectWorkFromGlobalSpace(workData));
				var local = Task.Run(() => CollectWorkFromTiles(workData));

				global.Wait();
				local.Wait();
			}

			Task treeTask = null;

			void T_GenTrackData(Task task)
			{
				ThrowIfFaultedOrCanceled(task);

				if (Settings.ExportTrees)
				{
					treeTask = RunOnMainThread.EnqueueAndWaitForCompletion(() => GetRequiredComponent<TreeExporter>().Export());
				}
				GenTrackData(workData);

				ConnectTracksGlobally(workData);
			}
			void T_WriteData(Task task)
			{
				ThrowIfFaultedOrCanceled(task);
				WriteData(workData);
			}
			void T_OnCompletion(Task task)
			{
				ThrowIfFaultedOrCanceled(task);

				treeTask?.Wait(RunOnMainThread.CancelToken);

				OnCompletion();
			}

			void T_AfterCompletion(Task task)
			{
				ThrowIfFaultedOrCanceled(task);
				SignalSaveSuccessToTiles(workData);
			}

			exportTask = Task.Run(T_CollectWorkData)
			                 .ContinueWith(T_GenTrackData)
			                 .ContinueWith(T_WriteData)
			                 .ContinueWith(T_OnCompletion)
			                 .ContinueWith(T_AfterCompletion);

			ExportTask = exportTask;

			Console.WriteLine($"[{nameof(TileExporter)}] Export Task Created.");

			return exportTask;


			void OnCompletion()
			{
				Debug.Log($"{nameof(TileExporter)}.{nameof(Export)}() Done", this);

				string lengthStr;

				if (lastExportCurveLength > 2000)
				{
					lengthStr = $"{Mathf.RoundToInt(lastExportCurveLength / 1000)}km";
				}
				else
				{
					lengthStr = $"{Mathf.RoundToInt(lastExportCurveLength)}m";
				}

				Status =
					$"Done: Generated {lastExportCurveCount} curves, together {lengthStr}."
				  + $" Placed {lastExportObjectsCount} StaticObjects."
				  + $" Saved {workData.tiles.Count} tiles with Items.";
			}
		}

		public void Button_ExportTracksSingleMesh()
		{
			ExportTracksAsSingleMesh();
		}

		public Task ExportTracksAsSingleMesh()
		{
			if (null != exportTask && !exportTask.IsCompleted)
			{
				throw new InvalidOperationException($"Export task already running.");
			}

			var workData = new WorkData();

			Status = "Waiting for Task to start.";

			void T_Work()
			{
				Console.WriteLine($"[{nameof(TileExporter)}] Export Task Started.");
				MaybeWaitForTileLoader();

				CheckForLoadingFailed();

				TileDataSearcher.Reload();

				CollectWorkFromGlobalSpace(workData);

				GenTrackData(workData, allowMesh: true, allowPhys: false, allowNodes: false);

				var mergeData = new List<MeshCombiner.DataItem>();
				foreach (CurveDataItem item in workData.curves)
				{
					//Vector3 pos1 = item.decoratorData.Meshes.First().Position;
					Vector3 pos2 = item.connection.Position;
					Vector3 pos = /*pos1 +*/ pos2;
					var trans = Matrix4.Identity;
					trans.Column3 = pos.WithW(1);

					foreach(var md in item.meshData)
					{
						var merge = new MeshCombiner.DataItem(md, trans);
						mergeData.Add(merge);
					}
				}

				var result = MeshCombiner.Combine(mergeData, true, false);

				var destination = Path.Combine(StormworksPaths.creatorToolkitData, "MergedTracks.ply");
				Ply.Save(result, destination);
				Status = $"Saved tracks to '{destination}'.";
			}

			exportTask = Task.Run(T_Work);
			return exportTask;
		}

		private void ThrowIfFaultedOrCanceled(Task t)
		{
			if (t.Status == TaskStatus.Running) return;

			if (null != t.Exception)
			{
				Console.WriteLine($"Exporting failed, last state was: {Status}");
				Status = $"Failed: {t.Exception.Message}, see log file for details.";
				throw t.Exception;
			}

			if (t.IsCanceled)
			{
				Status = "Failed: task canceled, see log file for clues.";
				throw new OperationCanceledException($"Operation was canceled for unknown reason, the last states was: {Status}");
			}

			if (t.IsFaulted)
			{
				Console.WriteLine($"Exporting failed, last state was: {Status}");
				Status = "Failed: unknown, see log file for clues.";
				throw new Exception($"Task failed for unknown reason, the last status was: {Status}");
			}
		}

		public static Vector2Int GetTilePos(Vector3 position)
		{
			float x = position.X / 1000f;
			float z = position.Z / 1000f;

			int tnx = Mathf.FloorToInt(x);
			int tnz = Mathf.FloorToInt(z);

			var t = new Vector2Int(tnx, tnz);
			return t;
		}

		/// <summary>
		/// Returns a transformation from world space to the space of the given tile.
		/// </summary>
		/// <param name="tile"></param>
		/// <returns></returns>
		private static Vector3 TransformationForTile(Vector2Int tile)
		{
			var m = new Vector3(1000 * tile.x, 0, 1000 * tile.y);
			var v = new Vector3(500, 0, 500);
			return -(m + v);
		}


		static void Assert(bool condition, string message, [CallerMemberName] string callerMemberName = "unknown", [CallerLineNumber] int callerLineNum = -1)
		{
			if (condition) return;

			string logMessage = $"Assertion failed: {callerMemberName} @ {callerLineNum}: {message}";
#if UNITY_EDITOR
			Debug.LogError(logMessage);
			return;
#else
			Console.WriteLine(message);
#endif
		}


		private const float defaultMargin = 0.001f;
		private static bool ApproximatelyEquals(float a, float b, float margin = defaultMargin)
		{
			return Math.Abs(a - b) < margin;
		}

		private static bool ApproximatelyEquals(Vector3 a, Vector3 b, float margin = defaultMargin)
		{
			return ApproximatelyEquals(a.X, b.X, margin)
			    && ApproximatelyEquals(a.Y, b.X, margin)
			    && ApproximatelyEquals(a.Z, b.Z, margin);
		}

		public static readonly Regex trackExtrusionMeshIDRegex =
			new Regex("^ExtrudedTrackMesh_Curve_(phys_)?(\\d+)$");
		public static readonly Regex trackExtrusionMeshFileRegex =
			new Regex("^meshes/extruded_track_pieces/.*ExtrudedTrackMesh_Curve_(phys_)?(\\d+)_(\\d+)((\\.mesh)|(\\.phys))$");
		public static readonly Regex staticObjectIDRegex =
			new Regex("^StaticObject_(\\d+)(_phys)?$");
		public static readonly Regex trackIDRegex =
			new Regex("^SplineTrack_Curve_(phys_)?(\\d+)_#(\\d+)$");

		


		private static long tileWithWorkCount;
		private static long saveTileProgress;
		private static readonly object statusLock = new object();



		#region Export Process


		private int lastExportCurveCount = 0;
		private float lastExportCurveLength = 0;
		private float lastExportObjectsCount = 0;

		/// <summary>
		/// Retrieve workItems whose representation lives in GlobalSpace.
		/// </summary>
		/// <param name="_data"></param>
		private void CollectWorkFromGlobalSpace(WorkData _data)
		{
			var curves = _data.curves;
			var objects = _data.objects;

			RunOnMainThread.EnqueueAndWaitForCompletion(CollectCurveGraph);
			void CollectCurveGraph()
			{
				Status = "Collecting track information";
				Console.WriteLine($"{nameof(TileExporter)}.{nameof(CollectWorkFromGlobalSpace)}() Curves");


				lastExportCurveCount = 0;
				lastExportCurveLength = 0;

				var nodes = curveSaver.GetDataFromWorld();
				if (nodes.Count <= 0)
				{
					Console.WriteLine($"[{nameof(TileExporter)}]:{nameof(CollectWorkFromGlobalSpace)} There are no tracks to save.");
					return;
				}

				foreach (var connection in nodes[0].NetworkConnections(nodes))
				{
					var curve = connection.curve;
					lastExportCurveLength += curve.Length;

					var customData = connection.data as ConnectionCustomData;
					var item = new CurveDataItem() { connection = connection, spec = customData?.CurveSpec };
					curves.Add(item);
				}


				lastExportCurveCount = curves.Count;

				Console.WriteLine
				(
					$"{nameof(TileExporter)}.{nameof(CollectWorkFromGlobalSpace)}() Splines: Done: {curves.Count} curves, "
				  + $"{lastExportCurveLength} meters of track."
				);
			}

			RunOnMainThread.EnqueueAndWaitForCompletion(CollectStaticObjects);
			void CollectStaticObjects()
			{
				Status = "Collecting Static Object information";
				Console.WriteLine($"{nameof(TileExporter)}.{nameof(CollectWorkFromGlobalSpace)}() StaticObjects");

				foreach (var rawData in objectSaver.GetDataFromWorld())
				{
					var item = new ObjectDataItem();

					item.myObject = rawData;

					objects.Add(item);
				}

				lastExportObjectsCount = objects.Count;
				Console.WriteLine($"{nameof(TileExporter)}.{nameof(CollectWorkFromGlobalSpace)}() StaticObjects: Done {lastExportObjectsCount} objects");
			}
		}

		private void CollectWorkFromTiles(WorkData workData)
		{
			List<Tile> toSaveTiles = workData.tiles;

			foreach (var kvp in loader.MapGameObjectToTile)
			{
				var go = kvp.Key;
				var wrapper = kvp.Value;

				if (wrapper.tile != null && wrapper.tileBehaviour.ChangedSinceLastSaved)
				{
					toSaveTiles.Add(wrapper.tile);
				}
			}
		}

		private void SignalSaveSuccessToTiles(WorkData workData)
		{
			RunOnMainThread.EnqueueAndWaitForCompletion(Action);
			void Action()
			{
				foreach (var kvp in loader.MapGameObjectToTile)
				{
					var go = kvp.Key;
					var wrapper = kvp.Value;

					if (wrapper.tile == null) continue;

					wrapper.tileBehaviour.ChangedSinceLastSaved = false;
				}
			}
		}


		private void GenTrackData(WorkData _data, bool allowMesh = true, bool allowPhys = true, bool allowNodes = true)
		{
			// Generates the Track Mesh, Phys and Nodes for the entire world.

			var curves = _data.curves;

			Console.WriteLine($"{nameof(TileExporter)}.{nameof(GenTrackData)}() Start");

			long total = curves.Count();
			long count = 0;

			Parallel.ForEach(curves, parallelOptions, EachCurve);
			void EachCurve(CurveDataItem data, ParallelLoopState state, long iteration)
			{
				uint curveID = ConnectionUIDProvider.Instance.GetID(data.connection);
				var curve = data.curve;

				Console.WriteLine($"{nameof(TileExporter)}.{nameof(GenTrackData)}() I{iteration} Curve#{curveID,3}");

				var nearestTile = loader.GetNearestTile(curve.Position.ToUnity());
				Vector3 transformation = - nearestTile.worldPosition.ToOpenTK();
				data.transformation = transformation;

				if (null != data.spec)
				{
					var decorator = new CurveDecorator();
					decorator.GenerateMesh  = Settings.ExportCurveMesh && allowMesh;
					decorator.GeneratePhys  = Settings.ExportCurvePhys && allowPhys;
					decorator.GenerateTrack = Settings.ExportTrainTrack && allowNodes;


					var result = decorator.Generate(curve, data.spec, $"Curve#{curveID}");
					data.decoratorData = result;

					if (Settings.ExportCurveMesh && allowMesh && data.spec.WillGenerateMesh)
					{
						int counter = 0;
						foreach(var resultItem in result.Meshes)
						{
							data.meshData.Add(resultItem.Mesh);
							var info = new MeshInfo();
							info.id = $"ExtrudedTrackMesh_Curve_{curveID}_{counter++}";
							info.LTransform = Matrix4.CreateTranslation(-resultItem.Position + transformation);

							data.meshInfo.Add(info);
						}
					}

					if (Settings.ExportCurvePhys && allowPhys && data.spec.WillGeneratePhys)
					{
						int counter = 0;
						foreach (var resultItem in result.Physes)
						{
							data.physData.Add(resultItem.Phys);
							var info = new PhysInfo();
							info.id = $"ExtrudedTrackMesh_Curve_{curveID}_{counter++}";
							info.LTransform = Matrix4.CreateTranslation(-resultItem.Position + transformation);

							data.physInfo.Add(info);
						}
					}

					if (Settings.ExportTrainTrack && allowNodes)
					{
						void AddDecorators(TrainTrackNode node)
						{
							var decoratorData = data.connection.decoratorData;
							if (decoratorData == null) return;

							foreach(var link in node.TrackLinks.Values)
							{
								link.TrackSpeed_ms     = decoratorData.TrackSpeed_ms;
								link.SuperElevation_mm = decoratorData.SuperElevation_mm;

								link.TrackName    = decoratorData.TrackName;
								link.CatenarySpec = decoratorData.CatenarySpec;
							}
						}
						foreach (TrainTrackNode node in result.Tracks)
						{
							AddDecorators(node);

							//Assert(trackIDRegexV2.IsMatch(node.id), $"TrackID regex does not match: '{node.id}'");
							data.trackNodes.Add(node);
							// global position is needed for ConnectTracksGlobally()
						}
					}
				}


				lock (statusLock)
				{
					Status = $"Generating Extrusions {++count,5} / {total,5}";
				}
			}
			Console.WriteLine($"{nameof(TileExporter)}.{nameof(GenTrackData)}() Done: {count} extrusions generated.");
		}

		/// <summary>
		/// Connect tracks in globals space, so that we can catch all junctions and handle them properly.
		/// </summary>
		/// <param name="work"></param>
		private void ConnectTracksGlobally(WorkData workData)
		{
			var context = new TrackConnectionContext(workData.curves.SelectMany(c => c.trackNodes), Settings, CancellationToken.None, startTimers: true);

			context.OnStatus += Context_OnStatus;

			try
			{
				TrainTrackConnector.ConnectTracksGlobally(context);
			}
			catch (Exception e)
			{
				// todo: perhaps also have a popup or something, with the full error message etc.
				Debug.LogError($"Warning: Something went wrong while connection TrainTrackNodes. It should be safe to load the world in stormworks (should not crash), but the wheel train physics may be wonky at junctions. Trying again may or may not make a difference.\n{e}");
			}
			finally
			{
				context.OnStatus -= Context_OnStatus;
			}
			
			// Ensure removeBuffer is applied.
			if(context.ApplyRemoveBuffer() > 0)
			{
				Debug.LogWarning("RemoveBuffer contained items to remnove after all steps ware completed. This should not happen.");
			}

			Console.WriteLine($"Finalizing TrainTrackNodes: Applying FinalRemoveBuffer: {context.finalRemoveBuffer.Count} total nodes will be removed.");

			var i = 0;
			var max = workData.curves.Count;

			foreach (CurveDataItem item in workData.curves)
			{

				Status = $"Finalizing, tile {++i} / {max}";

				// Apply the removals in a single operation
				item.trackNodes.RemoveAll(n => context.finalRemoveBuffer.Contains(n));

				// Try to keep a stable order to prevent unnecessary changes in change trackers.
				item.trackNodes.Sort(TrackIDComparer.Instance);

				foreach (TrainTrackNode node in item.trackNodes)
				{
					// Change the Transform to Tile-Space
					// This wasn't done before because the junction detection needs World-Space
					// coordinates to work.

					node.LPosition += item.transformation;
				}
			}

			Debug.Log($"ConnectTracksGlobally: Done after {context.totalTimer.Elapsed}.");
		}

		private void Context_OnStatus(string obj)
		{
			lock (statusLock)
			{
				Status = obj;
			}
		}

		private void WriteData(WorkData _data)
		{
			Status = "Preparation before saving";
			Console.WriteLine($"{nameof(TileExporter)}.{nameof(WriteData)} Init");

			var oldContentTask = Task.Run(DeleteOldMeshes);

			SortIntoTileDictionary(_data);


			foreach (Tile t in _data.tiles)
			{
				TileDataSearcher.tileDictionary[t].tileDirty = true;
			}


			TileDataItem[] dataItems =
				TileDataSearcher.tileDictionary.Values.Where(tdi => tdi.hasWork).ToArray();


			tileWithWorkCount = dataItems.Length;

			Status = "Preparing Tile data";
			Console.WriteLine($"{nameof(TileExporter)}.{nameof(WriteData)}/{nameof(PrepareTileData)}");
			Parallel.ForEach(Partitioner.Create(dataItems, EnumerablePartitionerOptions.NoBuffering)
						   , parallelOptions, PrepareTileData);

			TrainTrackNodeTileBorderFixup(dataItems);

			Status = "Sanity check";
			Console.WriteLine($"{nameof(TileExporter)}.{nameof(WriteData)}/{nameof(SanityCheck)}");
			Parallel.ForEach(Partitioner.Create(dataItems, EnumerablePartitionerOptions.NoBuffering)
						   , parallelOptions, SanityCheck);


			Status = $"Saving Tile Xml {"-",5} / {tileWithWorkCount,5}";
			Console.WriteLine($"{nameof(TileExporter)}.{nameof(WriteData)}/{nameof(SaveXml)}");
			Parallel.ForEach(Partitioner.Create(dataItems, EnumerablePartitionerOptions.NoBuffering)
						   , parallelOptions, SaveXml);

			try
			{
				if (immediatelyReloadTrackLines)
				{
					loader.StartReloadTrackLines();
				}
			}
			catch (Exception e)
			{
				RunOnMainThread.LogException(e, loader);
			}

			Status = "Waiting for cleanup";
			oldContentTask.Wait();

			saveTileProgress = 0;

			Status = $"Saving Meshes for Tile {"-",5} / {tileWithWorkCount,5}";
			Console.WriteLine($"{nameof(TileExporter)}.{nameof(WriteData)}/{nameof(SaveTileMeshes)}");
			Parallel.ForEach(Partitioner.Create(dataItems, EnumerablePartitionerOptions.NoBuffering)
						   , parallelOptions, SaveTileMeshes);

			Console.WriteLine($"{nameof(TileExporter)}.{nameof(WriteData)} Done");
		}


		#region WriteData Steps

		private static void DeleteOldMeshes()
		{
			Console.WriteLine($"{nameof(TileExporter)}.{nameof(WriteData)}/{nameof(DeleteOldMeshes)}: Start.");

			// Creates if does not exists, otherwise returns DirectoryInfo object which is ignored.
			var dir = Directory.CreateDirectory(StormworksPaths.Meshes.extruded_track_pieces);

			int count = 0;
			foreach (var file in dir.EnumerateFiles("*.mesh").Concat(dir.EnumerateFiles("*.phys")))
			{
				file.Delete();
				count++;
			}

			dir = Directory.CreateDirectory(StormworksPaths.Meshes.generated_object_geom);
			foreach (var file in dir.EnumerateFiles("*.mesh").Concat(dir.EnumerateFiles("*.phys")))
			{
				file.Delete();
				count++;
			}

			Console.WriteLine($"{nameof(TileExporter)}.{nameof(WriteData)}/{nameof(DeleteOldMeshes)}: Removed {count} items.");
		}



		private static class TileDataSearcher
		{
			internal static readonly Dictionary<Vector2Int, TileDataItem> positionDictionary = new Dictionary<Vector2Int, TileDataItem>();
			internal static readonly Dictionary<Tile, TileDataItem> tileDictionary  = new Dictionary<Tile, TileDataItem>();

			internal static void Reload()
			{
				positionDictionary.Clear();
				tileDictionary.Clear();

				foreach (var kvp in TileLoader.instance.MapGameObjectToTile)
				{
					var go = kvp.Key;
					var tileWrapper = kvp.Value;

					if (null == tileWrapper.tile)
					{
						RunOnMainThread.EnqueueAndWaitForCompletion(Log);
						void Log()
						{
							Console.WriteLine($"Skipping load-failed tile '{go.name}'");
						}
						continue;
					}

					var rootWorldPosition = tileWrapper.worldPosition.ToOpenTK();
					var tilePosition = GetTilePos(rootWorldPosition);

					var tdi = new TileDataItem();
					tdi.tile = tileWrapper.tile;
					tdi.worldToTileSpace = -rootWorldPosition;

					positionDictionary.Add(tilePosition, tdi);
					tileDictionary.Add(tileWrapper.tile, tdi);
				}
			}

			/// <summary>
			/// Gets the tile that emcompasses the given position, or false if there is no tile there.
			/// </summary>
			/// <param name="worldPosition"></param>
			/// <param name="tdi"></param>
			/// <returns></returns>
			internal static bool TryGet(Vector3 worldPosition, out TileDataItem tdi)
			{
				Vector2Int intVector = GetTilePos(worldPosition);

				if (positionDictionary.TryGetValue(intVector, out tdi))
				{
					return true;
				}

				if (! TileLoader.instance.TryGetCurrentTile(worldPosition, out TileWrapper wrapper))
				{
					return false;
				}

				if (tileDictionary.TryGetValue(wrapper.tile, out tdi))
				{
					return true;
				}

				return false;// Can you remember a number for me? 115
			}

			/// <summary>
			/// Gets the tile closest to the given position, up to a limit.
			/// </summary>
			/// <param name="worldPosition"></param>
			/// <param name="tdi"></param>
			/// <param name="maxDistance">The maximum distance in meters from the given point ddto search for a tile</param>
			/// <returns></returns>
			internal static bool TryGetNear(Vector3 worldPosition, out TileDataItem tdi, float maxDistance = 1500f)
			{
				Vector2Int intVector = GetTilePos(worldPosition);

				if (positionDictionary.TryGetValue(intVector, out tdi))
				{
					return true;
				}

				var wrapper = TileLoader.instance.GetNearestTile(worldPosition);

				var diff = worldPosition - wrapper.worldPosition.ToOpenTK();
				float sqMax = maxDistance * maxDistance;
				float sqLen = diff.LengthSquared;

				if(sqLen > sqMax)
				{
					return false;
				}
			

				if (tileDictionary.TryGetValue(wrapper.tile, out tdi))
				{
					return true;
				}


				return false;
			}
		}


		private static readonly Regex stripPhysSuffix = new Regex("_[Pp]hys$", RegexOptions.Compiled);

		private void SortIntoTileDictionary(WorkData _data)
		{
			List<CurveDataItem> curves = _data.curves;
			List<ObjectDataItem> objects = _data.objects;

			foreach (CurveDataItem curveItem in curves)
			{
				var worldPosition = curveItem.curve.Position;
				var tilePosition = GetTilePos(worldPosition);

				if (!TileDataSearcher.TryGetNear(worldPosition, out var tdi))
				{
					Console.WriteLine($"{nameof(TileExporter)}.{nameof(WriteData)}/{nameof(SortIntoTileDictionary)}:"
									+ $" Discarded CurveItem without matching tile: {curveItem} @ {worldPosition}");
					continue;
				}
				
				const string meshFolder = "meshes/extruded_track_pieces/";

				var tileName = tdi.tile.fileName;

				foreach(var meshInfo in curveItem.meshInfo)
				{
					meshInfo.file_name = $"{meshFolder}{tileName}_{meshInfo.id}.mesh";
					Assert
						(
							trackExtrusionMeshFileRegex.IsMatch(meshInfo.file_name)
						, $"Regex does not match: StaticObject.meshInfo.file_name '{meshInfo.file_name}'"
						);
				}


				foreach (var physInfo in curveItem.physInfo)
				{
					// Remove redundant _phys suffix
					string fixedID = stripPhysSuffix.Replace(physInfo.id, "");

					physInfo.file_name = $"{meshFolder}{tileName}_{fixedID}.phys";
					Assert(trackExtrusionMeshFileRegex.IsMatch(physInfo.file_name), $"Regex does not match: StaticObject.physInfo.file_name '{physInfo.file_name}'");
				}

				tdi.curves.Add(curveItem);
			}

			foreach (ObjectDataItem item in objects)
			{
				var worldPosition = item.myObject.transform.ExtractTranslation();

				if (!TileDataSearcher.TryGetNear(worldPosition, out var tdi))
				{
					Console.WriteLine($"{nameof(TileExporter)}.{nameof(WriteData)}/{nameof(SortIntoTileDictionary)}:"
									+ $" Discarded StaticObject without matching tile {item} @ {worldPosition}");
					continue;
				}
				tdi.objects.Add(item);
			}
		}




		private void PrepareTileData(TileDataItem tileItem, ParallelLoopState state, long iteration)
		{
			try
			{
				///////////////////////////////////////////////////////////////
				// Remove generated content:
				{
					var tile = tileItem.tile;
					foreach (var item in tile.AllTileItems().ToArray())
					{
						if (item.LoadIgnore)
						{
							tile.Remove(item);
						}
					}
				}


				uint staticObjectID = uint.MaxValue; // Incremented before use to become 0.
				foreach (ObjectDataItem dataItem in tileItem.objects)
				{
					unchecked { staticObjectID++; }

					StaticObject staticObject = dataItem.myObject;

					var meshInfo = new MeshInfo();
					dataItem.meshInfo = meshInfo;

					string originalMeshFilePath = staticObject.meshFilePath;
					meshInfo.file_name = originalMeshFilePath;

					Vector3 originalPosition = staticObject.transform.ExtractTranslation() + tileItem.worldToTileSpace;
					Quaternion originalRotation = staticObject.transform.ExtractRotation();
					Vector3 originalScale = staticObject.transform.ExtractScale();

					// todo: this **seems** to work properly, even though that same function in other places definitely doesn't.
					Matrix4 originalTransform = Matrix4Ext.CreateTransScaleRot(originalPosition, originalScale, originalRotation);
					meshInfo.LTransform = originalTransform;




					if (! ApproximatelyEquals(originalScale, Vector3.One))
					{
						// Scaled meshes have trouble with the save-load transition, so we need to bake them also.

						Matrix4 keyTransformation = Matrix4.CreateScale(originalScale);

						DMesh meshData = null;
						MeshTransformer.Key key;

						if (string.IsNullOrWhiteSpace(meshInfo.file_name))
						{
							throw new InvalidOperationException();
						}

						DMesh original = MeshCache.GetOrLoadMesh(meshInfo.file_name);
						key = new MeshTransformer.Key(original, keyTransformation);

						meshData = MeshTransformer.Instance.GetOrCreate(key);

						// The .phys is now scaled, so it's transform should not include scale.
						Matrix4 unscaledTransform = Matrix4Ext.CreateTransScaleRot(originalPosition, Vector3.One, originalRotation);

						meshInfo.LTransform = unscaledTransform;
						meshInfo.file_name = null; // The file has not been saved yet, so we don't know the name yet.
						dataItem.meshData = meshData;
					}


					meshInfo.id = $"StaticObject_{staticObjectID}";

					// Physics ------------------------------------------------

					PhysInfo physInfo = null;

					if (! string.IsNullOrWhiteSpace(staticObject.physMeshFilePath))
					{
						var fullpath = Path.Combine(StormworksPaths.rom, staticObject.physMeshFilePath);
						if (File.Exists(fullpath))
						{
							physInfo = new PhysInfo();
							physInfo.id = $"{meshInfo.id}_phys";
							physInfo.LTransform = originalTransform;
							physInfo.file_name = staticObject.physMeshFilePath;
						}
						else
						{
							// todo: ensure the generated mesh is placed in the specified location instead.
						}
					}


					if (staticObject.createPhys || null != physInfo)
					{
						if (null == physInfo)
						{
							physInfo = new PhysInfo();
							physInfo.id = $"{meshInfo.id}_phys";
							physInfo.LTransform = originalTransform;

							// If the phys is generated we don't give it a name yet, that will happen later.
							physInfo.file_name = null;

							Phys generatedPhys = PhysGenerator.Instance.GetOrCreate(staticObject.meshFilePath);
							dataItem.physData = generatedPhys;
						}

						// If mesh is scaled we need to generate a scaled version of the .phys because the physics engine does not support scaling.
						if (! ApproximatelyEquals(originalScale, Vector3.One))
						{
							Matrix4 keyTransformation = Matrix4.CreateScale(originalScale);

							Phys physData = null;
							PhysTransformer.Key key;

							if (! string.IsNullOrWhiteSpace(physInfo.file_name))
							{
								Phys original = MeshCache.GetOrLoadPhys(physInfo.file_name);
								key = new PhysTransformer.Key(original, keyTransformation);
							}
							else
							{
								key = new PhysTransformer.Key(dataItem.physData, keyTransformation);
							}

							physData = PhysTransformer.Instance.GetOrCreate(key);

							// The .phys is now scaled, so it's transform should not include sacle.
							Matrix4 unscaledTransform = 
								Matrix4Ext.CreateTransScaleRot(originalPosition, Vector3.One, originalRotation);

							physInfo.LTransform = unscaledTransform;
							physInfo.file_name =
								null; // The file has not been saved yet, so we don't know the name yet.
							dataItem.physData = physData;
						}
					}

					if (dataItem.meshData != null)
					{
						// We generated a new .mesh for this item.
						// We need to generate a fileName for it.
						// The mesh data will be saved in a later stage.

						// todo: ideally we could override the save path.
						var originalFilePath = originalMeshFilePath;

						var relativePath = PathHelper.GetFilePathWithoutExtension(originalFilePath)
						                              // We need to insert stuff between meshes and the rest of the path.
						                             .Replace("meshes/",  "")
						                             .Replace("meshes\\", "");

						var outputRomPath =
							$"meshes/generated_object_geom/tiles/{tileItem.tile.tileName}/{relativePath}__{meshInfo.id}.mesh";
						// physMeshInfo.id already contains _phys, so don't include it *again*.

						meshInfo.file_name = outputRomPath;
					}

					if (dataItem.physData != null)
					{
						// We generated a new .phys for this item.
						// We need to generate a fileName for it.
						// The mesh data will be saved in a later stage.

						// todo: ideally we could override the save path.
						var originalFilePath = originalMeshFilePath;

						var relativePath = PathHelper.GetFilePathWithoutExtension(originalFilePath)
						                              // We need to insert stuff between meshes and the rest of the path.
						                             .Replace("meshes/",  "")
						                             .Replace("meshes\\", "");

						var outputRomPath =
							$"meshes/generated_object_geom/tiles/{tileItem.tile.tileName}/{relativePath}__{physInfo.id}.phys";
						// physMeshInfo.id already contains _phys, so don't include it *again*.

						physInfo.file_name = outputRomPath;
					}

					Assert
						(
						 staticObjectIDRegex.IsMatch(meshInfo.id)
					   , $"Regex does not match: StaticObject.meshInfo.id '{meshInfo.id}'"
						);
					if (null != dataItem.physMeshInfo)
					{
						Assert
							(
							 staticObjectIDRegex.IsMatch(dataItem.physMeshInfo.id)
						   , $"Regex does not match: StaticObject.physInfo.id '{dataItem.physMeshInfo.id}'"
							);
					}

					// Write-Back the value into the container.
					dataItem.physMeshInfo = physInfo;

					// Make the id show up in the Editor.
					dataItem.myObject.id = meshInfo.id;
				}

				if (Settings.ExportTrainTrack)
				{
					tileItem.tile.AddMany(tileItem.curves.SelectMany(curve => curve.trackNodes));
				}
			}
			catch (Exception e)
			{
				throw new TileExportException(tileItem.tile.tileName, e);
			}
		}

		/// <summary>
		/// All 8 possible single tile offsets.
		/// </summary>
		private static IReadOnlyList<Vector3> PossibleOffsets = new List<Vector3>
			(
			 new[]
			 {
				 new Vector3(-1000, 0,  1000) // ^--
			   , new Vector3(    0, 0,  1000) // -^-
			   , new Vector3( 1000, 0,  1000) // --^
			   , new Vector3( 1000, 0,     0) // --X
			   , new Vector3( 1000, 0, -1000) // --.
			   , new Vector3(    0, 0, -1000) // -.-
			   , new Vector3(-1000, 0, -1000) // .--
			   , new Vector3(-1000, 0,     0) // x--
			 }
			);

		/// <summary>
		/// Fixes TrainTrackNode links that cross tile borders so that they are loaded properly by the game
		/// by cloning the node on the other tile to the current one.
		/// </summary>
		/// <param name="items"></param>
		/// <exception cref="NullReferenceException"></exception>
		private void TrainTrackNodeTileBorderFixup(IEnumerable<TileDataItem> items)
		{
			foreach (TrainTrackNode node in items.SelectMany(item => item.tile.trackNodes).OrderBy(t => t.id).ToArray())
			{
				node.LoadIgnore = true;

				if (!doTileBorderFixup)
				{
					continue;
				}

				var nodePosition = node.GPosition;

				foreach (var linkedNode in node.LinkedNodes.OrderBy(l => l.id).ToArray())
				{
					if (node.tile == null)
						throw new NullReferenceException($"{nameof(TrainTrackNode.tile)} should not be null");

					if (node.tile == linkedNode.tile) continue;


					// Find the position relative to node.tile, we just guess the 8 possibilities.

					Vector3 position = Vector3.Zero;
					float bestSquaredDistance = float.PositiveInfinity;

					foreach (var offset in PossibleOffsets)
					{
						var adjusted = linkedNode.GPosition + offset;
						float squaredDistance = (nodePosition - adjusted).LengthSquared;

						if (squaredDistance < bestSquaredDistance)
						{
							position = adjusted;

							bestSquaredDistance = squaredDistance;
						}
					}

					if (bestSquaredDistance >= float.PositiveInfinity)
					{
						Console.WriteLine($"{nameof(TileExporter)}.{nameof(TrainTrackNodeTileBorderFixup)}: Could not find offset to create a link for '{node.id}' in Tile '{node.tile.tileName}' to link '{linkedNode.tile.tileName}' in Tile '{linkedNode.tile.tileName}' for Tile border transition.");
						continue;
					}

					// The linked node is on another tile, so it cannot be linked to from the current one.
					// Thus delete the link.
					node.RemoveLink(linkedNode);

					// Clone the linkedNode (that is on anotther tile) and place it on the current one.
					var clone = new TrainTrackNode($"Cross-Tile-Proxy[{linkedNode.TileName}]:{linkedNode.id}", position);
					clone.LoadIgnore = true;
					clone.AddLink(node);
					node.tile.Add(clone);

					try
					{
						string nodeId = node.id;
						string tilename = node.tile.tileName;
						string linkedTileName = linkedNode.tile.tileName;
						Console.WriteLine($"{nameof(TileExporter)}.{nameof(TrainTrackNodeTileBorderFixup)}: Added '{node.id}' in Tile '{node.tile.tileName}' also to Tile '{linkedNode.tile.tileName}' for Tile border transition. {bestSquaredDistance}");
					}
					catch(Exception e)
					{
						Console.WriteLine(e.ToString());
					}
				}
			}

			foreach(var item in items)
			{
				var sorted = item.tile.trackNodes.ToList();
				sorted.Sort(TrackIDComparer.Instance);
				for (int i = 0; i < sorted.Count; i++)
				{
					var node = sorted[i];
					node.ElementOrder = 10_000 + i;
				}
			}
		}

		private void SanityCheck(TileDataItem data, ParallelLoopState state, long iteration)
		{
			try
			{
				foreach (var item in data.objects)
				{
					Assert(null != item.meshInfo, "null != item.meshInfo");
					Assert
						(
						 ! string.IsNullOrWhiteSpace(item.meshInfo.file_name)
					   , "! string.IsNullOrWhiteSpace(item.meshInfo.file_name)"
						);
					Assert
						(
						 staticObjectIDRegex.IsMatch(item.meshInfo.id)
					   , "staticObjectIDRegex.IsMatch(item.meshInfo.id)"
						);

					if (null != item.physMeshInfo)
					{
						Assert
							(
							 ! string.IsNullOrWhiteSpace(item.physMeshInfo.file_name)
						   , "!string.IsNullOrWhiteSpace(item.physMeshInfo.file_name)"
							);
						Assert
							(
							 staticObjectIDRegex.IsMatch(item.physMeshInfo.id)
						   , "staticObjectIDRegex.IsMatch(item.physMeshInfo.id)"
							);
					}
				}

				foreach (var item in data.curves)
				{
					if (Settings.ExportCurveMesh && item.spec != null && item.spec.WillGenerateMesh)
					{
						Assert(null != item.meshInfo, "null != item.meshInfo");
						Assert(null != item.meshData, "null != item.meshData");
						/*
						if (null != item.meshInfo)
						{

							Assert
								(
								 ! string.IsNullOrWhiteSpace(item.meshInfo.file_name)
							   , "!string.IsNullOrWhiteSpace(item.meshInfo.file_name)"
								);
							Assert
								(
								 trackExtrusionMeshIDRegex.IsMatch(item.meshInfo.id)
							   , "trackExtrusionMeshIDRegex.IsMatch(item.meshInfo.id)"
								);
						}
						*/
					}

					if (Settings.ExportCurvePhys && item.spec != null && item.spec.WillGeneratePhys)
					{
						Assert(null != item.physInfo,     "null != item.physInfo");
						Assert(null != item.physData, "null != item.physMeshData");
						/*
						if (null != item.physInfo)
						{
							Assert
								(
								 ! string.IsNullOrWhiteSpace(item.physInfo.file_name)
							   , "!string.IsNullOrWhiteSpace(item.physInfo.file_name)"
								);
							Assert
								(
								 trackExtrusionMeshIDRegex.IsMatch(item.physInfo.id)
							   , "trackExtrusionMeshIDRegex.IsMatch(item.physInfo.id)"
								);
						}
						*/
					}
				}
			}
			catch (Exception e)
			{
				RunOnMainThread.LogWarning("Warning: Assertions crashed. This could mean stuff broke badly.\n" + new TileExportException(data.tile.tileName, e).ToString());
			}
		}


		private void SaveXml(TileDataItem item, ParallelLoopState state, long iteration)
		{
			try
			{
				var data = item;
				if (! data.hasWork) return; // continue, but Parallel.

				// Tile is not completely implemented, so we need to load the XML and edit the XDocument's nodes directly.
				// However MeshInfo and TrainTracks are fully implemented, so we can convert them to XML elements.
				var tile = data.tile;
				var tileFilePath = Path.Combine(StormworksPaths.rom, tile.meta_romPath);
				var tileFileName = tile.fileName;

				Console.WriteLine
					($"{nameof(TileExporter)}.{nameof(WriteData)}() I{iteration,6}| Load xml:       {tileFileName,8}");

				var doc = XMLHelper.LoadFromFile(tileFilePath);

				///////////////////////////////////////////////////////////////////////////
				// Add new data:
				Console.WriteLine
					($"{nameof(TileExporter)}.{nameof(WriteData)}() I{iteration,6}| Add new data:   {tileFileName,8}");
				foreach (CurveDataItem curve in data.curves)
				{
					foreach(var meshInfo in curve.meshInfo)
					{
						meshInfo.LoadIgnore = true;
						tile.Add(meshInfo);
					}
					foreach (var physInfo in curve.physInfo)
					{
						physInfo.LoadIgnore = true;
						tile.Add(physInfo);
					}
				}

				foreach (var objectItem in data.objects)
				{
					objectItem.meshInfo.LoadIgnore = true;
					tile.Add(objectItem.meshInfo);

					if (null != objectItem.physMeshInfo)
					{
						objectItem.physMeshInfo.LoadIgnore = true;
						tile.Add(objectItem.physMeshInfo);
					}
				}


				///////////////////////////////////////////////////////////////
				// Write TileData to document.

				foreach (EditArea tileEditArea in tile.edit_areas)
				{
					if ((tileEditArea.LTransform.ExtractScale() - Vector3.One).LengthSquared > 0.001f)
					{
						// For breakpoint.
					}
				}

				tile.SaveDataToDocument(doc);


				// Save xml back to file
				Console.WriteLine
					($"{nameof(TileExporter)}.{nameof(WriteData)}() I{iteration,6}| Save xml:       {tileFileName,8}");

				XMLHelper.SaveToFile(doc, tileFilePath);


				// todo: notify the relevant TileBehaviour that it was saved.


				lock (statusLock)
				{
					Status = $"Saving Tile Xml {saveTileProgress++,5} / {tileWithWorkCount,5}";
				}

			}
			catch (Exception e)
			{
				throw new TileExportException(item.tile.tileName, e);
			}
		}

		private static void SaveTileMeshes(TileDataItem item, ParallelLoopState state, long iteration)
		{
			try
			{
				var tileDataItem = item;
				if (! tileDataItem.hasWork) return; // continue, but Parallel.

				for (int curveIndex = 0; curveIndex < tileDataItem.curves.Count; curveIndex++)
				{
					var curve = tileDataItem.curves[curveIndex];

					for(int itemIndex = 0; itemIndex < curve.meshInfo.Count; itemIndex++)
					{
						var info = curve.meshInfo[itemIndex];
						var mesh = curve.meshData[itemIndex];

						try
						{
							Console.WriteLine
								(
									$"{nameof(TileExporter)}.{nameof(WriteData)} I{iteration,6}| Curve Mesh: {info.file_name}"
								);

							var filePath = Path.Combine(StormworksPaths.rom, info.file_name);

							using (var stream = File.Open(filePath, FileMode.Create))
							{
								Binary.Save(stream, mesh);
							}
						}
						catch (Exception e)
						{
							throw new TileItemExportException(info.id, e);
						}
					}


					for (int itemIndex = 0; itemIndex < curve.physInfo.Count; itemIndex++)
					{
						var info = curve.physInfo[itemIndex];
						var phys = curve.physData[itemIndex];

						try
						{
							Console.WriteLine
								(
									$"{nameof(TileExporter)}.{nameof(WriteData)} I{iteration,6}| Curve Phys: {info.file_name}"
								);

							var filePath = Path.Combine(StormworksPaths.rom, info.file_name);

							using (var stream = File.Open(filePath, FileMode.Create))
							{
								Binary.Save(stream, phys);
							}
						}
						catch (Exception e)
						{
							throw new TileItemExportException(info.id, e);
						}
					}
				}

				foreach (var objectItem in tileDataItem.objects)
				{
					if (null != objectItem.meshData)
					{
						try
						{
							var romPath = objectItem.meshInfo.file_name;
							var fullPath = Path.Combine(StormworksPaths.rom, romPath);

							var folderPath = Path.GetDirectoryName(fullPath);
							Directory.CreateDirectory(folderPath); // ensures exists.

							Console.WriteLine
								(
								 $"{nameof(TileExporter)}.{nameof(WriteData)}() I{iteration,6}| Object Mesh: {romPath}"
								);

							using (var stream = File.Open(fullPath, FileMode.Create))
							{
								Binary.Save(stream, objectItem.meshData);
							}
						}
						catch (Exception e)
						{
							throw new TileItemExportException(objectItem.meshInfo.id, e);
						}
					}

					if (null != objectItem.physData)
					{
						try
						{
							var romPath = objectItem.physMeshInfo.file_name;

							var fullPath = Path.Combine(StormworksPaths.rom, romPath);

							var folderPath = Path.GetDirectoryName(fullPath);
							Directory.CreateDirectory(folderPath); // ensures exists.

							Console.WriteLine
								(
								 $"{nameof(TileExporter)}.{nameof(WriteData)}() I{iteration,6}| Object Phys: {romPath}"
								);

							using (var stream = File.Open(fullPath, FileMode.Create))
							{
								Binary.Save(stream, objectItem.physData);
							}
						}
						catch (Exception e)
						{
							throw new TileItemExportException(objectItem.physMeshInfo.id, e);
						}
					}
				}


				lock (statusLock)
				{
					Status = $"Saving Meshes for Tile {saveTileProgress++,5} / {tileWithWorkCount,5}";
				}
			}
			catch (Exception e)
			{
				throw new TileExportException(item.tile.tileName, e);
			}
		}


		#endregion WriteData

		#endregion Export Process






		/// <summary>
		/// Global data to be exported.
		/// </summary>
		private class WorkData
		{
			/// <summary>
			/// All curves in the world to export.
			/// </summary>
			public readonly List<CurveDataItem> curves = new List<CurveDataItem>();

			/// <summary>
			/// All objects in the world to export.
			/// </summary>
			public readonly List<ObjectDataItem> objects = new List<ObjectDataItem>();

			/// <summary>
			/// All tiles to be saved.
			/// </summary>
			public readonly List<Tile> tiles = new List<Tile>();
		}


		private class TileDataItem
		{
			/// <summary>
			/// The <see cref="Tile"/> as it was loaded.
			/// </summary>
			public Tile tile;

			/// <summary>
			/// The vector to transform world space coordinates to Tile coordinates.
			/// </summary>
			public Vector3 worldToTileSpace;


			public bool tileDirty = false;

			public bool hasWork => curves.Any() || objects.Any() || tileDirty;

			public readonly List<CurveDataItem> curves = new List<CurveDataItem>(0);
			public readonly List<ObjectDataItem> objects = new List<ObjectDataItem>(0);

			public IEnumerable<TrainTrackNode> GetTrackNodes()
			{
				foreach (var curve in curves)
				{
					foreach (var node in curve.trackNodes)
					{
						yield return node;
					}
				}
			}

			/// <inheritdoc />
			public override string ToString()
			{
				return $"TileDataItem: {tile?.fileName ?? "<<Unknown>>"}: {hasWork}";
			}
		}

		/// <summary>
		/// Contains the train track related information for a asingle curve.
		/// </summary>
		private class CurveDataItem
		{
			/// <summary>
			/// The connection that the data is based on.
			/// </summary>
			public Connection connection;
			public CubicBezierCurve curve => connection.curve;

			/// <summary>
			/// CurveSpec
			/// </summary>
			public CurveSpec spec;

			public List<MeshInfo> meshInfo = new List<MeshInfo>();
			public List<PhysInfo> physInfo = new List<PhysInfo>();

			public CurveDecorator.ResultData decoratorData;

			public List<DMesh> meshData = new List<DMesh>();
			public List<Phys> physData = new List<Phys>();

			public Vector3 transformation;

			public readonly List<TrainTrackNode> trackNodes = new List<TrainTrackNode>();

			public override string ToString()
			{
				var sb = new StringBuilder();
				sb.Append($"{{{nameof(CurveDataItem)}:");
				if(null != connection && CurveGraphConnectionManager.ConnectionMap.TryGetValue(connection, out ConnectionBehaviour cb))
				{
					sb.Append($" connection.id: {cb.UniqueId}");
				}

				if(null != spec)
				{
					sb.Append($" curveSpec: {spec.name ?? "null"}");
				}


				return sb.ToString();
			}
		}

		private class ObjectDataItem
		{
			public StaticObject myObject;

			public DataModel.Tiles.MeshInfo meshInfo;
			public DataModel.Tiles.PhysInfo physMeshInfo;

			public BinaryDataModel.DataTypes.Mesh meshData;
			public BinaryDataModel.DataTypes.Phys physData;
		}
	}
}
