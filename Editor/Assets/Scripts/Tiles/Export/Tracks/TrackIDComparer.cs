﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using DataModel.Tiles;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Tiles.Export
{
	internal class TrackIDComparer : IComparer<TrainTrackNode>
	{
		public static readonly Regex trackIDBorderRegex =
			new Regex("^Cross-Tile-Proxy\\[([^\\]]+)\\]:(.*)$");
		public static readonly Regex trackIDRegexV2 =
			new Regex("^(.*?)_(\\d+)_#(\\d+)$");

		public int Compare(TrainTrackNode x, TrainTrackNode y)
		{
			if (ReferenceEquals(x, y)) return 0;
			if (ReferenceEquals(null, y)) return 1;
			if (ReferenceEquals(null, x)) return -1;

			string idx = x.id;
			string idy = y.id;

			Match tbcmX = trackIDBorderRegex.Match(idx);
			Match tbcmY = trackIDBorderRegex.Match(idy);

			if (idx.StartsWith("Cross-Tile-Proxy") || idy.StartsWith("Cross-Tile-Proxy"))
			{

			}

			if (tbcmX.Success && !tbcmY.Success)
			{
				return 1;
			}
			else if (! tbcmX.Success && tbcmY.Success)
			{
				return -1;
			}
			else if (tbcmX.Success && tbcmY.Success)
			{
				idx = tbcmX.Groups[2].Value;
				idy = tbcmY.Groups[2].Value;
			}

			// [1]: string Name
			// [2]: int CurveID
			// [3]: int SegmentID
			Match mx = trackIDRegexV2.Match(idx);
			Match my = trackIDRegexV2.Match(idy);

			if (!mx.Success || !my.Success)
			{
				return string.Compare(idx, idy, StringComparison.Ordinal);
			}

			string xName = mx.Groups[1].Value;
			int xCurve = int.Parse(mx.Groups[2].Value);
			int xSegment = int.Parse(mx.Groups[3].Value);

			string yName = my.Groups[1].Value;
			int yCurve = int.Parse(my.Groups[2].Value);
			int ySegment = int.Parse(my.Groups[3].Value);

			int comparison = string.Compare(xName, yName, StringComparison.Ordinal);
			if (comparison != 0) return comparison;

			comparison = xCurve.CompareTo(yCurve);
			if (comparison != 0) return comparison;

			comparison = xSegment.CompareTo(ySegment);
			return comparison;
		}

		// ReSharper disable once MemberHidesStaticFromOuterClass
		internal static readonly TrackIDComparer Instance = new TrackIDComparer();
	}
}
