﻿// Copyright 2022-2025 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using DataModel.Tiles;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace Tiles.Export.Tracks
{
	public class TrackConnectionContext
	{
		public readonly float edgeNodeConnectRadius;
		public readonly float edgeNodeConnectRadiusSquared;

		public readonly bool doJunctionSmartClearance;
		public readonly float smartJunctionRadius;
		public readonly float smartJunctionRadiusSquared;

		public readonly bool doPreventUnexpectedJunctions;
		public readonly float unintendedJunctionRadius;
		public readonly float unintendedJunctionRadiusSquared;

		public readonly bool doTileBorderFixup;


		public CancellationToken cancellationToken;

		private int numStages = 6;
		private int prevStage = 0;

		public string Status { get; set; }

		public event Action<string> OnStatus;

		public Stopwatch totalTimer = new Stopwatch();
		public Stopwatch stageTimer = new Stopwatch();


		public readonly HashSet<TrainTrackNode> allNodes;
		public readonly HashSet<TrainTrackNode> junctions = new HashSet<TrainTrackNode>();
		public readonly HashSet<TrainTrackNode> removeBuffer = new HashSet<TrainTrackNode>();
		public readonly HashSet<TrainTrackNode> finalRemoveBuffer = new HashSet<TrainTrackNode>();

		public TrackConnectionContext(IEnumerable<TrainTrackNode> nodes, TileExportSettings settings, CancellationToken cancellationToken, bool startTimers = true)
		{
			if(startTimers)
			{
				StartTimers();
			}

			allNodes = new HashSet<TrainTrackNode>(nodes);

			edgeNodeConnectRadius        = settings.ConnectedNodeDetectionRadius;
			edgeNodeConnectRadiusSquared = edgeNodeConnectRadius * edgeNodeConnectRadius;

			doJunctionSmartClearance   = settings.EnableSmartJunctionHandling;
			smartJunctionRadius        = settings.SmartJunctionHandlingRadius;
			smartJunctionRadiusSquared = smartJunctionRadius * smartJunctionRadius;

			doPreventUnexpectedJunctions    = settings.EnableUnintendedJunctionprevention;
			unintendedJunctionRadius        = settings.UnintendedJunctionPreventionRadius;
			unintendedJunctionRadiusSquared = unintendedJunctionRadius * unintendedJunctionRadius;

			doTileBorderFixup = settings.EnableTrackTileBorderMitigation;

			this.cancellationToken = cancellationToken;
		}


		private static void WriteLine(string s)
		{
			Console.WriteLine(s);
		}

		public void StartTimers()
		{
			totalTimer.Start();
			stageTimer.Start();
		}

		public void UpdateStatus(int stage, string message = null)
		{
			if (stage != prevStage)
			{
				WriteLine($"Done with stage {prevStage} after {totalTimer.Elapsed}, stage took {stageTimer.Elapsed}.");

				stageTimer.Restart();
				prevStage = stage;
			}

			if (null != message) message = " " + message;
			Status = $"Connecting Train Track Nodes (Stage: {stage}/{numStages}){message}";

			OnStatus?.Invoke(Status);
		}


		public void RemoveNodePreserveConnection(TrainTrackNode toRemove)
		{
			removeBuffer.Add(toRemove);

			if (toRemove.TrackLinks.Count == 0) return;

			// Order by ID to ensure consistent results.
			// todo: ideally we prefer a Node based on some criteria that improves (more stable) the result.
			TrainTrackNode connectionReceiver = toRemove.LinkedNodes.OrderBy(t => t, TrackIDComparer.Instance).First();

			if (!connectionReceiver.RemoveButPreserveLinkPast(toRemove))
			{
				WriteLine($"Error: [{connectionReceiver.id}].RemoveButPreserveLinkPast({toRemove.id}) returned false.");
			}
		}

		public int ApplyRemoveBuffer()
		{
			int pendingCount = removeBuffer.Count;
			WriteLine($"RemoveBuffer: Removing {pendingCount} nodes.");
			foreach (var track in removeBuffer)
			{
				finalRemoveBuffer.Add(track);

				allNodes.Remove(track);
				junctions.Remove(track);
			}
			removeBuffer.Clear();
			return pendingCount;
		}

	}
}
