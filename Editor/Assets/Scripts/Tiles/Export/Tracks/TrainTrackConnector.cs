﻿// Copyright 2022-2025 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using CurveGraph;
using DataModel.Tiles;
using KdTree;
using KdTree.Math;
using Shared;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Tools;
using Debug = UnityEngine.Debug;
using Vector3 = OpenTK.Mathematics.Vector3;

namespace Tiles.Export.Tracks
{
	internal static class TrainTrackConnector
	{
		private static int defaultEdgeNodesImplementation = 1;
		private static int defaultUnexpectedJunctionsImplementation = 2;


		private static ConnectionUIDProvider ConnectionUIDProvider => ConnectionUIDProvider.Instance;

		private static void WriteLine(string s)
		{
			Console.WriteLine(s);
		}



		private static float RoundToIncrement(float input, float increment)
		{
			if (increment == 0)
			{
				return input;
			}
			else
			{
				return (float)Math.Round(input / increment) * increment;
			}
		}

		private static Vector3 RoundForLookup(Vector3 input, float increment)
		{
			float x = RoundToIncrement(input.X, increment);
			float y = RoundToIncrement(input.Y, increment);
			float z = RoundToIncrement(input.Z, increment);

			return new Vector3(x, y, z);
		}

		/// <summary>
		/// Connect tracks in globals space, so that we can catch all junctions and handle them properly.
		/// </summary>
		/// <param name="work"></param>
		public static void ConnectTracksGlobally(TrackConnectionContext context)
		{
			// todo: add customException with flag to indicate fatality.
			var exceptions = new List<Exception>(capacity: 0);

			try
			{
				context.StartTimers();
				context.UpdateStatus(1, null);
				WriteLine($"Connecting {context.allNodes.Count} Train Track Nodes");

				FindExplicitJunctions(context);
			}
			catch (Exception e)
			{
				exceptions.Add(e);
			}

			try
			{
				// Try to find nodes that should connect, typically this is transitions from double track to single track because they are not explicitly connected.
				context.UpdateStatus(2, null);
				switch (defaultEdgeNodesImplementation)
				{
					case 0: ConnectEdgeNodes_Lookup(context); break;
					case 1: ConnectEdgeNodes_Pairs(context); break;
					default: throw new ArgumentOutOfRangeException(nameof(defaultEdgeNodesImplementation), defaultEdgeNodesImplementation, "0, 1");
				}
			}
			catch (Exception e)
			{
				exceptions.Add(e);
			}

			try
			{
				context.UpdateStatus(3, null);
				FindCutOffJunctions(context);
			}
			catch (Exception e)
			{
				exceptions.Add(e);
			}

			if (context.doJunctionSmartClearance)
			{
				try
				{
					context.UpdateStatus(4, null);
					SmartJunctionForkClearance(context);
				}
				catch (Exception e)
				{
					exceptions.Add(e);
				}
			}

			if (context.doPreventUnexpectedJunctions)
			{
				try
				{
					context.UpdateStatus(5, null);
					switch (defaultUnexpectedJunctionsImplementation)
					{
						case 0: PreventUnexpectedJunctions_Simple(context); break;
						case 1: PreventUnexpectedJunctions_Pairs(context); break;
						case 2: PreventUnexpectedJunctions_KDTree(context); break;
						default: throw new ArgumentOutOfRangeException(nameof(defaultUnexpectedJunctionsImplementation), defaultUnexpectedJunctionsImplementation, "0, 1, 2");
					}
				}
				catch (Exception e)
				{
					exceptions.Add(e);
				}
			}

			context.UpdateStatus(6, null);
			WriteLine($"Done with all stages after {context.totalTimer.Elapsed}, stage 6 took {context.stageTimer.Elapsed}");

			if (exceptions.Count > 0)
			{
				throw new AggregateException("Exception(s) occurred in ConnectTracksGlobally.", exceptions.ToArray());
			}
		}

		/// <summary>
		/// Collects the nodes that have more than two links
		/// </summary>
		/// <param name="context"></param>
		public static void FindExplicitJunctions(TrackConnectionContext context)
		{
			// First put known junctions into the collection:
			WriteLine($"Detecting nodes that are already junctions.");
			foreach (TrainTrackNode trackNode in context.allNodes)
			{
				if (trackNode.TrackLinks.Count > 2)
				{
					context.junctions.Add(trackNode);
				}
			}
			WriteLine($"Found {context.junctions.Count} junctions.");
		}

		/// <summary>
		/// Finds nodes that have two links but the angle between those links is low, suggesting it is a junction with the non-forked end missing.
		/// </summary>
		/// <param name="context"></param>
		public static void FindCutOffJunctions(TrackConnectionContext context)
		{
			WriteLine("Collecting cut-off junctions...");
			int counter = 0;
			foreach (TrainTrackNode mainNode in context.allNodes)
			{
				if (context.junctions.Contains(mainNode)) continue;
				if (mainNode.TrackLinks.Count < 2) continue;

				Vector3 direction = Vector3.Zero;

				foreach (TrainTrackNode linkedNode in mainNode.LinkedNodes)
				{
					Vector3 myDirection = mainNode.LPosition - linkedNode.LPosition;
					myDirection.NormalizeFast();

					if (direction != Vector3.Zero)
					{
						double angle = Math.Acos(Vector3.Dot(direction, myDirection));
						if (angle < Math.PI / 2) // 90 degrees.
						{
							// The angle between directions is < 90 which means they point in the same direction (more or less)
							// This is a junction with a part cut off, add it to the list for processing later.
							context.junctions.Add(mainNode);
							counter++;
							break;
						}
					}

					direction = myDirection;
				}
			}

			WriteLine($"Found {counter} junctions.");
		}


		public static void ConnectEdgeNodes_Lookup(TrackConnectionContext context)
		{
			WriteLine($"Detecting EdgeNodes in the same location... (Implementation: Lookup)");
			int oldCount = context.junctions.Count;

			var edgeNodes = context.allNodes.Where(n => n.IsEdgeOfCurve);
			var posLookup = edgeNodes.ToLookup(t => RoundForLookup(t.LPosition, context.edgeNodeConnectRadiusSquared), t => t);


			WriteLine($"Found {posLookup.Count} groups of EdgeNodes in the same location.");
			int totalCount = 0;
			foreach (IGrouping<Vector3, TrainTrackNode> grouping in posLookup)
			{
				var first = grouping.First();
				int count = 0;
				foreach (TrainTrackNode toAbsorb in grouping.Skip(1))
				{
					if (first == toAbsorb) continue;
					WriteLine($"    EdgeNode '{first.id}' [{first.LinkIds.Count()}] absorbing '{toAbsorb.id}' at the same position: {first.GPosition} ~ {toAbsorb.GPosition}.");

					first.Absorb(toAbsorb);
					context.removeBuffer.Add(toAbsorb);
					count++;

					totalCount++;
				}

				if (count > 0)
				{
					if (first.TrackLinks.Count > 2)
					{
						context.junctions.Add(first);
						WriteLine($"    EdgeNode '{first.id}' [{first.LinkIds.Count()}] done absorbing, marked as a junction.");
					}
					else
					{
						WriteLine($"    EdgeNode '{first.id}' [{first.LinkIds.Count()}] done absorbing.");
					}
				}
			}
			WriteLine($"Processed {totalCount} nodes, Created {context.junctions.Count - oldCount} junctions from EdgeNodes. Now have {context.junctions.Count} potential junctions.");
		}

		public static void ConnectEdgeNodes_Pairs(TrackConnectionContext context)
		{
			WriteLine($"Detecting EdgeNodes that sould connect... (Implementation: Pairs)");
			int oldCount = context.junctions.Count;

			var edgeNodes = context.allNodes
				.Where(n => n.IsEdgeOfCurve)
				.OrderBy(n => n.id)
				.ToArray(); // 

			int pairCount = edgeNodes.Length * (edgeNodes.Length - 1) / 2;
			int progress = 0;
			int mergeCount = 0;
			int junctionCount = 0;

			var statusKey = new object();
			var mergeKey = new object();

			var opts = new ParallelOptions();
			//opts.CancellationToken = // todo
			opts.MaxDegreeOfParallelism = Globals.Constants.ForceSingleThreaded ? 1 : 1;//  Environment.ProcessorCount; // FORCED_SINGLE_THREAD

			// Use a non-buffering partitioner because the work size is relatively large and thus chunking is undesirerable.
			Parallel.ForEach(Partitioner.Create(edgeNodes.UniquePairs(), EnumerablePartitionerOptions.NoBuffering), opts, ProcessPair);
			void ProcessPair((TrainTrackNode, TrainTrackNode) data, ParallelLoopState state, long iteration)
			{
				if (iteration % 1000 == 0)
				{
					lock (statusKey)
					{
						context.UpdateStatus(2, message: $"{iteration}/{pairCount}");
					}
				}


				(TrainTrackNode first, TrainTrackNode toAbsorb) = data;
				if (context.removeBuffer.Contains(toAbsorb)) return;

				var delta = first.LPosition - toAbsorb.LPosition;
				float diff = delta.LengthSquared;

				if (diff < context.edgeNodeConnectRadiusSquared)
				{
					lock (mergeKey)
					{
						// WriteLine must be locked because access Links.Count
						WriteLine($"    EdgeNode '{first.id}' [{first.TrackLinks.Count}] absorbing '{toAbsorb.id}' at the same position: {first.GPosition} ~ {toAbsorb.GPosition}.");
						first.Absorb(toAbsorb);
						context.removeBuffer.Add(toAbsorb);
						mergeCount++;

						if (first.TrackLinks.Count > 2)
						{
							if (context.junctions.Add(first))
							{
								junctionCount++;
							}
						}
					}
				}
			}

			WriteLine($"Processed {edgeNodes.Count()} EdgeNodes, {pairCount} node pairs. Merged {mergeCount} nodes. Found {junctionCount} potential junctions.");
		}

		public static void SmartJunctionForkClearance(TrackConnectionContext context)
		{
			WriteLine($"Ensuring the diverging tracks of {context.junctions.Count} junctions have enough clearance with each other...");
			float smartJunctionRadius = context.smartJunctionRadius;
			float smartJunctionRadiusSquared = context.smartJunctionRadiusSquared;
			var seenIds = new HashSet<TrainTrackNode>();

			int progressCounter = 0;
			int progressDone = context.junctions.Count;
			foreach (TrainTrackNode junction in context.junctions.OrderBy(j => j.id))
			{
				context.UpdateStatus(4, message: $"{progressCounter}/{progressDone}");

				TrainTrackNode forkA;
				TrainTrackNode forkB;

				seenIds.Clear();
				seenIds.Add(junction);

				if (junction.TrackLinks.Count == 2)
				{
					var linkedNodes = junction.LinkedNodes.OrderBy(n => n.id).ToArray();

					forkA = linkedNodes[0];
					forkB = linkedNodes[1];

					var d_a = junction.GPosition - forkA.GPosition;
					var d_b = junction.GPosition - forkB.GPosition;

					d_a.NormalizeFast();
					d_b.NormalizeFast();

					double angle = Math.Acos(Vector3.Dot(d_a, d_b));
					if (angle > Math.PI / 2) // 90 degrees.
					{
						// Not a fork.
						WriteLine($"   [Warning] Skipping '{junction.id}' because this junction has 2 links and those two links opposite instead of diverging.");
						continue;
					}
				}
				else if (junction.TrackLinks.Count == 3)
				{
					var linkedNodes = junction.LinkedNodes.OrderBy(n => n.id).ToArray();

					// Discover the diverging side.
					var a = linkedNodes[0];
					var b = linkedNodes[1];
					var c = linkedNodes[2];

					var d_a = junction.GPosition - a.GPosition;
					var d_b = junction.GPosition - b.GPosition;
					var d_c = junction.GPosition - c.GPosition;

					d_a.NormalizeFast();
					d_b.NormalizeFast();
					d_c.NormalizeFast();

					var dot_ab = Vector3.Dot(d_a, d_b);
					var dot_bc = Vector3.Dot(d_b, d_c);
					var dot_ca = Vector3.Dot(d_c, d_a);

					if (dot_ab > 0)
					{
						forkA = a;
						forkB = b;
					}
					else if (dot_bc > 0)
					{
						forkA = b;
						forkB = c;
					}
					else if (dot_ca > 0)
					{
						forkA = a;
						forkB = c;
					}
					else
					{
						throw new InvalidOperationException("This should be mathematically impossible.");
					}
				}
				else
				{
					// todo: catch this case properly

					WriteLine($"    [Warning] Skipping '{junction.id}' because this junction is {junction.TrackLinks.Count} way which is not supported.");
					continue;
				}

				#region Local Methods
				float SquareDist(TrainTrackNode a, TrainTrackNode b)
				{
					return (a.GPosition - b.GPosition).LengthSquared;
				}

				bool TooClose(TrainTrackNode a, TrainTrackNode b)
				{
					float squareLen = SquareDist(a, b);
					// todo: when debugging complete remove Math.Sqrt and non-squared clearance radius.
					WriteLine(
							  $"        Checking {a.id,-10} @ {a.GPosition}"
						  + $"\n         against {b.id,-10} @ {b.GPosition}: ({Math.Sqrt(squareLen)} < {smartJunctionRadius}) = {squareLen < smartJunctionRadiusSquared}"
							 );

					return squareLen < smartJunctionRadiusSquared;
				}

				bool SearchTooCloseNodes(TrainTrackNode nodeA, TrainTrackNode startB, HashSet<TrainTrackNode> seen)
				{
					seen.Clear();
					seen.Add(junction);

					float closest = float.PositiveInfinity;
					bool hasRemoved = false;

					WriteLine($"        Checking {nodeA.id,-10} @ {nodeA.GPosition}:");

					foreach (var nodeB in startB.AllReachableTrackNodes(seen).Prepend(startB))
					{
						float dist = SquareDist(nodeA, nodeB);
						WriteLine($"         against {nodeB.id,-10} @ {nodeB.GPosition}: ({Math.Sqrt(dist)} < {smartJunctionRadius}) = {(dist < smartJunctionRadiusSquared ? "Remove" : "Keep")}");

						if (dist < smartJunctionRadiusSquared)
						{
							if (nodeB.TrackLinks.Count > 2)
							{
								WriteLine($"         << Not removed because it is a junction >>");
								continue;
							}

							// todo: if removing the node changes what side of the junction is left or right we must not remove it.
							// that will cause a lot of pain down the line.
							// Perhaps we can delete the other side instead?

							// Too close
							// todo: ideally we would remove whichever node makes the track look the best.
							hasRemoved |= context.removeBuffer.Add(nodeB);
							continue;
						}
						if (dist < closest)
						{
							// Getting closer, keep looking...
							closest = dist;
							continue;
						}
						// Not getting closer, stop looking and swap remove side.
						WriteLine($"            Not getting closer anymore, moving on...");
						return hasRemoved;
					}
					WriteLine($"            Reached end, moving on...");
					return hasRemoved;
				}

				bool MoveNext(IEnumerator<TrainTrackNode> enumerable)
				{
					WriteLine($"       Moving to next node for: {enumerable.Current?.id ?? "<first>"}");
					bool hasMore = false;
					while (true)
					{
						hasMore = enumerable.MoveNext();

						if (!hasMore) break;

						if (context.removeBuffer.Contains(enumerable.Current))
						{
							WriteLine($"       * Skipping to-be-removed {enumerable.Current.id}");
							// Skip removed.
						}
						else
						{
							break;
						}
					}

					return hasMore;
				}

				#endregion Local Methods

				WriteLine($"    Working on Junction '{junction.id}'.");

				var forkASeen = new HashSet<TrainTrackNode>();
				forkASeen.Add(junction);
				var forkBSeen = new HashSet<TrainTrackNode>();
				forkBSeen.Add(junction);
				var sharedSeen = new HashSet<TrainTrackNode>();

				var forkASearch = forkA.AllReachableTrackNodes(forkASeen).GetEnumerator();
				var forkBSearch = forkB.AllReachableTrackNodes(forkBSeen).GetEnumerator();

				// Initial case because the enumerators start at forkA/B.
				SearchTooCloseNodes(forkB, forkA, sharedSeen);


				int searchCount = 5;
				while (searchCount > 0)
				{
					bool firstDone = false;

					if (MoveNext(forkASearch))
					{
						if (!SearchTooCloseNodes(forkASearch.Current, forkB, sharedSeen)) searchCount--;
					}
					else
					{
						firstDone = true;
					}

					if (MoveNext(forkBSearch))
					{
						if (!SearchTooCloseNodes(forkBSearch.Current, forkA, sharedSeen)) searchCount--;
					}
					else if (firstDone) break;
				}
				forkASearch.Dispose();
				forkBSearch.Dispose();

				WriteLine($"    Done with {junction.id}");
			}

			foreach (TrainTrackNode node in context.removeBuffer)
			{
				context.RemoveNodePreserveConnection(node);
			}

			context.ApplyRemoveBuffer();
		}


		private static void PreventUnexpectedJunctions_Simple(TrackConnectionContext context)
		{
			// Second: tracks that cross each other or are close for some other reason.

			float unintendedJunctionRadius = context.unintendedJunctionRadius;
			float unintendedJunctionRadiusSquared = context.unintendedJunctionRadiusSquared;

			int progressCounter = 0;


			WriteLine("[PreventUnexpectedJunctions] Ensuring nodes have enough clearance to prevent unintended junctions from appearing in-game... (Implementation: Simple)");
			int progressTotalEstimate = context.allNodes.Count;
			int removedCounter = 0;

			var key = new object();

			var opts = new ParallelOptions();
			opts.CancellationToken = context.cancellationToken;
			opts.MaxDegreeOfParallelism = Globals.Constants.ForceSingleThreaded ? 1 : Environment.ProcessorCount;

			var allNodesSorted = context.allNodes.OrderBy(n => n.id).ToArray();

			// Use a non-buffering partitioner because the work size is relatively large and thus chunking is undesirerable.
			Parallel.ForEach(Partitioner.Create(allNodesSorted, EnumerablePartitionerOptions.NoBuffering), opts, ProcessNode);
			void ProcessNode(TrainTrackNode node)
			{
				lock (key)
				{
					context.UpdateStatus(5, message: $"{progressCounter++}/{progressTotalEstimate}");
				}
				foreach (TrainTrackNode node2 in allNodesSorted)
				{
					if (node == node2) continue;

					// todo: can we ensure a deterministic order somehow?

					var delta = node.LPosition - node2.LPosition;
					float diff = delta.LengthSquared;

					if (diff < unintendedJunctionRadiusSquared)
					{
						if (node2.TrackLinks.Count < 3)
						{

							bool didRemove = false;
							lock (key)
							{
								if (!context.removeBuffer.Contains(node))
								{
									context.RemoveNodePreserveConnection(node2);
									removedCounter++;
									didRemove = true;
								}
								else
								{
									// node1 will be removed already, so skip removing this node.
								}
							}

							if (didRemove)
							{
								WriteLine($"    Removing '{node2.id}' because to close to Node '{node.id}' ({Math.Sqrt(diff)} < {unintendedJunctionRadius}).");
							}
						}
						else
						{
							WriteLine($"    Not removing '{node2.id}' even though its too close to Node '{node.id}' because '{node2.id}' is a junction ({Math.Sqrt(diff)} < {unintendedJunctionRadius}).");
						}
					}
				}
			}

			context.ApplyRemoveBuffer();
			WriteLine($"[PreventUnexpectedJunctions] Removed {removedCounter} nodes that would become unwanted junctions.");
		}
		private static void PreventUnexpectedJunctions_Pairs(TrackConnectionContext context)
		{
			WriteLine("[PreventUnexpectedJunctions] Ensuring nodes have enough clearance to prevent unintended junctions from appearing in-game... (Implementation: Pairs)");
			float unintendedJunctionRadius = context.unintendedJunctionRadius;
			float unintendedJunctionRadiusSquered = context.unintendedJunctionRadiusSquared;
			int progressTotalEstimate = context.allNodes.Count * (context.allNodes.Count - 1) / 2;
			int progressCounter = 0;
			int removedCounter = 0;

			var statusKey = new object();
			var removeKey = new object();

			var opts = new ParallelOptions();
			opts.CancellationToken = context.cancellationToken;
			opts.MaxDegreeOfParallelism = Globals.Constants.ForceSingleThreaded ? 1 : Environment.ProcessorCount;

			// Use a non-buffering partitioner because the work size is relatively large and thus chunking is undesirerable.
			Parallel.ForEach(Partitioner.Create(context.allNodes.UniquePairs(), EnumerablePartitionerOptions.NoBuffering), opts, ProcessNode);

			void ProcessNode((TrainTrackNode, TrainTrackNode) data, ParallelLoopState state, long iteration)
			{
				if (iteration % 1000 == 0)
				{
					lock (statusKey)
					{
						context.UpdateStatus(5, message: $"{progressCounter++}/{progressTotalEstimate}");
					}
				}

				(TrainTrackNode node, TrainTrackNode node2) = data;

				var delta = node.LPosition - node2.LPosition;
				float diff = delta.LengthSquared;

				if (diff < unintendedJunctionRadiusSquered)
				{
					if (node2.TrackLinks.Count < 3)
					{
						bool didRemove = false;
						lock (removeKey)
						{
							if (!context.removeBuffer.Contains(node2))
							{
								context.RemoveNodePreserveConnection(node2);
								removedCounter++;
								didRemove = true;
							}
							else
							{
								// node marked for removal already by another thread.
							}
						}

						if (didRemove)
						{
							WriteLine($"    Removing '{node2.id}' because to close to Node '{node.id}' ({Math.Sqrt(diff)} < {unintendedJunctionRadius}).");
						}
					}
					else
					{
						WriteLine($"    Not removing '{node2.id}' even though its too close to Node '{node.id}' because '{node2.id}' is a junction ({Math.Sqrt(diff)} < {unintendedJunctionRadius}).");
					}
				}
			}

			context.ApplyRemoveBuffer();
			WriteLine($"[PreventUnexpectedJunctions] Removed {removedCounter} nodes that would become unwanted junctions.");
		}

		private static void PreventUnexpectedJunctions_KDTree(TrackConnectionContext context)
		{
			WriteLine("[PreventUnexpectedJunctions] Ensuring nodes have enough clearance to prevent unintended junctions from appearing in-game... (Implementation: KD-Tree)");

			float unintendedJunctionRadius = context.unintendedJunctionRadius;
			float unintendedJunctionRadiusSquered = context.unintendedJunctionRadiusSquared;
			int progressTotalEstimate = context.allNodes.Count;
			int progressCounter = 0;
			int removedCounter = 0;

			var statusKey = new object();
			var removeKey = new object();

			var opts = new ParallelOptions();
			opts.CancellationToken = context.cancellationToken;
			opts.MaxDegreeOfParallelism = Globals.Constants.ForceSingleThreaded ? 1 : 1;//Environment.ProcessorCount; // todo: FORCED_SINGLE_THREAD


			var tree = new KdTree<float, TrainTrackNode>(3, new FloatMath());

			float[] VecToFloat(Vector3 v)
			{
				return new[] { v.X, v.Y, v.Z };
			}
			foreach (var node in context.allNodes)
			{
				tree.Add(VecToFloat(node.GPosition), node);
			}

			// Use a non-buffering partitioner because the work size is relatively large and thus chunking is undesirerable.
			Parallel.ForEach(Partitioner.Create(context.allNodes, EnumerablePartitionerOptions.NoBuffering), opts, ProcessNode);

			void ProcessNode(TrainTrackNode node, ParallelLoopState state, long iteration)
			{
				if (iteration % 100 == 0)
				{
					lock (statusKey)
					{
						context.UpdateStatus(5, message: $"{iteration}/{progressTotalEstimate}");
					}
				}

				int num = 2; // Skip the first result that will be only the Node itself.
				while (true)
				{
					var treeNode = tree.GetNearestNeighbours(VecToFloat(node.GPosition), num++).LastOrDefault();

					if (treeNode == null) break;

					var node2 = treeNode.Value;

					var delta = node.GPosition - node2.GPosition;
					float diff = delta.LengthSquared;

					if (diff < unintendedJunctionRadiusSquered)
					{
						if (node2.TrackLinks.Count < 3)
						{
							bool didRemove = false;
							lock (removeKey)
							{
								if (!context.removeBuffer.Contains(node))
								{
									context.RemoveNodePreserveConnection(node2);
									removedCounter++;
									didRemove = true;
								}
								else
								{
									// node marked for removal already by another thread.
								}
							}

							if (didRemove)
							{
								WriteLine($"    Removing '{node2.id}' because to close to Node '{node.id}' ({Math.Sqrt(diff)} < {unintendedJunctionRadius}).");
							}
							else
							{
								WriteLine($"    not Removing '{node2.id}' because to close to Node '{node.id}' is already marked for removal ({Math.Sqrt(diff)} < {unintendedJunctionRadius}).");
							}
						}
						else
						{
							WriteLine($"    Not removing '{node2.id}' even though its too close to Node '{node.id}' because '{node2.id}' is a junction ({Math.Sqrt(diff)} < {unintendedJunctionRadius}).");
						}
					}
					else
					{
						// Nodes are returned in order of distance, so no closer node will be returned.
						break;
					}
				}
			}

			context.ApplyRemoveBuffer();
			WriteLine($"[PreventUnexpectedJunctions] Removed {removedCounter} nodes that would become unwanted junctions.");
		}
	}
}
