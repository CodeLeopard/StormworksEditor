﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using Behaviours;

using Control;

using GUI;

using RuntimeGizmos;

using StaticObjects;

using UnityEngine;

namespace Tiles
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(Camera))]
	[RequireComponent(typeof(SelectionManager))]
	public class TreeInteraction : M0noBehaviour
	{
		private SelectionManager selectionManager;
		private ObjectInteraction objectInteraction;

		private TileLoader tileLoader;

		private bool _treeGunMode = false;
		public bool TreeGunMode
		{
			get => _treeGunMode;
			set => InternalSetTreeGunMode(value);
		}

		protected override void OnAwake()
		{
			selectionManager = GetRequiredComponent<SelectionManager>();
			objectInteraction = GetRequiredComponent<ObjectInteraction>();

			tileLoader = TileLoader.instance;
		}


		/// <inheritdoc />
		protected override void OnEnableAndAfterStart()
		{
			selectionManager.AddDecider(GizmoSelectionAllowed);
		}

		private bool GizmoSelectionAllowed(Transform t)
		{
			// todo: Danger! may break other stuff's expectations!
			var behaviour = t.gameObject.GetComponent<TransformGizmoEventReceiver>();

			var allowed = behaviour != null;
			return allowed;
		}

		public void SetTreeGunMode(bool newValue)
		{
			TreeGunMode = newValue;
		}

		private void InternalSetTreeGunMode(bool newValue)
		{
			_treeGunMode = newValue;

			if (_treeGunMode)
			{
				MouseEvents.OnWorldMouseClick += MouseEvents_OnWorldMouseClick;
			}
			else
			{
				MouseEvents.OnWorldMouseClick -= MouseEvents_OnWorldMouseClick;
			}
		}


		private void MouseEvents_OnWorldMouseClick()
		{
			if (! MouseEvents.MouseLeft) return;
			if (! objectInteraction.TerrainRayCast(out Ray ray, out RaycastHit hit))
				return;

			var nearestTile = tileLoader.GetNearestTile(hit.point);
			if (null == nearestTile)
				return;

			var treeManager = nearestTile.tileRoot.GetComponentInChildren<TileTreeManager>();

			treeManager.CreateTreeAtWorldPos(hit.point);
		}


	}
}
