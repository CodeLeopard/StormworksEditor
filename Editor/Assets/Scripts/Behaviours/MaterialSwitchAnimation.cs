﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System.Diagnostics;

using UnityEngine;

namespace Behaviours
{
	[RequireComponent(typeof(Renderer))]
	public class MaterialSwitchAnimation : M0noBehaviour
	{
		public Material DefaultMaterial;
		public Material OtherMaterial;

		[Range(0.0001f, 10f)]
		public float switchSpeed;

		public float state;

		public bool TargetStateIsOther;

		[SerializeField]
		[HideInInspector]
		private new Renderer renderer;


		protected override void OnEnableAndAfterStart()
		{
			renderer = GetComponent<Renderer>();
		}

		[DebuggerStepThrough]
		void Update()
		{
			float target = 0;
			if (TargetStateIsOther) target = 1;

			state = state * (1 - switchSpeed * Time.deltaTime) + target * switchSpeed *Time.deltaTime;

			renderer.material.Lerp(DefaultMaterial, OtherMaterial, state);
		}
	}
}
