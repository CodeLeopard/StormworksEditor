﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System.Collections;

using UnityEngine;

namespace Behaviours
{
	/// <summary>
	/// Automatic On/Off LOD handler for the attached <see cref="GameObject"/>.
	/// </summary>
	public class OnOffLOD : M0noBehaviour
	{
		private CullingGroup cullingGroup;
		private readonly BoundingSphere[] boundingSpheres = new BoundingSphere[1] { new BoundingSphere(new Vector4()) };
		private readonly float[] distanceBands = new float[1] {float.PositiveInfinity};

		/// <summary>
		/// The radius of the boundingSphere.
		/// </summary>
		public float Radius
		{
			get => boundingSpheres[0].radius;
			set => boundingSpheres[0].radius = value;
		}

		/// <summary>
		/// The visibility Threshold distance.
		/// Set to <see cref="float.PositiveInfinity"/> to disable.
		/// </summary>
		public float Threshold
		{
			get => distanceBands[0];
			set
			{
				distanceBands[0] = value;
				cullingGroup.SetBoundingDistances(distanceBands);
			}
		}

		#region Unitymessages

		/// <inheritdoc />
		protected override void OnAwake()
		{
			cullingGroup = new CullingGroup();
			cullingGroup.targetCamera = Camera.main;
			cullingGroup.SetDistanceReferencePoint(Camera.main.transform);

			boundingSpheres[0].position =  transform.position;
			cullingGroup.SetBoundingSpheres(boundingSpheres);
			cullingGroup.SetBoundingSphereCount(1);
			// The cullingGroup will use the array by reference
			// so we can change the content without calling these methods again.
			// This only applies to the boundingSpheres though, not the distanceBounds.

			cullingGroup.onStateChanged += OnCullingStateChanged;
		}

		/// <inheritdoc />
		protected override void OnEnableAndAfterStart()
		{
			StartCoroutine(CheckCullingStateNextFrame());
		}

		private void Update()
		{
			// todo: this is performance intensive, find some event driven way to do this.
			boundingSpheres[0].position = transform.position;
		}

		private void OnDestroy()
		{
			cullingGroup.Dispose();
			cullingGroup = null;
		}

		#endregion Unitymessages

		private void OnCullingStateChanged(CullingGroupEvent sphere)
		{
			if (sphere.hasBecomeInvisible)
			{
				gameObject.SetActive(false);
				return;
			}

			if (sphere.hasBecomeVisible /* Should be included already && sphere.currentDistance == 0 */)
			{
				gameObject.SetActive(true);
			}
		}

		private IEnumerator CheckCullingStateNextFrame()
		{
			yield return null; // Wait for next frame.

			if (! cullingGroup.IsVisible(0))
			{
				gameObject.SetActive(false);
			}
		}
	}
}
