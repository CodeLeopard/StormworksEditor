﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using UnityEngine;
using Object = UnityEngine.Object;

using DataTypes.Attributes;
using DataTypes.Extensions;

using Debug = UnityEngine.Debug;

namespace Behaviours
{
	/// <summary>
	/// Like the standard <see cref="MonoBehaviour"/>, but with more convenient event order.
	/// </summary>
	/// <remarks>
	/// By default OnEnable runs before Start, which renders Start pointless and the implementation of OnEnable inconvenient
	///		because you need to beware of uninitialized state in OnEnable.
	///
	/// For reference: the standard order is Reset(), Awake(), OnEnable(), [At this point AddComponent(), if it was used, returns] Start()
	/// The new order is: Reset(), Awake() [At this point AddComponent(), if it was used, returns] OnStart(), OnEnableAndAfterStart()
	/// Note that Reset is only called in the UnityEditor, and contrary to the diagram here: https://docs.unity3d.com/Manual/ExecutionOrder.html
	///		it is actually called before Awake() and OnEnable(): it is called immediately upon adding the component, and Awake() isn't called until PlayMode starts.
	/// 
	/// </remarks>
	public abstract class M0noBehaviour : MonoBehaviour
	{
		// @Leopard: original source lives in StorworksEditor repo.


		/// <summary>
		/// <see cref="M0noBehaviour.Start"/> has run.
		/// </summary>
		[field: SerializeField] // We need to serialize this because after Hot-Reload unity will not call Start again.
		[field: InspectorReadonly] // Inspector shouldn't be able to edit it.
		[field: Tooltip("Only mess with it in case it's yelling at you in the console! Should manage itself automatically. Uncomment the InspectorReadonly attribute to enable editing.")]
		public bool Started
		{
			[DebuggerStepThrough]
			get;
			[DebuggerStepThrough]
			private set;
		}


		#region UnityMessages

		private void Awake()
		{
			if (Started)
			{
				UnityEngine.Debug.LogWarning(
					  $"{GetType().FullName} : {typeof(M0noBehaviour)} Awoke with {nameof(Started)} == true."
					+ $"This shouldn't happen, but may be caused when state is saved when it shouldn't,"
					+ $"Or when the GameObject is set to run in Edit mode,"
					+ $"Or during Hot-Reload,", this);

				Started = false;
			}

			OnAwake();
		}

		/// <summary>
		/// Called once after the creation of the <see cref="M0noBehaviour"/> before any of the other methods.
		/// Replaces <see cref="Awake"/>.
		/// </summary>
		protected virtual void OnAwake()
		{
			// Virtual
		}



		private void Start()
		{
			Started = true;
			OnStart();
			OnEnableAndAfterStart();
		}

		/// <summary>
		/// Called once, just before the first call to Update. Replaces <see cref="Start"/>.
		/// </summary>
		[DebuggerStepThrough]
		protected virtual void OnStart()
		{
			// Virtual
		}

		private void OnEnable()
		{
			OnEnableAlways();

			if (Started)
				OnEnableAndAfterStart();
		}

		/// <summary>
		/// Replaces <see cref="OnEnable"/>. Runs before <see cref="OnEnableAndAfterStart"/>
		/// </summary>
		protected virtual void OnEnableAlways()
		{
			// Virtual
		}

		/// <summary>
		/// OnEnable, but only if started
		/// (<see cref="Started"/> is <see langword="true"/> and <see cref="Start"/> was called).
		/// Runs immediately after <see cref="Start"/> is called.
		/// Replaces <see cref="OnEnable"/>.
		/// </summary>
		[DebuggerStepThrough]
		protected virtual void OnEnableAndAfterStart()
		{
			// Virtual
		}




		private void OnDisable()
		{
			if (Started)
				OnDisableIfStarted();

			OnDisableAlways();
		}

		/// <summary>
		/// Replaces <see cref="OnDisable"/>. Runs after <see cref="OnDisableIfStarted"/>.
		/// </summary>
		protected virtual void OnDisableAlways()
		{
			// Virtual
		}

		/// <summary>
		/// <see cref="OnDisable"/>, but only if <see cref="Start"/> has run.
		/// </summary>
		[DebuggerStepThrough]
		protected virtual void OnDisableIfStarted()
		{
			// Virtual
		}


		#endregion UnityMessages


		#region Helpers



		/// <inheritdoc cref="ComponentExt.GetRequiredComponent&lt;TComponent&gt;(Component)"/>
		protected TComponent GetRequiredComponent<TComponent>() where TComponent : Component
		{
			return ComponentExt.GetRequiredComponent<TComponent>(this);
		}

		/// <inheritdoc cref="ComponentExt.GetComponentInParents&lt;TComponent&gt;(Component, bool)"/>
		protected TComponent GetComponentInParents<TComponent>(bool includeInactive = true) where TComponent : Component
		{
			return ComponentExt.GetComponentInParents<TComponent>(this, includeInactive);
		}

		/// <inheritdoc cref="ComponentExt.GetRequiredComponentInParents&lt;TComponent&gt;(Component, bool)"/>
		protected TComponent GetRequiredComponentInParents<TComponent>(bool includeInactive = true) where TComponent : Component
		{
			return ComponentExt.GetRequiredComponentInParents<TComponent>(this, includeInactive);
		}


		/// <summary>
		/// Helper that automatically picks Destroy, or DestroyImmediate depending on context.
		/// </summary>
		/// <param name="obj">The object to Destroy</param>
		protected void DestroyOrImmediate(Object obj)
		{
#if UNITY_EDITOR
			if (Application.isPlaying) Destroy(obj);
			else DestroyImmediate(obj);
#else //UNITY_EDITOR
			Destroy(obj);
#endif //UNITY_EDITOR
		}


		/// <summary>
		/// Finds at most <paramref name="maxResults"/> <see cref="GameObject"/>s with tag <paramref name="tag"/>
		/// within <paramref name="radius"/> from <paramref name="position"/>, in ascending order of distance.
		/// </summary>
		/// <param name="position"></param>
		/// <param name="tag"></param>
		/// <param name="radius"></param>
		/// <param name="maxResults"></param>
		/// <returns></returns>
		public static IEnumerable<GameObject> FindNearbyWithTag
			(Vector3 position, string tag, float radius = float.MaxValue, int maxResults = int.MaxValue)
		{
			var all = GameObject.FindGameObjectsWithTag(tag);
			if (all.Length == 0) yield break;

			float sqrRadius = radius * radius;

			var sorted = all
				.Select(g => ((g.transform.position - position).sqrMagnitude, g))
				.OrderBy(t => t.sqrMagnitude);

			foreach (var t in sorted)
			{
				var g = t.g;
				var sqDist = t.sqrMagnitude;
				if (sqDist > sqrRadius) yield break;

				yield return g;
				maxResults--;

				if (maxResults <= 0) yield break;
			}
		}


		/// <summary>
		/// Set the activeState of <paramref name="go"/> in the next tick, using a <see cref="Coroutine"/>.
		/// </summary>
		/// <param name="go"></param>
		/// <param name="newState"></param>
		/// <returns>The <see cref="Coroutine"/> created</returns>
		public Coroutine SetActiveNextAfterFrame(GameObject go, bool newState, int frames = 1)
		{
			if (! (frames > 0)) throw new ArgumentOutOfRangeException(nameof(frames), frames, "> 0");

			return StartCoroutine(Co_SetActiveAfterFrame(go, newState, frames));
		}

		/// <summary>
		/// The <see cref="IEnumerator"/> used for <see cref="SetActiveNextAfterFrame"/>.
		/// <see langword="yield"/>s once and then set the activeState of <paramref name="go"/> to <paramref name="newState"/>.
		/// </summary>
		/// <param name="go"></param>
		/// <param name="newState"></param>
		/// <returns>nothing</returns>
		protected static IEnumerator Co_SetActiveAfterFrame(GameObject go, bool newState, int frames = 1)
		{
			for (int i = 0; i < frames; i++)
			{
				yield return null;
			}

			try
			{
				go.SetActive(newState);
			}
			catch (Exception e)
			{
				Debug.LogException(e, go);
			}
		}

		#endregion Helpers
	}
}
