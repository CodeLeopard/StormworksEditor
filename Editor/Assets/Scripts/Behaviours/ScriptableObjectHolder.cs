﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections.Generic;

using UnityEngine;

using DataTypes;

using Tools;

/// <summary>
/// Holds onto ScriptableObjects because for some reason the DB itself can't.
/// todo: this sucks, find better way.
/// </summary>
[DefaultExecutionOrder(ExecutionOrder.ScriptableObjectHolder)]
public class ScriptableObjectHolder : MonoBehaviour
{
	public List<ScriptableObject_> Objects;
	void Awake()
	{
		var dbObjects = ScriptableObjectDB.GetAll();

		foreach (var so in dbObjects)
		{
			if (! Objects.Contains(so))
			{
				Console.WriteLine($"[ScriptableObjectHolder] Adding [{so.GetType().Name}] {so.GUID} to list in Awake.");
				Objects.Add(so);
			}
		}

		foreach (var object_ in Objects)
		{
			object_.HolderAwake();
		}
		Objects.Sort(); // Ensure consistent serialization order (to prevent phantom changes in git)
	}

	void Update()
	{
		// We only need to exist in the scene at compile/build time to trick Unity into including the objects we hold in the build.
		// Once the scene is loaded we aren't needed anymore.
		//Destroy(gameObject);
	}


	public void GetFromDB()
	{
		Objects = ScriptableObjectDB.GetAll();
		Objects.Sort(); // Ensure consistent serialization order (to prevent phantom changes in git)
	}
}
