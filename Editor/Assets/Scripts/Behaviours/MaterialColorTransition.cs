﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System.Collections;

using UnityEngine;

namespace Behaviours
{
	[DisallowMultipleComponent]
	public class MaterialColorTransition : MonoBehaviour
	{
		[Range(0.001f, 2f)]
		public float lerpDurationSeconds = 0.5f;


		private new MeshRenderer renderer;
		private Material material => renderer.material;


		private Color currentColor => material.color;
		private Color targetColor;


		private Coroutine lerpRoutine;

		private void Reset()
		{
			Awake();
		}

		private void Awake()
		{
			renderer = GetComponent<MeshRenderer>();
		}


		public void SetImmediate(Color color)
		{
			if (null != lerpRoutine) StopCoroutine(lerpRoutine);
			material.color = color;
			lerpRoutine = null; // Free the object so it does not stay behind.
		}

		public void SetTarget(Color color)
		{
			if (color == currentColor) return;
			if (color == targetColor) return;

			if (null != lerpRoutine) StopCoroutine(lerpRoutine);

			targetColor = color;
			lerpRoutine = StartCoroutine(LerpToTargetColor());
		}

		private IEnumerator LerpToTargetColor()
		{
			Color startColor = currentColor;

			float lerpProgress = 0;

			while (lerpProgress < 1)
			{
				material.color = Color.Lerp(startColor, targetColor, lerpProgress);

				lerpProgress += Time.deltaTime / lerpDurationSeconds;
				yield return null;
			}

			material.color = targetColor;
			lerpRoutine = null; // Free the object so it does not stay behind.
		}
	}
}
