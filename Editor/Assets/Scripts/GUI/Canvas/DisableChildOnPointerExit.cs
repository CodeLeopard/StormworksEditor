// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System.Collections;

using UnityEngine;
using UnityEngine.EventSystems;

namespace GUI.Canvas
{
	/// <summary>
	/// Disable the GameObject on the PointerExit event, keeping in mind that Unity is broken
	/// and may send PointerExit immediately followed by PointerEnter for various reasons
	/// in which case the GameObject should stay Enabled.
	/// </summary>
	// Reasons include: moving the pointer to a child element.
	// Fun fact: if this event wasn't broken you could use a EventTriggerComponent and hook that to the GameObject's
	// SetActive(false) directly.
	public class DisableChildOnPointerExit : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
	{
		public DisableOnPointerExit ToDisable;

		private bool actuallyDisable = false;

		/// <inheritdoc />
		public void OnPointerEnter(PointerEventData eventData)
		{
			actuallyDisable = false;
		}

		/// <inheritdoc />
		public void OnPointerExit(PointerEventData eventData)
		{
			actuallyDisable = true;
			StartCoroutine(MaybeDisableNextTick());
		}

		private IEnumerator MaybeDisableNextTick()
		{
			yield return null; // Wait for next frame.
			if (actuallyDisable) StartCoroutine(ToDisable.MaybeDisableNextTick());
		}
	}
}
