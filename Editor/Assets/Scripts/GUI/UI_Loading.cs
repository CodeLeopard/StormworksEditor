﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System.Linq;
using System.Text;

using Tiles;

using Tools;

using UnityEngine;
using UnityEngine.UI;

namespace GUI {
	public class UI_Loading : MonoBehaviour
	{
		public bool ShowFrameTime = false;

		private RectTransform rectTransform;
		private Text myText;

		private TileLoader loader;

		private readonly StringBuilder sb = new StringBuilder();

		// Start is called before the first frame update
		void Start()
		{
			rectTransform = GetComponent<RectTransform>();
			myText = GetComponent<Text>();
			loader = TileLoader.instance;
		}


		// Update is called once per frame
		void Update()
		{
			if (RunOnMainThread.IsInstantiated)
			{
				sb.AppendLine($"MainThreadDispatcher queue: {RunOnMainThread.instance.queue.Count}");
			}
			else
			{
				sb.AppendLine("MainThreadDispatcher queue: not ready.");
			}

			if (ShowFrameTime)
			{
				sb.AppendLine($"FrameTime {Time.deltaTime * 1000}ms");
			}
			sb.AppendLine($"Tile Loader: {CheckNotTooLong(TileLoader.Status)}");
			{
				sb.Append($"Tile Exporter: {CheckNotTooLong(TileExporter.Status)}");
				sb.Append(", Tree Exporter: ");
				sb.Append(TreeExporter.instance == null ? "null" : CheckNotTooLong(TreeExporter.instance.status));
			}

			myText.text = sb.ToString();
			sb.Clear();
		}

		private const int maxLen = 300;
		private const int maxNewLines = 3;
		private const string tooLongText = "...";

		private string CheckNotTooLong(string input)
		{
			if (input == null) return null;
			if (input.Length > maxLen
			 || input.Count(c => c.Equals('\n')) > maxNewLines)
			{
				return input[..(300 - tooLongText.Length)] + tooLongText;
			}

			return input;
		}
	}
}
