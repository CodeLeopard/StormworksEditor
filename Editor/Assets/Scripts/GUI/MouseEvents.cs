﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;

using RuntimeGizmos;

using Tools;

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GUI
{
	/// <summary>
	/// Generates events mouse clicks, but ignores events swallowed by UI elements.
	/// </summary>
	/// <remarks>
	/// Note: Takes Exclusive control over the <see cref="EventTrigger"/> and <see cref="Image"/> components.
	/// </remarks>
	[DisallowMultipleComponent]
	[RequireComponent(typeof(EventTrigger))]
	[RequireComponent(typeof(Image))]
	[DefaultExecutionOrder(ExecutionOrder.MouseEvents)]
	public class MouseEvents : MonoBehaviour
	{
		/// <summary>
		/// Fires when user presses the mouse and no UI element was hit.
		/// </summary>
		public static event Action OnWorldMouseDown;

		/// <summary>
		/// Fires when user releases the mouse and no UI element was hit.
		/// </summary>
		public static event Action OnWorldMouseUp;

		/// <summary>
		/// Fires when user clicks and no UI element was hit.
		/// </summary>
		public static event Action OnWorldMouseClick;


		/// <summary>
		/// True in the tick that the mouse was pressed down.
		/// </summary>
		public static bool MouseDown { get; private set; }

		/// <summary>
		/// True in the tick that the mouse was released.
		/// </summary>
		public static bool MouseUp { get; private set; }

		/// <summary>
		/// True in the tick after the mouse was released.
		/// </summary>
		public static bool MouseClicked { get; private set; }

		/// <summary>
		/// True while the left mouse button is held down.
		/// </summary>
		public static bool MouseLeft { get; private set; }

		/// <summary>
		/// True while the middle mouse button is held down.
		/// </summary>
		public static bool MouseMiddle { get; private set; }

		/// <summary>
		/// True while the right mouse button is held down.
		/// </summary>
		public static bool MouseRight { get; private set; }

		/// <summary>
		/// The mouse is currently over the world (not over any UI).
		/// </summary>
		public static bool MouseOverWorld { get; private set; }

		/// <summary>
		/// The mouse is currently over any UI object and therefore should not interact with the world.
		/// </summary>
		public static bool MouseOverUI => ! MouseOverWorld;

		[SerializeField]
		private TransformGizmo gizmo;


#if UNITY_EDITOR
		[SerializeField]
		private bool mouseOverUI;
#endif

		[SerializeField]
		[HideInInspector]
		private static MouseEvents _instance;

		[SerializeField]
		[HideInInspector]
		private EventTrigger eventTrigger;

		void Awake()
		{
			if (null == _instance)
			{
				_instance = this;
			}
			else
			{
				Debug.LogError($"Singleton found multiple instances!", this);
			}

			if (null == transform.parent.GetComponent<UnityEngine.Canvas>())
			{
				Debug.LogError($"{nameof(MouseEvents)} should be a child of the main Canvas!");
			}

			transform.SetAsFirstSibling(); // Ensure we are **last** wrt raycasts

			SetupImage();

			SetupEvents();
		}

		private void SetupImage()
		{
			var image = GetComponent<Image>();
			image.sprite = null;
			image.color = new Color(0, 0, 0, 0);
			image.material = null;
			image.raycastTarget = true;
			image.raycastPadding = Vector4.zero;
			image.maskable = true;
		}

		private void SetupEvents()
		{
			eventTrigger = GetComponent<EventTrigger>();
			eventTrigger.triggers.Clear();
			{
				var entry = new EventTrigger.Entry { eventID = EventTriggerType.PointerDown };
				eventTrigger.triggers.Add(entry);
				entry.callback.AddListener(OnTriggerDown);
			}

			// Please note: the click that (re)gains focus to the game will trigger these two *immediately* for some Unity reason.
			{
				var entry = new EventTrigger.Entry { eventID = EventTriggerType.PointerUp };
				eventTrigger.triggers.Add(entry);
				entry.callback.AddListener(OnTriggerUp);
			}
			{
				var entry = new EventTrigger.Entry { eventID = EventTriggerType.PointerClick };
				eventTrigger.triggers.Add(entry);
				entry.callback.AddListener(OnTriggerClick);
			}

			{
				var entry = new EventTrigger.Entry() { eventID = EventTriggerType.PointerEnter };
				eventTrigger.triggers.Add(entry);
				entry.callback.AddListener(OnEnter);
			}
			{
				var entry = new EventTrigger.Entry() { eventID = EventTriggerType.PointerExit };
				eventTrigger.triggers.Add(entry);
				entry.callback.AddListener(OnExit);
			}
		}

		private void SubScribeEvents()
		{
			eventTrigger = GetComponent<EventTrigger>();
			eventTrigger.triggers[0].callback.AddListener(OnTriggerDown);

			// todo: these two are broken.
			eventTrigger.triggers[1].callback.AddListener(OnTriggerUp);
			eventTrigger.triggers[2].callback.AddListener(OnTriggerClick);
		}

		#region TriggerHandlers

		private void OnTriggerDown(BaseEventData eventData)
		{
			if (null != gizmo && gizmo.mouseIsOverGizmo) return;

			MouseDown = true;

			MouseLeft = Input.GetMouseButton(0);
			MouseRight = Input.GetMouseButton(1);
			MouseMiddle = Input.GetMouseButton(2);

			OnWorldMouseDown?.Invoke();
		}

		private void OnTriggerUp(BaseEventData eventData)
		{
			if (null != gizmo && gizmo.mouseIsOverGizmo) return;

			MouseUp = true;

			OnWorldMouseUp?.Invoke();
		}

		private void OnTriggerClick(BaseEventData eventData)
		{
			if (null != gizmo && gizmo.mouseIsOverGizmo) return;

			MouseClicked = true;
			OnWorldMouseClick?.Invoke();
		}

		private void OnEnter(BaseEventData eventData)
		{
			MouseOverWorld = true;

#if UNITY_EDITOR
			mouseOverUI = MouseOverWorld;
#endif
		}

		private void OnExit(BaseEventData eventData)
		{
			MouseOverWorld = false;

#if UNITY_EDITOR
			mouseOverUI = MouseOverWorld;
#endif
		}

		#endregion TriggerHandlers


		void LateUpdate()
		{
			MouseDown = false;
			MouseUp = false;
			MouseClicked = false;

			MouseLeft = Input.GetMouseButton(0);
			MouseRight = Input.GetMouseButton(1);
			MouseMiddle = Input.GetMouseButton(2);
		}
	}
}
