﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Behaviours;

using DataTypes.Extensions;

using Tools;

using UnityEngine;
using UnityEngine.UI;

namespace GUI.ItemSelector
{
	internal class ButtonGrid : M0noBehaviour
	{
		private GridLayoutGroup layout;

		public GridLayoutGroup Layout => layout;

		#region UnityMessages
		/// <inheritdoc />
		protected override void OnAwake()
		{
			layout = GetRequiredComponent<GridLayoutGroup>();
		}

		private void OnDestroy()
		{
			if (null            == waitTask) return;
			if (waitTask.Status != TaskStatus.Created) return;
			waitTask.RunSynchronously();
		}

		#endregion UnityMessages


		#region WaitTask

		private bool result;
		private Task<bool> waitTask;

		public Task<bool> WaitForInteraction()
		{
			waitTask = waitTask ?? new Task<bool>(InteractionTask);
			return waitTask;
		}

		private bool InteractionTask()
		{
			return result;
		}

		#endregion WaitTask

		public void AddCancelButton(string label = "Cancel")
		{
			AddButton(label, () => { result = false; });
		}

		public void AddButton(string label, Action onClick)
		{
			var go = Instantiate(Globals.Prefabs.Button, transform);
			go.name = label;

			var button = go.GetRequiredComponent<ButtonHelper>();
			button.Label = label;
			button.Button.onClick.AddListener(Call);
			void Call()
			{
				try
				{
					result = true;
					onClick.Invoke();
				}
				catch (Exception e)
				{
					Debug.LogException(e, this);
				}
				finally
				{
					waitTask?.RunSynchronously();
					Destroy(gameObject);
				}
			}
		}

		public static ButtonGrid Create(UnityEngine.Canvas parent = null)
		{
			if (parent == null)
			{
				parent = GameObject.FindGameObjectWithTag("GUI_MainCanvas").GetRequiredComponent<UnityEngine.Canvas>();
			}
			var go = Instantiate(Globals.Prefabs.ButtonGrid, parent.transform);
			var panel = go.GetRequiredComponent<ButtonGrid>();
			return panel;
		}
	}
}
