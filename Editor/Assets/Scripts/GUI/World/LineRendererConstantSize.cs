﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;

using DataTypes.Attributes;

using UnityEngine;

namespace GUI.World
{
	[RequireComponent(typeof(LineRenderer))]
	public class LineRendererConstantSize : MonoBehaviour
	{
		[Tooltip("Will be assigned to Camera.Main if null")]
		public Camera myCamera;

		private LineRenderer myRenderer;

		public float minimumSize = 0.1f;
		public float maximumSize = 1000f;
		public float sizeDivisor = 32f;

		/// <summary>
		/// The current scale factor. Read only (value is only written to).
		/// </summary>
		[Tooltip("The current scale factor, read-only")]
		[InspectorReadonly]
		public float currentSize = Single.NaN;

		void Awake()
		{
			if (null == myCamera)
			{
				myCamera = Camera.main;
			}

			myRenderer = GetComponent<LineRenderer>();
			if (myRenderer == null)
			{
				throw new NullReferenceException
					($"{nameof(myRenderer)} should be guaranteed to exist by {nameof(RequireComponent)}");
			}
		}

		void LateUpdate()
		{
			var fromCamToThis = transform.position - myCamera.transform.position; // + Vector3.up * 2;

			currentSize = Mathf.Clamp(fromCamToThis.magnitude / sizeDivisor, minimumSize, maximumSize);

			myRenderer.widthMultiplier = currentSize;
		}

		void OnValidate()
		{
			minimumSize = Math.Max(minimumSize, float.Epsilon);
			maximumSize = Math.Max(maximumSize, minimumSize);
			sizeDivisor = Math.Max(sizeDivisor, float.Epsilon);
		}
	}
}
