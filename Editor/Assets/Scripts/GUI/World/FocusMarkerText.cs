﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;

using Behaviours;
using BinaryDataModel.Converters;
using DataTypes.Extensions;

using Tiles;

using TMPro;

using UnityEngine;

namespace GUI.World
{
	[DisallowMultipleComponent]
	public class FocusMarkerText : M0noBehaviour
	{
		[HideInInspector]
		public Transform cameraTr;

		public TextMeshPro textX;
		public TextMeshPro textZ;

		public TextMeshPro textX2;
		public TextMeshPro textZ2;

		public TextMeshPro textTileName;

		[NonSerialized]
		public int lastX;
		[NonSerialized]
		public int lastZ;

		private int lastTX;
		private int lastTZ;

		private TileLoader loader => TileLoader.instance;

		private Transform monitoredTransform => transform.parent;


		// Update is called once per frame
		void LateUpdate()
		{
			LateUpdateScale();

			// Our own transform moves up and down to keep the bottom of the capsule on the ground as it scales.
			// so we monitor the parent transform instead because that only moves when the user interacts with it.
			if (!monitoredTransform.hasChanged) return;
			monitoredTransform.hasChanged = false;

			var tilePosition = TileExporter.GetTilePos(monitoredTransform.position.ToOpenTK());

			int x = Mathf.RoundToInt(monitoredTransform.position.x);
			int z = Mathf.RoundToInt(monitoredTransform.position.z);

			int tx = (x % 1000) - 500;
			int tz = (z % 1000) - 500;

			int tnx = tilePosition.x;
			int tnz = tilePosition.y;

			if (x != lastX)
			{
				textX.text  = $">X:{tx}, {tnx}";
				textX2.text = $"{tnx}, X:{tx} <";
				lastX = x;
			}

			if (z != lastZ)
			{
				textZ.text  = $">Z:{tz}, {tnz}";
				textZ2.text = $"{tnz}, Z:{tz} <";
			}

			if (null != loader
			    //&& (tnx != lastTX || tnz != lastTZ) // Has rounding error problems
			    )
			{
				if (loader.TryGetCurrentTile(monitoredTransform.position, out var result) && result.tile != null)
				{
					textTileName.text = result.tile.tileName;
				}
				else
				{
					textTileName.text = string.Empty;
				}
			}

			lastTX = tnx;
			lastTZ = tnz;
		}

		private void LateUpdateScale()
		{
			if (null != cameraTr)
			{
				var fromCamToThis = monitoredTransform.position - cameraTr.position; // + Vector3.up * 2;

				if (Vector2.Dot(fromCamToThis.Xz(), Vector2.up) > 0)
				{
					textX.gameObject.SetActive(true);
					textX2.gameObject.SetActive(false);
				}
				else
				{
					textX.gameObject.SetActive(false);
					textX2.gameObject.SetActive(true);
				}

				if (Vector2.Dot(fromCamToThis.Xz(), Vector2.right) < 0)
				{
					textZ.gameObject.SetActive(true);
					textZ2.gameObject.SetActive(false);
				}
				else
				{
					textZ.gameObject.SetActive(false);
					textZ2.gameObject.SetActive(true);
				}

				var camx = cameraTr.rotation.eulerAngles.x;
				var arr = new[] { textX, textX2, textZ, textZ2 };

				var scale = Mathf.Max(0.1f, fromCamToThis.magnitude / 32);
				transform.localScale = new Vector3(scale, scale, scale);
				var localPos = transform.localPosition;
				localPos.y = scale;
				transform.localPosition = localPos;

				foreach (var text in arr)
				{
					var textR = text.rectTransform.rotation.eulerAngles;
					textR.x = camx;

					text.rectTransform.rotation = Quaternion.Euler(textR);
				}

				textTileName.transform.rotation = cameraTr.rotation;
			}
		}
	}
}
