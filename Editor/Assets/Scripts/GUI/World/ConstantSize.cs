﻿using System;

using DataTypes.Attributes;

using UnityEngine;

namespace GUI.World
{
	public class ConstantSize : MonoBehaviour
	{
		[Tooltip("Will be assigned to Camera.Main if null")]
		public Camera myCamera;

		public float minimumSize = 0.1f;
		public float maximumSize = 1000f;
		public float sizeDivisor = 32f;

		/// <summary>
		/// The current scale factor. Read only (value is only written to).
		/// </summary>
		[Tooltip("The current scale factor, read-only")]
		[InspectorReadonly]
		public float currentSize = Single.NaN;

		void Awake()
		{
			if (null == myCamera)
			{
				myCamera = Camera.main;
			}
		}

		void LateUpdate()
		{
			var fromCamToThis = transform.position - myCamera.transform.position; // + Vector3.up * 2;

			currentSize = Mathf.Clamp(fromCamToThis.magnitude / sizeDivisor, minimumSize, maximumSize);

			transform.localScale = new Vector3(currentSize, currentSize, currentSize);
		}

		void OnValidate()
		{
			minimumSize = Math.Max(minimumSize, float.Epsilon);
			maximumSize = Math.Max(maximumSize, minimumSize);
			sizeDivisor = Math.Max(sizeDivisor, float.Epsilon);
		}
	}
}
