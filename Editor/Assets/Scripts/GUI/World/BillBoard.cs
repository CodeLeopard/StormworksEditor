﻿using UnityEngine;

namespace GUI.World
{
	public class BillBoard : MonoBehaviour
	{
		[Tooltip("Will be assigned to Camera.Main if null")]
		public Camera myCamera;

		void Awake()
		{
			if (null == myCamera)
			{
				myCamera = Camera.main;
			}
		}

		void LateUpdate()
		{
			transform.rotation = myCamera.transform.rotation;
		}
	}
}
