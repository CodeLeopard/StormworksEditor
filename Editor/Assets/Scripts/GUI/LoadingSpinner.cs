// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using Behaviours;

using UnityEngine;

namespace GUI
{
	public class LoadingSpinner : M0noBehaviour
	{
		[SerializeField]
		private RectTransform Spinner;



		public float Speed = 360f;

		[SerializeField]
		private bool spinning = false;

		public bool Spinning
		{
			get => spinning;
			set
			{
				if (value == spinning) return;

				spinning = value;

				if (spinning)
				{
					Spinner.gameObject.SetActive(true);
				}
				else
				{
					Spinner.transform.eulerAngles = Vector4.zero;
					Spinner.gameObject.SetActive(false);
				}
			}
		}


		#region UnityMessages

		/// <inheritdoc />
		protected override void OnAwake()
		{
			Spinner.gameObject.SetActive(false);
		}

		/// <inheritdoc />
		protected override void OnEnableAndAfterStart()
		{
			Spinner.gameObject.SetActive(true);
		}


		void Update()
		{
			if (spinning)
			{
				var rotation = Spinner.transform.eulerAngles;
				rotation.z -= Speed * Time.deltaTime;
				Spinner.transform.eulerAngles = rotation;
			}
		}


		/// <inheritdoc />
		protected override void OnDisableIfStarted()
		{
			
		}

		#endregion UnityMessages

	}
}
