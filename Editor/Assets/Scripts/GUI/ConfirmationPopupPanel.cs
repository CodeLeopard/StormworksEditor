﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Behaviours;

using DataTypes.Extensions;

using TMPro;

using Tools;

using UnityEngine;
using UnityEngine.UI;

namespace GUI
{
	/// <summary>
	/// A generic confirmation popup panel.
	/// </summary>
	internal class ConfirmationPopupPanel : M0noBehaviour
	{
		[SerializeField]
		private TMP_Text headerTxtMP;
		[SerializeField]
		private TMP_Text bodyTxtMP;

		[SerializeField]
		private Button cancelButton;
		[SerializeField]
		private TMP_Text cancelTxtMP;

		[SerializeField]
		private Button confirmButton;
		[SerializeField]
		private TMP_Text confirmTxtMP;

		private LayoutElement panelLayoutElement;

		/// <inheritdoc />
		protected override void OnAwake()
		{
			const string message = "Should be assigned on the Prefab using the UnityInspector";
			if (null == headerTxtMP)   throw new ArgumentNullException(nameof(headerTxtMP), message);
			if (null == bodyTxtMP)     throw new ArgumentNullException(nameof(bodyTxtMP), message);
			if (null == cancelButton)  throw new ArgumentNullException(nameof(cancelButton), message);
			if (null == confirmButton) throw new ArgumentNullException(nameof(confirmButton), message);
			if (null == cancelTxtMP)   throw new ArgumentNullException(nameof(cancelTxtMP), message);
			if (null == confirmTxtMP)  throw new ArgumentNullException(nameof(confirmTxtMP), message);
			panelLayoutElement = GetRequiredComponent<LayoutElement>();

			cancelButton.onClick.AddListener(Button_Cancel_Click);
			confirmButton.onClick.AddListener(Button_Confirm_Click);
		}

		/// <summary>
		/// The text in the Header of the popup.
		/// </summary>
		public string HeaderText
		{
			get => headerTxtMP.text;
			set => headerTxtMP.text = value;
		}

		/// <summary>
		/// The text in the Body of the popup.
		/// </summary>
		public string BodyText
		{
			get => bodyTxtMP.text;
			set => bodyTxtMP.text = value;
		}

		/// <summary>
		/// The text on the button with negative result.
		/// </summary>
		public string CancelText
		{
			get => cancelTxtMP.text;
			set => cancelTxtMP.text = value;
		}

		/// <summary>
		/// The text on the button with positive result.
		/// </summary>
		public string ConfirmText
		{
			get => confirmTxtMP.text;
			set => confirmTxtMP.text = value;
		}

		/// <summary>
		/// The preferred with of the panel.
		/// </summary>
		public float PreferredWidth
		{
			get => panelLayoutElement.preferredWidth;
			set => panelLayoutElement.preferredWidth = value;
		}

		/// <summary>
		/// The action to run when the negative button is pressed.
		/// Note that the panel will disappear by itself once the action completes or throws.
		/// </summary>
		public Action Cancel;

		/// <summary>
		/// The action to run when the positive button is pressed.
		/// Note that the panel will disappear by itself once the action completes or throws.
		/// </summary>
		public Action Confirm;


		private bool? result = null;
		private Task<bool?> waitTask;

		/// <summary>
		/// A task that will complete once any interaction occurred.
		/// The result of the task is <see langword="true"/> when the confirm button is pressed,
		/// <see langword="false"/> when the cancel button is pressed,
		/// and <see langword="null"/> when the popup is closed for any other reason.
		/// </summary>
		/// <returns>The task</returns>
		public Task<bool?> WaitForInteraction()
		{
			waitTask = waitTask ?? new Task<bool?>(InteractionTask);
			return waitTask;
		}

		private bool? InteractionTask()
		{
			return result;
		}

		private void Button_Cancel_Click()
		{
			result = false;
			try
			{
				Cancel?.Invoke();
			}
			catch (Exception e)
			{
				Debug.LogException(e, this);
			}
			finally
			{
				waitTask?.RunSynchronously();
				Destroy(gameObject);
			}
		}

		private void Button_Confirm_Click()
		{
			result = true;
			try
			{
				Confirm?.Invoke();
			}
			catch (Exception e)
			{
				Debug.LogException(e, this);
			}
			finally
			{
				waitTask?.RunSynchronously();
				Destroy(gameObject);
			}
		}

		private void OnDestroy()
		{
			if (null == waitTask) return;
			if (waitTask.Status != TaskStatus.Created) return;
			waitTask.RunSynchronously();
		}

		/// <summary>
		/// Create a new <see cref="ConfirmationPopupPanel"/> instance.
		/// </summary>
		/// <returns></returns>
		public static ConfirmationPopupPanel Create(UnityEngine.Canvas parent = null)
		{
			if (parent == null)
			{
				parent = GameObject.FindGameObjectWithTag("GUI_MainCanvas").GetRequiredComponent<UnityEngine.Canvas>();
			}

			var go = Instantiate(Globals.Prefabs.ConfirmationPanel, parent.transform);
			var panel = go.GetRequiredComponent<ConfirmationPopupPanel>();
			return panel;
		}
	}
}
