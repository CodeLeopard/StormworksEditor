﻿// Copyright 2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/





using Behaviours;
using DataTypes.Extensions;
using Shared;
using System.Collections.Generic;
using System.Linq;
using Tools;
using UnityEngine;

namespace GUI.Menu
{
	public class StormworksInstallList : M0noBehaviour
	{
		[SerializeField]
		private GameObject EntryPrefab;

		[SerializeField]
		private GameObject ColumnHeader;

		[SerializeField]
		private GameObject AddEntryGameObject;

		public static StormworksInstallList Instance { get; private set; }

		protected override void OnAwake()
		{
			Instance = this;
		}

		/// <inheritdoc />
		protected override void OnEnableAndAfterStart()
		{
			UpdateList();
		}

		private void ClearList()
		{
			foreach (Transform t in transform) // Direct children
			{
				if (t == ColumnHeader.transform) continue;
				if (t == AddEntryGameObject.transform) continue;

				Destroy(t.gameObject);
			}
		}

		public void UpdateList()
		{
			ClearList();

			var settings = EditorSettingsManager.Settings;
			var installs = settings.StormworksInstalls ?? new List<EditorSettings.StormworksInstallEntry>();

			var steamInstall = StormworksPaths.LocateStormworks();

			if (installs.All(e => e.Path != steamInstall))
			{
				installs.Add(new EditorSettings.StormworksInstallEntry("SteamLibrary", steamInstall));
			}

			foreach (var info in installs)
			{
				var g = Instantiate(EntryPrefab, transform, false);

				g.name = info.Name;

				var entry = g.GetRequiredComponent<StormworksInstallListEntry>();

				entry.SetEntry(info);
			}

			AddEntryGameObject.transform.SetAsLastSibling();
		}
	}
}
