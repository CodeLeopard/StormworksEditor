﻿// Copyright 2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/





using Behaviours;
using SimpleFileBrowser;
using System.IO;
using TMPro;
using Tools;
using UnityEngine;

namespace GUI.Menu
{
	public class StormworksInstallListAddHandler : M0noBehaviour
	{
		[SerializeField]
		private TMP_InputField NameInput;
		[SerializeField]
		private TMP_InputField PathInput;
		[SerializeField]
		private ButtonHelper AddButton;
		[SerializeField]
		private ButtonHelper BrowseButton;

		private bool NameValid = false;
		private bool PathValid = false;

		/// <inheritdoc />
		protected override void OnAwake()
		{
			NameInput.onValueChanged.AddListener(NameInput_Changed);
			PathInput.onValueChanged.AddListener(PathInput_Changed);

			AddButton.Button.onClick.AddListener(Add_Click);
			AddButton.Button.interactable = false; // Initial values are not valid.
			BrowseButton.Button.onClick.AddListener(Browse_Click);
		}

		private void NameInput_Changed(string newValue)
		{
			NameValid = ! string.IsNullOrWhiteSpace(newValue);
			OnValidChanged();
		}

		private void PathInput_Changed(string newValue)
		{
			if (string.IsNullOrWhiteSpace(newValue))
			{
				PathValid = false;
				OnValidChanged();
				return;
			}

			if (File.Exists(newValue))
			{
				newValue = Path.GetDirectoryName(newValue);
				PathInput.SetTextWithoutNotify(newValue);
			}

			// todo: validate there is a SW install at newValue
			PathValid = Directory.Exists(newValue);


			OnValidChanged();
		}

		private void Add_Click()
		{
			EditorSettingsManager.Settings.StormworksInstalls.Add(new EditorSettings.StormworksInstallEntry(NameInput.text, PathInput.text));
			EditorSettingsManager.Instance.SaveSettings();

			StormworksInstallList.Instance.UpdateList();

			NameInput.SetTextWithoutNotify(string.Empty);
			NameValid = false;
			PathInput.SetTextWithoutNotify(string.Empty);
			PathValid = false;
			OnValidChanged();
		}

		private void Browse_Click()
		{
			FileBrowser.SetFilters(false, ".exe");

			if (!FileBrowser.ShowLoadDialog
				    (
				     OnFilesSelected
				   , OnFileSelectionCanceled
				   , FileBrowser.PickMode.Files
				   , false
				   , null
				   , null
				   , "Select a Stormworks Installation Folder (or Stormworks.exe)."
				    ))
			{
				Debug.LogError($"Failed to open the SelectFileDialog.");
			}

			void OnFilesSelected(string[] results)
			{
				if (results.Length == 0)
				{
					// todo: complain
					return;
				}

				if (results.Length > 1)
				{
					// todo: complain
					return;
				}

				PathInput.text = results[0];
			}

			void OnFileSelectionCanceled()
			{
				// todo?: complain?
			}
		}

		private void OnValidChanged()
		{
			AddButton.Button.interactable = NameValid && PathValid;
		}
	}
}
