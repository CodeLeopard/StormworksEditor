﻿// Copyright 2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/





using Shared;
using System.Linq;
using Tools;
using UnityEngine;

namespace GUI.Menu
{
	public class StormworksInstallListEntry : MonoBehaviour
	{
		public TMPro.TMP_InputField Name;
		public TMPro.TMP_InputField Path;

		public ButtonHelper Open;
		public ButtonHelper Select;
		public ButtonHelper Remove;

		public EditorSettings.StormworksInstallEntry Entry { get; private set; }

		public void SetEntry(EditorSettings.StormworksInstallEntry entry)
		{
			Entry = entry;

			Name.SetTextWithoutNotify(Entry.Name);
			Path.SetTextWithoutNotify(Entry.Path);

			Open.Button.onClick.AddListener(Open_Click);
			//Select.Button.onClick.AddListener(Select_Click);
			Remove.Button.onClick.AddListener(Remove_Click);
		}

		private void Open_Click()
		{
			StormworksPaths.Install = Entry.Path;
			GameStateManager.GotoScene("World");
		}

		private void Select_Click()
		{
			StormworksPaths.Install = Entry.Path;
		}

		private void Remove_Click()
		{
			var settings = EditorSettingsManager.Settings;
			settings.StormworksInstalls = settings.StormworksInstalls.Where(e => e != this.Entry).ToList();
			EditorSettingsManager.Instance.SaveSettings();

			StormworksInstallList.Instance.UpdateList();
		}
	}
}
