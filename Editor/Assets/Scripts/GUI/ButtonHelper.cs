﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Behaviours;

using TMPro;

using Tools;

using UnityEngine;
using UnityEngine.UI;

namespace GUI
{
	[DefaultExecutionOrder(ExecutionOrder.ButtonHelper)]
	public class ButtonHelper : MonoBehaviour
	{
		private Button myButton;
		private Text myText;
		private TMP_Text myTMPText;


		public Button Button => myButton;

		public string Label
		{
			get => myText?.text
			    ?? myTMPText?.text
			    ?? throw new NullReferenceException($"Both {nameof(myText)} and {nameof(myTMPText)} ware null somehow.");
			set
			{
				if (null != myText)
				{
					myText.text = value;
				}
				else
				{
					myTMPText.text = value;
				}
			}
		}

		private void Awake()
		{
			myButton = GetComponent<Button>() ?? throw new Exception("Could not find required component 'Button'");

			{
				var child = transform.Find("Label") ?? throw new Exception("Could not find required child 'Text'.");
				myText = child.GetComponent<Text>();
				myTMPText = child.GetComponent<TMP_Text>();

				if(myText == null && myTMPText == null) throw new Exception("Could not find required component 'Text'.");
			}
		}

	}
}
