﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;

using UnityEngine;
using UnityEngine.UI;

using DataModel.Missions;

using Playlists;

namespace GUI.MenuBar
{
	public class PlaylistComponentAdd : MonoBehaviour
	{
		public GameObject ButtonPrefab;

		public PlaylistManager playlistManager;


		void Awake()
		{
			foreach (ComponentType value in Enum.GetValues(typeof(ComponentType)))
			{
				var instance = Instantiate(ButtonPrefab, transform);

				instance.name = $"Button_PlaylistAddComponent_{value}";
				var label = instance.transform.Find("Text");
				label.GetComponent<Text>().text = $"{value}";

				var button = instance.GetComponent<UnityEngine.UI.Button>();
				//button.onClick.AddListener(() => { playlistManager.CreatePlaylistObject(value); });
			}
		}
	}
}
