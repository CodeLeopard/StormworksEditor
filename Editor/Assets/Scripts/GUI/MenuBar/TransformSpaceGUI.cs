﻿using Behaviours;

using DataTypes.Extensions;

using RuntimeGizmos;

using TMPro;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace GUI.MenuBar
{
	[RequireComponent(typeof(ToggleGroup))]
	public class TransformSpaceGUI : M0noBehaviour
	{
		[SerializeField]
		private TransformGizmo Gizmo;

		[SerializeField]
		private GameObject TogglePrefab;

		[SerializeField]
		private TMP_Text MenuButtonLabel;

		private ToggleGroup toggleGroup;


		private Toggle TSpace_Global;
		private Toggle TSpace_Local;

		private UnityAction<bool> ASpace_Global;
		private UnityAction<bool> ASpace_Local;

		/// <inheritdoc />
		protected override void OnAwake()
		{
			toggleGroup = GetRequiredComponent<ToggleGroup>();

			SetupToggle(ref TSpace_Global, ref ASpace_Global, "Global", SetSpaceGlobal);
			SetupToggle(ref TSpace_Local, ref ASpace_Local, "Local", SetSpaceLocal);

			var titleUpdater = transform.parent.gameObject.AddComponent<ButtonTitleUpdater>();
			titleUpdater.Updater = () => MenuButtonLabel.text = $"Space: {Gizmo.space}";
		}

		/// <inheritdoc />
		protected override void OnStart()
		{
			LayoutRebuilder.ForceRebuildLayoutImmediate(GetRequiredComponent<RectTransform>());
			gameObject.SetActive(false); // ExpandedItems needs to start open to set the event and layout but then should close.
		}

		/// <inheritdoc />
		protected override void OnEnableAndAfterStart()
		{
			InitToggle(TSpace_Global, Gizmo.space == TransformSpace.Global, true);
			InitToggle(TSpace_Local, Gizmo.space == TransformSpace.Local, true);
		}

		private void InitToggle(Toggle toggle, bool isOn, bool interactable)
		{
			toggle.SetIsOnWithoutNotify(isOn);
			toggle.interactable = interactable;
			toggle.group = toggleGroup;
		}

		private void SetupToggle(ref Toggle button, ref UnityAction<bool> unityEvent, string name, UnityAction<bool> action)
		{
			var go = Instantiate(TogglePrefab, transform, false);
			go.name = $"Toggle {name}";
			go.GetComponentInChildren<TMP_Text>().text = name;
			button = go.GetRequiredComponent<Toggle>();

			unityEvent += action;

			button.onValueChanged.AddListener(unityEvent);
		}

		#region Transform Space

		private void SetSpace(TransformSpace space, bool value)
		{
			if (!value) return;
			Gizmo.space = space;
		}

		private void SetSpaceGlobal(bool value)
		{
			SetSpace(TransformSpace.Global, value);
		}

		private void SetSpaceLocal(bool value)
		{
			SetSpace(TransformSpace.Local, value);
		}

		#endregion Transform Space
	}
}
