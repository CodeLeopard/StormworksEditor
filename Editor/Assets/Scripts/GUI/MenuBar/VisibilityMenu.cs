﻿// Copyright 2022-2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

using Behaviours;

using DataTypes.Extensions;

using Manage;

using Tiles;

using TMPro;
using System.Collections.Generic;
using System;

namespace GUI.MenuBar
{
	public class VisibilityMenu : M0noBehaviour
	{
		private ViewManager Manager => ViewManager.Instance;
		private TileLoadSettings Settings => TileLoader.instance.Settings;

		[SerializeField]
		private GameObject TogglePrefab;

		private List<MenuEntry> entries;

		private bool LoadAnyMeshPhys => Settings.LoadMesh || Settings.LoadPhys;
		private bool LoadAnyLight => Settings.LoadOmniLights || Settings.LoadSpotLights || Settings.LoadTubeLights;

		/// <inheritdoc />
		protected override void OnAwake()
		{
			entries = new List<MenuEntry>();
			SetupToggle("Mesh",               () => Manager.MeshVisible,                       () => Settings.LoadMesh,         Manager.SetMeshVisible);
			SetupToggle("Phys",               () => Manager.PhysVisible,                       () => Settings.LoadPhys,         Manager.SetPhysVisible);
			SetupToggle("Flatten Terrain",    () => Manager.FlattenTerrain,                    () => LoadAnyMeshPhys,           Manager.SetFlattenTerrain_void, prependShow: false);
			
			SetupToggle("Light Selector",     () => Manager.LightSelectorEnabled,              () => LoadAnyLight,              Manager.SetLightSelectorEnabled);
			SetupToggle("Light Emission",     () => Manager.LightEmissionEnabled,              () => LoadAnyLight,              Manager.SetLighsEmissionEnabled);
			SetupToggle("Omni Light",         () => Manager.OmniLightsVisible,                 () => Settings.LoadOmniLights,   Manager.SetOmniLighsVisible);
			SetupToggle("Spot Light",         () => Manager.SpotLightsVisible,                 () => Settings.LoadSpotLights,   Manager.SetSpotLightsVisible);
			SetupToggle("Tube Light",         () => Manager.TubeLightsVisible,                 () => Settings.LoadTubeLights,   Manager.SetTubeLightsVisible);

			SetupToggle("Edit Area",          () => Manager.EditAreasVisible,                  () => Settings.LoadEditArea,     Manager.SetEditAreasVisible);
			SetupToggle("Vehicle (EditArea)", () => Manager.EditAreasWithVehicleVisible,       () => Settings.LoadEditArea,     Manager.SetEditAreasWithVehicleVisible);
			SetupToggle("Vehicle Bounds",     () => Manager.EditAreasWithVehicleBoundsVisible, () => Settings.LoadEditArea,     Manager.SetEditAreasForceBoundsVisible);
			SetupToggle("Interacable",        () => Manager.InteractablesVisible,              () => Settings.LoadInteractable, Manager.SetInteractablesVisible);
			SetupToggle("Xml Track",          () => Manager.TrackLinesVisible,                 () => Settings.LoadTracks,       Manager.SetTrackLinesVisible);
			SetupToggle("Trees",              () => Manager.TreesVisible,                      () => Settings.LoadTrees,        Manager.SetTreesVisible);

			SetupToggle("Sea",                () => Manager.SeaVisible,                        () => true,                      Manager.SetSeaVisible);
			SetupToggle("Sea Floor",          () => Manager.SeaFloorVisible,                   () => true,                      Manager.SetSeaFloorVisible);

			SetupToggle("Sea Raycast",        () => Manager.SeaRaycast,                        () => true,                      Manager.SetSeaRaycast, prependShow: false);
		}

		/// <inheritdoc />
		protected override void OnStart()
		{
			LayoutRebuilder.ForceRebuildLayoutImmediate(GetRequiredComponent<RectTransform>());
		}

		private void SetupToggle(string name, Func<bool> getInitialActive, Func<bool> getToggleEnabled, UnityAction<bool> action, bool prependShow = true)
		{
			var go = Instantiate(TogglePrefab, transform, false);
			string show = prependShow ? "Show " : "";
			go.name = $"Toggle {show}{name}";
			go.GetComponentInChildren<TMP_Text>().text = $"{show}{name}";
			var button = go.GetRequiredComponent<Toggle>();

			button.onValueChanged.AddListener(action);

			if (prependShow)
			{

				name = $"Show {name}";
			}
			var entry = new MenuEntry(name, button, getInitialActive, getToggleEnabled, action);
			entries.Add(entry);
		}

		/// <inheritdoc />
		protected override void OnEnableAndAfterStart()
		{
			foreach(var entry in entries)
			{
				entry.Init();
			}
		}

		private class MenuEntry
		{
			public string name;
			public Toggle toggle;
			public UnityAction<bool> action;

			public Func<bool> getToggleEnabled;
			public Func<bool> getInitialValue;

			public MenuEntry(string name, Toggle toggle, Func<bool> getinitalValue, Func<bool> getToggleEnabled, UnityAction<bool> action)
			{
				this.name = name;
				this.toggle = toggle;
				this.getInitialValue = getinitalValue;
				this.getToggleEnabled = getToggleEnabled;
				this.action = action;
			}

			public void Init()
			{
				toggle.SetIsOnWithoutNotify(getInitialValue());
				toggle.interactable = getToggleEnabled();
			}
		}
	}
}
