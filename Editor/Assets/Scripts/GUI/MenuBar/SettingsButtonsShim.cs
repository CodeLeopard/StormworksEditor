﻿using Behaviours;
using Tools;

namespace Assets.Scripts.GUI.MenuBar
{
	internal class SettingsButtonsShim : M0noBehaviour
	{

		public void Open()
		{
			EditorSettingsManager.Instance.OpenSettings();
		}

		public void Save()
		{
			EditorSettingsManager.Instance.SaveSettings();
		}
	}
}
