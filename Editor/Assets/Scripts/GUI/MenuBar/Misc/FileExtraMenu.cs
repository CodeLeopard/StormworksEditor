// Copyright 2025 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using Behaviours;
using CurveGraph;
using Shared;
using SimpleFileBrowser;
using StaticObjects;
using System;
using System.IO;
using Tools;
using UnityEngine;

namespace GUI.MenuBar.Misc
{

	public class FileExtraMenu : M0noBehaviour
	{
		[SerializeField]
		private CurveSaveState curveSaveState;

		[SerializeField]
		private ObjectSaveManager objectSaveManager;

		public void OnLoadAdditiveCurves()
		{
			FileBrowser.SetFilters(false, ".v2.cs.xml");

			if (!FileBrowser.ShowLoadDialog
			(
				OnFilesSelected
				, OnFileSelectionCanceled
				, FileBrowser.PickMode.Files
				, false
				, StormworksPaths.creatorToolkitData
				, null
				, "Select curves file to load"
			))
			{
				Debug.LogError($"Failed to open the SelectFileDialog.");
			}

			return;

			void OnFilesSelected(string[] selection)
			{
				if (null == selection || selection.Length == 0)
				{
					Debug.Log("No file was selected");
					return;
				}

				if (selection.Length > 1)
				{
					Debug.LogError("Multiple selection is not supported.");
					return;
				}

				var filePath = selection[0];

				// Cursed AF
				var targetFileNameWithoutExtension = Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(filePath));

				Console.WriteLine($"Loading {filePath}");
				RunOnMainThread.Enqueue(ExecuteLoad);
				return;

				void ExecuteLoad()
				{
					curveSaveState.LoadAdditive(targetFileNameWithoutExtension);
				}
			}
		}

		public void OnLoadAdditiveObjects()
		{
			FileBrowser.SetFilters(false, ".sys.xml");

			if (!FileBrowser.ShowLoadDialog
			(
				OnFilesSelected
				, OnFileSelectionCanceled
				, FileBrowser.PickMode.Files
				, false
				, StormworksPaths.creatorToolkitData
				, null
				, "Select objects file to load"
			))
			{
				Debug.LogError($"Failed to open the SelectFileDialog.");
			}

			return;

			void OnFilesSelected(string[] selection)
			{
				if (null == selection || selection.Length == 0)
				{
					Debug.Log("No file was selected");
					return;
				}

				if (selection.Length > 1)
				{
					Debug.LogError("Multiple selection is not supported.");
					return;
				}

				var filePath = selection[0];

				// Cursed AF
				var targetFileNameWithoutExtension = Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(filePath));

				Console.WriteLine($"Loading {filePath}");
				RunOnMainThread.Enqueue(ExecuteLoad);
				return;

				void ExecuteLoad()
				{
					objectSaveManager.LoadAdditive(targetFileNameWithoutExtension);
				}
			}
		}


		private void OnFileSelectionCanceled()
		{
			Console.WriteLine($"User canceled file selection");
		}

		public void OnSaveCurvesOnly()
		{
			curveSaveState.Save();
		}
	}
}
