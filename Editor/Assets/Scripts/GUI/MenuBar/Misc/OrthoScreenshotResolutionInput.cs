﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using Behaviours;
using Control;
using GUI.SidePanel;
using GUI.SubElements;
using UnityEngine;

namespace GUI.MenuBar
{
	internal class OrthoScreenshotResolutionInput : M0noBehaviour
	{
		private IntEditPanelElement editPanelElement;

		[SerializeField]
		private OrthographicCamera camera;

		protected override void OnAwake()
		{
			editPanelElement = GetRequiredComponent<IntEditPanelElement>();

			var type = typeof(OrthographicCamera);
			var member = type.GetProperty(nameof(OrthographicCamera.ScreenShotResolution));

			editPanelElement.Init(camera, new EditPanelPropertyInfo(member, "Orthographic Screenshot Resolution", false));
		}
	}
}
