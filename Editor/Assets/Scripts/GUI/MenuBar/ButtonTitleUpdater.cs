﻿using System;

using Behaviours;

namespace GUI.MenuBar
{
	public class ButtonTitleUpdater : M0noBehaviour
	{
		public Action Updater;

		private void Update()
		{
			Updater?.Invoke();
		}
	}
}
