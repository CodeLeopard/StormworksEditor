﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/



using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Behaviours;

using DataTypes;
using DataTypes.Extensions;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

using Shared;

using Debug = UnityEngine.Debug;
using Assets.Scripts.Tools;

namespace GUI.MenuBar
{
	internal class AboutButtons : M0noBehaviour
	{
		[SerializeField]
		private GameObject ButtonPrefab;


		private List<ButtonHelper> myButtons;

		/// <inheritdoc />
		protected override void OnAwake()
		{
			myButtons = new List<ButtonHelper>();

			MakeButton("Readme",       ReadmeClick);
			MakeButton("Controls",     ControlsClick);
			MakeButton("ChangeNotes",  ChangeLogClick);
			MakeButton("License",      LicenseClick);
			MakeButton("Website",      WebsiteClick);
			MakeButton("LogFile",      LogFileClick);
			MakeButton("Version info", VersionInfoClick);
		}

		/// <inheritdoc />
		protected override void OnStart()
		{
			LayoutRebuilder.ForceRebuildLayoutImmediate(GetRequiredComponent<RectTransform>());
		}

		private void MakeButton(string label, UnityAction handler)
		{
			var go = Instantiate(ButtonPrefab, transform);
			var bh = go.GetRequiredComponent<ButtonHelper>();
			myButtons.Add(bh);
			bh.Label = label;


			void NewHandler()
			{
				// Close the panel.
				gameObject.SetActive(false);

				handler();
			}

			bh.Button.onClick.AddListener(NewHandler);
		}


		private static void ReadmeClick()
		{
			string path = Path.Combine(Application.dataPath, "../Readme.txt");
			OpenUri(path);
		}

		private static void ControlsClick()
		{
			string path = Path.Combine(Application.dataPath, "../Controls.txt");
			OpenUri(path);
		}

		private static void ChangeLogClick()
		{
			string path = Path.Combine(Application.dataPath, "../ChangeLog.md");
			OpenUri(path);
		}

		private static void LicenseClick()
		{
			string path = Path.Combine(Application.dataPath, "../LICENSE.txt");
			OpenUri(path);

			path = Path.Combine(Application.dataPath, "../COPYING.txt");
			OpenUri(path);

			path = Path.Combine(Application.dataPath, "../COPYING.LESSER.txt");
			OpenUri(path);
		}

		private static void WebsiteClick()
		{
			OpenUri("https://gitlab.com/CodeLeopard/StormworksEditor");
		}


		private static async void VersionInfoClick()
		{
			var popup = ConfirmationPopupPanel.Create(null);
			popup.HeaderText = "Version Information";
			popup.PreferredWidth = 800f;

			try
			{
				var info = BuildAndVersionInfomanager.GetBuildAndVersionInfo();
				var sb = new StringBuilder();

				if (! info.hasExactTag)
				{
					sb.AppendLine("No version number (git tag) assigned to this build.");
					sb.AppendLine($"This build is {info.NearestTagDistance} commits after version '{info.NearestTag}'");
				}
				else
				{
					sb.AppendLine($"Version number (git tag): '{info.GitTag}'.");
				}

				if (info.GitWasDirty)
				{
					sb.AppendLine($"Commit: '{info.GitCommit}' with additional untracked changes.");
				}
				else
				{
					sb.AppendLine($"Commit: '{info.GitCommit}'.");
				}
				sb.AppendLine($"Branch: '{info.GitBranch}'.");
				sb.AppendLine($"Built by '{info.Builder}' at: { info.BuildDate.ToLocalTime():yyyy-MM-dd HH:mm}.");

				popup.BodyText = sb.ToString();
			}
			catch (Exception e)
			{
				Debug.LogException(e);
				popup.BodyText = $"Could not get version info.";
			}

			popup.ConfirmText = "Close";
			popup.CancelText = "Dismiss";

			await popup.WaitForInteraction();
		}

		private static void OpenUri(string uri)
		{
			var info = new ProcessStartInfo(uri);
			info.UseShellExecute = true;
			Process.Start(info);
		}

		public static void LogFileClick()
		{
			if (Application.isEditor)
			{
				switch (SystemInfo.operatingSystemFamily)
				{
					case OperatingSystemFamily.Windows:
					{
						string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Unity/Editor/Editor.log");
						OpenUri(path);
						break;
					}
					case OperatingSystemFamily.MacOSX:
					{
						OpenUri("~/Library/Logs/Unity/Editor.log");
						break;
					}
					case OperatingSystemFamily.Linux:
					{
						OpenUri("~/.config/unity3d/Editor.log");
						break;
					}
					default:
					{
						throw new NotSupportedException
							($"Unknown operating system '{SystemInfo.operatingSystem}': unknown log file location.");
					}
				}
			}
			else
			{
				string path = Application.consoleLogPath;

				if (string.IsNullOrEmpty(path))
				{
					throw new NotSupportedException
						($"Unknown operating system '{SystemInfo.operatingSystem}': unknown log file location.");
				}

				OpenUri(path);
			}
		}
	}
}
