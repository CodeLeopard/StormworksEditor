using Behaviours;

using DataTypes.Extensions;

using RuntimeGizmos;

using TMPro;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace GUI.MenuBar
{
	[RequireComponent(typeof(ToggleGroup))]
	public class TransformModeGUI : M0noBehaviour
	{
		[SerializeField]
		private TransformGizmo Gizmo;

		[SerializeField]
		private GameObject TogglePrefab;

		[SerializeField]
		private TMP_Text MenuButtonLabel;


		private ToggleGroup toggleGroup;


		private Toggle TMode_Move;
		private Toggle TMode_Rotate;
		private Toggle TMode_Scale;
		private Toggle TMode_RayCastDrag;

		private UnityAction<bool> AMode_Move;
		private UnityAction<bool> AMode_Rotate;
		private UnityAction<bool> AMode_Scale;
		private UnityAction<bool> AMode_RayCastDrag;

		/// <inheritdoc />
		protected override void OnAwake()
		{
			toggleGroup = GetRequiredComponent<ToggleGroup>();

			SetupToggle(ref TMode_Move,        ref AMode_Move,   "Move",   SetMove);
			SetupToggle(ref TMode_Rotate,      ref AMode_Rotate, "Rotate", SetRotate);
			SetupToggle(ref TMode_Scale,       ref AMode_Scale,  "Scale",  SetScale);
			SetupToggle(ref TMode_RayCastDrag, ref AMode_RayCastDrag, "RayCastDrag", SetRayCastDrag);

			var titleUpdater = transform.parent.gameObject.AddComponent<ButtonTitleUpdater>();
			titleUpdater.Updater = () => MenuButtonLabel.text = $"Gizmo: {Gizmo.transformType}";
		}

		/// <inheritdoc />
		protected override void OnStart()
		{
			LayoutRebuilder.ForceRebuildLayoutImmediate(GetRequiredComponent<RectTransform>());
			gameObject.SetActive(false); // ExpandedItems needs to start open to set the event and layout but then should close.
		}

		/// <inheritdoc />
		protected override void OnEnableAndAfterStart()
		{
			InitToggle(TMode_Move,        Gizmo.transformType == TransformType.Move, true);
			InitToggle(TMode_Rotate,      Gizmo.transformType == TransformType.Rotate, true);
			InitToggle(TMode_Scale,       Gizmo.transformType == TransformType.Scale, true);
			InitToggle(TMode_RayCastDrag, Gizmo.transformType == TransformType.RayCast, true);
		}

		private void InitToggle(Toggle toggle, bool isOn, bool interactable)
		{
			toggle.SetIsOnWithoutNotify(isOn);
			toggle.interactable = interactable;
			toggle.group = toggleGroup;
		}

		private void SetupToggle(ref Toggle button, ref UnityAction<bool> unityEvent, string name, UnityAction<bool> action)
		{
			var go = Instantiate(TogglePrefab, transform, false);
			go.name = $"Toggle {name}";
			go.GetComponentInChildren<TMP_Text>().text = name;
			button = go.GetRequiredComponent<Toggle>();

			unityEvent += action;

			button.onValueChanged.AddListener(unityEvent);
		}


		#region Transform Mode
		private void SetMode(TransformType mode, bool value)
		{
			if (! value) Gizmo.transformType = TransformType.None;
			else Gizmo.transformType = mode;
		}

		private void SetMove(bool value)
		{
			SetMode(TransformType.Move, value);
		}

		private void SetRotate(bool value)
		{
			SetMode(TransformType.Rotate, value);
		}

		private void SetScale(bool value)
		{
			SetMode(TransformType.Scale, value);
		}

		private void SetRayCastDrag(bool value)
		{
			SetMode(TransformType.RayCast, value);
		}

		#endregion Transform Mode
	}
}
