﻿using Behaviours;

using DataTypes.Extensions;

using RuntimeGizmos;

using TMPro;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace GUI.MenuBar
{
	[RequireComponent(typeof(ToggleGroup))]
	public class TransformPivotGUI : M0noBehaviour
	{
		[SerializeField]
		private TransformGizmo Gizmo;

		[SerializeField]
		private GameObject TogglePrefab;

		[SerializeField]
		private TMP_Text MenuButtonLabel;


		private ToggleGroup toggleGroup;


		private Toggle TMode_Origin;
		private Toggle TMode_Center;

		private UnityAction<bool> AMode_Origin;
		private UnityAction<bool> AMode_Center;

		/// <inheritdoc />
		protected override void OnAwake()
		{
			toggleGroup = GetRequiredComponent<ToggleGroup>();

			SetupToggle(ref TMode_Origin,       ref AMode_Origin,       "Origin",        SetOrigin);
			SetupToggle(ref TMode_Center,      ref AMode_Center,      "Center",      SetCenter);

			var titleUpdater = transform.parent.gameObject.AddComponent<ButtonTitleUpdater>();
			titleUpdater.Updater = () => MenuButtonLabel.text = $"Pivot: {Gizmo.pivot}";
		}

		/// <inheritdoc />
		protected override void OnStart()
		{
			LayoutRebuilder.ForceRebuildLayoutImmediate(GetRequiredComponent<RectTransform>());
			gameObject.SetActive(false); // ExpandedItems needs to start open to set the event and layout but then should close.
		}

		/// <inheritdoc />
		protected override void OnEnableAndAfterStart()
		{
			InitToggle(TMode_Origin,  Gizmo.pivot == TransformPivot.Origin, true);
			InitToggle(TMode_Center, Gizmo.pivot == TransformPivot.Center, true);
		}

		private void InitToggle(Toggle toggle, bool isOn, bool interactable)
		{
			toggle.SetIsOnWithoutNotify(isOn);
			toggle.interactable = interactable;
			toggle.group = toggleGroup;
		}

		private void SetupToggle(ref Toggle button, ref UnityAction<bool> unityEvent, string name, UnityAction<bool> action)
		{
			var go = Instantiate(TogglePrefab, transform, false);
			go.name = $"Toggle {name}";
			go.GetComponentInChildren<TMP_Text>().text = name;
			button = go.GetRequiredComponent<Toggle>();

			unityEvent += action;

			button.onValueChanged.AddListener(unityEvent);
		}


		#region Transform Mode
		private void SetMode(TransformPivot pivot, bool value)
		{
			Gizmo.pivot = pivot;
		}

		private void SetOrigin(bool value)
		{
			SetMode(TransformPivot.Origin, value);
		}

		private void SetCenter(bool value)
		{
			SetMode(TransformPivot.Center, value);
		}

		#endregion Transform Mode
	}
}