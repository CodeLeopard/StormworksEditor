﻿using System;
using System.Collections;

using Behaviours;

using TMPro;

using Tools;

using UnityEngine;
using UnityEngine.UI;

namespace GUI
{
	[RequireComponent(typeof(CanvasGroup))]
	internal class LoadingScreen : M0noBehaviour
	{
		[SerializeField]
		private Image backgroundImage;

		[SerializeField]
		private Slider progressBar;

		[SerializeField]
		private TMP_Text progressText;

		private CanvasGroup canvasGroup;

		public static LoadingScreen Instance { get; private set; }

		[LogRange(min: 0.0001f, center: 0.1f, max: 3f)]
		public float Responsiveness = 0.125f;

		/// <summary>
		/// The minimum velocity note that progress is clamped to the target.
		/// </summary>
		[LogRange(min: 0.0001f, center: 0.1f, max: 3f)]
		public float MinVelocity = 0.001f;

		[Range(min: 0, max: 1)]
		public float FadeTime = 0.3f;

		private float ProgressTarget = 0;
		private float Velocity = 0f;
		private float ProgressValue = 0;

		/// <inheritdoc />
		protected override void OnAwake()
		{
			Instance = this;
			canvasGroup = GetRequiredComponent<CanvasGroup>();
		}

		/// <inheritdoc />
		protected override void OnEnableAlways()
		{
			ProgressTarget = 0;
			ProgressValue = 0;
			Velocity = 0;
		}

		private IEnumerator FadeIn()
		{
			float progress = 0;

			yield return null;

			progress += Time.deltaTime;

			if (progress >= FadeTime)
			{
				canvasGroup.alpha = 1;
				yield break;
			}

			canvasGroup.alpha = Mathf.Lerp(0, 1, FadeTime / progress);
		}

		private IEnumerator FadeOut()
		{
			float progress = 0;

			yield return null;

			progress += Time.deltaTime;

			if (progress >= FadeTime)
			{
				canvasGroup.alpha = 0;
				yield break;
			}

			canvasGroup.alpha = Mathf.Lerp(1, 0, FadeTime / progress);
		}


		public void StartFadeInInternal()
		{
			canvasGroup.alpha = 0;
			StartCoroutine(FadeIn());
		}

		public static void StartFadeIn()
		{
			Instance?.StartFadeInInternal();
		}

		void Update()
		{
			Velocity = Mathf.Clamp((ProgressTarget - ProgressValue) * Responsiveness, MinVelocity, 1f);
			ProgressValue = Mathf.Clamp(ProgressValue + Velocity * Time.deltaTime, 0f, ProgressTarget);

			var progress = ProgressValue;


			progressBar.value = progress;
			progressText.text = $"{(ProgressTarget * 100f):0.00}%";

			if (ProgressTarget > 0.9f)
			{
				StartCoroutine(FadeOut());
			}
		}

		public void SetProgressInternal(float progress)
		{
			ProgressTarget = Mathf.Clamp01(progress);
		}

		public static void SetProgress(float progress)
		{
			Instance?.SetProgressInternal(progress);
		}
	}
}
