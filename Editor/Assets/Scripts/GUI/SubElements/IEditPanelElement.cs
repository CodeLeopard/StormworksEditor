﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;

namespace GUI.SubElements
{
	interface IEditPanelElement
	{
		/// <summary>
		/// The label (name) for this field
		/// </summary>
		string Label { get; set; }

		/// <summary>
		/// Is the field currently Editable
		/// </summary>
		bool Editable { get; set; }
	}

	interface IEditPanelElement<TData>
	{
		/// <summary>
		/// The value, if the InputField contains an invalid value, the last recorded valid value is returned instead.
		/// Setting updates the last recorded value and does not trigger <see cref="InteractValue"/>
		/// </summary>
		TData Value { get; set; }

		/// <summary>
		/// Event that fires when the value was changed using the UI (changes caused otherwise are ignored)
		/// </summary>
		event Action<TData> InteractValue;

		/// <summary>
		/// Event that fires when the user finishes editing the value and submits it.
		/// </summary>
		event Action<TData> SubmitValue;
	}
}
