﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections.Generic;

using TMPro;

using UnityEngine;
using UnityEngine.UI;

namespace GUI.SubElements
{
	/// <summary>
	/// This generic component provides all* of the implementation for the Edit panel that wraps
	/// <see cref="TEnum"/>. However Unity can't do generic <see cref="UnityEngine.Component"/>s
	/// so a thin wrapper is used to provide a concrete implementation.
	/// Use <see cref="AbstractEditPanelElementBehaviour.GetBehaviourForEnumType"/> to retrieve
	/// the <see cref="Type"/> of that wrapper.
	/// </summary>
	/// <typeparam name="TEnum">The type of the <see langword="enum"/></typeparam>
	[DisallowMultipleComponent]
	[RequireComponent(typeof(EnumEditElementHelper))]
	public abstract class GenericEnumEditPanelElement<TEnum> : AbstractEditPanelElementBehaviour, IEditPanelElement<TEnum> where TEnum : System.Enum
	{
		private TMP_Dropdown inputField;

		/// <inheritdoc />
		public override bool Editable
		{
			get => inputField.interactable;
			set => inputField.interactable = value;
		}

		/// <inheritdoc />
		public override bool isFocused => false;

		/// <inheritdoc />
		public TEnum Value
		{
			get => IndexToEnum(inputField.value);
			set => inputField.SetValueWithoutNotify(EnumToIndex(value));
		}

		/// <inheritdoc />
		[field: NonSerialized]
		public event Action<TEnum> InteractValue;

		/// <inheritdoc />
		[field: NonSerialized]
		public event Action<TEnum> SubmitValue;

		#region UnityMessages

		protected override void DerivedAwake()
		{
			var enumEdit = GetComponent<EnumEditElementHelper>();
			textLabel = enumEdit.Label;
			inputField = enumEdit.DropDown;
			if (null == inputField)
				throw new NullReferenceException($"{nameof(inputField)} must be set on the {nameof(EnumEditElementHelper)} Component with the Inspector.");

			inputField.options.Clear();

			foreach (var value in _indexToValue)
			{
				inputField.options.Add(new TMP_Dropdown.OptionData(value.ToString()));
			}
		}

		/// <inheritdoc />
		protected override void OnEnableAndAfterStart()
		{
			base.OnEnableAndAfterStart();
			inputField.onValueChanged.AddListener(OnInteract);
		}

		/// <inheritdoc />
		protected override void OnDisableIfStarted()
		{
			base.OnDisableIfStarted();
			inputField.onValueChanged.RemoveListener(OnInteract);
		}

		#endregion UnityMessages
		#region Data Handling

		private void OnInteract(int index)
		{
			var newValue = IndexToEnum(index);
			SetDataValue(newValue);

			InteractValue?.Invoke(newValue);
			RaiseInteract();

			SubmitValue?.Invoke(newValue);
			RaiseSubmit();
		}

		/// <inheritdoc />
		public override void SetUIValueFromData()
		{
			Value = GetDataValue<TEnum>();
		}

		/// <inheritdoc />
		public override void PollData()
		{
			SetUIValueFromData();
		}

		#endregion Data Handling

		#region Static


		// Rather than casting back and forth we use mappings,
		// which has the added benefit of not breaking for gaps or negative values.
		// TODO: This assumes there are no flags enums (which is not true!) and may break things.
		// The current workaround is to ensure all valid flag combinations have a defined name in the enum definition.
		//todo: Find some way to support enums with flags in a better way, like with checkbox list or something like that.
		private static readonly TEnum[] _indexToValue = (TEnum[])Enum.GetValues(typeof(TEnum));
		private static readonly Dictionary<TEnum, int> _valueToIndex = new Dictionary<TEnum, int>();



		static GenericEnumEditPanelElement()
		{
			for (int i = 0; i < _indexToValue.Length; i++)
			{
				_valueToIndex.Add(_indexToValue[i], i);
			}
		}


		private static TEnum IndexToEnum(int index)
		{
			return _indexToValue[index];
		}

		private static int EnumToIndex(TEnum value)
		{
			return _valueToIndex[value];
		}

		/// <summary>
		/// Register <paramref name="t"/> as the thin wrapper around <see cref="GenericEnumEditPanelElement{TEnum}"/>
		/// for the actual type of <see cref="TEnum"/>.
		/// </summary>
		/// <param name="t"></param>
		protected static void Register(Type t)
		{
			Register(t, typeof(TEnum));
		}

		#endregion Static
	}
}
