﻿// Copyright 2022-2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;

using TMPro;

using UnityEngine;
using UnityEngine.UI;

using OVector2 = OpenTK.Mathematics.Vector2;
using OVector3 = OpenTK.Mathematics.Vector3;
using OVector4 = OpenTK.Mathematics.Vector4;


namespace GUI.SubElements
{
	public abstract class GenericVectorEditPanelElement<TVector> : AbstractEditPanelElementBehaviour, IEditPanelElement<TVector>
	{
		protected abstract int NumComponents { get; }

		[SerializeField]
		protected TMP_InputField[] inputFields;

		private string[] inputValueBuffer;
		private float[] valueBuffer;

		protected Image[] elementBackgrounds;

		protected TVector lastValidValue;
		/// <inheritdoc />
		public TVector Value
		{
			get => lastValidValue;
			set => SetValueToInputField(value);
		}


		/// <inheritdoc />
		[field: NonSerialized]
		public event Action<TVector> InteractValue;


		/// <inheritdoc />
		[field: NonSerialized]
		public event Action<TVector> SubmitValue;

		/// <inheritdoc />
		public override bool Editable
		{
			get => inputFields[0].interactable;
			set
			{
				foreach (var field in inputFields)
				{
					field.interactable = value;
				}
			}
		}

		public override bool isFocused
		{
			get
			{
				foreach (var field in inputFields)
				{
					if (field.isFocused) return true;
				}

				return false;
			}
		}

		private int _decimalPlaces = 4;
		public int decimalPlaces
		{
			get => _decimalPlaces;
			set => _decimalPlaces = Math.Clamp(value, 0, 99);
		}

		#region UnityMessages

		/// <inheritdoc />
		protected override void DerivedAwake()
		{
			if(null == inputFields)
				throw new NullReferenceException($"{nameof(inputFields)} must be set in the Inspector.");

			if (inputFields.Length != NumComponents)
			{
				throw new NullReferenceException
					(
					 $"{nameof(inputFields)} must be set in the Inspector and contain the exactly {NumComponents} elements."
					);
			}

			elementBackgrounds = new Image[inputFields.Length];
			for (int i = 0; i < elementBackgrounds.Length; i++)
			{
				elementBackgrounds[i] = inputFields[i].image;
			}

			inputValueBuffer = new string[inputFields.Length];
			valueBuffer = new float[inputFields.Length];

			// Otherwise it may be interactable for a moment until Editable is set to the correct value.
			Editable = false;
		}

		protected override void OnEnableAndAfterStart()
		{
			base.OnEnableAndAfterStart();
			if (isReadonly)
			{
				Editable = false;
			}
			else
			{
				foreach (var inputField in inputFields)
				{
					inputField.onValueChanged.AddListener(OnInteract);
					inputField.onSubmit.AddListener(OnSubmit);
					inputField.onEndEdit.AddListener(OnSubmit);
				}
			}
		}

		protected override void OnDisableIfStarted()
		{
			base.OnDisableIfStarted();
			if (!isReadonly)
			{
				foreach (var inputField in inputFields)
				{
					inputField.onValueChanged.RemoveListener(OnInteract);
					inputField.onSubmit.RemoveListener(OnSubmit);
					inputField.onEndEdit.RemoveListener(OnSubmit);
				}
			}
		}


		#endregion UnityMessages
		#region InputEvents

		protected void OnInteract(string _)
		{
			// We ignore the parameter because we need the entire Vector, not the single float that caused the update.

			for (int i = 0; i < inputFields.Length; i++)
			{
				inputValueBuffer[i] = inputFields[i].text;
			}


			if (TryParseInput(inputValueBuffer, out TVector result))
			{
				if (TrySetDataValue(result))
				{
					lastValidValue = result;

					InteractValue?.Invoke(result);
					RaiseInteract();

					SetValidColor(true);
					return;
				}
			}

			SetValidColor(false);
		}

		protected void OnSubmit(string value)
		{
			// Submit does not itself carry a value change.

			SubmitValue?.Invoke(lastValidValue);
			RaiseSubmit();
		}

		#endregion InputEvents

		#region Data Handling

		#region Trivial Implementations
		#region Unity
		protected void _FillFloatArray(float[] arr, Vector2 v)
		{
			arr[0] = v.x;
			arr[1] = v.y;
		}

		protected void _FillFloatArray(float[] arr, Vector3 v)
		{
			arr[0] = v.x;
			arr[1] = v.y;
			arr[2] = v.z;
		}

		protected void _FillFloatArray(float[] arr, Vector4 v)
		{
			arr[0] = v.x;
			arr[1] = v.y;
			arr[2] = v.z;
			arr[3] = v.w;
		}
		protected Vector2 _ReadFloatArray2(float[] arr)
		{
			return new Vector2(arr[0], arr[1]);
		}
		protected Vector3 _ReadFloatArray3(float[] arr)
		{
			return new Vector3(arr[0], arr[1], arr[2]);
		}
		protected Vector4 _ReadFloatArray4(float[] arr)
		{
			return new Vector4(arr[0], arr[1], arr[2], arr[3]);
		}
		#endregion Unity
		#region OpenTK
		protected void _FillFloatArray(float[] arr, OVector2 v)
		{
			arr[0] = v.X;
			arr[1] = v.Y;
		}

		protected void _FillFloatArray(float[] arr, OVector3 v)
		{
			arr[0] = v.X;
			arr[1] = v.Y;
			arr[2] = v.Z;
		}

		protected void _FillFloatArray(float[] arr, OVector4 v)
		{
			arr[0] = v.X;
			arr[1] = v.Y;
			arr[2] = v.Z;
			arr[3] = v.W;
		}
		protected OVector2 _ReadFloatArray2O(float[] arr)
		{
			return new OVector2(arr[0], arr[1]);
		}
		protected OVector3 _ReadFloatArray3O(float[] arr)
		{
			return new OVector3(arr[0], arr[1], arr[2]);
		}
		protected OVector4 _ReadFloatArray4O(float[] arr)
		{
			return new OVector4(arr[0], arr[1], arr[2], arr[3]);
		}
		#endregion OpenTK
		#endregion Trivial Implementations

		/// <summary>
		/// Write the <see cref="float"/> components of <typeparamref name="TVector"/> <paramref name="v"/> into <paramref name="arr"/>.
		/// </summary>
		/// <param name="arr"></param>
		/// <param name="v"></param>
		protected abstract void FillFloatArray(float[] arr, TVector v);

		/// <summary>
		/// Read the <see cref="float"/> components of <typeparamref name="TVector"/> from <paramref name="arr"/>.
		/// </summary>
		/// <param name="arr"></param>
		/// <returns></returns>
		protected abstract TVector ReadFloatArray(float[] arr);

		/// <summary>
		/// Apply <paramref name="value"/> to the <see cref="inputFields"/> and also <see cref="lastValidValue"/>.
		/// </summary>
		/// <param name="value"></param>
		protected virtual void SetValueToInputField(TVector value)
		{
			FillFloatArray(valueBuffer, value);

			for (int i = 0; i < inputFields.Length; i++)
			{
				inputFields[i].SetTextWithoutNotify(valueBuffer[i].ToString($"f{decimalPlaces}"));
			}
			lastValidValue = value;
			SetValidColor(true);
		}

		/// <inheritdoc />
		public override void SetUIValueFromData()
		{
			Value = GetDataValue<TVector>();
		}


		protected virtual bool TryParseInput(string[] values, out TVector result)
		{
			result = default;

			for (int i = 0; i < values.Length; i++)
			{
				if (! TryParseComponent(values[i], out valueBuffer[i])) return false;
			}

			result = ReadFloatArray(valueBuffer);

			return true;
		}

		protected virtual bool TryParseComponent(string value, out float result)
		{
			return float.TryParse(value, out result);
		}



		#endregion Data Handling

		protected void SetValidColor(bool isValid)
		{
			foreach (var inputBackground in elementBackgrounds)
			{
				inputBackground.color = isValid ? validColor : invalidColor;
			}
		}
	}
}
