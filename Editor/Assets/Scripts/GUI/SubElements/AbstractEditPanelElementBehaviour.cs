﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections.Generic;
using System.Reflection;

using Behaviours;

using GUI.SidePanel;

using TMPro;

using UnityEngine;
using UnityEngine.UI;

namespace GUI.SubElements
{
	/// <summary>
	/// Base class containing the common functionality for all EditPanels.
	/// </summary>
	[DisallowMultipleComponent]
	[RequireComponent(typeof(DerivedEditBehaviourHelper))]
	public abstract class AbstractEditPanelElementBehaviour : M0noBehaviour, IEditPanelElement
	{
		public Color validColor = new Color(1f, 1f, 1f);
		public Color invalidColor = new Color(1f, 0.5f, 0.5f);

		/// <summary>
		/// The instance for which we represent a single property.
		/// </summary>
		public object instance;


		private EditPanelMemberInfo _memberInfo;

		/// <summary>
		/// The specific property of <see cref="instance"/> that we represent.
		/// </summary>
		public EditPanelMemberInfo memberInfo
		{
			get => _memberInfo;
			set
			{
				_memberInfo = value;
				_OnMemberInfoAssigned();
			}
		}

		/// <summary>
		/// The label (name) for this property.
		/// </summary>
		[SerializeField]
		protected TMP_Text textLabel;

		/// <summary>
		/// The label (name) for this property.
		/// </summary>
		public string Label
		{
			get => textLabel.text;
			set => textLabel.text = value;
		}

		/// <summary>
		/// Is this property editable.
		/// It should always be false if the underlying property is readonly.
		/// </summary>
		public abstract bool Editable { get; set; }

		/// <summary>
		/// Is this Element currently Focused by the user.
		/// </summary>
		public abstract bool isFocused { get; }

		/// <summary>
		/// Is the underlying property readonly.
		/// </summary>
		public bool isReadonly => memberInfo.IsReadonly;

		#region UnityMessages
		protected override void OnAwake()
		{
			var derivedHelper = GetComponent<DerivedEditBehaviourHelper>();
			if (null != derivedHelper)
			{
				derivedHelper.AbstractEditPanelElementBehaviour = this;
			}
			else
			{
				Debug.Log($"Missing {nameof(DerivedEditBehaviourHelper)}!", this);
			}

			DerivedAwake();

			if (null == textLabel)
				throw new NullReferenceException($"{nameof(textLabel)} needs to be set in the Inspector.");
		}

		// Use an abstract method because there is no way to ensure the overridden implementation of Awake
		// would call base.OnAwake() and not doing that would break things.
		// This way makes it explicit so that can't go wrong. The cost is that for some classes it will be empty.
		/// <summary>
		/// Deriving class should not override <see cref="OnAwake"/> but instead implement this <see langword="abstract"/> method.
		/// </summary>
		protected abstract void DerivedAwake();

		#endregion UnityMessages

		private void _OnMemberInfoAssigned()
		{
			Editable = !isReadonly;
			OnMemberInfoAssigned();
		}

		protected virtual void OnMemberInfoAssigned()
		{

		}

		/// <summary>
		/// Initialization. Takes the values to be shown.
		/// </summary>
		/// <param name="target"></param>
		/// <param name="member"></param>
		public virtual void Init(object target, EditPanelMemberInfo member)
		{
			Label = member.DisplayName;
			instance = target;
			memberInfo = member;
			SetUIValueFromData();
		}


		/// <summary>
		/// Update the value in the UI to match the value stored in <see cref="instance"/>.
		/// </summary>
		public abstract void SetUIValueFromData();

		/// <summary>
		/// Update the value in the UI using <see cref="SetUIValueFromData"/> but may be subject to some conditions
		/// such as the input field not being focused.
		/// </summary>
		public virtual void PollData()
		{
			if (! isFocused)
			{
				SetUIValueFromData();
			}
		}

		[field: NonSerialized]
		public event Action Interact;

		[field: NonSerialized]
		public event Action Submit;

		protected void RaiseInteract()
		{
			Interact?.Invoke();
		}

		protected void RaiseSubmit()
		{
			Submit?.Invoke();
		}

		/// <summary>
		/// Read a correctly typed value from <see cref="instance"/> using <see cref="memberInfo"/>
		/// </summary>
		/// <typeparam name="TData"></typeparam>
		/// <returns></returns>
		protected TData GetDataValue<TData>()
		{
#if DEBUG
			// Values for the debugger to show.
			var dataTypeName = typeof(TData).Name;
			var propertyTypeName = memberInfo.MemberType.Name;
#endif
			return memberInfo.GetValue<TData>(instance);
		}

		/// <summary>
		/// Update the value in <see cref="instance"/> to match the value from the UI.
		/// </summary>
		/// <param name="value"></param>
		protected void SetDataValue(object value)
		{
			memberInfo.SetValue(instance, value);
		}

		protected bool TrySetDataValue(object newValue)
		{
			SetDataValue(newValue);

			object changedValue = memberInfo.GetValue(instance);
			if (null == changedValue) return ReferenceEquals(changedValue, newValue);

			bool wasApplied = changedValue.Equals(newValue);

			// Cursed special cases because floating point errors.
			{
				if (newValue is float newVec && changedValue is float changedVec)
				{
					return (newVec - changedVec) < 0.001f;
				}
			}
			{
				if (newValue is double newVec && changedValue is double changedVec)
				{
					return (newVec - changedVec) < 0.001f;
				}
			}
			{
				if (newValue is Vector2 newVec && changedValue is Vector2 changedVec)
				{
					return (newVec - changedVec).sqrMagnitude < 0.001f;
				}
			}
			{
				if (newValue is Vector3 newVec && changedValue is Vector3 changedVec)
				{
					return (newVec - changedVec).sqrMagnitude < 0.001f;
				}
			}
			{
				if (newValue is Vector4 newVec && changedValue is Vector4 changedVec)
				{
					return (newVec - changedVec).sqrMagnitude < 0.001f;
				}
			}

			// A property could refuse to use the new value as a signal that it's invalid.
			return wasApplied;
		}


		#region Static

		static AbstractEditPanelElementBehaviour()
		{
			// Find and register the Behaviours that derive from GenericEnumEditPanelElement<TENum>
			foreach (Type t in Assembly.GetExecutingAssembly().GetTypes())
			{
				if(t.IsAbstract)
					continue;
				if (t.BaseType == null)
					continue;
				if (!t.BaseType.IsGenericType)
					continue;
				if (t.BaseType.GetGenericTypeDefinition() != typeof(GenericEnumEditPanelElement<>))
					continue;

				Register(t, t.BaseType.GetGenericArguments()[0]);
			}
		}

		/// <summary>
		/// Register <paramref name="tWrapper"/> as the thin wrapper around <see cref="GenericEnumEditPanelElement{TEnum}"/>
		/// for the actual type of <see cref="tEnum"/>.
		/// </summary>
		/// <param name="tWrapper">The <see cref="Type"/> of the wrapper <see langword="class"/>.</param>
		/// <param name="tEnum">The <see cref="Type"/> of the wrapped <see langword="enum"/>.</param>
		protected static void Register(Type tWrapper, Type tEnum)
		{
			EnumToEditPanelMap.Add(tEnum, tWrapper);
		}

		/// <summary>
		/// Mapping from the <see langword="enum"/> to the type that holds actual implementation.
		/// This is <see cref="GenericEnumEditPanelElement{TEnum}"/> but Unity can't do generic behaviours
		/// so there is a thin wrapper around that that only defines the generic type to a concrete one.
		/// </summary>
		internal static Dictionary<Type, Type> EnumToEditPanelMap = new Dictionary<Type, Type>();

		/// <summary>
		/// For the <see cref="Type"/> of an <see cref="Enum"/> give matching thin wrapper around
		/// <see cref="GenericEnumEditPanelElement{TEnum}"/> for that enum.
		/// </summary>
		/// <param name="type"><see cref="Type"/> of an <see cref="Enum"/></param>
		/// <returns>The <see cref="Type"/> of a thin wrapper around
		/// <see cref="GenericEnumEditPanelElement{TEnum}"/> for that <see cref="Enum"/>.</returns>
		public static Type GetBehaviourForEnumType(Type type)
		{
			if (EnumToEditPanelMap.TryGetValue(type, out Type result))
			{
				return result;
			}

			throw new ArgumentException($"Unknown enum type: '{type.FullName}'.");
		}

		#endregion Static
	}
}
