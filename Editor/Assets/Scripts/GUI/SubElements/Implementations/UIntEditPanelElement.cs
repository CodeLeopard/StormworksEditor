﻿// Copyright 2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using UnityEngine;

namespace GUI.SubElements
{
	/// <summary>
	/// GUI element for editing of <see cref="UInt32"/> value.
	/// </summary>
	[DisallowMultipleComponent]
	public class UIntEditPanelElement : InputFieldEditPanelElementBehaviour<UInt32>
	{
		/// <inheritdoc />
		protected override bool TryParseInput(string value, out UInt32 result)
		{
			return UInt32.TryParse(value, out result);
		}

		/// <inheritdoc />
		protected override void SetValueToInputField(UInt32 value)
		{
			inputField.SetTextWithoutNotify(value.ToString());
			lastValidValue = value;
			SetValidColor(true);
		}
	}
}
