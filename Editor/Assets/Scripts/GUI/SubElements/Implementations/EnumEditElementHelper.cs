﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;

using TMPro;

using UnityEngine;
using UnityEngine.UI;

namespace GUI.SubElements
{
	/// <summary>
	/// This behaviour simply holds the <see cref="Label"/> and <see cref="DropDown"/>
	/// for enums. The actual implementation is provided by
	/// (a thin wrapper around) <see cref="GenericEnumEditPanelElement{TEnum}"/>.
	/// Which should be added to the <see cref="GameObject"/> at runtime, based on the
	/// actual <see cref="Type"/> of the <see cref="Enum"/> to be represented.
	/// </summary>
	[DisallowMultipleComponent]
	public class EnumEditElementHelper : MonoBehaviour
	{
		[SerializeField]
		public TMP_Text Label;

		[SerializeField]
		public TMP_Dropdown DropDown;

		private void Awake()
		{
			if (null == Label)
				throw new NullReferenceException($"{nameof(Label)} must be set in the Inspector.");
			if (null == DropDown)
				throw new NullReferenceException($"{nameof(DropDown)} must be set in the Inspector.");
		}
	}
}
