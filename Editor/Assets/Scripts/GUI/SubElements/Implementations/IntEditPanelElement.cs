﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using UnityEngine;

namespace GUI.SubElements
{
	/// <summary>
	/// GUI element for editing of <see cref="Int32"/> value.
	/// </summary>
	[DisallowMultipleComponent]
	public class IntEditPanelElement : InputFieldEditPanelElementBehaviour<Int32>
	{
		/// <inheritdoc />
		protected override bool TryParseInput(string value, out Int32 result)
		{
			return Int32.TryParse(value, out result);
		}

		/// <inheritdoc />
		protected override void SetValueToInputField(Int32 value)
		{
			inputField.SetTextWithoutNotify(value.ToString());
			lastValidValue = value;
			SetValidColor(true);
		}
	}
}
