﻿// Copyright 2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using UnityEngine;
using Vector3 = OpenTK.Mathematics.Vector3;

namespace GUI.SubElements
{
	/// <summary>
	/// GUI element for editing a <see cref="Vector3"/>, as 3 <see cref="float"/>s.
	/// </summary>
	[DisallowMultipleComponent]
	public class Vector3OEditPanelElement : GenericVectorEditPanelElement<Vector3>
	{
		/// <inheritdoc />
		protected override int NumComponents => 3;

		/// <inheritdoc />
		protected override void FillFloatArray(float[] arr, Vector3 v) => _FillFloatArray(arr, v);

		/// <inheritdoc />
		protected override Vector3 ReadFloatArray(float[] arr) => _ReadFloatArray3O(arr);
	}
}
