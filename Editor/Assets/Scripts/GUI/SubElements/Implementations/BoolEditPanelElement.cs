﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;

using UnityEngine;
using UnityEngine.UI;

namespace GUI.SubElements
{
	/// <summary>
	/// GUI element for editing a <see cref="bool"/>, using a checkbox.
	/// </summary>
	[DisallowMultipleComponent]
	public class BoolEditPanelElement : AbstractEditPanelElementBehaviour, IEditPanelElement<bool>
	{
		[SerializeField]
		private Toggle toggle;

		/// <inheritdoc />
		public override bool Editable
		{
			get => toggle.interactable;
			set => toggle.interactable = value;
		}

		/// <inheritdoc />
		public override bool isFocused => false;

		/// <inheritdoc />
		public bool Value
		{
			get => toggle.isOn;
			set => toggle.SetIsOnWithoutNotify(value);
		}


		/// <inheritdoc />
		[field: NonSerialized]
		public event Action<bool> InteractValue;


		/// <inheritdoc />
		[field: NonSerialized]
		public event Action<bool> SubmitValue;

		protected override void DerivedAwake()
		{
			if(null == toggle)
				throw new NullReferenceException($"{nameof(toggle)} must be set in the Inspector.");
		}

		protected override void OnEnableAndAfterStart()
		{
			base.OnEnableAndAfterStart();
			toggle.onValueChanged.AddListener(MyOnValueChanged);
		}

		protected override void OnDisableIfStarted()
		{
			base.OnDisableIfStarted();
			toggle.onValueChanged.RemoveListener(MyOnValueChanged);
		}

		/// <inheritdoc />
		public override void SetUIValueFromData()
		{
			Value = GetDataValue<bool>();
		}

		/// <inheritdoc />
		public override void PollData()
		{
			SetUIValueFromData();
		}

		private void MyOnValueChanged(bool newValue)
		{
			SetDataValue(newValue);
			InteractValue?.Invoke(newValue);
			SubmitValue?.Invoke(newValue);

			RaiseInteract();
			RaiseSubmit();
		}
	}
}
