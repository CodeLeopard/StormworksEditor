﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;

using DataTypes.Attributes;

using TMPro;

using UnityEngine;
using UnityEngine.UI;

namespace GUI.SubElements
{
	/// <summary>
	/// Base class for all EditBehaviours that use an <see cref="InputField"/>.
	/// </summary>
	/// <typeparam name="TData"></typeparam>
	public abstract class InputFieldEditPanelElementBehaviour<TData> : AbstractEditPanelElementBehaviour, IEditPanelElement<TData>
	{
		/// <summary>
		/// Our inputField
		/// </summary>
		[SerializeField]
		protected TMP_InputField inputField;

		protected Image inputBackground;

		/// <summary>
		/// The last known valid value.
		/// </summary>
		[SerializeField]
		[InspectorReadonly]
		protected TData lastValidValue;


		/// <inheritdoc />
		public TData Value
		{
			get => lastValidValue;
			set => SetValueToInputField(value);
		}

		/// <inheritdoc />
		[field: NonSerialized]
		public event Action<TData> InteractValue;


		/// <inheritdoc />
		[field: NonSerialized]
		public event Action<TData> SubmitValue;


		/// <inheritdoc />
		public override bool Editable
		{
			get => inputField.interactable;
			set => inputField.interactable = value;
		}

		/// <inheritdoc />
		public override bool isFocused => inputField.isFocused;

		#region UnityMessages

		/// <inheritdoc />
		protected override void DerivedAwake()
		{

			if (null == inputField)
				throw new NullReferenceException($"{nameof(inputField)} must be set in the Inspector.");

			inputBackground = inputField.image;

			// Otherwise it may be interactable for a moment until Editable is set to the correct value.
			inputField.interactable = false;
		}


		protected override void OnEnableAndAfterStart()
		{
			base.OnEnableAndAfterStart();
			if (isReadonly)
			{
				Editable = false;
			}
			else
			{
				inputField.onValueChanged.AddListener(OnInteract);
				inputField.onSubmit.AddListener(OnSubmit);
				inputField.onEndEdit.AddListener(OnSubmit);
			}
		}

		protected override void OnDisableIfStarted()
		{
			base.OnDisableIfStarted();
			if (! isReadonly)
			{
				inputField.onValueChanged.RemoveListener(OnInteract);
				inputField.onSubmit.RemoveListener(OnSubmit);
				inputField.onEndEdit.RemoveListener(OnSubmit);
			}
		}

		#endregion UnityMessages
		#region InputEvents

		protected void OnInteract(string value)
		{
			if (TryParseInput(value, out TData result))
			{
				if (TrySetDataValue(result))
				{
					lastValidValue = result;

					InteractValue?.Invoke(result);
					RaiseInteract();

					SetValidColor(true);
					return;
				}
			}

			SetValidColor(false);
		}

		protected void OnSubmit(string value)
		{
			// Submit does not itself carry a value change.

			SubmitValue?.Invoke(lastValidValue);
			RaiseSubmit();
		}

		#endregion InputEvents

		#region Data Handling


		/// <summary>
		/// Apply <paramref name="value"/> to the <see cref="inputField"/> and also <see cref="lastValidValue"/>.
		/// </summary>
		/// <param name="value"></param>
		protected virtual void SetValueToInputField(TData value)
		{
			inputField.SetTextWithoutNotify(value.ToString());
			lastValidValue = value;
			SetValidColor(true);
		}

		/// <inheritdoc />
		public override void SetUIValueFromData()
		{
			Value = GetDataValue<TData>();
		}


		/// <summary>
		/// Used to parse the <see cref="string"/> <paramref name="value"/> into a <typeparamref name="TData"/>.
		/// Returns <see langword="true"/> on success and <see langword="false"/> otherwise.
		/// out <paramref name="result"/> holds the value in the success case.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="result"></param>
		/// <returns></returns>
		protected abstract bool TryParseInput(string value, out TData result);


		#endregion Data Handling


		protected void SetValidColor(bool isValid)
		{
			inputBackground.color = isValid ? validColor : invalidColor;
		}
	}
}
