﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using RuntimeGizmos;

using Behaviours;

using DataTypes.Extensions;

using TMPro;

namespace GUI.SidePanel
{
	[DisallowMultipleComponent]
	public class SidePanelManager : M0noBehaviour
	{
		[SerializeField]
		private TMP_Text header;

		[SerializeField]
		private TransformEditPanel transformEdit; // We keep this around because destroying and recreating it would be a waste.

		[SerializeField]
		private GameObject Prefab_EditPanel;

		[SerializeField]
		private GameObject panelRoot;


		[SerializeField]
		private object target;

		public object Target
		{
			get => target;
			set => SetTarget(value);
		}


		public GameObject TargetAsGameObject => Target as GameObject;

		/// <summary>
		/// Used to prevent allocating temp stuff.
		/// </summary>
		private readonly List<Component> componentBuffer = new List<Component>();

		public static SidePanelManager Instance { get; protected set; }



		/// <inheritdoc />
		protected override void OnAwake()
		{
			if (null == panelRoot)
				throw new NullReferenceException($"{nameof(panelRoot)} needs to be assigned in the inspector.");

			Instance = this;
		}


		public void SetTarget(object value)
		{
			if (value == target) return;

			ClearPanel();
			target = value;
			PopulatePanel();
		}

		public void SetTarget(Transform t)
		{
			SetTarget(t.gameObject);
		}

		public void ClearTarget()
		{
			SetTarget((object) null);
		}

		private void ClearPanel()
		{
			panelRoot.SetActive(false);

			header.text = "No Target";

			transformEdit.Target = null;

			foreach (RectTransform t in transform)
			{
				if (null == t.gameObject.GetComponent<EditPanel>())
					continue;

				DestroyOrImmediate(t.gameObject);
			}
		}



		private void PopulatePanel()
		{
			if (target == null) return;

			if (TargetAsGameObject == null)
			{
				if (target is not IInspectorPanelContentProvider provider) return;

				var content = provider.GetInspectorContent();
				if (content == null) return;

				var panel = Instantiate(Prefab_EditPanel, transform);
				var controller = panel.GetRequiredComponent<EditPanel>();

				panel.name = content.GetType().Name;
				controller.Target = content;
				controller.eventTarget = provider;

				panelRoot.SetActive(true);



			return;
			}

			header.text = TargetAsGameObject.name; // todo: override opportunity.

			var transformGizmoOptions = TargetAsGameObject.GetComponent<TransformGizmoEventReceiver>();
			if (transformGizmoOptions)
			{
				transformEdit.Target = TargetAsGameObject.transform;
			}

			componentBuffer.Clear();
			TargetAsGameObject.GetComponents(componentBuffer);
			foreach (var component in componentBuffer)
			{
				if (component is not IInspectorPanelContentProvider provider) continue;

				var content = provider.GetInspectorContent();
				if (content == null) continue;

				var panel = Instantiate(Prefab_EditPanel, transform);
				var controller = panel.GetRequiredComponent<EditPanel>();

				panel.name = content.GetType().Name;
				controller.Target = content;
				controller.eventTarget = provider;
			}

			panelRoot.SetActive(true);
		}
	}
}
