﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using DataTypes.Attributes;
using DataTypes.Extensions;
using GUI.SubElements;
using Shared;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using TMPro;
using UnityEngine;

namespace GUI.SidePanel
{
	/// <summary>
	/// A <see cref="UnityEngine.Component"/> that will generate GUI elements to represent
	/// a <see cref="Target"/> object.
	/// </summary>
	[DisallowMultipleComponent]
	public class EditPanel : MonoBehaviour
	{
		/// <summary>
		/// The Prefab to use to create SubPanels when recursing.
		/// </summary>
		[SerializeField]
		private GameObject Prefab;

		[SerializeField]
		private GameObject Header;
		private TMP_Text HeaderText;


		[SerializeField]
		[InspectorReadonly]
		private object target;

		public IInspectorPanelContentProvider eventTarget;

		private readonly List<AbstractEditPanelElementBehaviour> panelElements = new List<AbstractEditPanelElementBehaviour>();

		protected bool needsPopulateSubItems;


		public virtual string Title { get; set; }

		public object Target
		{
			get => target;
			set
			{
				if (target != value)
				{
					ClearSubItems();
					target = value;
					eventTarget = null;

					if (target == null)
					{
						Title = null;
						gameObject.SetActive(false);
						return;
					}

					needsPopulateSubItems = true;
					gameObject.SetActive(true);
				}
			}
		}

		public static EditPanel Root;

		#region UnityMessages

		private void Awake()
		{
			HeaderText = Header.GetRequiredComponent<TMP_Text>();

			if (null == Root)
			{
				Root = this;
			}
		}


		private void Update()
		{
			// todo: remove polling.
			if (needsPopulateSubItems)
			{
				PopulateSubItems();
				return;
			}

			foreach (var panelElement in panelElements)
			{
				try
				{
					// todo: be able to configure the polling interval.
					panelElement.PollData();
				}
				catch (Exception e)
				{
					Debug.LogException(e, this);
				}
			}
		}

		#endregion UnityMessages

		protected void ClearSubItems()
		{
			panelElements.Clear();
			foreach (Transform child in transform)
			{
				if (child == Header.transform) continue;

				Destroy(child.gameObject);
			}
		}


		/// <summary>
		/// Setup the MissionEdit sub-items for this instance of MissionComponent
		/// </summary>
		private void PopulateSubItems()
		{
			var instanceType = target.GetType();

			HeaderText.text = Title ?? instanceType.Name;

			if (TryGetSpecification(instanceType, out var specification))
				InstantiateSubItems(specification);
		}

		/// <summary>
		/// Instantiate subItems to show the members in the specification.
		/// </summary>
		/// <param name="specification"></param>
		private void InstantiateSubItems(ICollection<EditPanelMemberInfo> specification)
		{
			needsPopulateSubItems = false;
			panelElements.Clear();
			if (null == Target) return;

			var wrapperType = Target.GetType();


			var sb = new StringBuilder();
			sb.AppendLine($"WrapperType: {wrapperType}");

			// todo: seal deriving classes because we can't deal with multiple levels of inheritance here.
			// todo: why can't we?

			foreach (var member in specification)
			{
				string memberName = member.MemberName;
				string displayName = member.DisplayName;
				Type declaredType = member.MemberType;
				Type searchType = member.SearchType ?? declaredType;
				bool isEnum = false;

				if (declaredType.IsSubclassOf(typeof(Enum)))
				{
					// There is one Prefab for all Enums, see below.
					searchType = typeof(Enum);
					isEnum = true;
				}

				if (SidePanelPrefabs.DefaultEditPanels.TryGetValue(searchType, out var prefab))
				{
					sb.AppendLine($"    property: {memberName} : {declaredType} => {prefab.name} (readonly: {member.IsReadonly})");

					var item = Instantiate(prefab, transform);

					item.name = $"Edit {declaredType} {memberName}";

					if (isEnum)
					{
						// All enums share a single Generic Prefab.
						// But Unity can't have generic components.
						// So there is a thin wrapper around the generic implementation for enums.
						// We need to fetch that wrapper and put it on the GameObject.
						var enumBehaviourType = AbstractEditPanelElementBehaviour.GetBehaviourForEnumType(declaredType);
						item.AddComponent(enumBehaviourType);
					}

					// The helper is just a thing that holds a reference to whatever derived version of
					// AbstractEditPanel is actually in use on this object.
					// GetComponent can't find implementations of abstract classes for us.
					var helper = item.GetComponent<DerivedEditBehaviourHelper>();
					if (null != helper && null != helper.AbstractEditPanelElementBehaviour)
					{
						var behaviour = helper.AbstractEditPanelElementBehaviour;
						behaviour.Init(Target, member);
						panelElements.Add(behaviour);

						behaviour.Interact += Element_Interact;
						behaviour.Submit   += Element_Submit;
					}
					else
					{
						Debug.LogError($"Failed to initialize property {memberName} : {declaredType} => {prefab.name}");
					}

					continue;
				}


				// If there was no prefab for the memberType we get here.
				// We may need to recurse into that type (if it has a specification) to show it.
				// todo: cyclic structures will break if the specification is not careful.
				// todo: prevent that here by catching that case.

				var value = member.GetValue(target);
				{
					if (value != null)
					{
						Type runtimeType = value.GetType();
						if (TryMakeSubPanel(runtimeType, value, displayName))
						{
							sb.AppendLine($"    property: {memberName} : {declaredType} => Recurse with value.");
							continue;
						}
					}

					if (TryMakeSubPanel(declaredType, null, displayName))
					{
						sb.AppendLine($"    property: {memberName} : {declaredType} => Recurse with null.");
						continue;
					}
				}

				sb.AppendLine($"    property: {memberName} : {declaredType} => no way to show.");
			}

			Console.WriteLine(sb);
		}


		private void Element_Interact()
		{
			eventTarget?.OnInteract();
		}
		private void Element_Submit()
		{
			eventTarget?.OnSubmit();
		}


		private bool TryMakeSubPanel(Type type, object value, string memberName)
		{
			if (TryGetSpecification(type, out _))
			{
				if (value == null)
				{
					// todo: make something show up at least.
				}
				else
				{
					// Generate child EditPanel.
					var sub = Instantiate(Prefab, transform);
					var subEP = sub.GetRequiredComponent<EditPanel>();
					subEP.Title = memberName;
					subEP.Target = value;
					subEP.eventTarget = eventTarget;
				}
				return true;
			}
			return false;
		}


		#region Static

		/// <summary>
		/// For the given <paramref name="type"/> fetch or generate a specification for how to show it.
		/// </summary>
		/// <param name="type"></param>
		/// <param name="specification"></param>
		/// <returns>true if there is a specification</returns>
		public static bool TryGetSpecification(Type type, out ICollection<EditPanelMemberInfo> specification)
		{
			if (specifications.TryGetValue(type, out specification))
				return specification != null;

			Console.WriteLine($"GenericEditPanel: No existing specification for type {type.FullName} attempting to create one using attributes.");
			specification = GetVisiblePropertiesFromAttributePresence(type);

			// If there is no auto-generated spec save that result so we don't re-try every time.
			specifications[type] = specification;
			return null != specification;
		}

		/// <summary>
		/// Attempt to auto-generate a specification based on
		/// <see cref="VisiblePropertyAttribute"/> attached to fields and properties.
		/// </summary>
		/// <param name="toInspect"></param>
		/// <returns>list with at least one element or null</returns>
		private static List<EditPanelMemberInfo> GetVisiblePropertiesFromAttributePresence(Type toInspect)
		{
			var result = new List<EditPanelMemberInfo>();

			foreach (var field in toInspect.GetFields(BindingFlags.Instance | BindingFlags.Public))
			{
				var attribute = field.GetCustomAttribute<VisiblePropertyAttribute>();
				if (null == attribute) continue;

				result.Add(new EditPanelFieldInfo(field, attribute.nameOverride ?? field.Name, attribute.isReadonly, attribute.large, attribute.order));
			}

			foreach (var property in toInspect.GetProperties(BindingFlags.Instance | BindingFlags.Public))
			{
				var attribute = property.GetCustomAttribute<VisiblePropertyAttribute>();
				if (null == attribute) continue;

				result.Add(new EditPanelPropertyInfo(property, attribute.nameOverride ?? property.Name, attribute.isReadonly, attribute.large, attribute.order));
			}

			if (result.Count <= 0)
			{
				result = null;
				Debug.LogWarning($"GenericEditPanel There is no existing specification, and could not find anything to show based on {nameof(VisiblePropertyAttribute)} for type {toInspect.FullName}");
			}

			result?.Sort((a, b) => a.order.CompareTo(b.order));

			return result;
		}


		/// <summary>
		/// Cached specifications, both manually assigned and automatically generated.
		/// Can contain <see langword="null"/> which means that auto-generation was attempted but did not succeed
		/// or that it was manually set to null to not even try to generate a spec.
		/// </summary>
		private static readonly Dictionary<Type, ICollection<EditPanelMemberInfo>> specifications =
			new Dictionary<Type, ICollection<EditPanelMemberInfo>>();


		/// <summary>
		/// Manually Assign a <paramref name="specification"/> for <paramref name="type"/>.
		/// </summary>
		/// <param name="type"></param>
		/// <param name="specification"></param>
		public static void AssignSpecification(Type type, ICollection<EditPanelMemberInfo> specification)
		{
			specifications[type] = specification;
			Console.WriteLine($"Assigned specification for type {type.FullName}");
		}


		/// <summary>
		/// Helper to more easily generate and assign a specification.
		/// </summary>
		/// <param name="t"></param>
		/// <param name="members"></param>
		public static void MkSpec(Type t, IEnumerable<SpecElement> members)
		{
			var list = new List<EditPanelMemberInfo>();
			foreach (var member in members)
			{
				var memberName = member.memberName;
				var displayName = member.displayName;
				var isReadonly = member.isReadonly;
				var isLarge = member.isLarge;

				var prop = t.GetProperty(memberName);
				if (null != prop)
				{
					list.Add(new EditPanelPropertyInfo(prop, displayName, isReadonly, isLarge));
					continue;
				}

				var field = t.GetField(memberName);
				if (null != field)
				{
					list.Add(new EditPanelFieldInfo(field, displayName, isReadonly, isLarge));
					continue;
				}

				// todo: we may want to support methods and show them as buttons.
				throw new NotImplementedException($"Member {memberName} was neither a field nor a property.");
			}

			AssignSpecification(t, list);
		}

		/// <summary>
		/// Helper for making specifications.
		/// </summary>
		public readonly struct SpecElement
		{
			public readonly string memberName;
			public readonly string displayName;
			public readonly bool isReadonly;
			public readonly bool isLarge;

			public SpecElement(string memberName, string displayName = null, bool isReadonly = false, bool isLarge = false)
			{
				this.memberName = memberName;
				this.displayName = displayName ?? memberName;
				this.isReadonly = isReadonly;
				this.isLarge = isLarge;
			}
		}


		static EditPanel()
		{
			MkSpec(typeof(OpenTK.Mathematics.Vector2), new SpecElement[]
			{
				new SpecElement(nameof(OpenTK.Mathematics.Vector2.X)),
				new SpecElement(nameof(OpenTK.Mathematics.Vector2.Y)),
			});
			MkSpec(typeof(OpenTK.Mathematics.Vector3), new SpecElement[]
			{
				new SpecElement(nameof(OpenTK.Mathematics.Vector3.X)),
				new SpecElement(nameof(OpenTK.Mathematics.Vector3.Y)),
				new SpecElement(nameof(OpenTK.Mathematics.Vector3.Z)),
			});
			MkSpec(typeof(OpenTK.Mathematics.Vector4), new SpecElement[]
			{
				new SpecElement(nameof(OpenTK.Mathematics.Vector4.X)),
				new SpecElement(nameof(OpenTK.Mathematics.Vector4.Y)),
				new SpecElement(nameof(OpenTK.Mathematics.Vector4.Z)),
				new SpecElement(nameof(OpenTK.Mathematics.Vector4.W)),
			});
		}


		#endregion Static
	}
}
