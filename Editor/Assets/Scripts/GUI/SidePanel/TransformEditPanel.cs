﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;

using UnityEngine;
using UnityEngine.UI;

using RuntimeGizmos;

using Control;

using DataTypes.Attributes;

using GUI.SubElements;

using TMPro;

namespace GUI.SidePanel
{
	/// <summary>
	/// GUI element for editing a Transform value, as 3 <see cref="Vector3"/>:
	/// Position, Rotation (as euler angles in degrees) and Scale
	/// </summary>
	[DisallowMultipleComponent]
	public class TransformEditPanel : MonoBehaviour
	{
		[SerializeField]
		private TMP_Text itemHeaderText;

		[SerializeField]
		private Vector3EditPanelElement _positionEditPanelElement;

		[SerializeField]
		private Vector3EditPanelElement _rotationEditPanelElement;

		[SerializeField]
		private Vector3EditPanelElement _scaleEditPanelElement;

		[SerializeField]
		[InspectorReadonly]
		private Transform target;

		public Transform Target
		{
			get => target;
			set => SetTarget(value);
		}

		public string HeaderText
		{
			get => itemHeaderText.text;
			set => itemHeaderText.text = value;
		}

		public bool isFocused
		{
			get =>
				_positionEditPanelElement.isFocused
			 || _rotationEditPanelElement.isFocused
			 || _scaleEditPanelElement.isFocused;
		}

		/// <summary>
		/// True if an event from <see cref="TransformGizmoEventReceiver"/> is actually sent from the <see cref="TransformEditPanel"/> instead.
		/// </summary>
		public static bool GizmoInteractEventIsTransformEditPanel
		{
			get;
			private set;
		}

		#region UnityMessages
		private void Awake()
		{
			if (null == itemHeaderText) throw new NullReferenceException($"{nameof(itemHeaderText)} was null. It must be set in the inspector.");
			if (null == _positionEditPanelElement) throw new NullReferenceException($"{nameof(_positionEditPanelElement)} was null. It must be set in the inspector.");
			if (null == _rotationEditPanelElement) throw new NullReferenceException($"{nameof(_rotationEditPanelElement)} was null. It must be set in the inspector.");
			if (null == _scaleEditPanelElement) throw new NullReferenceException($"{nameof(_scaleEditPanelElement)} was null. It must be set in the inspector.");

			var t = typeof(Transform);
			_positionEditPanelElement.memberInfo = new EditPanelPropertyInfo
				(t.GetProperty(nameof(Transform.localPosition)), "Position", false);
			_rotationEditPanelElement.memberInfo = new EditPanelPropertyInfo
				(t.GetProperty(nameof(Transform.localRotation.eulerAngles)), "Rotation", false);
			_scaleEditPanelElement.memberInfo = new EditPanelPropertyInfo
				(t.GetProperty(nameof(Transform.localScale)), "Scale", false);
		}

		private void OnEnable()
		{
			_positionEditPanelElement.Interact += OnInteract;
			_rotationEditPanelElement.Interact += OnInteract;
			_scaleEditPanelElement.Interact    += OnInteract;


			_positionEditPanelElement.Submit += OnSubmit;
			_rotationEditPanelElement.Submit += OnSubmit;
			_scaleEditPanelElement.Submit    += OnSubmit;
		}


		private void Update()
		{
			if (null == Target)
			{
				gameObject.SetActive(false);
				return;
			}

			// todo: we may not be the only user of hasChanged!
			if (target.hasChanged && ! isFocused)
			{
				SetUIValue(target);
			}

			target.hasChanged = false;
		}

		private void OnDisable()
		{
			_positionEditPanelElement.Interact -= OnInteract;
			_rotationEditPanelElement.Interact -= OnInteract;
			_scaleEditPanelElement.Interact    -= OnInteract;


			_positionEditPanelElement.Submit -= OnSubmit;
			_rotationEditPanelElement.Submit -= OnSubmit;
			_scaleEditPanelElement.Submit    -= OnSubmit;
		}


		#endregion UnityMessages


		private void SetTarget(Transform value)
		{
			if (target == value) return;

			target = value;

			// Must also set to null so it can be Garbage Collected.
			_positionEditPanelElement.instance = target;
			_rotationEditPanelElement.instance = target;
			_scaleEditPanelElement.instance    = target;

			if (target == null)
			{
				gameObject.SetActive(false);
				return;
			}


			var events = value.GetComponent<TransformGizmoEventReceiver>();
			if (null != events)
			{
				// todo: more granular!
				_positionEditPanelElement.Editable = events.AllowedMovementAxis != Axis.None;
				_scaleEditPanelElement.Editable = events.AllowedScaleAxis       != Axis.None;
				_rotationEditPanelElement.Editable = events.AllowedRotateAxis   != Axis.None;
			}
			else
			{
				_positionEditPanelElement.Editable = true;
				_scaleEditPanelElement.Editable = true;
				_rotationEditPanelElement.Editable = true;
			}

			//HeaderText = target.name; // name is shown in a different place now.
			SetUIValue(target);
			gameObject.SetActive(true);
		}

		private void OnInteract()
		{
			var (p, r, s) = GetUIValue();
			SetValueOn(target);
		}

		/// <summary>
		/// Set the value displayed in the GUI
		/// </summary>
		/// <param name="position"></param>
		/// <param name="rotation"></param>
		/// <param name="scale"></param>
		private void SetUIValue(Vector3 position, Vector3 rotation, Vector3 scale)
		{
			_positionEditPanelElement.Value = position;
			_rotationEditPanelElement.Value = rotation;
			_scaleEditPanelElement.Value = scale;
		}

		/// <summary>
		/// Set the value displayed in the GUI
		/// </summary>
		private void SetUIValue(Transform t)
		{
			SetUIValue(t.localPosition, t.localEulerAngles, t.localScale);
		}

		/// <summary>
		/// Retrieve the value displayed in the GUI
		/// </summary>
		private (Vector3 position, Vector3 rotation, Vector3 scale) GetUIValue()
		{
			return (_positionEditPanelElement.Value, _rotationEditPanelElement.Value, _scaleEditPanelElement.Value);
		}

		/// <summary>
		/// Retrieve the value displayed in the GUI, and apply it to the transform instance.
		/// </summary>
		private void SetValueOn(Transform t)
		{
			var localPosition = _positionEditPanelElement.Value;
			var localRotation = Quaternion.Euler(_rotationEditPanelElement.Value);
			var localScale = _scaleEditPanelElement.Value;

			SelectionManager.TransformPivotRespectingValueChange(t, localPosition, localRotation, localScale);

			var events = t.GetComponent<TransformGizmoEventReceiver>();
			if (null != events)
			{
				try
				{
					GizmoInteractEventIsTransformEditPanel = true;

					// todo? should we fire StartInteract?
					events.FireGizmoInteract();
				}
				finally
				{
					GizmoInteractEventIsTransformEditPanel = false;
				}
			}
		}


		private void OnSubmit()
		{
			var events = Target.GetComponent<TransformGizmoEventReceiver>();
			if (null != events)
			{
				try
				{
					GizmoInteractEventIsTransformEditPanel = true;
					events.FireGizmoEndInteract();
				}
				finally
				{
					GizmoInteractEventIsTransformEditPanel = false;
				}
			}
		}
	}
}
