﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using GUI.SubElements;
using System;
using System.Reflection;

namespace GUI.SidePanel
{
	public abstract class EditPanelMemberInfo
	{
		public readonly string DisplayName;
		public readonly bool IsReadonly;
		public readonly bool large;
		public readonly int order;

		public abstract Type MemberType { get; }

		/// <summary>
		/// Override the <see cref="Type"/> to be used to search for an EditPanelElement.
		/// </summary>
		public Type SearchType {
			get
			{
				// todo: Yucky tight coupling.
				if(large && MemberType == typeof(string))
				{
					return typeof(StringBigMarkerType);
				}
				return null;
			}
		}

		public abstract string MemberName { get; }

		protected EditPanelMemberInfo(string displayName, bool isReadonly, bool large, int order)
		{
			this.DisplayName = displayName;
			this.IsReadonly = isReadonly;
			this.large = large;
			this.order = order;
		}

		public abstract object GetValue(object instance);

		public virtual TValue GetValue<TValue>(object instance)
		{
			return (TValue) GetValue(instance);
		}

		public abstract void SetValue(object instance, object value);

		/// <inheritdoc />
		public override string ToString()
		{
			string displayName = "";
			if (DisplayName != null)
			{
				displayName = $" ({displayName})";
			}

			return $"{{{nameof(EditPanelFieldInfo)} : [{MemberType.Name}] '{MemberName}'{displayName}}}";
		}
	}

	public class EditPanelPropertyInfo : EditPanelMemberInfo
	{
		public readonly PropertyInfo property;

		/// <inheritdoc />
		public override Type MemberType => property.PropertyType;

		/// <inheritdoc />
		public override string MemberName => property.Name;

		/// <inheritdoc />
		public override object GetValue(object instance)
		{
			return property.GetValue(instance);
		}

		/// <inheritdoc />
		public override void SetValue(object   instance, object value)
		{
			property.SetValue(instance, value);
		}


		public EditPanelPropertyInfo(PropertyInfo property, string displayName, bool isReadonly, bool large = false, int order = 0) : base(displayName, isReadonly | ! property.CanWrite, large, order)
		{
			this.property = property;
		}
	}

	public class EditPanelFieldInfo : EditPanelMemberInfo
	{
		public readonly FieldInfo field;

		/// <inheritdoc />
		public override Type MemberType => field.FieldType;

		/// <inheritdoc />
		public override string MemberName => field.Name;

		/// <inheritdoc />
		public override object GetValue(object instance)
		{
			return field.GetValue(instance);
		}

		/// <inheritdoc />
		public override void SetValue(object   instance, object value)
		{
			field.SetValue(instance, value);
		}


		/// <inheritdoc />
		public EditPanelFieldInfo(FieldInfo field, string displayName, bool isReadonly, bool large = false, int order = 0) : base(displayName, isReadonly, large, order)
		{
			this.field = field;
		}
	}
}
