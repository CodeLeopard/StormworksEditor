﻿// Copyright 2022-2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using DataTypes.Attributes;
using GUI.SubElements;
using UnityEngine;

using Vector3 = UnityEngine.Vector3;

namespace GUI
{
	public class SidePanelPrefabs : MonoBehaviour
	{
		[InspectorReadonly]
		public int ItemCount;

		public GameObject edit_Bool;

		public GameObject edit_Enum;

		public GameObject edit_Float;
		public GameObject edit_Float_Slider;

		public GameObject edit_Int;
		public GameObject edit_UInt;

		public GameObject edit_String;
		public GameObject edit_String_big;

		public GameObject edit_Vector2;
		public GameObject edit_Vector3;
		public GameObject edit_Vector4;

		public GameObject edit_Transform;


		void Awake()
		{
			if (null != instance)
			{
				Debug.LogWarning($"{nameof(SidePanelPrefabs)} expected to exist only once, but {name} found already existing {instance.name}."
				               + $"\nThis shouldn't break anything, but may signal other issues.");
				return;
			}
			else
			{
				instance = this;
			}

			void Set(Type type, GameObject value)
			{
				if (null != value)
				{
					_DefaultEditPanels[type] = value;
				}
			}

			Set(typeof(bool),                edit_Bool);
			Set(typeof(Enum),                edit_Enum);
			Set(typeof(float),               edit_Float);
			Set(typeof(Int32),               edit_Int);
			Set(typeof(UInt32),              edit_UInt);
			Set(typeof(string),              edit_String);
			Set(typeof(StringBigMarkerType), edit_String_big);
			Set(typeof(Vector2),             edit_Vector2);
			Set(typeof(Vector3),             edit_Vector3);
			Set(typeof(Vector4),             edit_Vector4);
			Set(typeof(Matrix4x4),           edit_Transform);

			ItemCount = instance.ItemCount;
		}

		void Validate()
		{
			ItemCount = instance.ItemCount;
		}

		private static SidePanelPrefabs instance;
		private static readonly Dictionary<Type, GameObject> _DefaultEditPanels = new Dictionary<Type, GameObject>();
		public static readonly IReadOnlyDictionary<Type, GameObject> DefaultEditPanels = new ReadOnlyDictionary<Type, GameObject>(_DefaultEditPanels);
	}
}
