﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Tools
{
	public static class DebugSerializer
	{
		public static void Dump(StringBuilder sb, object instance, int maxDepth = 100, bool includeProperties = false, bool includeStatic = false)
		{
			int counter = 0;
			BindingFlags flags = BindingFlags.Instance | BindingFlags.Public;
			if (includeStatic) flags |= BindingFlags.Static;
			DumpElement(new State(sb, flags), "", true, "", instance, instance.GetType(), maxDepth, includeProperties);
		}

		public static void DumpStatic(StringBuilder sb, Type t, int maxDepth = 100, bool includeProperties = false, IEnumerable<Type> excludeMembersOfType = null)
		{
			int counter = 0;

			BindingFlags flags = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public;

			var state = new State(sb, flags);
			if(null != excludeMembersOfType) state.ExcludeMembersOfType.UnionWith(excludeMembersOfType);
			DumpElement(state, "", true, "", null, t, maxDepth, includeProperties);
		}

		private const string continueIndent        = "├─ ";
		private const string continueBranchIndent  = "├─┬ ";
		private const string continueRecurseIndent = "│ ";

		private const string finalIndent        = "└─ ";
		private const string finalBranchIndent  = "└─┬ ";
		private const string finalRecurseIndent = "  ";

		private class State
		{
			internal StringBuilder Sb;
			internal Dictionary<object, int> Seen = new Dictionary<object, int>();
			internal HashSet<Type> StaticSeen = new HashSet<Type>();
			internal HashSet<Type> ExcludeMembersOfType = new HashSet<Type>();
			internal int Counter;
			internal bool IncludeProperties;
			internal BindingFlags Flags;

			internal State(StringBuilder sb, BindingFlags flags = BindingFlags.Public | BindingFlags.Instance)
			{
				Sb = sb;
				Flags = flags;
			}
		}

		private static void DumpElement(State s, string previousIndent, bool isFinalIndent, string header, object instance, Type t, int depth, bool includeProperties, Exception e = null) {
			string myIndent, myRecurseIndent;
			if (! isFinalIndent)
			{
				myIndent = continueIndent;
				myRecurseIndent = continueRecurseIndent;
			}
			else
			{
				myIndent = finalIndent;
				myRecurseIndent = finalRecurseIndent;
			}

			if (null != e)
			{
				s.Sb.AppendLine($"{previousIndent}{myIndent}{header} = [{t.Name}] <<{e.GetType().Name} fetching value>>");
				return;
			}

			if (null == instance && (! s.Flags.HasFlag(BindingFlags.Static) || s.StaticSeen.Contains(t)))
			{
				s.Sb.AppendLine($"{previousIndent}{myIndent}{header} = [{t.Name}] <<null>>");
				return;
			}

			if (t == typeof(string))
			{
				s.Sb.AppendLine($"{previousIndent}{myIndent}{header} = '{instance}'");
			}
			else if (t.IsValueType)
			{
				string h = $"{header} = [{t.Name}] ";
				var valueStr = instance.ToString().Replace($"\n", $"\n{previousIndent}{myIndent}{new string(' ', h.Length)}");
				s.Sb.AppendLine($"{previousIndent}{myIndent}{h}{valueStr}");
			}
			else
			{
				// Class -> recurse

				if (null != instance && s.Seen.TryGetValue(instance, out int id))
				{
					s.Sb.AppendLine($"{previousIndent}{myIndent}{header} = <<Already Seen #{id}>> [{t.Name}] {instance}");
				}
				else if (depth < 0)
				{
					s.Sb.AppendLine($"{previousIndent}{myIndent}{header} = <<Maximum Depth Reached>> [{t.Name}] {instance}");
				}
				else
				{
					s.Counter += 1;
					if(null != instance) s.Seen.Add(instance, s.Counter);
					DumpClass(s, instance, previousIndent + myRecurseIndent, isFinalIndent, previousIndent, header, depth - 1, includeProperties, instanceType: t);
				}
			}
		}

		private static void DumpClass
		(
			State                   s
		  , object                  instance
		  , string                  indent
		  , bool                    meIsFinalIndent
		  , string                  classHeaderIndent
		  , string                  myFieldName
		  , int                     depth
		  , bool                  includeProperties
		  , Type instanceType
		)
		{
			instanceType = instanceType ?? instance.GetType();

			if (IsCollection(instanceType))
			{
				DumpCollection
					(
					 s
				   , instance
				   , indent
				   , meIsFinalIndent
				   , classHeaderIndent
				   , myFieldName
				   , depth
				   , includeProperties
				   , instanceType: instanceType
					);
				return;
			}
			var fieldList = new List<FieldInfo>();
			var propertyList = new List<PropertyInfo>();

			var myFlags = s.Flags;
			if (!s.StaticSeen.Add(instanceType))
			{
				myFlags = s.Flags & ~BindingFlags.Static;
			}

			foreach (var memberInfo in instanceType.GetMembers(myFlags))
			{
				var fieldInfo = memberInfo as FieldInfo;
				if (null != fieldInfo && ! s.ExcludeMembersOfType.Contains(fieldInfo.FieldType))
					fieldList.Add(fieldInfo);

				if(! includeProperties)
					continue;

				var propertyInfo = memberInfo as PropertyInfo;
				if (null != propertyInfo && !s.ExcludeMembersOfType.Contains(propertyInfo.PropertyType))
				{
					propertyList.Add(propertyInfo);
				}
			}

			var t = instanceType;

			if (fieldList.Count == 0 && propertyList.Count == 0)
			{
				s.Sb.AppendLine($"{classHeaderIndent}{(meIsFinalIndent ? finalIndent : continueIndent)}{myFieldName} = #{s.Counter} [{t.Name}] <<No members to show>> {instance}");
				return;
			}

			s.Sb.AppendLine($"{classHeaderIndent}{(meIsFinalIndent ? finalBranchIndent : continueBranchIndent)}{myFieldName} = #{s.Counter} [{t.Name}] {instance}");

			var totalCount = fieldList.Count + propertyList.Count;

			for (int i = 0; i < fieldList.Count; i++)
			{
				bool isFinalIndent = i >= totalCount - 1;

				var fieldInfo = fieldList[i];
				DumpElement
					(
					 s
				   , indent
				   , isFinalIndent
				   , fieldInfo.Name
				   , fieldInfo.GetValue(instance)
				   , fieldInfo.FieldType
				   , depth
				   , includeProperties
					);
			}

			for (int i = 0; i < propertyList.Count; i++)
			{
				bool isFinalIndent = (i + fieldList.Count) >= totalCount - 1;

				var propertyInfo = propertyList[i];

				try
				{
					DumpElement
						(
						 s
					   , indent
					   , isFinalIndent
					   , propertyInfo.Name
					   , propertyInfo.GetValue(instance)
					   , propertyInfo.PropertyType
					   , depth
					   , includeProperties
						);
				}
				catch (Exception e)
				{
					DumpElement
						(
						 s
					   , indent
					   , isFinalIndent
					   , propertyInfo.Name
					   , null
					   , propertyInfo.PropertyType
					   , depth
					   , includeProperties
					   , e
						);
				}
			}
		}

		private static void DumpCollection
		(
			State                   s
		  , object                  instance
		  , string                  indent
		  , bool                    meIsFinalIndent
		  , string                  classHeaderIndent
		  , string                  myFieldName
		  , int                     depth
		  , bool                    includeProperties
		  , Type instanceType
		)
		{
			Type itemType;
			if (typeof(Array).IsAssignableFrom(instanceType))
			{
				itemType = instanceType.GetElementType();
			}
			else
			{
				itemType = instanceType.GetGenericArguments()[0];
			}

			var collection = instance as ICollection;

			bool any = collection.Count > 0;
			s.Sb.Append(classHeaderIndent);

			if (any) s.Sb.Append(meIsFinalIndent ? finalBranchIndent : continueBranchIndent);
			else     s.Sb.Append(meIsFinalIndent ? finalIndent       : continueIndent);

			s.Sb.AppendLine($"{myFieldName} = #{s.Counter} {collection} <<{collection.Count} elements>>");

			if(! any) return;

			int i = 0;
			foreach (object o in collection)
			{
				bool isFinalIndent = i >= collection.Count - 1;

				var element = o;
				DumpElement
					(
					 s
				   , indent
				   , isFinalIndent
				   , $"[{i,2}]"
				   , element
				   , element?.GetType() ?? itemType
				   , depth
				   , includeProperties
					);

				i++;
			}
		}

		private static bool IsCollection(Type t)
		{
			return typeof(ICollection).IsAssignableFrom(t);
		}
	}
}
