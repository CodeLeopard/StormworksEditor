// Copyright 2022-2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using BinaryDataModel.DataTypes;
using DataTypes;
using Extrusion;
using Extrusion.Extrusion;
using Extrusion.Support;
using LuaIntegration;
using OpenTK.Mathematics;
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Mesh = UnityEngine.Mesh;
using Vector3 = OpenTK.Mathematics.Vector3;

namespace Tools
{
	[CreateAssetMenu(fileName = "Globals", menuName = "Globals")]
	public class Globals : ScriptableObject_
	{
		protected override void OnAwake()
		{
			instance = this;

#if DEBUG
			StringBuilder sb = new StringBuilder();
			DebugSerializer.Dump(sb, this);
			Console.WriteLine(sb);
#endif
		}

		/// <inheritdoc />
		protected override void OnHolderAwake()
		{
			instance = this;
		}

		[Serializable]
		public class Prefabs_
		{
			public GameObject SeaFloorApproximation;
			public GameObject ConfirmationPanel;
			public GameObject Button;
			public GameObject ButtonGrid;
			public GameObject LoadingScreen;
		}

		public Prefabs_ prefabs = new Prefabs_();
		public static Prefabs_ Prefabs => instance.prefabs;


		[Serializable]
		public class Materials_
		{
			public Material VertexColorOpaque;
			public Material VertexColorTransparent;
			public Material VertexColorEmissive;
			public Material VertexColorLava;

			public Material VertexColorForcedTransparent;
			public Material PlaylistZone;
			public Material EditAreaDynamic;
			public Material EditAreaStatic;
			public Material Interactable;
			public Material LightPositionIndicator;

			public Material Terrain;
			/// <summary>
			/// The material used to visualize a physics mesh.
			/// </summary>
			public Material Physics;
			public Material SeaFloor;

			public Material Tree;

			public Material SplitPlaneVis;
		}
		public Materials_ materials = new Materials_();
		public static Materials_ Materials => instance.materials;


		[Serializable]
		public class PhysicMaterials_
		{
			public PhysicMaterial Terrain;
			public PhysicMaterial Tree;
		}

		public PhysicMaterials_ physicMaterial = new PhysicMaterials_();
		public static PhysicMaterials_ PhysicMaterials => instance.physicMaterial;


		[Serializable]
		public class Meshes_
		{
			public Mesh Cube;
			public Mesh Sphere;
			public Mesh Capsule;
			public Mesh Cylinder;
			public Mesh Cone;

			public Mesh SeaFloorApproximation;
		}
		public Meshes_ meshes = new Meshes_();

		public static Meshes_ Meshes => instance.meshes;

		[Serializable]
		public class WireFrame_
		{
			public Mesh Cube;
		}

		public WireFrame_ wireFrames = new WireFrame_();

		public static WireFrame_ WireFrames => instance.wireFrames;


		[Serializable]
		public class Constants_
		{
			public bool ForceSingleThreaded = false;
			public bool InterpolationDiag = false;
		}
		public Constants_ constants = new Constants_();

		public static Constants_ Constants => instance.constants;


		[Serializable]
		public class Defaults_
		{

			public ConnectionCustomData DefaultConnectionCustomData => Get();

			private ConnectionCustomData Get()
			{
				var instance = new ConnectionCustomData();


				//instance.CurveSpecFilePath = SettingsManager.Settings.CurveGraphSettings.DefaultCurveSpecPath;

				// todo: fetch from path
				instance.CurveSpec = FallbackCurveSpec;


				return instance;
			}

			public readonly CurveSpec ErrorCurveSpec = CreateCurveSpec(new List<Color4>()
			{
				Color4.Red,
				Color4.Orange,
				Color4.Yellow,
				Color4.Orange,
			});
			public readonly CurveSpec FallbackCurveSpec = CreateCurveSpec(new List<Color4>()
			{
				Color4.Red,
				Color4.Orange,
				Color4.Yellow,
				Color4.Green,
				Color4.Blue,
				Color4.Indigo,
				Color4.Violet,
			});

			private static CurveSpec CreateCurveSpec(IEnumerable<Color4> colors)
			{
				var spec = new CurveSpec();
				var extrusion = new ExtrusionSpec();
				spec.Extrusions.Add(extrusion);
				//extrusion.Enabled = false;

				// Generate a pipe (circle)
				const int steps = 10;
				const float radius = 1f;

				var set = new LoopSet();
				extrusion.LoopSets.Add(set);

				var loop = new Loop();

				var iset = new InterpolationSettings();
				iset.MinInterpolationDistance = 0.5f;
				iset.MaxInterpolationDistance = 10f;

				iset.MaxAngleBetweenInterpolations = 2f;
				iset.MaxSegmentOffset = 2f;

				loop.InterpolationSettings = iset;

				loop.Vertices.Capacity = steps;
				for (int i = 0; i < steps; i++)
				{
					double t = ((double)i) / steps * Math.PI * 2;

					float x = (float)Math.Sin(t);
					float y = (float)Math.Cos(t);

					var v = new VertexRecord();
					v.position = new Vector3(x * radius, y * radius, 0);

					// Math provides normalized vector.
					v.normal = new Vector3(x, y, 0);

					v.color = Color4.White;

					loop.Vertices.Add(v);
				}
				set.LineIndices.Capacity = (steps) * 2;
				for (ushort i = 0; i < steps - 1; i++)
				{
					set.LineIndices.Add((ushort)(i + 1));
					set.LineIndices.Add(i);
				}
				set.LineIndices.Add(0);
				set.LineIndices.Add(steps - 1);

				foreach (Color4 color in colors)
				{
					var myLoop = loop.Clone();
					for (int i = 0; i < loop.Vertices.Count; i++)
					{
						var v = loop.Vertices[i];
						v.color = color;
						myLoop.Vertices[i] = v;
					}
					set.Loops.Add(myLoop);
				}


				return spec;
			}
		}

		public Defaults_ defaults = new Defaults_();
		public static Defaults_ Defaults => instance.defaults;



		#region Static

		private static Globals instance;


		#endregion Static
	}
}
