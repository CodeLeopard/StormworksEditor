﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.IO;

using Shared.Serialization;

using UnityEngine;

namespace Tools
{
	internal static class HourTracker
	{
		private const string fileName = "PlayTime.txt";
		private static string filePath => Path.Combine(Application.persistentDataPath, fileName);

		public static void SaveTimePlayed()
		{
			TimeSpan totalTime = TimeSpan.Zero;
			if (File.Exists(filePath))
			{
				try
				{
					totalTime = TimeSpan.Parse(SerializationHelper.Pure.LoadFromFile<string>(filePath));
				}
				catch(Exception e)
				{
					Debug.LogException(e);
				}
			}

			totalTime += TimeSpan.FromSeconds(Math.Max(Time.realtimeSinceStartupAsDouble, Time.unscaledTimeAsDouble));

			string output = $"{Math.Floor(totalTime.TotalHours)}:{totalTime.Minutes}:{totalTime.Seconds}";

			SerializationHelper.Pure.SaveToFile(output, filePath);

			Console.WriteLine($"Total Playtime now: {output}");
		}
	}
}
