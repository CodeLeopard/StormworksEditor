﻿// Adapted from: https://github.com/JulienHeijmans/EditorScripts/blob/master/Scripts/Utility/Editor/LODExtendedUtility.cs

using UnityEngine;
using UnityEditor;
using System.Linq;
using System;

namespace Tools
{
	public static class LODHelper
	{
		/// <summary>
		/// Return the LODGroup component with a renderer pointing to a specific GameObject. If the GameObject is not part of a LODGroup, returns null 
		/// </summary>
		/// <param name="GO"></param>
		/// <returns></returns>
		public static LODGroup GetParentLODGroupComponent(GameObject GO)
		{
			var myRenderer = GO.GetComponent<Renderer>();
			LODGroup LODGroupParent = GO.GetComponentInParent<LODGroup>();
			if (LODGroupParent == null) return null;
			LOD[] LODs = LODGroupParent.GetLODs();

			return LODs.Any(lod => lod.renderers.Any(renderer => renderer == myRenderer)) ? LODGroupParent : null;
		}

		/// <summary>
		/// Return the GameObject of the LODGroup component with a renderer pointing to a specific GameObject. If the GameObject is not part of a LODGroup, returns null.
		/// </summary>
		/// <param name="GO"></param>
		/// <returns></returns>
		public static GameObject GetParentLODGroupGameObject(GameObject GO)
		{
			var LODGroup = GetParentLODGroupComponent(GO);

			return LODGroup == null ? null : LODGroup.gameObject;
		}

		/// <summary>
		/// Get the LOD # of a selected GameObject. If the GameObject is not part of any LODGroup returns -1.
		/// </summary>
		/// <param name="GO"></param>
		/// <returns></returns>
		public static int GetLODid(GameObject GO)
		{
			var myRenderer = GO.GetComponent<Renderer>();
			LODGroup LODGroupParent = GO.GetComponentInParent<LODGroup>();
			if (LODGroupParent == null) return -1;
			LOD[] LODs = LODGroupParent.GetLODs();

			var index = Array.FindIndex(LODs, lod => lod.renderers.Any(renderer => renderer == myRenderer));
			return index;
		}


		/// <summary>
		/// returns the currently visible LOD level of a specific LODGroup, from a specific camera. If no camera is defined, uses the Camera.current.
		/// </summary>
		/// <param name="lodGroup"></param>
		/// <param name="camera"></param>
		/// <returns></returns>
		public static int GetVisibleLOD(LODGroup lodGroup, Camera camera = null)
		{
			var LODs = lodGroup.GetLODs();
			var relativeHeight = GetRelativeHeight(lodGroup, camera ?? Camera.current);

			int lodIndex = GetMaxLOD(lodGroup);
			for (var i = 0; i < LODs.Length; i++)
			{
				var lod = LODs[i];

				if (relativeHeight >= lod.screenRelativeTransitionHeight)
				{
					lodIndex = i;
					break;
				}
			}

			return lodIndex;
		}

#if UNITY_EDITOR
		/// <summary>
		/// returns the currently visible LOD level of a specific LODGroup, from a the SceneView Camera.
		/// </summary>
		/// <param name="lodGroup"></param>
		/// <returns></returns>
		public static int GetVisibleLODSceneView(LODGroup lodGroup)
		{
			Camera camera = SceneView.lastActiveSceneView.camera;
			return GetVisibleLOD(lodGroup, camera);
		}
#endif // UNITY_EDITOR

		public static float GetRelativeHeight(LODGroup lodGroup, Camera camera)
		{
			var distance = (
				lodGroup.transform.TransformPoint(lodGroup.localReferencePoint)
			  - camera.transform.position
				).magnitude;
			return DistanceToRelativeHeight(camera, distance / QualitySettings.lodBias, GetWorldSpaceSize(lodGroup));
		}

		public static float DistanceToRelativeHeight(Camera camera, float distance, float size)
		{
			if (camera.orthographic) return size * 0.5F / camera.orthographicSize;

			var halfAngle = Mathf.Tan(Mathf.Deg2Rad * camera.fieldOfView * 0.5F);
			var relativeHeight = size * 0.5F / (distance * halfAngle);
			return relativeHeight;
		}

		public static int GetMaxLOD(LODGroup lodGroup)
		{
			return lodGroup.lodCount - 1;
		}

		public static float GetWorldSpaceSize(LODGroup lodGroup)
		{
			return GetWorldSpaceScale(lodGroup.transform) * lodGroup.size;
		}

		public static float GetWorldSpaceScale(Transform t)
		{
			var scale = t.lossyScale;
			float largestAxis = Mathf.Abs(scale.x);
			largestAxis = Mathf.Max(largestAxis, Mathf.Abs(scale.y));
			largestAxis = Mathf.Max(largestAxis, Mathf.Abs(scale.z));
			return largestAxis;
		}
	}
}