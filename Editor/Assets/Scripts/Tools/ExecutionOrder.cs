namespace Tools
{
	public static class ExecutionOrder
	{
		public const int MouseEvents = 1; // Must run after TransformGizmo, because it's arrows should block selections.

		public const int SplineSaveState = - 1;

		public const int ViewManager = -1;

		public const int ButtonHelper = -10; // Should run before general components that depend on it in Awake.

		public const int RunOnMainThread = -90;
		public const int ScriptableObjectHolder = -91;
		public const int SettingsManager = -92;
		public const int InputManagement = -96;
	}
}
