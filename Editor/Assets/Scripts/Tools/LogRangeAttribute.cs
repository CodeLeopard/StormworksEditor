﻿using System;
using UnityEditor;
using UnityEngine;

namespace Tools
{
	/// <summary>
	/// Add this attribute to a float property to make it a logarithmic range slider
	/// </summary>
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
	public class LogRangeAttribute : PropertyAttribute
	{
		public float min;
		public float center;
		public float max;

		/// <summary>
		/// Creates a float property slider with a logarithmic 
		/// </summary>
		/// <param name="min">Minimum range value</param>
		/// <param name="center">Value at the center of the range slider</param>
		/// <param name="max">Maximum range value</param>
		public LogRangeAttribute(float min, float center, float max)
		{
			this.min = min;
			this.center = center;
			this.max = max;
		}
	}

#if UNITY_EDITOR
	[CustomPropertyDrawer(typeof(LogRangeAttribute))]
	public class LogRangePropertyDrawer : PropertyDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			LogRangeAttribute logRangeAttribute = (LogRangeAttribute)attribute;
			LogRangeConverter rangeConverter = new LogRangeConverter(logRangeAttribute.min, logRangeAttribute.center, logRangeAttribute.max);

			EditorGUI.BeginProperty(position, label, property);
			position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

			float value = rangeConverter.ToNormalized(property.floatValue);
			value = EditorGUI.Slider(position, value, 0, 1); // todo: show remapped value!

			property.floatValue = rangeConverter.ToRange(value);
			EditorGUI.EndProperty();
		}
	}
#endif
}
