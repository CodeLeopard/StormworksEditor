﻿// Copyright 2024 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using Control;
using UnityEngine;

namespace Tools
{
	internal class NewObjectPosition
	{
		/// <summary>
		/// Standard raycast from camera and mouse cursor into the world. Triggers ignored.
		/// </summary>
		/// <param name="camera"></param>
		/// <param name="layerMask"></param>
		/// <param name="ray"></param>
		/// <param name="hit"></param>
		/// <returns></returns>
		public static bool RayCast(Camera camera, LayerMask layerMask, out Ray ray, out RaycastHit hit)
		{
			ray = camera.ScreenPointToRay(Input.mousePosition);

			if (!Physics.Raycast(ray, out hit, float.MaxValue, layerMask, QueryTriggerInteraction.Ignore))
				return false;
			else
				return true;
		}

		/// <summary>
		/// Find the position for a new object to be spawned.
		/// This selects an implementation based on the current settings.
		/// </summary>
		/// <param name="camera"></param>
		/// <param name="layerMask"></param>
		/// <param name="result"></param>
		/// <returns></returns>
		public static bool Position(Camera camera, LayerMask layerMask, out Vector3 result)
		{
			if (EditorSettingsManager.Settings.PlaceNewObjectsAtCameraFocus)
			{
				return PositionFromCameraFocus(camera, layerMask, out result);
			}
			else
			{
				return PositionFromRaycast(camera, layerMask, out result);
			}
		}

		/// <summary>
		/// Find a position for a new object to be spawned using raycast from the <paramref name="camera"/> to the mouse cursor onto asny object on <paramref name="layerMask"/>.
		/// </summary>
		/// <param name="camera"></param>
		/// <param name="layerMask"></param>
		/// <param name="result"></param>
		/// <returns></returns>
		public static bool PositionFromRaycast(Camera camera, LayerMask layerMask, out Vector3 result)
		{
			if (RayCast(camera, layerMask, out Ray ray, out RaycastHit hit))
			{
				result = hit.point;
				return true;
			}

			result = default;
			return false;
		}

		/// <summary>
		///  Find a position for a new object to be spawned using the position of the camera focus marker.
		/// </summary>
		/// <param name="camera"></param>
		/// <param name="layerMask"></param>
		/// <param name="result"></param>
		/// <returns></returns>
		public static bool PositionFromCameraFocus(Camera camera, LayerMask layerMask, out Vector3 result)
		{
			result = OrbitCamera.ActiveFocusPosition;
			return true;
			
		}
	}
}
