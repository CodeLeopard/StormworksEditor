﻿// Copyright 2022-2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Behaviours;

using BinaryDataModel;
using BinaryDataModel.Converters;

using DataModel.Definitions;
using Shared.Serialization;
using DataModel.Vehicles;

using DataTypes;

using Shared;

using Tiles;

using Tools;

using UnityEditor;

using UnityEngine;

using Component = DataModel.Vehicles.Component;
using Debug = UnityEngine.Debug;
using Mesh = UnityEngine.Mesh;
using SMesh = BinaryDataModel.DataTypes.Mesh;
using Vector3 = OpenTK.Mathematics.Vector3;
using VectorExt = DataTypes.Extensions.VectorExt;
using Shared.Exceptions;

namespace Tools
{
	public class VehicleLoader : M0noBehaviour
	{
		/// <summary>
		/// How to position the vehicle in local space.
		/// </summary>
		public enum PositionMode : byte
		{
			/// <summary>
			/// Do not manage the position at all.
			/// </summary>
			None,
			/// <summary>
			/// Set localPosition to <see cref="Vector3.Zero"/>.
			/// </summary>
			Origin,
			/// <summary>
			/// Set localPosition such that the origin of <see cref="Vehicle.voxelBounds"/> align with 0,0,0 in local space.
			/// </summary>
			Bounds,
			/// <summary>
			/// Set localPosition to the <see cref="Vehicle.editor_placement_offset"/>.
			/// </summary>
			EditorOffset,
		}

		private PositionMode _position;

		[SerializeField]
		private PositionMode _inspectorPositionMode;

		/// <summary>
		/// How to position the vehicle in local space.
		/// </summary>
		public PositionMode Position
		{
			get => _position;
			set
			{
				_position = value;
				if (null != activeVehicle)
				{
					ApplyPosition();
				}
			}
		}

		private TileLOD tileLodHandler;
		private OnOffLOD myLodHandler;


		private string _vehiclePath;

		public string vehiclePath
		{
			get => _vehiclePath;
			set
			{
				if (value != _vehiclePath)
				{
					_vehiclePath = value;
					InspectorVehiclePath = value;

					StartLoadVehicle();
				}
			}
		}


		private DateTime loadedVehicleLastWriteTime;

		[SerializeField]
		private string InspectorVehiclePath;

		protected Material VertexColor;

		private Task worker;
		private bool cancelLoading;

		public Vehicle activeVehicle { get; private set; }

		private const int loaderBasePriority = 1000;

		public event Action<Vehicle> Loaded;

		#region UnityMessages

		protected override void OnAwake()
		{
			allLoaders.Add(this);

			if (!definitionsLoaded && null == definitionWorker)
			{
				definitionWorker = Task.Run(LoadDefinitions);
			}

			VertexColor = Globals.Materials.VertexColorOpaque;

			SetupLoDHandler();


			if (! string.IsNullOrEmpty(InspectorVehiclePath))
			{
				// For when used as stand-alone component.

				
				RunOnMainThread.Enqueue(// Needs to go through RunOnMainThread because Unity is cursed and does not obey the load order that I explicitly defined.
										() => vehiclePath = InspectorVehiclePath);
			}
		}


		private void OnValidate()
		{
			if (! Started) return;
			if (vehiclePath != InspectorVehiclePath)
			{
				vehiclePath = InspectorVehiclePath;
			}

			if (Position != _inspectorPositionMode)
			{
				Position = _inspectorPositionMode;
			}
		}

		private void OnDestroy()
		{
			allLoaders.Remove(this);

			if (tileLodHandler != null)
			{
				tileLodHandler.LODChanged -= TileLodHandlerTileLodChanged;
			}

			cancelLoading = true;
		}

		#endregion UnityMessages

		#region Events

		private void TileLodHandlerTileLodChanged(byte newLOD)
		{
			if (newLOD == 0)
			{
				gameObject.SetActive(true);
			}
			else
			{
				gameObject.SetActive(false);
			}
		}

		/// <summary>
		/// Should be called when the object was moved to another tile and thus needs to re-setup it's LoD event.
		/// </summary>
		public void MaybeTileChanged()
		{
			RemoveLoDHandler();
			SetupLoDHandler();
		}

		#endregion Events


		private void RemoveLoDHandler()
		{
			if(null != tileLodHandler)
			{
				tileLodHandler.LODChanged -= TileLodHandlerTileLodChanged;
				tileLodHandler = null;
			}

			if (null != myLodHandler)
			{
				Destroy(myLodHandler);
			}
		}

		private void SetupLoDHandler()
		{
			tileLodHandler = GetComponentInParents<TileLOD>();
			if (tileLodHandler != null)
			{
				tileLodHandler.LODChanged += TileLodHandlerTileLodChanged;

				bool shouldBeActive = tileLodHandler.lod == 0;
				gameObject.SetActive(shouldBeActive);
			}
			else
			{
				// todo: ideally we never have to use this, because it uses Update()
				myLodHandler = gameObject.AddComponent<OnOffLOD>();
				myLodHandler.Radius = 128;
				myLodHandler.Threshold = 5000;

				Console.WriteLine($"[{nameof(VehicleLoader)}] Did not find LOD handler in parents, using our own instance of {nameof(OnOffLOD)}.");
			}
		}

		private void ClearVehicle()
		{
			activeVehicle = null;
			foreach (Transform t in transform)
			{
				DestroyOrImmediate(t.gameObject);
			}
		}

		private void ApplyPosition()
		{
			switch (Position)
			{
				case PositionMode.None:
				{
					return;
				}
				case PositionMode.Origin:
				{
					transform.localPosition = UnityEngine.Vector3.zero;
					break;
				}
				case PositionMode.Bounds:
				{
					UnityEngine.Vector3 position = - activeVehicle.voxelBoundsF.Center.ToUnity();
					transform.localPosition = position;
					break;
				}
				case PositionMode.EditorOffset:
				{
					UnityEngine.Vector3 position = activeVehicle.editor_placement_offset.ToUnity();
					transform.localPosition = position;
					break;
				}
				default:
				{
					throw new ArgumentOutOfRangeException(nameof(Position), $"unexpected enum value '{Position}'.");
				}
			}
		}

		#region TaskStuff
		/// <summary>
		/// Start the <see cref="Task"/> (<see cref="worker"/>) that will load the vehicle.
		/// Note that if the vehicle is already loaded it will only be reloaded if it is outdated according to <see cref="vehicles"/>.
		/// </summary>
		public void StartLoadVehicle()
		{
			if (string.IsNullOrWhiteSpace(vehiclePath))
			{
				ClearVehicle();
				return;
			}

			if (null == worker)
			{
				worker = Task.Run(Task_LoadVehicle);
			}
			else
			{
				// todo: ideally we would cancel the currently running loading task.
				worker = worker.ContinueWith(ContinueWithLoadingVehicle, null);
			}
		}

		private void ContinueWithLoadingVehicle(Task task, object state)
		{
			Task_LoadVehicle();
		}

		private void Task_ThrowIfCanceled()
		{
			RunOnMainThread.CancelToken.ThrowIfCancellationRequested();

			if (cancelLoading)
				throw new OperationCanceledException();
		}

		private void Task_LoadVehicle()
		{
			try
			{
				Task_ThrowIfCanceled();

				string path = vehiclePath;
				if (! path.EndsWith(".xml"))
				{
					path += ".xml";
				}

				if (! definitionsLoaded)
				{
					definitionWorker?.Wait(RunOnMainThread.CancelToken);
				}
				Task_ThrowIfCanceled();

				var timer = new Stopwatch();
				timer.Start();

				var record = vehicles.GetOrAdd(path);
				if (activeVehicle != null && record.LastWriteTime <= loadedVehicleLastWriteTime)
				{
					// We have a vehicle and it was not updated -> Nothing has changed -> do nothing.
					return;
				}

				RunOnMainThread.EnqueueAndWaitForCompletion(ClearVehicle, loaderBasePriority - 1);

				activeVehicle = record?.Vehicle;

				if (null == activeVehicle)
				{
					// Loading failed or the file does not exist.
					// This was already logged by LoadVehicle()
					return;
				}

				RunOnMainThread.Enqueue(ApplyPosition, loaderBasePriority - 1);

				loadedVehicleLastWriteTime = record.LastWriteTime;

				Task_ThrowIfCanceled();

				var subTasks = new Task[activeVehicle.bodies.Count];
				for(int i = 0; i < activeVehicle.bodies.Count; i++)
				{
					var body = activeVehicle.bodies[i];
					subTasks[i] = Task.Run(() => Task_LoadBody(body));
				}

				Task.WaitAll(subTasks.ToArray());

				RunOnMainThread.EnqueueAndWaitForCompletion(FireLoaded);
			}
			catch (Exception e)
			{
				RunOnMainThread.LogException(e, this);
				throw; // Task needs to know it failed.
			}
		}

		private void FireLoaded()
		{
			var vehicle = activeVehicle;
			if (null == vehicle) return;

			Loaded?.Invoke(vehicle);
		}

		private void Task_LoadBody(Body body)
		{
			try
			{
				Task_ThrowIfCanceled();
				var sMesh = body.mesh;

				if (sMesh == null)
				{
					// The property uses a Lazy<Mesh> behind the scenes to ensure it's generated exactly once.
					// If that failed for any reason null will be assigned.
					// The Exception will be shown in the console.
					return;
				}

				Task_ThrowIfCanceled();

				var meshInfo = sMesh.ToUnityMesh();

				RunOnMainThread.EnqueueAndWaitForCompletion(SetupGO, loaderBasePriority + 1);
				void SetupGO()
				{
					Task_ThrowIfCanceled();

					var go = new GameObject($"Body{body.unique_id}");
					go.transform.SetParent(gameObject.transform, false);


					// always 0 it seems...
					var bodyTransform = body.initial_transform * body.local_transform;

					bodyTransform.SetDataOn(go.transform, true);

					var meshFilter = go.AddComponent<MeshFilter>();
					meshFilter.sharedMesh = meshInfo.Mesh;
					var meshRenderer = go.AddComponent<MeshRenderer>();
					meshRenderer.materials = meshInfo.Materials.ToArray();
					var collider = go.AddComponent<MeshCollider>();
					collider.sharedMesh = meshFilter.sharedMesh;
					collider.cookingOptions = MeshColliderCookingOptions.EnableMeshCleaning
					                        | MeshColliderCookingOptions.CookForFasterSimulation
					                        | MeshColliderCookingOptions.UseFastMidphase
					                        | MeshColliderCookingOptions.WeldColocatedVertices;
				}
			}
			catch (Exception e)
			{
				RunOnMainThread.LogException(e, this);
				throw; // Task needs to know it failed.
			}
		}

		#endregion TaskStuff

		#region Static
		private static string definitionsPath => StormworksPaths.Data.definitions;

		private static Task definitionWorker;
		private static bool definitionsLoaded;

		private static readonly LazyCache<string, VehicleRecord> vehicles = new LazyCache<string, VehicleRecord>(LoadVehicle);
		private static readonly HashSet<VehicleLoader> allLoaders = new HashSet<VehicleLoader>();

		private class VehicleRecord
		{
			internal readonly Vehicle Vehicle;
			internal readonly Exception Exception;
			internal readonly DateTime LastWriteTime;

			public VehicleRecord(Vehicle vehicle, DateTime lastWriteTime)
			{
				this.Vehicle = vehicle;
				this.LastWriteTime = lastWriteTime;
			}

			public VehicleRecord(Exception exception, DateTime lastWriteTime)
			{
				this.Exception = exception;
				this.LastWriteTime = lastWriteTime;
			}
		}


		private static VehicleRecord LoadVehicle(string key)
		{
			string path = key;

			try
			{
				var timer = new Stopwatch();
				timer.Start();

				var nameNoExt = Path.GetFileNameWithoutExtension(path);
				var lastWriteTime = new FileInfo(path).LastWriteTime;
				var vehicle = new Vehicle(XMLHelper.LoadFromFile(path), nameNoExt);

				timer.Stop();
				Console.WriteLine($"[{nameof(VehicleLoader)}] Loaded xml for '{key}'. Elapsed: {timer.Elapsed}.");
				return new VehicleRecord(vehicle, lastWriteTime);
			}
			catch (FileNotFoundException e)
			{
				Console.WriteLine($"[{nameof(VehicleLoader)}] File does not Exist '{key}'.");
				return new VehicleRecord(e, DateTime.Now);
			}
			catch (Exception e)
			{
				string message = $"[{nameof(VehicleLoader)}] '{key}' --> {e.ToString()}";
				RunOnMainThread.LogError(message);
				return new VehicleRecord(e, DateTime.Now);
			}
		}

		private static void LoadDefinitions()
		{
			try
			{
				Definition.UseOnTheFlyDefinitions(definitionsPath);
				definitionsLoaded = true;
				MeshGenerator.GetOrLoadMesh = MeshCache.GetOrLoadMesh;
			}
			catch (Exception e)
			{
				RunOnMainThread.LogException(e);
			}
		}

		/// <summary>
		/// Remove the entry from <see cref="vehicles"/>.
		/// </summary>
		/// <param name="key">The key of the entry to remove.</param>
		/// <returns><see langword="true"/> if the entry was removed, <see langword="false"/> if it wasn't present.</returns>
		public static bool Remove(string key)
		{
			return vehicles.TryRemove(key, out _);
		}

		/// <summary>
		/// Clear all data from the cache.
		/// Also causes <see cref="Definition"/>s to reload.
		/// </summary>
		public static void ClearCache()
		{
			vehicles.Clear();
			LoadDefinitions();
		}

		/// <summary>
		/// Remove the vehicles that have been updated so they will load again when requested.
		/// </summary>
		public static void ClearOutdated()
		{
			var buffer = new List<string>();
			foreach (var kvp in vehicles)
			{
				var key = kvp.Key;
				var record = kvp.Value;

				var info = new FileInfo(key);
				if (! info.Exists)
				{
					buffer.Add(key);
					continue;
				}

				// Updated since last read.
				if (info.LastWriteTime > record.LastWriteTime)
				{
					buffer.Add(key);
					continue;
				}

				// Failed, could be caused by definitions which are not tracked for changes here.
				if (record.Vehicle == null)
				{
					buffer.Add(key);
				}
			}

			foreach (string key in buffer)
			{
				vehicles.TryRemove(key, out _);
			}
		}


		/// <summary>
		/// Trigger all <see cref="VehicleLoader"/>s to reload their vehicle if it is outdated.
		/// </summary>
		public static void ReloadUpdatedVehicles()
		{
			ClearOutdated();
			foreach (var loader in allLoaders)
			{
				loader.StartLoadVehicle();
			}
		}

		/// <summary>
		/// Force-Reload all vehicles.
		/// </summary>
		public void ReloadAllVehicles()
		{
			ClearCache();
			foreach (var loader in allLoaders)
			{
				loader.loadedVehicleLastWriteTime = DateTime.MinValue;
				loader.StartLoadVehicle();
			}
		}

		#endregion Static
	}
}
