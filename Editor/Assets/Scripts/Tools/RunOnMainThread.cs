﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

using UnityEngine;
using UnityEditor;

using Behaviours;

using DataTypes.Extensions;

using Priority_Queue;

using Shared;

using Debug = UnityEngine.Debug;

namespace Tools
{
	[DisallowMultipleComponent]
	[DefaultExecutionOrder(ExecutionOrder.RunOnMainThread)] // Needs to initialize before others try to use it.
	public class RunOnMainThread : M0noBehaviour
	{
		[Range(0, 1000)]
		public int TimeAllotmentMilliseconds = 5;

		public bool LogExceedingAllotment = false;
		public float LogExceedingAllotmentAfterMilliseconds = 50;



		public TimeSpan WaitTimeout = TimeSpan.FromMinutes(5);


		private static CancellationTokenSource cancelSource;
		public static CancellationToken CancelToken => cancelSource.Token;


		// Defaults to true because Unity and threading and assembly reload.
		private static bool __cancelableTaskShouldCancel = true;

		private static bool _cancelableTasksShouldCancel
		{
			get => __cancelableTaskShouldCancel;
			set
			{
				__cancelableTaskShouldCancel = value;
				if(value) cancelSource.Cancel();
			}
		}

		public static bool CancelableTasksShouldCancel => _cancelableTasksShouldCancel;



		private Thread mainThread;

		private static RunOnMainThread _instance;
		public static RunOnMainThread instance
		{
			[DebuggerStepThrough]
			get
			{
				EnsureInitialized();

				return _instance;
			}
		}

		public static bool IsInstantiated => null != _instance;

		// Thread safe
		public SimplePriorityQueue<QueueItem> queue;

		public static IDictionary<float, int> CountsByPriority => _instance.queue.GetItemCountByPriority();

		public static int QueueCount
		{
			get
			{
				if (null != _instance && null != _instance.queue)
					return _instance.queue.Count;
				else
					return -1;
			}
		}


		protected int queueItemStackFramesToSkip = 2;
		protected bool queueItemStackTraceCaptureSourceInfo = true;

		#region ExceptionHandeling
		/// <summary>
		/// A handler that, when an exception is thrown by an <see cref="Action"/>
		/// will determine if the Exception is to be swallowed (return <see langword="true"/>) or rethrown (return <see langword="false"/>)
		/// If the <see cref="Exception"/> is rethrown the <see cref="Action"/>s still in the <see cref="ConcurrentQueue{Action}"/> will be discarded.
		/// If the <see cref="ExceptionHandler"/> is <see langword="null"/> exceptions will always be rethrown.
		/// The default is <see cref="SwallowAll"/>, which will swallow any <see cref="Exception"/> and log it to System.Console.Error.
		/// </summary>
		public Func<Exception, bool> ExceptionHandler { get; set; } = SwallowAllAndLogToConsole;


		/// <summary>
		/// The Default <see cref="ExceptionHandler"/> that will silently swallow any <see cref="Exception"/>.
		/// </summary>
		/// <param name="_">parameter ignored</param>
		/// <returns>always true</returns>
		public static bool SwallowAll(Exception _)
		{
			return true;
		}

		/// <summary>
		/// The Default <see cref="ExceptionHandler"/> that will swallow any <see cref="Exception"/> and log them to System.Console.Error
		/// </summary>
		/// <param name="_">parameter ignored</param>
		/// <returns>always true</returns>
		public static bool SwallowAllAndLogToConsole(Exception e)
		{
			Console.Error.WriteLine(e);
			return true;
		}

		#endregion ExceptionHandeling



		protected override void OnAwake()
		{
			if (_instance == null)
			{
				_instance = this;
			}
			_instance.mainThread = Thread.CurrentThread;
			queue ??= new SimplePriorityQueue<QueueItem>();


#if UNITY_EDITOR
			EditorApplication.playModeStateChanged -= EditorApplication_playModeStateChanged;
			EditorApplication.playModeStateChanged += EditorApplication_playModeStateChanged;
#endif
		}

		/// <inheritdoc />
		protected override void OnEnableAndAfterStart()
		{
			_cancelableTasksShouldCancel = false;

			// This shouldn't be needed again, but Unity.
			if (_instance == null)
			{
				_instance = this;
			}
			_instance.mainThread = Thread.CurrentThread;
			queue ??= new SimplePriorityQueue<QueueItem>();

			cancelSource = new CancellationTokenSource();


#if UNITY_EDITOR
			AssemblyReloadEvents.beforeAssemblyReload -= AssemblyReloadEvents_beforeAssemblyReload;
			AssemblyReloadEvents.beforeAssemblyReload += AssemblyReloadEvents_beforeAssemblyReload;

			AssemblyReloadEvents.afterAssemblyReload -= AssemblyReloadEvents_afterAssemblyReload;
			AssemblyReloadEvents.afterAssemblyReload += AssemblyReloadEvents_afterAssemblyReload;

		}


		private void AssemblyReloadEvents_beforeAssemblyReload()
		{
			// Actions can't be serialized so we HAVE TO run them all NOW or they will be destroyed.

			Console.WriteLine($"[{nameof(RunOnMainThread)}] AssemblyReload detected.");
			EnsureQueueEmpty();
		}
		private void AssemblyReloadEvents_afterAssemblyReload()
		{
			queue ??= new SimplePriorityQueue<QueueItem>();

			_instance = this;
		}

		/// <inheritdoc />
		protected override void OnDisableIfStarted()
		{
			AssemblyReloadEvents.beforeAssemblyReload -= AssemblyReloadEvents_beforeAssemblyReload;
		}

		private void EditorApplication_playModeStateChanged(PlayModeStateChange obj)
		{
			// This ensures that there are no tasks left running that might reference now-destroyed GameObjects or Components.


			if (obj == PlayModeStateChange.ExitingPlayMode)
			{
				StopService();
			}
		}
#else
	// Ugly AF, but it works, and it's less messy than #if EVERYWHERE
	}
#endif

		private void Update()
		{
			RunCycle();
		}

		/// <summary>
		/// Run a single cycle of queued actions. This will do the same as Update().
		/// </summary>
		public void RunCycle()
		{

			QueueItem lastItem = null;
			int count = 0;
			var timer = new Stopwatch();
			var lastItemStart = TimeSpan.Zero;
			timer.Start();
			do
			{
				if (queue.TryDequeue(out lastItem))
				{
					count++;
					lastItemStart = timer.Elapsed;
					try
					{
						lastItem.action.Invoke();
					}
					catch (Exception e)
					{
						Debug.LogException(e, this);
					}
				}
				else
				{
					break;
				}
			}
			while (timer.ElapsedMilliseconds < TimeAllotmentMilliseconds);

			timer.Stop();


			// Somehow we can exceed the timer just waiting for TryDequeue to give us nothing.
			if (null == lastItem) return;

			if (! (timer.ElapsedMilliseconds > LogExceedingAllotmentAfterMilliseconds))
				return;

			var lastItemTime = timer.Elapsed - lastItemStart;

			var message =
				$"[{nameof(RunOnMainThread)}] exceeded allotted time by {timer.ElapsedMilliseconds - TimeAllotmentMilliseconds}ms. ({count} items processed this tick)"
			  + $"\nLast item took {lastItemTime.Milliseconds} ms, caller stack trace:"
			  + $"\n{lastItem.callerStackTrace}";


			if (LogExceedingAllotment)
				Debug.Log(message, this);
			else
				Console.WriteLine(message);
		}

		private void OnDestroy()
		{
			StopService();
		}

		private void OnApplicationQuit()
		{
			StopService();
		}

		private void StopService()
		{
			if (queue == null) return;

			Console.WriteLine($"[{nameof(RunOnMainThread)}] Ending service, stopping cancelable tasks.");
			_cancelableTasksShouldCancel = true;
			EnsureQueueEmpty();
		}

		private void EnsureQueueEmpty()
		{
			if (null == _instance) return;
			if (null == queue) return;

			Console.WriteLine($"[{nameof(RunOnMainThread)}] Force-emptying queue, {queue.Count} remaining.");

			var timer = new Stopwatch();
			timer.Start();

			var messageInterval = TimeSpan.FromSeconds(1);
			var nextMessageTime = TimeSpan.Zero;

			while (queue.TryDequeue(out var item))
			{
				try
				{
					item.action.Invoke();
				}
				catch (Exception e)
				{
					Debug.LogException(e, this);
				}

				if (timer.Elapsed <= nextMessageTime)
					continue;

				nextMessageTime = timer.Elapsed + messageInterval;
				Console.WriteLine
					(
					 $"[{nameof(RunOnMainThread)}] Emptying queue... {timer.Elapsed.TotalSeconds}s elapsed, {queue.Count} items remaining."
					);
			}

			timer.Stop();

			Console.WriteLine($"[{nameof(RunOnMainThread)}] Force-emptied the queue in {timer.Elapsed.TotalSeconds}s");

			if (timer.Elapsed > TimeSpan.FromSeconds(10))
			{
				Debug.LogWarning($"[{nameof(RunOnMainThread)}] Emptying queue took a long time: {timer.Elapsed}", this);
			}

			//queue = null;
		}


		public static int PendingActions => instance.queue.Count;

		[DebuggerStepThrough]
		public static void EnsureInitialized()
		{
			if (null == _instance)
			{
				throw new InvalidOperationException($"{nameof(RunOnMainThread)} not initialized!");
			}
		}

		#region Enqueue

		/// <summary>
		/// Enqueue <paramref name="action"/> to run on the ASync thread and return immediately.
		/// Any uncaught Exceptions will be handled by <see cref="ExceptionHandler"/>.
		/// </summary>
		/// <param name="action"></param>
		/// <param name="priority">Priority, lower is executed sooner</param>
		public static void Enqueue(Action action, float priority = 0)
		{
			InternalEnqueue(action, priority);
		}

		private static void InternalEnqueue(Action action, float priority = 0, int stackFrameSkipAdjustment = 1)
		{
			instance.queue.Enqueue(new QueueItem(action, stackFrameSkipAdjustment), priority);
		}

		/// <summary>
		/// Enqueue <paramref name="action"/> to run on the ASync thread and wait for it to complete.
		/// </summary>
		/// <param name="action"></param>
		/// <param name="priority">Priority, lower is executed sooner</param>
		/// <exception cref="Exception">reThrows any Exception caused by the <paramref name="action"/></exception>
		public static void EnqueueAndWaitForCompletion(Action action, float priority = 0)
		{
			if (instance.mainThread == Thread.CurrentThread)
			{
				action.Invoke();
				return;
			}

			RunOnMainThreadException exception = null;
			var enqueueStackTrace = new StackTrace(instance.queueItemStackFramesToSkip, instance.queueItemStackTraceCaptureSourceInfo);
			using (var trigger = new ManualResetEventSlim())
			{
				void EnclosingAction()
				{
					try
					{
						action.Invoke();
					}
					catch (Exception e)
					{
						exception = new RunOnMainThreadException(e, enqueueStackTrace);
					}
					finally
					{
						trigger.Set();
					}
				}

				InternalEnqueue(EnclosingAction, priority);


				if (! trigger.Wait(instance.WaitTimeout))
				{
					throw new RunOnMainThreadException(new TimeoutException(), enqueueStackTrace);
				}

				ReflectionExt.ThrowIfException(exception);
			}
		}


		/// <summary>
		/// Enqueue <paramref name="func"/> to run on the ASync thread, wait for it to complete, and return the result.
		/// </summary>
		/// <param name="func"></param>
		/// <param name="priority">Priority, lower is executed sooner</param>
		/// <exception cref="Exception">reThrows any Exception caused by the <paramref name="func"/></exception>
		/// <returns>The result of <paramref name="func"/>, or <see cref="default"/></returns>
		public static TResult EnqueueAndWaitForCompletion<TResult>(Func<TResult> func, float priority = 0)
		{
			//if (thread == Thread.CurrentThread) throw new InvalidOperationException(exceptionMessage_WorkerThreadEnqueueWaitForSelf);

			TResult result = default;
			RunOnMainThreadException exception = null;
			var enqueueStackTrace = new StackTrace(instance.queueItemStackFramesToSkip, instance.queueItemStackTraceCaptureSourceInfo);

			if (instance.mainThread == Thread.CurrentThread)
				return _RunNow();
			else
				return _Enqueue();


			TResult _RunNow()
			{ // We are already running on the thread, so we can't wait for ourselves.
				try
				{
					result = func.Invoke();
				}
				catch (Exception e)
				{
					throw new RunOnMainThreadException(e, enqueueStackTrace);
				}
				return result;
			}

			TResult _Enqueue()
			{
				using (var trigger = new ManualResetEventSlim())
				{
					void EnclosingAction()
					{
						try
						{
							result = func.Invoke();
						}
						catch (Exception e)
						{
							exception = new RunOnMainThreadException(e, enqueueStackTrace);
						}
						finally
						{
							trigger.Set();
						}
					}

					InternalEnqueue(EnclosingAction, priority);

					if (!trigger.Wait(instance.WaitTimeout))
					{
						throw new RunOnMainThreadException(new TimeoutException(), enqueueStackTrace);
					}
					ReflectionExt.ThrowIfException(exception);
					return result;
				}
			}
		}

		public static void ThrowIfCanceled()
		{
			if (CancelableTasksShouldCancel) throw new OperationCanceledException();
		}

		#region Debug Log shorthands

		/// <summary>
		/// Short-hand for logging a message to <see cref="Debug.Log(object, UnityEngine.Object)"/> from a Task.
		/// (Because that doesn't work properly there)
		/// </summary>
		/// <param name="message"></param>
		/// <param name="context"></param>
		public static void Log(object message, UnityEngine.Object context = null)
		{
			InternalEnqueue(Action);
			void Action()
			{
				Debug.Log(message, context);
			}
		}

		/// <summary>
		/// Short-hand for logging a message to <see cref="Debug.LogWarning(object, UnityEngine.Object)"/> from a Task.
		/// (Because that doesn't work properly there)
		/// </summary>
		/// <param name="message"></param>
		/// <param name="context"></param>
		public static void LogWarning(object message, UnityEngine.Object context = null)
		{
			InternalEnqueue(Action);
			void Action()
			{
				Debug.LogWarning(message, context);
			}
		}

		/// <summary>
		/// Short-hand for logging a message to <see cref="Debug.LogError(object, UnityEngine.Object)"/> from a Task.
		/// (Because that doesn't work properly there)
		/// </summary>
		/// <param name="message"></param>
		/// <param name="context"></param>
		public static void LogError(object message, UnityEngine.Object context = null)
		{
			InternalEnqueue(Action);
			void Action()
			{
				Debug.LogError(message, context);
			}
		}


		/// <summary>
		/// Short-hand for logging an Exception to <see cref="Debug.LogException(Exception, UnityEngine.Object)"/> from a Task.
		/// (Because that doesn't work properly there)
		/// </summary>
		/// <param name="exception"></param>
		/// <param name="context"></param>
		public static void LogException(Exception exception, UnityEngine.Object context = null)
		{
			InternalEnqueue(Action);
			void Action()
			{
				Debug.LogException(exception, context);
			}
		}

		#endregion Debug Log shorthands


		#endregion Enqueue

		public class QueueItem
		{
			internal readonly Action action;
			internal readonly DateTime enqueueTime;
			internal readonly StackTrace callerStackTrace;

			internal QueueItem(Action action, int stackFramesToSkipAdjustment = 0)
			{
				this.action = action;
				enqueueTime = DateTime.UtcNow;
				callerStackTrace = new StackTrace(instance.queueItemStackFramesToSkip + stackFramesToSkipAdjustment
					, instance.queueItemStackTraceCaptureSourceInfo);
			}
		}
	}

	[Serializable]
	public class RunOnMainThreadException : Exception
	{
		public readonly StackTrace EnqueueStackTrace;

		public RunOnMainThreadException(Exception innerException, StackTrace stackTrace)
			: this(
				$"{innerException.GetType().Name} thrown during execution of Action on the Main/Unity Thread."
				, innerException
				, stackTrace)
		{

		}

		public RunOnMainThreadException(string message, Exception inner, StackTrace stackTrace)
			: base(message, inner)
		{
			EnqueueStackTrace = stackTrace;
		}

		protected RunOnMainThreadException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context) { }


		/// <inheritdoc />
		public override string ToString()
		{
			return this.ExceptionToString(
				description =>
				{
					description.AppendFormat(
						"{0}   Enqueue was called at -->{0}{1}   --- End of Enqueue stack trace ---{0}",
						Environment.NewLine, EnqueueStackTrace);
				});
		}
	}
}
