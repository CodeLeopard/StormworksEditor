﻿// Copyright 2022-2023 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Diagnostics;

using DataTypes.Extensions;

using UnityEngine;

using OpenTK.Mathematics;

using UQuaternion = UnityEngine.Quaternion;
using UVector2 = UnityEngine.Vector2;
using UVector3 = UnityEngine.Vector3;
using UVector4 = UnityEngine.Vector4;

using OQuaternion = OpenTK.Mathematics.Quaternion;
using OVector2 = OpenTK.Mathematics.Vector2;
using OVector3 = OpenTK.Mathematics.Vector3;
using OVector4 = OpenTK.Mathematics.Vector4;


using OMatrix3 = OpenTK.Mathematics.Matrix3;
using OMatrix4 = OpenTK.Mathematics.Matrix4;


namespace BinaryDataModel.Converters
{
	public static class StructConversions
	{
		#region ToUnity

		[DebuggerStepThrough]
		public static UVector2 ToUnity(this OVector2 vec)
		{
			return new UVector2(vec.X, vec.Y);
		}

		[DebuggerStepThrough]
		public static UVector3 ToUnity(this OVector3 vec)
		{
			return new UVector3(vec.X, vec.Y, vec.Z);
		}

		[DebuggerStepThrough]
		public static UVector4 ToUnity(this OVector4 vec)
		{
			return new UVector4(vec.X, vec.Y, vec.Z, vec.W);
		}

		[DebuggerStepThrough]
		public static Vector2Int ToUnity(this Vector2i vec)
		{
			return new Vector2Int(vec.X, vec.Y);
		}

		[DebuggerStepThrough]
		public static Vector3Int ToUnity(this Vector3i vec)
		{
			return new Vector3Int(vec.X, vec.Y, vec.Z);
		}

		[DebuggerStepThrough]
		public static UVector4 ToUnity(this Vector4i vec)
		{
			return new UVector4(vec.X, vec.Y, vec.Z, vec.W);
		}

		[DebuggerStepThrough]
		public static Color ToUnity(this Color4 col)
		{
			return new Color(col.R, col.G, col.B, col.A);
		}

		/// <summary>
		/// RGB <see cref="OVector3"/> to <see cref="Color"/>.
		/// </summary>
		/// <param name="col"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public static Color ToUnityColor(this OVector3 col, float alpha = 1f)
		{
			return new Color(col.X, col.Y, col.Z, alpha);
		}

		/// <summary>
		/// RGBA <see cref="OVector4"/> to <see cref="Color"/>.
		/// </summary>
		/// <param name="col"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public static Color ToUnityColor(this OVector4 col)
		{
			return new Color(col.X, col.Y, col.Z, col.W);
		}

		/// <summary>
		/// RGBA <see cref="OVector4"/> to <see cref="Color32"/>.
		/// </summary>
		/// <param name="col"></param>
		/// <returns></returns>
		[DebuggerStepThrough]
		public static Color32 ToUnityColor32(this OVector4 col)
		{
			col *= 255f;
			return new Color32((byte)col.X, (byte)col.Y, (byte)col.Z, (byte)col.W);
		}

		[DebuggerStepThrough]
		public static UQuaternion ToUnity(this OQuaternion q)
		{
			return new UQuaternion(q.X, q.Y, q.Z, q.W);
		}

		[Obsolete("Does not properly convert because Unity and OpenTK use Column- and Row-Major respectively.")]
		[DebuggerStepThrough]
		public static Matrix4x4 ToUnity(this OMatrix4 m)
		{
			return new Matrix4x4(m.Column0.ToUnity(), m.Column1.ToUnity(), m.Column2.ToUnity(), m.Column3.ToUnity());
		}

		public static void SetDataOn(this OMatrix4 m, Transform target, bool local = false)
		{
			var position = m.ExtractTranslation().ToUnity();
			var scale    = m.ExtractScale().ToUnity();
			var rotation = m.ExtractRotation().ToUnity();
			if (local)
			{
				target.localPosition = position;
				target.localScale    = scale;
				target.localRotation = rotation;
			}
			else
			{
				target.position   = position;
				target.localScale = scale;
				target.rotation   = rotation;
			}
		}

		#endregion ToUnity


		#region ToOpenTK

		[DebuggerStepThrough]
		public static Vector2i ToOpenTK(this Vector2Int v)
		{
			return new Vector2i(v.x, v.y);
		}


		[DebuggerStepThrough]
		public static Vector3i ToOpenTK(this Vector3Int v)
		{
			return new Vector3i(v.x, v.y, v.z);
		}


		[DebuggerStepThrough]
		public static OVector2 ToOpenTK(this UVector2 v)
		{
			return new OVector2(v.x, v.y);
		}


		[DebuggerStepThrough]
		public static OVector3 ToOpenTK(this UVector3 v)
		{
			return new OVector3(v.x, v.y, v.z);
		}

		[DebuggerStepThrough]
		public static OVector4 ToOpenTK(this UVector4 v)
		{
			return new OVector4(v.x, v.y, v.z, v.w);
		}

		[DebuggerStepThrough]
		public static OVector3 ToOpenTKVectorRGB(this Color32 c)
		{
			return new OVector3(
				(float)c.r / (float)byte.MaxValue,
				(float)c.g / (float)byte.MaxValue,
				(float)c.b / (float)byte.MaxValue);
		}

		[DebuggerStepThrough]
		public static OVector3 ToOpenTKVectorRGB(this Color c)
		{
			return new OVector3(c.r, c.g, c.b);
		}

		[DebuggerStepThrough]
		public static OVector4 ToOpenTKVectorRGBA(this Color32 c)
		{
			return new OVector4(
				(float)c.r / (float)byte.MaxValue,
				(float)c.g / (float)byte.MaxValue,
				(float)c.b / (float)byte.MaxValue,
				(float)c.a / (float)byte.MaxValue);
		}

		[DebuggerStepThrough]
		public static OVector4 ToOpenTKVectorRGBA(this Color c)
		{
			return new OVector4(c.r, c.g, c.b, c.a);
		}

		[DebuggerStepThrough]
		public static OQuaternion ToOpenTK(this UQuaternion q)
		{
			return new OQuaternion(q.x, q.y, q.z, q.w);
		}

		[Obsolete("Does not properly convert because Unity and OpenTK use Column- and Row-Major respectively.")]
		public static OMatrix4 ToOpenTK(this Matrix4x4 m)
		{
			var result = OMatrix4.Zero;
			result.Column0 = m.GetColumn(0).ToOpenTK();
			result.Column1 = m.GetColumn(1).ToOpenTK();
			result.Column2 = m.GetColumn(2).ToOpenTK();
			result.Column3 = m.GetColumn(3).ToOpenTK();
			return result;
		}

		public static OMatrix4 ToOpenTK(this Transform t, bool local = false, bool scale = true)
		{
			OVector3 positionValue;
			OVector3 scaleValue;
			OQuaternion rotationValue;

			if (local)
			{
				positionValue = t.localPosition.ToOpenTK();
				scaleValue = t.localScale.ToOpenTK();
				rotationValue = t.localRotation.ToOpenTK();
			}
			else
			{
				positionValue = t.position.ToOpenTK();
				scaleValue = t.lossyScale.ToOpenTK();
				rotationValue = t.rotation.ToOpenTK();
			}

			if (! scale)
			{
				scaleValue = OVector3.One;
			}

			return Matrix4Ext.CreateTransScaleRot(positionValue, scaleValue, rotationValue);
		}

		#endregion ToOpenTK
	}
}
