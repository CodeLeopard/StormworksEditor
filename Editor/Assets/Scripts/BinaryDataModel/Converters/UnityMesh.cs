﻿// Copyright 2022-2025 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

using UnityEngine;

using Tools;

using UnityEngine.Rendering;

using BinaryDataModel.DataTypes;

using Shared;
using Shared.Exceptions;

using SMesh = BinaryDataModel.DataTypes.Mesh;
using UMesh = UnityEngine.Mesh;
using Shader = BinaryDataModel.DataTypes.Enums.Shader;
using Vector3 = UnityEngine.Vector3;


namespace BinaryDataModel.Converters
{
	public static class UnityMesh
	{
		/// <summary>
		/// Convert to a Stormworks Mesh format.
		/// </summary>
		/// <param name="_data"></param>
		/// <returns></returns>
		public static SMesh ToSWMesh(this UMesh _data)
		{
			RunOnMainThread.ThrowIfCanceled();
			var (data_vertices, data_colors, data_normals, data_subMeshes, data_triangleIndices)
				= RunOnMainThread.EnqueueAndWaitForCompletion(GetUnityData);

			(Vector3[] vertices, Color[] colors, Vector3[] normals, SubMeshDescriptor[] submeshes, int[] triangleIndices) GetUnityData()
			{
				RunOnMainThread.ThrowIfCanceled();
				var __subMeshes = new SubMeshDescriptor[_data.subMeshCount];
				for (int i = 0; i < __subMeshes.Length; i++)
				{
					__subMeshes[i] = _data.GetSubMesh(i);
				}
				return (_data.vertices, _data.colors, _data.normals, __subMeshes, _data.triangles);
			}

			RunOnMainThread.ThrowIfCanceled();
			var vertexRecords = new VertexRecord[data_vertices.Length];

			var globalBounds = new Shared.Bounds3(true);

			bool computeGlobalBounds = data_subMeshes.Length == 1;

			for (int i = 0; i < vertexRecords.Length; i++)
			{
				var v = new VertexRecord();
				v.position = data_vertices[i].ToOpenTK();

				if (computeGlobalBounds)
				{
					var p = v.position;

					globalBounds.Inflate(p);
				}

				{
					var c = data_colors[i];
					var r = c.r;
					var g = c.g;
					var b = c.b;
					var a = c.a;
					v.color = new OpenTK.Mathematics.Color4(r, g, b, a);
				}
				v.normal = data_normals[i].ToOpenTK();
				vertexRecords[i] = v;
			}

			RunOnMainThread.ThrowIfCanceled();
			var indices = new List<UInt32>();
			var subMeshes = new SubMesh[data_subMeshes.Length];
			for (int i = 0; i < subMeshes.Length; i++)
			{
				var smd = data_subMeshes[i];


				for (int j = smd.indexStart; j < smd.indexStart;)
				{
					indices.Add((UInt32) data_triangleIndices[j + 2]);
					indices.Add((UInt32) data_triangleIndices[j + 1]);
					indices.Add((UInt32) data_triangleIndices[j + 0]);

					j += 3;
				}

				if (data_subMeshes.Length == 1)
				{
					subMeshes[i] = new SubMesh("ID0", (UInt32)smd.indexStart, (UInt32)smd.indexCount, globalBounds);
				}
				else
				{
					var localBounds = new Shared.Bounds3(true);

					uint indexStart = (UInt32) smd.indexStart;
					uint indexCount = (UInt32) smd.indexCount;

					for (uint k = indexStart; k < indexStart + indexCount; k++)
					{
						var v = vertexRecords[k];
						var p = v.position;
						localBounds.Inflate(p);
					}

					subMeshes[i] = new SubMesh($"ID{i}", indexStart, indexCount, localBounds);
				}
			}

			return new SMesh(vertexRecords, indices.ToArray(), subMeshes);
		}


		[Flags]
		public enum MeshConvertOptions
		{
			None             = 0
		  , GenerateNormals  = 1 << 0
		  , GenerateTangents = 1 << 1
		  , GenerateBounds   = 1 << 2
		  , Optimize         = 1 << 3
		  , GenerateNormalsAndTangents = GenerateNormals | GenerateTangents
		  , Default = Optimize
		}


		private static readonly LazyCache<(SMesh, MeshConvertOptions), MeshWithMaterialInfo> mesh =
			new LazyCache<(SMesh, MeshConvertOptions), MeshWithMaterialInfo>(CreateUnityMesh);

		// In the case of phys we HAVE TO use the LazyCache because converting to Indexed triangles
		// can cause race conditions.
		private static readonly LazyCache<Phys, UMesh> phys
			= new LazyCache<Phys, UMesh>(CreateUnityMesh);

		public class MeshWithMaterialInfo
		{
			public readonly UMesh Mesh;
			public readonly IReadOnlyCollection<Material> Materials;

			internal MeshWithMaterialInfo(UMesh mesh, List<Material> materials)
			{
				Mesh = mesh;
				this.Materials = new ReadOnlyCollection<Material>(materials);
			}
		}

		private static MeshWithMaterialInfo CreateUnityMesh((SMesh data, MeshConvertOptions options) arguments)
		{
			var data = arguments.data;
			var options = arguments.options;
			try
			{
				RunOnMainThread.ThrowIfCanceled();
				var vertices = new Vector3[data.vertices.Count];
				var colors = new Color32[data.vertices.Count];
				Vector3[] normals = null;

				bool generateNormals = options.HasFlag(MeshConvertOptions.GenerateNormals);

				if (!generateNormals) normals = new Vector3[data.vertices.Count];

				for (int i = 0; i < vertices.Length; i++)
				{
					var r = data.vertices[i];
					vertices[i] = r.position.ToUnity();
					colors[i] = r.color.ToUnity();
					//todo: transparency, either by identifying magic colours, or by using submesh shader id

					if (!generateNormals) normals[i] = r.normal.ToUnity();
				}

				RunOnMainThread.ThrowIfCanceled();
				var m = RunOnMainThread.EnqueueAndWaitForCompletion(SetVertexData);

				UMesh SetVertexData()
				{
					RunOnMainThread.ThrowIfCanceled();
					var _m = new UMesh();
					_m.name = data.fileName ?? "<generated_unknown>";

					_m.SetVertices(vertices);
					_m.SetColors(colors);

					if (!options.HasFlag(MeshConvertOptions.GenerateNormals)) _m.SetNormals(normals);
					return _m;
				}

				UInt32[] indices32 = new UInt32[data.indexCount];
				{
					for (int i = 0; i < indices32.Length;)
					{
						UInt32 a = data.indices[i + 0];
						UInt32 b = data.indices[i + 1];
						UInt32 c = data.indices[i + 2];

						// Reversed order for Unity
						indices32[i + 0] = c;
						indices32[i + 1] = b;
						indices32[i + 2] = a;

						i += 3;
					}
				}


				var materials = new List<Material>();

				var result = new MeshWithMaterialInfo(m, materials);
				RunOnMainThread.ThrowIfCanceled();
				RunOnMainThread.EnqueueAndWaitForCompletion(Finalize);

				void Finalize()
				{
					RunOnMainThread.ThrowIfCanceled();
					var format = IndexFormat.UInt32;
					m.SetIndexBufferParams(data.indices.Count, format);
					m.SetIndexBufferData(indices32, 0, 0, indices32.Length);

					m.subMeshCount = data.subMeshes.Count;
					for (int i = 0; i < data.subMeshes.Count; i++)
					{
						var subMesh = data.subMeshes[i];
						var start = subMesh.indexBufferStart;
						var len = subMesh.indexBufferLength;

						var us = new SubMeshDescriptor();
						us.indexStart = (int) start;
						us.indexCount = (int) len;
						us.topology = MeshTopology.Triangles;

						us.bounds = new Bounds(subMesh.bounds.Center.ToUnity(), subMesh.bounds.Size.ToUnity());

						m.SetSubMesh(i, us, MeshUpdateFlags.DontRecalculateBounds);

						switch (subMesh.shaderId)
						{
							case Shader.Opaque:
							{
								materials.Add(Globals.Materials.VertexColorOpaque);
								break;
							}
							case Shader.Transparent:
							{
								materials.Add(Globals.Materials.VertexColorTransparent);
								break;
							}
							case Shader.Emissive:
							{
								materials.Add(Globals.Materials.VertexColorEmissive);
								break;
							}
							case Shader.Lava:
							{
								materials.Add(Globals.Materials.VertexColorLava);
								break;
							}
							default:
							{
								throw new InvalidOperationException
									($"unexpected enum value '{subMesh.shaderId}' for {nameof(subMesh.shaderId)}");
							}
						}
					}

					if (options.HasFlag(MeshConvertOptions.GenerateNormals))
					{
						m.RecalculateNormals();
					}

					if (options.HasFlag(MeshConvertOptions.GenerateNormals)
					 || options.HasFlag(MeshConvertOptions.GenerateTangents))
					{
						m.RecalculateTangents();
					}

					if (options.HasFlag(MeshConvertOptions.Optimize))
					{
						//m.Optimize();
					}

					m.UploadMeshData(false);
				}

				return result;
			}
			catch (OperationCanceledException)
			{
				throw;
			}
			catch (Exception e)
			{
				throw new FileInteractionException(data.fileName, e);
			}
		}

		private static UMesh CreateUnityMesh(Phys data)
		{
			try
			{
				RunOnMainThread.ThrowIfCanceled();
				var d = data;

				var vertices = new Vector3[d.subMeshes.Aggregate(0, (a, sm) => a + sm.vertices.Count)];
				Vector3[] normals = new Vector3[vertices.Length];

				data.ConvertToIndexedTriangles();
				var indices = new Int32[d.subMeshes.Aggregate(0, (a, sm) => a + sm.indices.Count)];

				int subMeshVertexOffset = 0;
				int vertexIndex = 0;
				int indiceIndex = 0;


				foreach (var phySMesh in d.subMeshes)
				{
					for (int i = 0; i < phySMesh.vertices.Count;)
					{
						vertices[vertexIndex++] = phySMesh.vertices[i++].ToUnity();
					}

					for (int i = 0; i < phySMesh.indices.Count;)
					{
						var a = phySMesh.indices[i + 0];
						var b = phySMesh.indices[i + 1];
						var c = phySMesh.indices[i + 2];

						// Reversed order for Unity
						indices[indiceIndex + 0] = subMeshVertexOffset + unchecked((Int32)c);
						indices[indiceIndex + 1] = subMeshVertexOffset + unchecked((Int32)b);
						indices[indiceIndex + 2] = subMeshVertexOffset + unchecked((Int32)a);
						i += 3;
						indiceIndex += 3;
					}

					subMeshVertexOffset += phySMesh.vertexCount;
				}
				RunOnMainThread.ThrowIfCanceled();
				UMesh result = RunOnMainThread.EnqueueAndWaitForCompletion(LoadMeshData);
				UnityEngine.Mesh LoadMeshData()
				{
					RunOnMainThread.ThrowIfCanceled();
					var _m = new UMesh();
					_m.name = data.fileName ?? "<generated_unknown>";
					if (vertices.Length > UInt16.MaxValue)
					{
						// For big mesh (Generated/combined)
						_m.indexFormat = IndexFormat.UInt32;
					}

					_m.SetVertices(vertices);
					_m.SetNormals(normals);
					_m.SetIndices(indices, MeshTopology.Triangles, 0);
					_m.Optimize();
					_m.UploadMeshData(false);

					return _m;
				}

				
				return result;
			}
			catch (OperationCanceledException)
			{
				throw;
			}
			catch (Exception e)
			{
				throw new FileInteractionException(data.fileName, e);
			}
		}

		/// <summary>
		/// Convert a Stormworks <see cref="SMesh"/> to a Unity <see cref="UMesh"/>.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="options"></param>
		/// <returns></returns>
		public static MeshWithMaterialInfo ToUnityMesh(this SMesh data, MeshConvertOptions options = MeshConvertOptions.Default)
		{
			return mesh.GetOrAdd((data, options));
		}

		/// <summary>
		/// Convert a Stormworks <see cref="Phys"/> to a Unity <see cref="UMesh"/>.
		/// Note: this is a lossy operation!
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public static UMesh ToUnityMesh(this Phys data)
		{
			return phys.GetOrAdd(data);
		}

		/// <summary>
		/// Remove all results for <paramref name="key"/> so that they will be generated again if required later.
		/// </summary>
		/// <param name="key"></param>
		public static int Remove(SMesh key)
		{
			int counter = 0;
			foreach ((SMesh, MeshConvertOptions) tuple in mesh.Keys.ToArray())
			{
				if (tuple.Item1 == key)
				{
					if (mesh.TryRemove(tuple, out _))
					{
						counter++;
					}
				}
			}

			return counter;
		}

		/// <summary>
		/// Remove all results for <paramref name="key"/> so that they will be generated again if required later.
		/// </summary>
		/// <param name="key"></param>
		public static bool Remove(SMesh key, MeshConvertOptions options)
		{
			return mesh.TryRemove((key, options), out _);
		}

		/// <summary>
		/// Remove all results for <paramref name="key"/> so that they will be generated again if required later.
		/// </summary>
		/// <param name="key"></param>
		public static bool Remove(Phys key)
		{
			return phys.TryRemove(key, out _);
		}

		public static MeshWithMaterialInfo LoadMesh(string path)
		{
			var meshData = MeshCache.GetOrLoadMesh(path);

			return ToUnityMesh(meshData);
		}

		public static UMesh LoadPhys(string path)
		{
			var meshData = MeshCache.GetOrLoadPhys(path);

			return ToUnityMesh(meshData);
		}
	}
}
