﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections.Concurrent;
using System.Threading;

using BinaryDataModel.Converters;
using BinaryDataModel.DataTypes;

namespace BinaryDataModel
{
	/// <summary>
	/// Helper to keep track of generated/Converted <see cref="PhyMesh"/>es.
	/// </summary>
	public class PhysGenerator
	{
		public static readonly PhysGenerator Instance = new PhysGenerator();

		private readonly ConcurrentDictionary<Key, Lazy<Phys>> phys = new ConcurrentDictionary<Key, Lazy<Phys>>();

		public Phys GetOrCreate(string meshRomPath)
		{
			return phys.GetOrAdd(new Key(meshRomPath), CreateEntry).Value;
		}

		private Lazy<Phys> CreateEntry(Key key)
		{
			return new Lazy<Phys>(() => Create(key), LazyThreadSafetyMode.ExecutionAndPublication);
		}

		private Phys Create(Key key)
		{
			var mesh = MeshCache.GetOrLoadMesh(key.MeshRomPath);

			var result = Binary.ConvertToVertexOnly(mesh);

			return result;
		}


		private readonly struct Key : IEquatable<Key>
		{
			internal readonly string MeshRomPath;

			public Key(string meshRomPath)
			{
				MeshRomPath = meshRomPath ?? throw new ArgumentNullException(nameof(meshRomPath));
			}

			public override bool Equals(object obj)
			{
				return obj is Key key &&
					   MeshRomPath == key.MeshRomPath;
			}

			public bool Equals(Key other)
			{
				return MeshRomPath   == other.MeshRomPath;
			}

			/// <inheritdoc />
			public override int GetHashCode()
			{
				unchecked
				{
					return (MeshRomPath != null ? MeshRomPath.GetHashCode() : 0) * 397;
				}
			}
		}
	}
}
