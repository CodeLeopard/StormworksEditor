﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections.Concurrent;
using System.Threading;

using OpenTK.Mathematics;

using BinaryDataModel.DataTypes;

namespace BinaryDataModel
{
	/// <summary>
	/// Helper to keep track of transformed Physics Meshes.
	/// </summary>
	public class PhysTransformer
	{
		public static readonly PhysTransformer Instance = new PhysTransformer();

		private readonly ConcurrentDictionary<Key, Lazy<Phys>> map = new ConcurrentDictionary<Key, Lazy<Phys>>();


		public Phys GetOrCreate(Key key)
		{
			return map.GetOrAdd(key, CreateEntry).Value;
		}


		private Lazy<Phys> CreateEntry(Key key)
		{
			return new Lazy<Phys>(() => CreateMesh(key), LazyThreadSafetyMode.ExecutionAndPublication);
		}

		private Phys CreateMesh(Key key)
		{
			var transformed = MeshCombiner.Transform(key.OriginalMesh, key.Transformation);
			return transformed;
		}

		public readonly struct Key : IEquatable<Key>
		{
			public readonly Phys OriginalMesh;
			public readonly Matrix4 Transformation;

			public Key(Phys originalMesh, Matrix4 transformation)
			{
				OriginalMesh = originalMesh ?? throw new ArgumentNullException(nameof(originalMesh));
				Transformation = transformation;
			}


			/// <inheritdoc />
			public bool Equals(Key other)
			{
				return Transformation.Equals(other.Transformation)
					&& OriginalMesh == other.OriginalMesh;
			}

			/// <inheritdoc />
			public override bool Equals(object obj)
			{
				return obj is Key other && Equals(other);
			}

			/// <inheritdoc />
			public override int GetHashCode()
			{
				unchecked
				{
					return (Transformation.GetHashCode() * 397)
					     ^ (OriginalMesh != null ? OriginalMesh.GetHashCode() : 0);
				}
			}
		}
	}
}
