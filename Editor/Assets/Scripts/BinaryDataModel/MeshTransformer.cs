﻿// Copyright 2022 CodeLeopard
// License: LGPL-3.0-or-later

/* This Program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

The Program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along
with the Program. If not, see <https://www.gnu.org/licenses/>.
*/







using System;
using System.Collections.Concurrent;
using System.Threading;

using BinaryDataModel.DataTypes;

using OpenTK.Mathematics;

namespace BinaryDataModel
{
	/// <summary>
	/// Helper to keep track of transformed Meshes.
	/// </summary>
	public class MeshTransformer
	{
		public static readonly MeshTransformer Instance = new MeshTransformer();

		private readonly ConcurrentDictionary<Key, Lazy<Mesh>> map = new ConcurrentDictionary<Key, Lazy<Mesh>>();


		public Mesh GetOrCreate(Key key)
		{
			return map.GetOrAdd(key, CreateEntry).Value;
		}


		private Lazy<Mesh> CreateEntry(Key key)
		{
			return new Lazy<Mesh>(() => CreateMesh(key), LazyThreadSafetyMode.ExecutionAndPublication);
		}

		private Mesh CreateMesh(Key key)
		{
			var transformed = MeshCombiner.Transform(key.OriginalMesh, key.Transformation);
			return transformed;
		}

		public readonly struct Key : IEquatable<Key>
		{
			public readonly Mesh OriginalMesh;
			public readonly Matrix4 Transformation;

			public Key(Mesh originalMesh, Matrix4 transformation)
			{
				OriginalMesh = originalMesh ?? throw new ArgumentNullException(nameof(originalMesh));
				Transformation = transformation;
			}


			/// <inheritdoc />
			public bool Equals(Key other)
			{
				return Transformation.Equals(other.Transformation)
					&& OriginalMesh == other.OriginalMesh;
			}

			/// <inheritdoc />
			public override bool Equals(object obj)
			{
				return obj is Key other && Equals(other);
			}

			/// <inheritdoc />
			public override int GetHashCode()
			{
				unchecked
				{
					return (Transformation.GetHashCode() * 397)
						 ^ (OriginalMesh != null ? OriginalMesh.GetHashCode() : 0);
				}
			}
		}
	}
}
