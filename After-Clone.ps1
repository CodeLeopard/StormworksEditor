# This script will setup the repository after it was first cloned.

# Get and update all submodules.
git submodule update --init

# Restore nuget packages.
./NugetRestore_Raw.ps1

# Build and fetch the DataModel library project.
./FetchDataModel.ps1

# You can now open Unity.
