# Install nuget packages when Unity can't.
# For example when the repository is first cloned and Unity can't compile because the nuget packages are missing.
# Will attempt to install NuGet if it is not found.



param(
	[Switch]$Force # Delete the packages to force nuget to restore them properly.
)


# Tell powershell to behave like a normal program and stop when something bad happens.
$ErrorActionPreference = "Stop"


# regex: match both 'netstandard1.0' and 'netstandard2.0'.

$frameworks = @("netstandard2.1", "netstandard2.0", "netstandard1.0", "net462" )
$platforms  = @("win-x64")

$currentFramework = "netstandard2.\d"



# Check if a command or program exists.
function Check-Command($cmdname)
{
	return [bool](Get-Command -Name $cmdname -ErrorAction SilentlyContinue)
}

function removeExceptPreferred([System.String] $root, [System.String[]]$preferred) {
	Write-Host "Root: $root"
	$all = Get-ChildItem -Directory -Path "$root" | Split-Path -Leaf

	Write-Host $all


	foreach ($_ in $preferred) {
		Write-Host "Looking for: $_"
		if($all -contains $_) {
			Write-Host "Match, deleting all but $_"
			$not_delete = $_
			$all | Where-Object { $_ -ne $not_delete } | ForEach-Object { Write-Host -ForegroundColor Red "$root/$_"; "$root/$_" } | Remove-Item -Recurse -ErrorAction SilentlyContinue
			return $true
		}
	}
	$false
}




# We need to run nuget in a specific directory, but should not leave the terminal there when we are done.
$old_Location = Get-Location
try
{
	Set-Location "$PSSCriptRoot/Editor/Assets"

	# If nuget is not installed then install it.
	if( -not (Check-Command('nuget'))) {
		Write-Host "Nuget not found, installing it..."
		if ( -not (Check-command('winget'))) {
			Write-Error "Unable to install NuGet using the windows package manager (winget), you will need to install nuget manually."
			exit 1
		}

		# Install NuGet
		winget install Microsoft.NuGet

		# Update the Environment variables so that nuget will be found.
		$env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path","User") 
	}



	# Remove the packages to force Nuget to *really* install them, instead of being lazy and just checking they are there.
	if($Force -eq $true) {
		Get-ChildItem -Directory -Path "$PSSCriptRoot/Editor/Assets/Packages"  | Remove-Item -Recurse -Force
	}


	Write-Host "Running nuget restore..."
	# This will use the config files:
	# - Editor/Assets/NuGet.config
	# - Editor/Assets/packages.config
	& nuget restore

	# Packages will be installed into Editor/Assets/Packages
	# Nuget can't properly install everything though so there is also the Packages2 folder.
	
	#Write-host "Press enter"
	#Read-Host

	# Fix Nuget being an ass and downloading all possible platforms and configurations.
	# Causing Unity's wonky build system to throw a fit and refuse to build.
	Write-Host "Fixing nuget's restore..."

	# Loop over the package folders:
	Get-ChildItem -Directory -Path "$PSSCriptRoot/Editor/Assets/Packages" `
	| ForEach-Object `
	{
		Write-Host "Fixing $_"

		$found = removeExceptPreferred "$_/Lib" $frameworks
		if (-not $found) {
			Write-Host -ForeGroundColor Red "Ree $_"
		}

		# If the runtimes folder exists:
		if (Test-Path "$_/runtimes") {
			# Do the same as above but for the runtimes.
			removeExceptPreferred "$_/runtimes" $platforms
		}

		# Remove the build folder, if it exists.
		Remove-Item "$_/build" -Recurse -Force -ErrorAction SilentlyContinue
	}
}
finally
{
	Set-Location $old_Location
}