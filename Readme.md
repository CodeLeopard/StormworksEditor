# StormworksEditor

An unofficial 3D Map and Mission Editor for the game Stormworks built in C# with Unity.

# Disclaimer

This is third party software and in no way affiliated to Stormworks or it's developers.
To use the Editor program you need to have the game [Stormworks](https://store.steampowered.com/app/573090) installed.

# About

This work-in-progress project aims to provide a better experience than the in-game Mission Editor as well as World editing tools.

Current major features include showing the entire world at once, a better gizmo to interact with objects and a tool to generate smooth and beautiful train tracks.

Some of the capabilities of the Editor are also available on the command line, see the submodule [Data](https://gitlab.com/CodeLeopard/StormworksData).

![](./images/MapOverview.png)

# Installation
Download the zip file for your operating system from the [deployments → releases page](https://gitlab.com/CodeLeopard/StormworksEditor/-/releases) the archives are near the bottom of the entry. Extract it to a location of choice and run `StormworksEditor.exe`. You may need to go through Windows SmartScreen, select "more info" and then a button "Run anyway" will appear.


Linux: The program should work on Linux, but it has not been tested and support for Linux may be limited. Automatic detection of the Stormworks installation is based on guesses, if it is not found you can set it manually in the settings.



# Usage
The primary goal of the Editor is to be able to view and edit the entire world of Stormworks at once. 



## Controls
Basic controls:
- `Left mouse` select object, to select terrain enable the `Edit Terrain` checkbox in the edit menu.
- `q` deselect gizmo.
- `w` select move gizmo.
- `e` select rotate gizmo.
- `r` select scale gizmo.
- `t` select raycast gizmo (click and drag to move object along the ground).
- `y` toggle between local space and global space for gizmo (changes the directions of the arrows, only visible when an object is rotated).
- `c` delete selected object.
- `f` place object (a menu will appear where you can select a .mesh file).
- `g` place object (a menu will appear where you can select the type of object to place).
- `Alt` + `left mouse` create spline, hold mouse down to drag. If a node is selected the new node will be connected to it.



There are multiple camera control schemes, you can cycle through by pressing `o`.
### Orbit camera
The orbit camera follows and looks at a focus point that you can move by right-clicking with the mouse. To rotate the camera holding the middle mouse button and dragging. Use the scroll wheel to zoom in and out.

### Fly camera
- `wasdqe` to move.
- `Shift` to move faster.
- `scroll` to change base speed.
### Orthographic camera
Cannot be controlled right now, it exists for the tile screenshot feature in the `Other` menu.


## Basic operation
When the program starts you can select the Stormworks installation to open. It is **strongly** recommended that you make a copy so that Steam does not delete your work when the game updates. The Steam installation should be detected automatically, and you can add additional installs using the panel on the bottom. To open the Editor press the button on the stormworks install.

It may take a moment for the world to load in. Note that not all tiles that you can see will show up in the game.

When you select an object you can see and edit its properties in the sidepanel on the right.



### Splines & Train tracks
Both the nodes and the curve objects have relevant properties in the right sidepanel.
Note that for nodes most properties are locked unless `AutoSmooth` is disabled. Ignore the scale in the Transform of a node, to change the scale of the curve use the property scale further down.

For a curve the SmoothingWeight applies only when using AutoSmooth on the nodes.

#### CurveSpec
The Curvespec property of a curve defines what the curve looks like (and the physics shape). The value should be a file path relative to `~Stormworks_instal~/rom/creatorToolkitData/CurveSpec` without file name extension. The Editor will look for a file `path.lua` there to load the definition from. The definitions themselves are very flexible, but also complicated. I'm working on a separate page with documentation for the format. In the meantime you can look [here](https://gitlab.com/CodeLeopard/StormworksData/-/tree/LuaIntegraion_ProvidedLibs/LuaIntegration/LuaResources) for some (potentially outdated) example code. Note also that the specification is subject to potentially breaking changes!

When a curve looks like a rainbow tube it means that the path is not found. If it is a red and yellow tube an error ocurred with the spec, see the log file for details. `About → LogFile`. You can also use the CommandLine from the [Data](https://gitlab.com/CodeLeopard/StormworksData) project to validate the CurveSpec.

#### Known issues
The game does some processing on the track XML meaning that what you specify in the XML is not necessarily what the game actually does, this can cause junctions to appear in unexpected places. There is code to try to fix this, but it does not catch all cases.

### Saving
To save your changes use the file menu. Note: If an error occurs a message will appear, you can inspect the log file to find out what went wrong.

## Repository Setup
Run this PowerShell script to perform the first time setup: `./After-Clone.ps1`.
Start Unity with the `~repository_root~/Editor` subfolder as the project folder.

## Building the project
In the UnityEditor use the `Build → Publish` menu item. The result will be placed at `~repository_root~/Editor/Build/Publish`.


# License
`LGPL-3.0-or-later` See [COPYING](https://gitlab.com/CodeLeopard/StormworksEditor/-/blob/develop/COPYING) and [COPYING.LESSER](https://gitlab.com/CodeLeopard/StormworksEditor/-/blob/develop/COPYING.LESSER)