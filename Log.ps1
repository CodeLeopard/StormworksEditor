# Open the UnityEditor log file.
Start-Process "$HOME\AppData\Local\Unity\Editor\Editor.log"

# Wait for the program to start, so that the next file opens in a tab.
Start-Sleep -Seconds 1

# Open the UnityPlayer log file.
Start-Process "$HOME\AppData\LocalLow\CodeLeopard\StormworksEditor\player.log"