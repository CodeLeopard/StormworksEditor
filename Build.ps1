# Run a build using a headless (no GUI) UnityEditor.
$ErrorActionPreference = "Stop"

# This needs to be a git ignored file with 'return "Path/To/Unity.exe"'
$Unity = & "./UnityInstallPath.ps1"

$unityLogFile = "$PSScriptRoot/Build.log"
$projectPath = "$PSScriptRoot/Editor"
$unityArguments = "-BatchMode", "-Quit", "-NoGraphics", "-LogFile $unityLogFile", "-Projectpath $projectPath", "-ExecuteMethod Build.CustomBuildButtons.Build_Publish_Debug"

Write-Host -ForegroundColor DarkGray "Starting Unity at '$Unity'"
Write-Host -ForegroundColor DarkGray "With ArgumentList '$unityArguments'"
Start-Process -Wait -FilePath $Unity -ArgumentList $unityArguments
